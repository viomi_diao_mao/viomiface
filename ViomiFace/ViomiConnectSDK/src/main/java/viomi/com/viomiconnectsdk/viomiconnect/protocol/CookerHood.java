package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodBleMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodPowerMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodWindMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;

/**
 * Created by Mocc on 2017/12/28
 */

//烟机
public class CookerHood {

    private volatile static CookerHood instance;
    private volatile static String did;
    private int id = 1;

    public static CookerHood getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (CookerHood.class) {
                if (instance == null) {
                    instance = new CookerHood();
                }
            }
        }
        return instance;
    }

    private CookerHood() {

    }

    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //电源模式：0 - 关机 1 - 待机 2 - 开机 3 - 清洗 4 - 清洗复位
    public String getCommand1(HoodPowerMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //抽风模式
    //mode0 - 低风OFF，mode1 - 低风ON
    //mode2 - 爆炒OFF，mode3 - 爆炒ON
    //mode4 - 高风OFF，mode5 - 高风ON
    public String getCommand2(HoodWindMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wind");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(mode.value);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //开关灯
    public String getCommand3(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_light");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //    设置换新风
    //    arg0: 0 - Disable, 1 - Enable
    //    arg1：[100, 2000] 换新风阈值
    //    arg2: [0, 23] 换新风时间小时
    //    arg3: [0, 59] 换新风时间分钟
    //    arg4: [0, 65535] 换新风时长
    public String getCommand4(Switch_ON_OFF on_off, int arg1, int arg2, int arg3, int arg4) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_aqi");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(on_off.ordinal());
            jsonArray.put(arg1);
            jsonArray.put(arg2);
            jsonArray.put(arg3);
            jsonArray.put(arg4);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //不知道是什么鬼
    // 0:正常工作模式
    //1 ：绑定模式
    //2 ：主机升级模式
    //3:从机升级模式
    public String getCommand5(HoodBleMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_ble_mode");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //0 - OFF, 1 - ON ( 0 - 关闭增压巡航 1 - 打开增压巡航 )
    public String getCommand6(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_curise");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //[0, 180] 延迟关机时间0-3分钟可调 (单位/s)
    public String getCommand7(int arg1) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_poweroff_delaytime");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(arg1);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //0 - OFF, 1 - ON ( 0 - 关机不关灯 1 - 关机关灯 )
    public String getCommand8(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_light_sync");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //arg1:定时时长（/min）
    public String getCommand9(int arg1) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_fixed_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();

            //todo
            jsonArray.put(arg1);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
