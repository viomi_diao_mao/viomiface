package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashCusMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashProMode;

/**
 * Created by Mocc on 2017/12/30
 */

//洗 *碗* 机
public class Dishwasher {

    private volatile static Dishwasher instance;
    private volatile static String did;
    private int id = 1;

    public static Dishwasher getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Dishwasher.class) {
                if (instance == null) {
                    instance = new Dishwasher();
                }
            }
        }
        return instance;
    }

    private Dishwasher() {

    }


    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //开关童锁功能
    public String getCommand1(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_child_lock");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置当前系统时间，单位s
    public String getCommand2(long time) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(time);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置洗涤程序，0 - 标准洗 1 - 经济洗 2 - 快速洗 3 - 自定义
    public String getCommand3(WashProMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_program");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //0：暂停；1：启动
    public String getCommand4(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wash_action");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置洗涤自定义模式，3 - 强力洗   4 - 预洗   5 - 玻璃洗    6 - 除菌
    public String getCommand5(WashCusMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_custom_program");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.value);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置预约洗时间参数并启动预约
    //arg1：预约洗开始的小时  （0~23）
    //arg2：预约洗开始的分钟  （0~59）
    public String getCommand6(String hour, String min) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_bespeakWash_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(hour);
            jsonArray.put(min);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //0：取消预约；1：按设备上的保存的预约时间启动预约
    public String getCommand7(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_bespeakWash");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //开关自动冲洗干燥功能
    public String getCommand8(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_auto_flash_dry");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
