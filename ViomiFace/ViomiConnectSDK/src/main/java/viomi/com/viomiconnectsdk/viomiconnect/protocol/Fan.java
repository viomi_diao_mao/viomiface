package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FanSpeedMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FanWorkMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;

/**
 * Created by Mocc on 2018/7/3
 */
public class Fan {

    private volatile static Fan instance;
    private volatile static String did;
    private int id = 1;

    public static Fan getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Fan.class) {
                if (instance == null) {
                    instance = new Fan();
                }
            }
        }
        return instance;
    }

    private Fan() {
    }

    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置风扇摇头
    //0 - 关闭摇头
    //1 - 启动摇头
    public String getCommand1(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_shake");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置风速 分206个等级，1最低转速，206最高转速
    //[1,    35] - 1档
    //[36,   69] - 2档
    //[70,  103] - 3档
    //[104, 137] - 4档
    //[138, 171] - 5档
    //[172, 206] - 6档
    public String getCommand2(FanSpeedMode speedMode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_windspeed");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(speedMode.speed);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置工作模式
    //0 - 标准风
    //1 - 自然风
    //2 - AI智能风
    public String getCommand3(FanWorkMode workMode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_windmode");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(workMode.ordinal());
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置定时时间（单位/min）
    //[60 - 360] - 定时1 - 6小时
    //65535 - 取消定时
    public String getCommand4(int min) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_worktime");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(min);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置LED灯开关
    //0 - 关闭LED持续显示功能
    //1 - 开启LED持续显示功能
    public String getCommand5(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_led");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置语音识别开关
    //0 - 关闭语音识别功能
    //1 - 开启语音识别功能
    public String getCommand6(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_voice");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置风扇工作开关
    //0 - 风扇停止工作
    //1 - 风扇开始工作
    public String getCommand7(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置风量大小
    //0 - 调小风量
    //1 - 调大风量
    public String getCommand8(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_windvolume");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
