package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.KettleLuanchMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.KettleWorkMode;

/**
 * Created by Mocc on 2017/12/30
 *
 * wifi水壶尚未上市，协议后期可能会修改
 */

public class Kettle {

    private volatile static Kettle instance;
    private volatile static String did;
    private int id = 1;

    public static Kettle getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Kettle.class) {
                if (instance == null) {
                    instance = new Kettle();
                }
            }
        }
        return instance;
    }

    private Kettle() {

    }


    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置保温温度  40-90℃
    public String getCommand1(int temp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_hold_temp");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(temp);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //0：煮沸降至保温温度，防止重复煮沸关；1：煮沸降至保温温度，防止重复煮沸开；2：加热至保温温度
    public String getCommand2(KettleWorkMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_work_mode");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //保温时间,单位分钟
    public String getCommand3(int holdetime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_hold_time");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(holdetime);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置按键值：取值为：0x01:加热按键 0x02：保温按键，0xff：取消加热
    public String getCommand4(KettleLuanchMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_key_value");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.value);

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置预约时间和启动方式，{时间戳，启动方式}，例如{1485760495，1}，启动方式见set_key_value，0xff代表取消预约
    public String getCommand5(long timeStamp, KettleLuanchMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_appoint_heat");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(timeStamp);
            jsonArray.put(mode.value);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
