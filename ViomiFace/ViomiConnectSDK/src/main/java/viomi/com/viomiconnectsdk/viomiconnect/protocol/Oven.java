package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_P_OFF;

/**
 * Created by Mocc on 2018/7/3
 */
public class Oven {

    private volatile static Oven instance;
    private volatile static String did;
    private int id = 1;

    public static Oven getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Oven.class) {
                if (instance == null) {
                    instance = new Oven();
                }
            }
        }
        return instance;
    }

    private Oven() {

    }

    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //0:开始，1:暂停,2:取消预约或结束,
    public String getCommand1(Switch_ON_P_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "setpauseStatus");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
