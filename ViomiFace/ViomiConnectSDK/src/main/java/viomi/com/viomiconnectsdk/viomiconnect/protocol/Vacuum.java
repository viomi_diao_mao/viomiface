package viomi.com.viomiconnectsdk.viomiconnect.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.VacuumDirection;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.VacuumWorkMode;

/**
 * Created by Mocc on 2018/7/4
 */
public class Vacuum {

    private volatile static Vacuum instance;
    private volatile static String did;
    private int id = 1;

    public static Vacuum getInstance(String mDid) {
        did = mDid;
        if (instance == null) {
            synchronized (Vacuum.class) {
                if (instance == null) {
                    instance = new Vacuum();
                }
            }
        }
        return instance;
    }

    private Vacuum() {

    }

    // 获取状态get_prop
    public String getCommand0(String[] args) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    jsonArray.put(i, args[i]);
                }
            }
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置扫地机运行方向 0 - 前 1 - 后 2 - 左 3 - 右 4 - 停止
    public String getCommand1(VacuumDirection direction) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_direction");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(direction.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    //设置扫地机启动/停止 0 - 停止运行 1 - 启动运行
    public String getCommand2(Switch_ON_OFF on_off) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method","set_power" );
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(on_off.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    //设置扫地机工作模式，0 - 自动清扫  1 - 拖地   2 - 重点清扫   3 - 回充
    public String getCommand3(VacuumWorkMode mode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_mode");
            jsonObject.put("did", did);
            jsonObject.put("id", id);
            JSONArray jsonArray = new JSONArray();
            //todo
            jsonArray.put(mode.ordinal());

            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


//    //
//    public String getCommand() {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("method", );
//            jsonObject.put("did", did);
//            jsonObject.put("id", id);
//            JSONArray jsonArray = new JSONArray();
//            //todo
//            jsonArray.put();
//
//            jsonObject.put("params", jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return jsonObject.toString();
//    }

}
