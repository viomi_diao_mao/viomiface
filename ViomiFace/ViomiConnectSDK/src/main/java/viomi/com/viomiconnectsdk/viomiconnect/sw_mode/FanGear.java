package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/16 0016.
 */

public enum FanGear {
    Gear_1(1, "1挡"), Gear_2(2, "2挡"), Gear_3(3, "3挡"), Gear_4(4, "4挡"), Gear_5(5, "5挡"), Gear_6(6, "6挡"),
    low(1, "最低挡"), middle(3, "中挡"), high(6, "最高挡");
    public int level;
    public int[] range = new int[2];
    public String text;

    FanGear(int value, String text) {
        this.level = value;
        this.text = text;
        setRange();
    }

    void setRange() {
        switch (level) {
            case 1:
                range[0] = 1;
                range[1] = 35;
                break;
            case 2:
                range[0] = 36;
                range[1] = 69;
                break;
            case 3:
                range[0] = 70;
                range[1] = 103;
                break;
            case 4:
                range[0] = 104;
                range[1] = 137;
                break;
            case 5:
                range[0] = 138;
                range[1] = 171;
                break;
            case 6:
                range[0] = 172;
                range[1] = 206;
                break;
        }
    }

    public String getText() {
        return text;
    }
}
