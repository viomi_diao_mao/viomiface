package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/17 0017.
 */

public enum FanGear3 {

    low(1, "最低挡"), middle(3, "中挡"), high(6, "最高挡");
    public int level;
    public int value;
    public String text;

    FanGear3(int value, String text) {
        this.level = value;
        this.text = text;
        setRange();
    }

    void setRange() {
        switch (level) {
            case 1:
                value = 1;
                break;
            case 3:
                value = 103;
                break;
            case 6:
                value = 206;
                break;
        }
    }

    public String getText() {
        return text;
    }
}
