package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/16 0016.
 */

public enum FanMode {
    nature(0, "自然风模式"), standart(1, "标准风模式"), ai(2, "AI智能风模式");

    public int value;
    public String text;

    FanMode(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
