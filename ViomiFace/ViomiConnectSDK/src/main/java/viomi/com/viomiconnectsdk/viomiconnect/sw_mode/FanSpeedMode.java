package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2018/7/3
 */
public enum FanSpeedMode {

    一档(35), 二档(69), 三档(103), 四档(137), 五档(171), 六档(206), 最小档(35), 中档(103), 最高档(206);

    public int speed;

    FanSpeedMode(int speed) {
        this.speed = speed;
    }
}
