package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/28
 */

public enum HoodPowerMode {

    /*
    * 0 - 关机
    * 1 - 待机
    * 2 - 开机
    * 3 - 清洗
    * 4 - 清洗复位
    **/

    mode0, mode1, mode2, mode3, mode4;

}
