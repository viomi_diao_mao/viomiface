package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/28
 */

public enum HoodWindMode {


    //mode0 - 低风OFF，mode1 - 低风ON
    //mode2 - 爆炒OFF，mode3 - 爆炒ON
    //mode4 - 高风OFF，mode5 - 高风ON
    mode0(0, "关闭"), mode1(1, "低挡"), mode2(0, "关闭"), mode3(4, "爆炒")
    , mode4(0, "关闭"), mode5(16, "高挡"), mode16(1, "低档"), mode7(16, "高档");

    public int value;
    public String text;

    HoodWindMode(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
