package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum KettleLuanchMode {

    //设置按键值：取值为：0x01:加热按键 0x02：保温按键，0xff：取消加热

    mode0("0x01"), mode1("0x02"), mode2("0xff");

    public String value;

    KettleLuanchMode(String value) {
        this.value = value;
    }
}
