package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

//0：煮沸降至保温温度，防止重复煮沸关；1：煮沸降至保温温度，防止重复煮沸开；2：加热至保温温度
public enum KettleWorkMode {

    mode0, mode1, mode2;

}
