package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/2/26 0026.
 */

public enum OvenMode {
    mode0(0, "无"),
    mode1(1, "蒸汽"),
    mode2(2, "热风烤"),
    mode3(3, "蒸烤"),
    mode4(4, "发酵"),
    mode5(5, "热菜"),
    mode6(6, "解冻"),
    mode7(7, "除垢"),
    mode8(8, "高温消毒"),
    mode9(9, "清洁"),
    mode10(10, "预热");
    public int value;
    public String name;

    OvenMode(int value,
             String name) {
        this.value = value;
        this.name = name;
    }

}
