package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/9/18
 */

public enum Switch_ON_OFF {

    off("关"), on("开");
    public String text;

    Switch_ON_OFF(String text) {
        this.text = text;
    }
}
