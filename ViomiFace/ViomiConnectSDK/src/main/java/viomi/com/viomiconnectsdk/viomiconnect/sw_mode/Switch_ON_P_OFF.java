package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2018/7/3
 */
public enum Switch_ON_P_OFF {
    on("开"), pause("暂停"), off("关");
    public String text;

    Switch_ON_P_OFF(String text) {
        this.text = text;
    }
}
