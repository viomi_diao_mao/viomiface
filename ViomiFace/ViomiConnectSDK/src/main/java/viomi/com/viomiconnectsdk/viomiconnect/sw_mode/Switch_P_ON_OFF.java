package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2018/7/3
 */
public enum Switch_P_ON_OFF {
    pause("暂停"), on("开"), off("关");
    public String text;

    Switch_P_ON_OFF(String text) {
        this.text = text;
    }
}
