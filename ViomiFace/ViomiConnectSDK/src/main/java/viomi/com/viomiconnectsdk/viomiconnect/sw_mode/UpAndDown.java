package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/17 0017.
 */

public enum UpAndDown {
    up("加"), down("减");
    public String text;

    UpAndDown(String text) {
        this.text = text;
    }
}
