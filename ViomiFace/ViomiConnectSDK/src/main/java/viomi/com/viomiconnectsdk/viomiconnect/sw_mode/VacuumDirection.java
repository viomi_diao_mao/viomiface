package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2018/7/4
 */
public enum VacuumDirection {
    前, 后, 左, 右, 停止
}
