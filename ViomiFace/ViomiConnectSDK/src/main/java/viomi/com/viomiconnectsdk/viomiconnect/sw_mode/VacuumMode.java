package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/17 0017.
 */

public enum VacuumMode {

    smart(0, "智能清扫"), auto(1, "自动拖地"), focus(2, "重点清扫"), charge(3, "回充");

    public int value;
    public String text;

    VacuumMode(int value, String text) {
        this.value = value;
        this.text = text;
    }
}
