package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/17 0017.
 */

public enum VacuumStatus {
    s_0(0, "睡眠"), s_1(1, "待机"), s_2(2, "工作中"), s_3(3, "工作中"), s_4(4, "工作中"), s_5(5, "回充中"), s_6(6, "充电中");

    public int value;
    public String text;

    VacuumStatus(int value, String text) {
        this.value = value;
        this.text = text;
    }
}
