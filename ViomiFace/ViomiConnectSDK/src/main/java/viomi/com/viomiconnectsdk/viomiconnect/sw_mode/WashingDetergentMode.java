package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashingDetergentMode {
    //设定是否自动添加洗涤剂，0：不投放；1：少量；2：正常；3：多量
    none, little, normal, lot;
}
