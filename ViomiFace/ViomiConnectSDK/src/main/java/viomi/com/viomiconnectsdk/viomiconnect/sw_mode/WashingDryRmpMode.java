package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashingDryRmpMode {

    //设定脱水强度，取值"none","gentle","mild","middle","strong"。分别代表  不脱水，柔脱水，轻脱水，中脱水，强脱水

    none("免脱水"), gentle("柔脱水"), mild("轻脱水"), middle("中脱水"), strong("强脱水");
    public String name;

    WashingDryRmpMode(String name) {
        this.name = name;
    }

}
