package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashingSoftenerMode {
    //设定是否自动添加柔顺剂，0：不投放；1：少量；2：正常；3：多量
    none, little, normal, lot;
}
