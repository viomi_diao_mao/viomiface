package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by Mocc on 2017/12/30
 */

public enum WashingTimes {

    one(1), two(2), three(3), four(4), five(5);

    public int value;

    WashingTimes(int value) {
        this.value = value;
    }
}
