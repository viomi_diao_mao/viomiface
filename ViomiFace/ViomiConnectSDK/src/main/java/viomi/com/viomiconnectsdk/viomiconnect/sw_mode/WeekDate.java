package viomi.com.viomiconnectsdk.viomiconnect.sw_mode;

/**
 * Created by hailang on 2018/5/17 0017.
 */

public enum WeekDate {
    Mon(0, "星期一"), Tue(1, "星期二"), Wed(2, "星期三"), Thu(3, "星期四"), Fri(4, "星期五"), Sat(5, "星期六"), Sun(6, "星期日");

    public int level;
    public String text;

    WeekDate(int value, String text) {
        this.level = value;
        this.text = text;
    }
}
