package viomi.com.viomiconnectsdk.viomiconnect.util;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mocc on 2017/9/18
 */

public class HTTPUtil {

    public static HTTPUtil getInstance() {
        return new HTTPUtil();
    }

    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();//CPU数
    private static final int CORE_POOL_SIZE = CPU_COUNT + 1;
    private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 3 + 1;
    private static final int KEEP_ALIVE = 1;

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "AsyncTask #" + mCount.getAndIncrement());
        }
    };

    private static final BlockingQueue<Runnable> sPoolWorkQueue =
            new LinkedBlockingQueue<Runnable>(128);

    public static final Executor THREAD_POOL_EXECUTOR
            = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
            TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);


    public void getRequst(final String url_, final ViomiDeviceCallback callback) {

        if (callback == null) {
            return;
        }
        try {
            THREAD_POOL_EXECUTOR.execute(new Runnable() {
                @Override
                public void run() {
                    HttpURLConnection con = null;
                    InputStream is = null;
                    StringBuffer result = new StringBuffer();
                    try {
                        URL url = new URL(url_);
                        con = (HttpURLConnection) url.openConnection();
                        con.setConnectTimeout(15 * 1000);
                        con.connect();
                        int reslutCode = con.getResponseCode();
                        is = con.getInputStream();
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = is.read(buf)) != -1) {
                            result.append(new String(buf, 0, len));
                        }
                        if (reslutCode == 200) {
                            callback.onSuccess(result.toString());
                        } else {
                            callback.onFail(result.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.onFail(e.toString());
                    } finally {
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (con != null) {
                            con.disconnect();
                        }
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void putRequest(final String url1, final Map<String, String> form, final ViomiDeviceCallback callback) {

        if (callback == null) {
            return;
        }
        THREAD_POOL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                InputStream is = null;
                HttpURLConnection urlConn = null;
                DataOutputStream dos = null;
                try {
                    URL url = new URL(url1);
                    StringBuilder stringBuilder = new StringBuilder();
                    urlConn = (HttpURLConnection) url.openConnection();
                    urlConn.setRequestProperty("content-type", "application/json");
                    urlConn.setDoInput(true);
                    urlConn.setDoOutput(true);
                    urlConn.setConnectTimeout(15 * 1000);
                    //设置请求方式为 PUT
                    urlConn.setRequestMethod("PUT");
                    urlConn.setRequestProperty("Content-Type", "application/json");
                    urlConn.setRequestProperty("Accept", "application/json");
                    urlConn.setRequestProperty("Charset", "UTF-8");
                    dos = new DataOutputStream(urlConn.getOutputStream());
                    JSONObject obj = new JSONObject();
                    if (form != null)
                        for (String s : form.keySet()) {
                            obj.put(s, form.get(s));
                        }
                    dos.writeBytes(obj.toString());
                    dos.flush();
                    dos.close();
                    int resultcode = urlConn.getResponseCode();
                    is = urlConn.getInputStream();
                    int len;
                    byte[] buf = new byte[1024];
                    while ((len = is.read(buf)) != -1) {
                        stringBuilder.append(new String(buf, 0, len));
                    }
                    if (resultcode == 200) {
                        callback.onSuccess(stringBuilder.toString());
                    } else {
                        callback.onFail(stringBuilder.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFail(e.getMessage());
                } finally {
                    try {
                        dos.close();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    try {
                        is.close();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    try {
                        urlConn.disconnect();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public void postRequest(final String url, final Map<String, String> form, final ViomiDeviceCallback callback) {

        if (callback == null) {
            return;
        }
        THREAD_POOL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                //post表单请求
                HttpURLConnection conn = null;
                PrintWriter pw = null;
                BufferedReader rd = null;
                StringBuilder out = new StringBuilder();
                StringBuilder sb = new StringBuilder();
                InputStream is = null;
                for (String key : form.keySet()) {
                    if (out.length() != 0) {
                        out.append("&");
                    }
                    out.append(key).append("=").append(form.get(key));
                }
                try {
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setRequestMethod("POST");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.setReadTimeout(20000);
                    conn.setConnectTimeout(20000);
                    conn.setUseCaches(false);
                    conn.connect();
                    int reslutCode = conn.getResponseCode();
                    is = conn.getInputStream();
                    int len;
                    byte[] buf = new byte[1024];
                    while ((len = is.read(buf)) != -1) {
                        sb.append(new String(buf, 0, len));
                    }
                    if (reslutCode == 200) {
                        callback.onSuccess(sb.toString());
                    } else {
                        callback.onFail(sb.toString());
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                    callback.onFail(e.toString());
                } finally {
                    try {
                        if (pw != null) {
                            pw.close();
                        }
                        if (rd != null) {
                            rd.close();
                        }
                        if (conn != null) {
                            conn.disconnect();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
