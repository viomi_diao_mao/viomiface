package com.viomi.defined;

import android.util.Log;

import com.xiaomi.miot.typedef.urn.ActionType;
import com.xiaomi.miot.typedef.urn.PropertyType;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class ViomiDefined {

    private static final String TAG = "ViomiDefined";
    private static final String DOMAIN = "Viomi";
    private static final String _UUID = "-0000-1000-2000-000000AABBCC";

    private ViomiDefined() {
    }

    /**
     * Properties
     * urn:Viomi:property:SceneMode:0000
     * urn:Viomi:property:ScreenStatus:0000
     * urn:Viomi:property:WaterAlarm:0000
     * urn:Viomi:property:SmokeAlarm:0000
     * urn:Viomi:property:InvadeAlarm:0000
     * urn:Viomi:property:GasLeakAlarm:0000
     * urn:Viomi:property:SeeYanmi:0000
     * urn:Viomi:property:SeeLaoban:0000
     * urn:Viomi:property:LaunchState:0000
     */
    public enum Property {
        Undefined(0),
        SceneMode(1),
        ScreenStatus(2),
        WaterAlarm(3),
        SmokeAlarm(4),
        InvadeAlarm(5),
        GasLeakAlarm(6),
        SeeYanmi(7),
        SeeLaoban(8),
        LaunchState(9);

        private int value;

        Property(int value) {
            this.value = value;
        }

        public static Property valueOf(int value) {
            for (Property c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Property valueOf(PropertyType type) {
            if (!type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Property c : values()) {
                if (c.toString().equals(type.getSubType())) {
                    return c;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public PropertyType toPropertyType() {
            return new PropertyType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }

    /**
     * Actions
     * urn:Viomi:action:setSeeLaoban:0000
     * urn:Viomi:action:setScreenStatus:0000
     * urn:Viomi:action:setSmokeAlarm:0000
     * urn:Viomi:action:setLaunchState:0000
     * urn:Viomi:action:setSceneMode:0000
     * urn:Viomi:action:setGasLeakAlarm:0000
     * urn:Viomi:action:setInvadeAlarm:0000
     * urn:Viomi:action:setSeeYanmi:0000
     * urn:Viomi:action:setWaterAlarm:0000
     * ...
     */
    public enum Action {
        Undefined(0),
        setSeeLaoban(1),
        setScreenStatus(2),
        setSmokeAlarm(3),
        setLaunchState(4),
        setSceneMode(5),
        setGasLeakAlarm(6),
        setInvadeAlarm(7),
        setSeeYanmi(8),
        setWaterAlarm(9);

        private int value;

        Action(int value) {
            this.value = value;
        }

        public static Action valueOf(int value) {
            for (Action c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Action valueOf(ActionType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Action v : values()) {
                if (v.toString().equals(type.getSubType())) {
                    return v;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public ActionType toActionType() {
            return new ActionType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }

    /**
     * Servics
     * urn:Viomi:service:FaceDeviceService:0000
     */
    public enum Service {
        Undefined(0),
        FaceDeviceService(1);

        private int value;

        Service(int value) {
            this.value = value;
        }

        public static Service valueOf(int value) {
            for (Service c : values()) {
                if (c.value() == value) {
                    return c;
                }
            }

            Log.e(TAG, "invalid value: " + value);

            return Undefined;
        }

        public static Service valueOf(ServiceType type) {
            if (! type.getDomain().equals(DOMAIN)) {
                return Undefined;
            }

            for (Service v : values()) {
                if (v.toString().equals(type.getSubType())) {
                    return v;
                }
            }

            return Undefined;
        }

        public int value() {
            return value;
        }

        public ServiceType toServiceType() {
            return new ServiceType(DOMAIN, this.toString(), toShortUUID());
        }

        public String toUUID() {
            return String.format("%08X%s", value, _UUID);
        }

        public String toShortUUID() {
            return String.format("%04X", value);
        }
    }
}