package com.viomi.defined.action;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.SceneMode;
import com.viomi.defined.property.ScreenStatus;
import com.viomi.defined.property.WaterAlarm;
import com.viomi.defined.property.SmokeAlarm;
import com.viomi.defined.property.InvadeAlarm;
import com.viomi.defined.property.GasLeakAlarm;
import com.viomi.defined.property.SeeYanmi;
import com.viomi.defined.property.SeeLaoban;
import com.viomi.defined.property.LaunchState;
import com.xiaomi.miot.typedef.device.operable.ActionOperable;
import com.xiaomi.miot.typedef.urn.ActionType;

public class SetLaunchState extends ActionOperable {

    public static final ActionType TYPE = ViomiDefined.Action.setLaunchState.toActionType();

    public SetLaunchState() {
        super(TYPE);

        super.addArgument(LaunchState.TYPE.toString());
    }
}