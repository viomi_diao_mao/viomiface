/* This file is auto-generated.*/

package com.viomi.defined.device;

import com.viomi.defined.service.FaceDeviceService;
import com.xiaomi.miot.host.manager.MiotDeviceConfig;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.device.operable.DeviceOperable;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CompletedListener;
import com.xiaomi.miot.typedef.urn.DeviceType;

public class ViomiFace extends DeviceOperable {

    private static final DeviceType DEVICE_TYPE = new DeviceType("Viomi", "ViomiFace", "1");

    private FaceDeviceService _FaceDeviceService = new FaceDeviceService(false);

    public ViomiFace(MiotDeviceConfig config) {
        super(DEVICE_TYPE);
        super.setDiscoveryTypes(config.discoveryTypes());
        super.setFriendlyName(config.friendlyName());
        super.setDeviceId(config.deviceId());
        super.setMacAddress(config.macAddress());
        super.setManufacturer(config.manufacturer());
        super.setModelName(config.modelName());
        super.setMiotToken(config.miotToken());
        super.setMiotInfo(config.miotInfo());
        super.addService(_FaceDeviceService);
        super.initializeInstanceID();
    }

    public FaceDeviceService FaceDeviceService() {
        return _FaceDeviceService;
    }

    public void start(CompletedListener listener) throws MiotException {
        MiotHostManager.getInstance().register(this, listener, this);
    }

    public void stop(CompletedListener listener) throws MiotException {
        MiotHostManager.getInstance().unregister(this, listener);
    }

    public void sendEvents() throws MiotException {
        MiotHostManager.getInstance().sendEvent(super.getChangedProperties());
    }

    public void send(String method, String params) throws MiotException {
        MiotHostManager.getInstance().send(method, params);
    }
}