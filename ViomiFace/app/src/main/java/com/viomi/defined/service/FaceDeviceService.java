package com.viomi.defined.service;

import android.util.Log;

import com.viomi.defined.ViomiDefined;
import com.viomi.defined.property.SceneMode;
import com.viomi.defined.property.ScreenStatus;
import com.viomi.defined.property.WaterAlarm;
import com.viomi.defined.property.SmokeAlarm;
import com.viomi.defined.property.InvadeAlarm;
import com.viomi.defined.property.GasLeakAlarm;
import com.viomi.defined.property.SeeYanmi;
import com.viomi.defined.property.SeeLaoban;
import com.viomi.defined.property.LaunchState;
import com.viomi.defined.action.SetSeeLaoban;
import com.viomi.defined.action.SetScreenStatus;
import com.viomi.defined.action.SetSmokeAlarm;
import com.viomi.defined.action.SetLaunchState;
import com.viomi.defined.action.SetSceneMode;
import com.viomi.defined.action.SetGasLeakAlarm;
import com.viomi.defined.action.SetInvadeAlarm;
import com.viomi.defined.action.SetSeeYanmi;
import com.viomi.defined.action.SetWaterAlarm;
import com.xiaomi.miot.typedef.data.DataValue;
import com.xiaomi.miot.typedef.data.value.Vbool;
import com.xiaomi.miot.typedef.data.value.Vfloat;
import com.xiaomi.miot.typedef.data.value.Vint;
import com.xiaomi.miot.typedef.data.value.Vstring;
import com.xiaomi.miot.typedef.data.value.Vuint8;
import com.xiaomi.miot.typedef.data.value.Vuint16;
import com.xiaomi.miot.typedef.data.value.Vuint32;
import com.xiaomi.miot.typedef.data.value.Vuint64;
import com.xiaomi.miot.typedef.device.Action;
import com.xiaomi.miot.typedef.device.ActionInfo;
import com.xiaomi.miot.typedef.device.operable.ServiceOperable;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.property.Property;
import com.xiaomi.miot.typedef.urn.ServiceType;

public class FaceDeviceService extends ServiceOperable {

    public static final ServiceType TYPE = ViomiDefined.Service.FaceDeviceService.toServiceType();
    private static final String TAG = "FaceDeviceService";

    public FaceDeviceService(boolean hasOptionalProperty) {
        super(TYPE);

        super.addProperty(new SceneMode());
        super.addProperty(new ScreenStatus());
        super.addProperty(new WaterAlarm());
        super.addProperty(new SmokeAlarm());
        super.addProperty(new InvadeAlarm());
        super.addProperty(new GasLeakAlarm());
        super.addProperty(new SeeYanmi());
        super.addProperty(new SeeLaoban());
        super.addProperty(new LaunchState());

        if (hasOptionalProperty) {
        }

        super.addAction(new SetSeeLaoban());
        super.addAction(new SetScreenStatus());
        super.addAction(new SetSmokeAlarm());
        super.addAction(new SetLaunchState());
        super.addAction(new SetSceneMode());
        super.addAction(new SetGasLeakAlarm());
        super.addAction(new SetInvadeAlarm());
        super.addAction(new SetSeeYanmi());
        super.addAction(new SetWaterAlarm());
    }

    /**
     * Properties
     */
    public SceneMode SceneMode() {
        Property p = super.getProperty(SceneMode.TYPE);
        if (p != null) {
            if (p instanceof SceneMode) {
                return (SceneMode) p;
            }
        }

        return null;
    }

    public ScreenStatus ScreenStatus() {
        Property p = super.getProperty(ScreenStatus.TYPE);
        if (p != null) {
            if (p instanceof ScreenStatus) {
                return (ScreenStatus) p;
            }
        }

        return null;
    }

    public WaterAlarm WaterAlarm() {
        Property p = super.getProperty(WaterAlarm.TYPE);
        if (p != null) {
            if (p instanceof WaterAlarm) {
                return (WaterAlarm) p;
            }
        }

        return null;
    }

    public SmokeAlarm SmokeAlarm() {
        Property p = super.getProperty(SmokeAlarm.TYPE);
        if (p != null) {
            if (p instanceof SmokeAlarm) {
                return (SmokeAlarm) p;
            }
        }

        return null;
    }

    public InvadeAlarm InvadeAlarm() {
        Property p = super.getProperty(InvadeAlarm.TYPE);
        if (p != null) {
            if (p instanceof InvadeAlarm) {
                return (InvadeAlarm) p;
            }
        }

        return null;
    }

    public GasLeakAlarm GasLeakAlarm() {
        Property p = super.getProperty(GasLeakAlarm.TYPE);
        if (p != null) {
            if (p instanceof GasLeakAlarm) {
                return (GasLeakAlarm) p;
            }
        }

        return null;
    }

    public SeeYanmi SeeYanmi() {
        Property p = super.getProperty(SeeYanmi.TYPE);
        if (p != null) {
            if (p instanceof SeeYanmi) {
                return (SeeYanmi) p;
            }
        }

        return null;
    }

    public SeeLaoban SeeLaoban() {
        Property p = super.getProperty(SeeLaoban.TYPE);
        if (p != null) {
            if (p instanceof SeeLaoban) {
                return (SeeLaoban) p;
            }
        }

        return null;
    }

    public LaunchState LaunchState() {
        Property p = super.getProperty(LaunchState.TYPE);
        if (p != null) {
            if (p instanceof LaunchState) {
                return (LaunchState) p;
            }
        }

        return null;
    }

    /**
     * Actions
     */
    public SetSeeLaoban setSeeLaoban() {
        Action a = super.getAction(SetSeeLaoban.TYPE);
        if (a != null) {
            if (a instanceof SetSeeLaoban) {
                return (SetSeeLaoban) a;
            }
        }

        return null;
    }

    public SetScreenStatus setScreenStatus() {
        Action a = super.getAction(SetScreenStatus.TYPE);
        if (a != null) {
            if (a instanceof SetScreenStatus) {
                return (SetScreenStatus) a;
            }
        }

        return null;
    }

    public SetSmokeAlarm setSmokeAlarm() {
        Action a = super.getAction(SetSmokeAlarm.TYPE);
        if (a != null) {
            if (a instanceof SetSmokeAlarm) {
                return (SetSmokeAlarm) a;
            }
        }

        return null;
    }

    public SetLaunchState setLaunchState() {
        Action a = super.getAction(SetLaunchState.TYPE);
        if (a != null) {
            if (a instanceof SetLaunchState) {
                return (SetLaunchState) a;
            }
        }

        return null;
    }

    public SetSceneMode setSceneMode() {
        Action a = super.getAction(SetSceneMode.TYPE);
        if (a != null) {
            if (a instanceof SetSceneMode) {
                return (SetSceneMode) a;
            }
        }

        return null;
    }

    public SetGasLeakAlarm setGasLeakAlarm() {
        Action a = super.getAction(SetGasLeakAlarm.TYPE);
        if (a != null) {
            if (a instanceof SetGasLeakAlarm) {
                return (SetGasLeakAlarm) a;
            }
        }

        return null;
    }

    public SetInvadeAlarm setInvadeAlarm() {
        Action a = super.getAction(SetInvadeAlarm.TYPE);
        if (a != null) {
            if (a instanceof SetInvadeAlarm) {
                return (SetInvadeAlarm) a;
            }
        }

        return null;
    }

    public SetSeeYanmi setSeeYanmi() {
        Action a = super.getAction(SetSeeYanmi.TYPE);
        if (a != null) {
            if (a instanceof SetSeeYanmi) {
                return (SetSeeYanmi) a;
            }
        }

        return null;
    }

    public SetWaterAlarm setWaterAlarm() {
        Action a = super.getAction(SetWaterAlarm.TYPE);
        if (a != null) {
            if (a instanceof SetWaterAlarm) {
                return (SetWaterAlarm) a;
            }
        }

        return null;
    }

    /**
     * PropertyGetter
     */
    public interface PropertyGetter {
        int getSceneMode();

        boolean getScreenStatus();

        boolean getWaterAlarm();

        boolean getSmokeAlarm();

        boolean getInvadeAlarm();

        boolean getGasLeakAlarm();

        boolean getSeeYanmi();

        boolean getSeeLaoban();

        boolean getLaunchState();

    }

    /**
     * PropertySetter
     */
    public interface PropertySetter {
        void setSceneMode(int value);

        void setScreenStatus(boolean value);

        void setWaterAlarm(boolean value);

        void setSmokeAlarm(boolean value);

        void setInvadeAlarm(boolean value);

        void setGasLeakAlarm(boolean value);

        void setSeeYanmi(boolean value);

        void setSeeLaoban(boolean value);

        void setLaunchState(boolean value);

    }

    /**
     * ActionsHandler
     */
    public interface ActionHandler {
        void onsetSeeLaoban(boolean SeeLaoban);

        void onsetScreenStatus(boolean ScreenStatus);

        void onsetSmokeAlarm(boolean SmokeAlarm);

        void onsetLaunchState(boolean LaunchState);

        void onsetSceneMode(int SceneMode);

        void onsetGasLeakAlarm(boolean GasLeakAlarm);

        void onsetInvadeAlarm(boolean InvadeAlarm);

        void onsetSeeYanmi(boolean SeeYanmi);

        void onsetWaterAlarm(boolean WaterAlarm);
    }


    private MiotError onsetSeeLaoban(ActionInfo action) {
        boolean seeLaoban = ((Vbool) action.getArgumentValue(SeeLaoban.TYPE)).getValue();
        actionHandler.onsetSeeLaoban(seeLaoban);

        return MiotError.OK;
    }

    private MiotError onsetScreenStatus(ActionInfo action) {
        boolean screenStatus = ((Vbool) action.getArgumentValue(ScreenStatus.TYPE)).getValue();
        actionHandler.onsetScreenStatus(screenStatus);

        return MiotError.OK;
    }

    private MiotError onsetSmokeAlarm(ActionInfo action) {
        boolean smokeAlarm = ((Vbool) action.getArgumentValue(SmokeAlarm.TYPE)).getValue();
        actionHandler.onsetSmokeAlarm(smokeAlarm);

        return MiotError.OK;
    }

    private MiotError onsetLaunchState(ActionInfo action) {
        boolean launchState = ((Vbool) action.getArgumentValue(LaunchState.TYPE)).getValue();
        actionHandler.onsetLaunchState(launchState);

        return MiotError.OK;
    }

    private MiotError onsetSceneMode(ActionInfo action) {
        int sceneMode = ((Vint) action.getArgumentValue(SceneMode.TYPE)).getValue();
        actionHandler.onsetSceneMode(sceneMode);

        return MiotError.OK;
    }

    private MiotError onsetGasLeakAlarm(ActionInfo action) {
        boolean gasLeakAlarm = ((Vbool) action.getArgumentValue(GasLeakAlarm.TYPE)).getValue();
        actionHandler.onsetGasLeakAlarm(gasLeakAlarm);

        return MiotError.OK;
    }

    private MiotError onsetInvadeAlarm(ActionInfo action) {
        boolean invadeAlarm = ((Vbool) action.getArgumentValue(InvadeAlarm.TYPE)).getValue();
        actionHandler.onsetInvadeAlarm(invadeAlarm);

        return MiotError.OK;
    }

    private MiotError onsetSeeYanmi(ActionInfo action) {
        boolean seeYanmi = ((Vbool) action.getArgumentValue(SeeYanmi.TYPE)).getValue();
        actionHandler.onsetSeeYanmi(seeYanmi);

        return MiotError.OK;
    }

    private MiotError onsetWaterAlarm(ActionInfo action) {
        boolean waterAlarm = ((Vbool) action.getArgumentValue(WaterAlarm.TYPE)).getValue();
        actionHandler.onsetWaterAlarm(waterAlarm);

        return MiotError.OK;
    }

    /**
     * Handle actions invocation & properties operation
     */
    private ActionHandler actionHandler;
    private PropertyGetter propertyGetter;
    private PropertySetter propertySetter;

    public void setHandler(ActionHandler handler, PropertyGetter getter, PropertySetter setter) {
        actionHandler = handler;
        propertyGetter = getter;
        propertySetter = setter;
    }

    @Override
    public MiotError onSet(Property property) {
        Log.e(TAG, "onSet");

        if (propertySetter == null) {
            return super.onSet(property);
        }

        ViomiDefined.Property p = ViomiDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case SceneMode:
                propertySetter.setSceneMode(((Vint) property.getCurrentValue()).getValue());
                break;
            case ScreenStatus:
                propertySetter.setScreenStatus(((Vbool) property.getCurrentValue()).getValue());
                break;
            case WaterAlarm:
                propertySetter.setWaterAlarm(((Vbool) property.getCurrentValue()).getValue());
                break;
            case SmokeAlarm:
                propertySetter.setSmokeAlarm(((Vbool) property.getCurrentValue()).getValue());
                break;
            case InvadeAlarm:
                propertySetter.setInvadeAlarm(((Vbool) property.getCurrentValue()).getValue());
                break;
            case GasLeakAlarm:
                propertySetter.setGasLeakAlarm(((Vbool) property.getCurrentValue()).getValue());
                break;
            case SeeYanmi:
                propertySetter.setSeeYanmi(((Vbool) property.getCurrentValue()).getValue());
                break;
            case SeeLaoban:
                propertySetter.setSeeLaoban(((Vbool) property.getCurrentValue()).getValue());
                break;
            case LaunchState:
                propertySetter.setLaunchState(((Vbool) property.getCurrentValue()).getValue());
                break;

            default:
                return MiotError.INTERNAL_INVALID_OPERATION;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onGet(Property property) {
        Log.e(TAG, "onGet");

        if (propertyGetter == null) {
            return super.onGet(property);
        }

        ViomiDefined.Property p = ViomiDefined.Property.valueOf(property.getDefinition().getType());
        switch (p) {
            case SceneMode:
                property.setValue(propertyGetter.getSceneMode());
                break;
            case ScreenStatus:
                property.setValue(propertyGetter.getScreenStatus());
                break;
            case WaterAlarm:
                property.setValue(propertyGetter.getWaterAlarm());
                break;
            case SmokeAlarm:
                property.setValue(propertyGetter.getSmokeAlarm());
                break;
            case InvadeAlarm:
                property.setValue(propertyGetter.getInvadeAlarm());
                break;
            case GasLeakAlarm:
                property.setValue(propertyGetter.getGasLeakAlarm());
                break;
            case SeeYanmi:
                property.setValue(propertyGetter.getSeeYanmi());
                break;
            case SeeLaoban:
                property.setValue(propertyGetter.getSeeLaoban());
                break;
            case LaunchState:
                property.setValue(propertyGetter.getLaunchState());
                break;

            default:
                return MiotError.INTERNAL_INVALID_OPERATION;
        }

        return MiotError.OK;
    }

    @Override
    public MiotError onAction(ActionInfo action) {
        Log.e(TAG, "onAction: " + action.getType().toString());

        if (actionHandler == null) {
            return super.onAction(action);
        }

        ViomiDefined.Action a = ViomiDefined.Action.valueOf(action.getType());
        switch (a) {
            case setSeeLaoban:
                return onsetSeeLaoban(action);
            case setScreenStatus:
                return onsetScreenStatus(action);
            case setSmokeAlarm:
                return onsetSmokeAlarm(action);
            case setLaunchState:
                return onsetLaunchState(action);
            case setSceneMode:
                return onsetSceneMode(action);
            case setGasLeakAlarm:
                return onsetGasLeakAlarm(action);
            case setInvadeAlarm:
                return onsetInvadeAlarm(action);
            case setSeeYanmi:
                return onsetSeeYanmi(action);
            case setWaterAlarm:
                return onsetWaterAlarm(action);

            default:
                Log.e(TAG, "invalid action: " + a);
                break;
        }

        return MiotError.INTERNAL_INVALID_OPERATION;
    }
}