package viomi.com.viomiaiface.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.utils.ApkUtil;
import viomi.com.viomiaiface.utils.LogUtils;

public class AboutActivity extends BaseActivity {

    private ImageView back_icon;
    private TextView title_view;
    private TextView appVersionName;
    private TextView romVersionName;
    private TextView sysVersionName;
    private TextView deviceDid;
    private TextView deviceMac;
    private TextView deviceToken;
    private RelativeLayout privacyAgreement;
    private LinearLayout sys_hide_layout;
    private RelativeLayout factory_test;
    private int clickcount;
    private Switch adb_switch;
    private RelativeLayout factory_reset;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_about);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);

        appVersionName = findViewById(R.id.appVersionName);
        romVersionName = findViewById(R.id.romVersionName);
        sysVersionName = findViewById(R.id.sysVersionName);

        deviceDid = findViewById(R.id.deviceDid);
        deviceMac = findViewById(R.id.deviceMac);
        deviceToken = findViewById(R.id.deviceToken);

        privacyAgreement = findViewById(R.id.privacyAgreement);

        sys_hide_layout = findViewById(R.id.sys_hide_layout);
        factory_test = findViewById(R.id.factory_test);
        adb_switch = findViewById(R.id.adb_switch);
        factory_reset = findViewById(R.id.factory_reset);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        title_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount++;
                if (clickcount == 5) {
                    sys_hide_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        privacyAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        factory_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String packageName = "ai.hij.device";
                    if (ApkUtil.checkApkExist(BaseApplication.getAppContext(), packageName)) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_MAIN);
                        String className = "ai.hij.device.activity.HijTestActivity";
                        ComponentName componentName = new ComponentName(packageName, className);
                        intent.setComponent(componentName);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                }
            }
        });

        adb_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LogUtils.e(TAG, "isChecked=" + isChecked);
                Intent adbIntent = new Intent("android.intent.action.SET_Adb_ENABLE");
                adbIntent.putExtra("isAdbEnable", isChecked);
                sendBroadcast(adbIntent);
            }
        });

        factory_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clearIntent = new Intent("android.intent.action.DO_MASTER_CLEAR");
                sendBroadcast(clearIntent);
            }
        });
    }

    @Override
    protected void init() {
        title_view.setText("关于");
        appVersionName.setText("V" + FaceConfig.CURRENTVERSION);
        sysVersionName.setText("Android"+android.os.Build.VERSION.RELEASE);

        String serial = android.os.Build.SERIAL;
        LogUtils.e(TAG, "serial=" + serial);

        if (serial != null) {
            String[] serials = serial.split("\\|");
            if (serials != null && serials.length == 3) {
                try {
                    deviceDid.setText(serials[1]);
                } catch (Exception e) {
                    deviceDid.setText("--");
                }

                try {
                    deviceMac.setText(serials[0]);
                } catch (Exception e) {
                    deviceMac.setText("--");
                }

                try {
                    deviceToken.setText(serials[2]);
                } catch (Exception e) {
                    deviceToken.setText("--");
                }
            }
        }
    }
}