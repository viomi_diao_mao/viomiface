package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.utils.DateUtils;
import viomi.com.viomiaiface.widget.MyClockView;

public class AlarmRemindActivity extends BaseActivity {

    private ImageView iv;
    private TextView remindTime;
    private ImageView back_icon;
    private MediaPlayer mediaPlayer;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_alarm_remind);
        back_icon = findViewById(R.id.back_icon);
        MyClockView clock_view = findViewById(R.id.clock_view);
        iv = findViewById(R.id.iv);
        remindTime = findViewById(R.id.remindTime);

        clock_view.setVisibility(View.GONE);

        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_speeaking_v).into(iv);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        Intent intent = getIntent();
        long alarmtime = intent.getLongExtra("alarmtime", 0);
        remindTime.setText(DateUtils.getDate2(alarmtime));

        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, 15 * 1000);
        preparePlayMusic();
    }

    private void preparePlayMusic() {
        mediaPlayer = MediaPlayer.create(this, R.raw.alarm_ok);
        mediaPlayer.start();

    }

    @Override
    protected void handleMessage(Message msg) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }
}
