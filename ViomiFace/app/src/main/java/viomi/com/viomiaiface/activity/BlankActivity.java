package viomi.com.viomiaiface.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import viomi.com.viomiaiface.R;

public class BlankActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
        finish();
    }
}
