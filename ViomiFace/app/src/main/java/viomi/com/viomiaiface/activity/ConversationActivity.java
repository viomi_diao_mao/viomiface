package viomi.com.viomiaiface.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.adapter.ConversationFloatAdapter;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.callback.ConversationUpdateCallback;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.service.ViomiVoiceService;
import viomi.com.viomiaiface.utils.LogUtils;

public class ConversationActivity extends BaseActivity {

    private ListView conlist;
    private ImageView statusIcon;
    private Intent serviceIntent;
    private ServiceConnection serviceConnection;
    private ViomiVoiceService voiceService;

    private List<ConversationBean> datalist;
    private ConversationFloatAdapter adapter;
    private static final long AUTOCLOSETIME = 2 * 1000L;
    private boolean isConversationLive = true;
    private boolean isspeaking;

    private static final String[] value_on = new String[]{"47", "202", "215"};//RGB颜色值
    private static final String[] value_off = new String[]{"0", "0", "0"};

    @Override
    protected void initView() {
        setContentView(R.layout.activity_conversation);
        statusIcon = findViewById(R.id.statusIcon);
        conlist = findViewById(R.id.conlist);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    private void initResource() {
        serviceIntent = new Intent(this, ViomiVoiceService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.e(TAG, "onServiceConnected");
                voiceService = ((ViomiVoiceService.MyBinder) service).getService();
                afterConnect();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };

        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);

        showVListening();
        Led_all(value_on);
    }

    private void releaseResource() {
        if (voiceService != null) {
            voiceService.clearConversationList();
            voiceService.removeConversationUpdateCallback();
        }
        unbindService(serviceConnection);
        Led_all(value_off);
    }


    private void afterConnect() {
        voiceService.setConversationUpdateCallback(new ConversationUpdateCallback() {
            @Override
            public void update(List<ConversationBean> conversationList) {
                LogUtils.e(TAG, "update");
                if (datalist != null) {
                    adapter.notifyDataSetChanged();
                    if (datalist.size() > 0) {
                        conlist.setSelection(datalist.size() - 1);
                    }
                }
            }

            @Override
            public void onListeningStart() {
            }

            @Override
            public void onListeningStop() {
                LogUtils.e(TAG, "onListeningStop");
                showSearching();
            }

            @Override
            public void onListenFail() {
            }

            @Override
            public void onSpeakingStart() {
                LogUtils.e(TAG, "onSpeakingStart");
                showVSpeaking();
                mHandler.removeMessages(HandlerMsgWhat.MSG0);
                isspeaking = true;
            }

            @Override
            public void onSpeakingStop() {
                LogUtils.e(TAG, "onSpeakingStop");
                isspeaking = false;

                if (isConversationLive) {
                    showVListening();
                    mHandler.removeMessages(HandlerMsgWhat.MSG0);
                } else {
                    showVFree();
                    mHandler.removeMessages(HandlerMsgWhat.MSG0);
                    mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, AUTOCLOSETIME);
                }
            }

            @Override
            public void onWakeup() {
                LogUtils.e(TAG, "onWakeup");
            }

            @Override
            public void onDialogStart() {
                LogUtils.e(TAG, "onDialogStart");
                isspeaking = false;
                isConversationLive = true;
                mHandler.removeMessages(HandlerMsgWhat.MSG0);
                showVListening();
            }

            @Override
            public void onDialogStop() {
                LogUtils.e(TAG, "onDialogStop");
                isConversationLive = false;
                if (!isspeaking) {
                    showVFree();
                    mHandler.removeMessages(HandlerMsgWhat.MSG0);
                    mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, AUTOCLOSETIME);
                }
            }
        });

        datalist = (voiceService.getConversationList());
        adapter = new ConversationFloatAdapter(datalist, this);
        conlist.setAdapter(adapter);
        if (datalist.size() > 0) {
            conlist.setSelection(datalist.size() - 1);
        }
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                onBackPressed();
                break;
            }
            default:
                break;
        }
    }

    private void showVFree() {
        LogUtils.e(TAG, "showVFree");
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_free_v).into(statusIcon);
    }

    private void showWakeup() {
        LogUtils.e(TAG, "showWakeup");
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_wakeup_v).into(statusIcon);
    }

    private void showVListening() {
        LogUtils.e(TAG, "showVListening");
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_listening_v).into(statusIcon);
    }

    private void showSearching() {
        LogUtils.e(TAG, "showSearching");
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_searching_v).into(statusIcon);
    }

    private void showVSpeaking() {
        LogUtils.e(TAG, "showVSpeaking");
        Glide.get(this).clearMemory();
        Glide.with(this).load(R.drawable.conversation_speeaking_v).into(statusIcon);
    }

    private void Led_all(String[] value) {
        Led_Run(1, value);
        Led_Run(2, value);
        Led_Run(3, value);
        Led_Run(4, value);
        Led_Run(5, value);
        Led_Run(6, value);
        Led_Run(7, value);
        Led_Run(8, value);
    }

    public void Led_Run(int num, String[] value) {
        try {
            for (int i = 0; i < value.length; i++) {
                int s = i * 8 + num;  //三个节点对应一个灯，比如：led1、led9、led17对应灯1  ， led2、led10、led18对应灯2
                BufferedWriter bufWriter = new BufferedWriter(new FileWriter("/sys/class/leds/led" + s + "/brightness"));
                bufWriter.write(value[i]);  // 写操作
                bufWriter.close();
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtils.e(TAG, "onNewIntent");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.e(TAG, "onResume");
        initResource();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtils.e(TAG, "onPause");
        releaseResource();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtils.e(TAG, "onStop");

        if (!isFinishing()) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}