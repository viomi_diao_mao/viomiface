package viomi.com.viomiaiface.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.adapter.CookDetailPGAdapter;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.model.FoodStepBean;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.widget.GlideRoundTransform;

public class CookDetailActivity extends BaseActivity {

    private ImageView back_icon;
    private ViewPager viewpager;
    private List<FoodStepBean> foodStepBeanList;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_cook_detail);
        viewpager = findViewById(R.id.viewpager);
        back_icon = findViewById(R.id.back_icon);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void init() {
        Intent intent = getIntent();
        setData(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setData(intent);
    }

    private void setData(Intent intent) {

        String jsonData = intent.getStringExtra("jsonData");
        foodStepBeanList = initData(jsonData);

        List<View> viewlist = new ArrayList<>();
        for (int i = 0; i < foodStepBeanList.size(); i++) {
            View step = LayoutInflater.from(this).inflate(R.layout.cook_detail_item_step, null);

            ImageView icon = step.findViewById(R.id.icon);
            TextView title = step.findViewById(R.id.step_title);
            TextView material = step.findViewById(R.id.material);
            TextView content = step.findViewById(R.id.content);

            if (i == 0) {
                material.setVisibility(View.VISIBLE);
            }

            Glide.with(this).load(foodStepBeanList.get(i).getPic()).into(icon);

            Glide.with(this).load(foodStepBeanList.get(i).getPic()).apply(new RequestOptions()
                    .transform(new GlideRoundTransform(this, getResources().getInteger(R.integer.img_cover_corner))))
                    .into(icon);

            title.setText(foodStepBeanList.get(i).getTitle() + "");
            content.setText(foodStepBeanList.get(i).getContent() + "");
            viewlist.add(step);
        }
        viewpager.setAdapter(new CookDetailPGAdapter(viewlist));
    }

    private List<FoodStepBean> initData(String jsonData) {
        List<FoodStepBean> foodStepBeanList = new ArrayList<>();

        if (jsonData == null) {
            return foodStepBeanList;
        }

        JSONObject json = JsonUitls.getJSONObject(jsonData);
        JSONObject extra = JsonUitls.getJSONObject(json, "extra");
        JSONArray list = JsonUitls.getJSONArray(extra, "list");
        if (list.length() > 0) {
            JSONObject list_item = JsonUitls.getJSONObject(list, 0);

            String name = JsonUitls.getString(list_item, "name");
            String pic = JsonUitls.getString(list_item, "pic");
            JSONArray material = JsonUitls.getJSONArray(list_item, "material");
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < material.length(); i++) {
                JSONObject list_item_item = JsonUitls.getJSONObject(material, i);
                String mname = JsonUitls.getString(list_item_item, "mname");
                String amount = JsonUitls.getString(list_item_item, "amount");
                buffer.append(mname).append(":").append(amount).append("; ");
            }
            foodStepBeanList.add(new FoodStepBean(name, buffer.toString(), pic));

            JSONArray process = JsonUitls.getJSONArray(list_item, "process");
            for (int i = 0; i < process.length(); i++) {
                JSONObject process_item = JsonUitls.getJSONObject(process, i);
                String pcontent = JsonUitls.getString(process_item, "pcontent");
                pcontent = pcontent.replace("br", "");
                String pic_s = JsonUitls.getString(process_item, "pic");
                String title = "第" + (i + 1) + "步";
                foodStepBeanList.add(new FoodStepBean(title, pcontent, pic_s));
            }

        }
        return foodStepBeanList;
    }

    BroadcastReceiver broadcastReceiverPre = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int currentItem = viewpager.getCurrentItem();
            if (currentItem > 0) {
                viewpager.setCurrentItem(currentItem - 1);
                Intent intent1 = new Intent(BroadcastAction.TTSSPKEAK);
                intent1.putExtra("skeak_word", foodStepBeanList.get(currentItem - 1).getTitle() + "  " + foodStepBeanList.get(currentItem - 1).getContent());
                sendBroadcast(intent1);
            }
        }
    };

    BroadcastReceiver broadcastReceiverNext = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int currentItem = viewpager.getCurrentItem();
            if (currentItem < foodStepBeanList.size() - 1) {
                viewpager.setCurrentItem(currentItem + 1);
                Intent intent1 = new Intent(BroadcastAction.TTSSPKEAK);
                intent1.putExtra("skeak_word", foodStepBeanList.get(currentItem + 1).getTitle() + "  " + foodStepBeanList.get(currentItem + 1).getContent());
                sendBroadcast(intent1);
            } else {
                Intent intent1 = new Intent(BroadcastAction.TTSSPKEAK);
                intent1.putExtra("skeak_word", "已到最后一步");
                sendBroadcast(intent1);
            }
        }
    };

    private void registerBroadcast() {
        IntentFilter filter1 = new IntentFilter(BroadcastAction.QIUCKORDER_PRE);
        registerReceiver(broadcastReceiverPre, filter1);
        IntentFilter filter2 = new IntentFilter(BroadcastAction.QIUCKORDER_NEXT);
        registerReceiver(broadcastReceiverNext, filter2);
    }

    private void unRegisterBroadcast() {
        unregisterReceiver(broadcastReceiverPre);
        unregisterReceiver(broadcastReceiverNext);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBroadcast();
        Intent intent = new Intent(BroadcastAction.PLAYCONTROL_COOK_STATUS);
        intent.putExtra("isCookOpen", true);
        sendBroadcast(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterBroadcast();
        Intent intent = new Intent(BroadcastAction.PLAYCONTROL_COOK_STATUS);
        intent.putExtra("isCookOpen", false);
        sendBroadcast(intent);
    }
}
