package viomi.com.viomiaiface.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.adapter.ShowFragmentAdapter;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.fragment.ClockFragment;
import viomi.com.viomiaiface.fragment.SettingFragment;
import viomi.com.viomiaiface.fragment.ViomiBallFragment;
import viomi.com.viomiaiface.fragment.ViomiLogoFragment;
import viomi.com.viomiaiface.fragment.WeatherFragment;
import viomi.com.viomiaiface.mijia.MiDeviceManager;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.service.DeviceService;
import viomi.com.viomiaiface.service.DownloadService;
import viomi.com.viomiaiface.service.ViomiVoiceService;
import viomi.com.viomiaiface.skill.handle.HandleSkill;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.SharedPreferencesUtil;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.viomiaiface.widget.FixedSpeedScroller;

public class DesktopActivity extends BaseActivity {

    private MiDeviceManager mMiDeviceManager;
    private LocalBroadcastManager mBroadcastManager;
    private static long SCROLLTIME = 10 * 1000;
    public ArrayList<AbstractDevice> mOnlinedevices = new ArrayList<>();
    public ArrayList<AbstractDevice> allBinddevices = new ArrayList<>();
    private String version;
    private String downlink;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    private TextView music_view;
    private ViewPager viewPager;
    private RelativeLayout guide_layout;
    private ImageView guide_img;
    private TextView guide_step;
    private RelativeLayout initLayout;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_desktop);
        viewPager = findViewById(R.id.viewPager);
        music_view = findViewById(R.id.music_view);

        guide_layout = findViewById(R.id.guide_layout);
        guide_img = findViewById(R.id.guide_img);
        guide_step = findViewById(R.id.guide_step);

        initLayout = findViewById(R.id.initLayout);

        boolean firstUseStatus = SharedPreferencesUtil.getFirstUseStatus();
        SharedPreferencesUtil.setFirstUseStatus(false);
//        if (firstUseStatus) {
//            guide_layout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    protected void initListener() {

        music_view.setOnClickListener(v -> {
            Intent intent = new Intent(this, MediaPlayActivity.class);
            startActivity(intent);
        });

        guide_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = guide_step.getText().toString();
                if ("完成".equals(text)) {
                    guide_layout.setVisibility(View.GONE);
                } else {
                    guide_img.setImageResource(R.drawable.guide_up_down);
                    guide_step.setText("完成");
                }
            }
        });

        initLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void init() {
        registerReceiver();
        startService();
        setScreenAlwaysOn();
        initFragmentList();

        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mMiDeviceManager = MiDeviceManager.getInstance();
        mHandler.sendEmptyMessage(HandlerMsgWhat.MSG04);
        mHandler.sendEmptyMessage(HandlerMsgWhat.MSG05);
    }

    private void startService() {
        Intent it = new Intent(this, DeviceService.class);
        startService(it);

        Intent voiceServiceIntent = new Intent(this, ViomiVoiceService.class);
        startService(voiceServiceIntent);
    }

    private void registerReceiver() {
        IntentFilter filter1 = new IntentFilter(BroadcastAction.VIOCESERVICEINIT);
        registerReceiver(voiceServiceInitReceive, filter1);

        IntentFilter filter2 = new IntentFilter(BroadcastAction.VIOCESERVICEINITCOMPLETE);
        registerReceiver(voiceServiceCompleteReceive, filter2);

        IntentFilter filter3 = new IntentFilter(BroadcastAction.SHOWMUSICINMAIN);
        registerReceiver(musicReceive, filter3);
    }

    private void unRegisterReceiver() {
        unregisterReceiver(voiceServiceInitReceive);
        unregisterReceiver(voiceServiceCompleteReceive);
        unregisterReceiver(musicReceive);
    }

    private void registerDeviceReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Miconfig.ACTION_BIND_SERVICE_SUCCEED);
        filter.addAction(Miconfig.ACTION_BIND_SERVICE_FAILED);
        filter.addAction(Miconfig.ACTION_DISCOVERY_DEVICE_SUCCEED);
        filter.addAction(Miconfig.ACTION_DISCOVERY_DEVICE_FAILED);
        mBroadcastManager.registerReceiver(mDeviceReceiver, filter);
    }

    private void unRegisterDeviceReceiver() {
        mBroadcastManager.unregisterReceiver(mDeviceReceiver);
    }

    private void setScreenAlwaysOn() {
        powerManager = (PowerManager) BaseApplication.getAppContext().getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "bright");
        wakeLock.acquire();
    }

    private void initFragmentList() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new WeatherFragment());
        fragmentList.add(new ClockFragment());
        fragmentList.add(new ViomiLogoFragment());
        fragmentList.add(new ViomiBallFragment());
        fragmentList.add(new SettingFragment());

        FragmentManager fm = getSupportFragmentManager();
        ShowFragmentAdapter adapter = new ShowFragmentAdapter(fm, fragmentList);
        viewPager.setAdapter(adapter);

        try {
            Field field = ViewPager.class.getDeclaredField("mScroller");
            field.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext(),
                    new AccelerateInterpolator());
            field.set(viewPager, scroller);
            scroller.setmDuration(300);
        } catch (Exception e) {
        }
    }

    private BroadcastReceiver mDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Miconfig.ACTION_BIND_SERVICE_SUCCEED:
                    break;

                case Miconfig.ACTION_BIND_SERVICE_FAILED:
                    break;

                case Miconfig.ACTION_DISCOVERY_DEVICE_SUCCEED:
                    List<AbstractDevice> devices = mMiDeviceManager.getWanDevices();
                    if (devices != null && devices.size() > 0) {
                        allBinddevices.clear();
                        allBinddevices.addAll(mMiDeviceManager.getWanDevices());

                        mOnlinedevices.clear();
                        for (int i = 0; i < devices.size(); i++) {
                            AbstractDevice device = devices.get(i);
                            if (device.isOnline()) {
                                mOnlinedevices.add(device);
                            }
                        }
                    }
                    HandleSkill.getInstance().setmOnlinedevices(mOnlinedevices);
                    break;

                case Miconfig.ACTION_DISCOVERY_DEVICE_FAILED:
                    break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        startScroll();
        registerDeviceReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScroll();
        unRegisterDeviceReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wakeLock != null) {
            wakeLock.release();
        }
        unRegisterReceiver();
    }

    private BroadcastReceiver voiceServiceInitReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            initLayout.setVisibility(View.VISIBLE);
        }
    };

    private BroadcastReceiver voiceServiceCompleteReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            initLayout.setVisibility(View.GONE);
        }
    };

    private BroadcastReceiver musicReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String musicName = intent.getStringExtra("musicName");
            boolean isShowMusic = intent.getBooleanExtra("isShowMusic", false);
            if (isShowMusic) {
                music_view.setVisibility(View.VISIBLE);
            } else {
                music_view.setVisibility(View.GONE);
            }
            music_view.setText(musicName + "");
        }
    };

    private void startScroll() {
        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG12, SCROLLTIME);
    }

    private void stopScroll() {
        mHandler.removeMessages(HandlerMsgWhat.MSG12);
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG02: {
                LogUtils.e(TAG, "HandlerMsgWhat.MSG2=" + msg.obj.toString());
                parseJson(msg.obj.toString());
            }
            break;
            case HandlerMsgWhat.MSG03: {

            }
            break;
            case HandlerMsgWhat.MSG04: {
                reqUpdate();
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG04, 4 * 60 * 60 * 1000);
                break;
            }

            case HandlerMsgWhat.MSG05:
                mMiDeviceManager.queryWanDeviceList();
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG05, 5000);
                break;

            case HandlerMsgWhat.MSG12:
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG12, SCROLLTIME);
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                stopScroll();
                break;
            case MotionEvent.ACTION_UP:
                startScroll();
                break;
            default:
                stopScroll();
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    private void reqUpdate() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "version");
        map.put("package", "viomi.com.viomiaiface");
        map.put("channel", FaceConfig.UPDATEENVIRONMENT);
        map.put("p", "1");
        map.put("l", "1");
        HttpApi.getRequestHandler(MURL.UPDATE, map, mHandler, HandlerMsgWhat.MSG02, HandlerMsgWhat.MSG03);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONArray data = JsonUitls.getJSONArray(json, "data");
            if (data != null && data.length() > 0) {
                JSONObject item = data.getJSONObject(0);
                String detail = JsonUitls.getString(item, "detail");
                version = JsonUitls.getString(item, "code");
                downlink = JsonUitls.getString(item, "url");
                if (!FaceConfig.CURRENTVERSION.equals(version)) {
                    showUpdateDialog(detail);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void showUpdateDialog(String code) {
        final Dialog dialog = new Dialog(this, R.style.selectorDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.update_dialog_layout, null);
        dialog.setContentView(view);

        TextView update_title = (TextView) view.findViewById(R.id.update_title);
        ListView tv_list = (ListView) view.findViewById(R.id.tv_list);
        TextView no_thanks = (TextView) view.findViewById(R.id.no_thanks);
        TextView ok_update = (TextView) view.findViewById(R.id.ok_update);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < version.length(); i++) {
            sb.append(version.charAt(i));
            if (i != version.length() - 1) {
                sb.append(".");
            }
        }
        String version_a = sb.toString();
        update_title.setText("V" + version_a + "版本发布啦，马上下载体验吧！");

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ok_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUpdate();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void downloadUpdate() {
        ToastUtil.show("正在后台下载，请稍后...");
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("downlink", downlink);
        intent.putExtra("version", version);
        startService(intent);
    }

    public void btnClick(View view) {
//        throw new NullPointerException();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(BaseApplication.getAppContext()).clearDiskCache();
            }
        }).start();
    }
}
