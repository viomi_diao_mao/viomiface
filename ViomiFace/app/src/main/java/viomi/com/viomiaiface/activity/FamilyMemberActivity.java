package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;

public class FamilyMemberActivity extends BaseActivity {

    private ImageView back_icon;
    private TextView title_view;
    private ListView memberlist;
    private TextView add_member;


    @Override
    protected void initView() {
        setContentView(R.layout.activity_family_member);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);
        title_view.setText("家庭成员管理");
        memberlist = findViewById(R.id.memberlist);
        add_member = findViewById(R.id.add_member);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FamilyMemberActivity.this, AddMenberStepActivity.class));


            }
        });
    }

    @Override
    protected void init() {

    }
}
