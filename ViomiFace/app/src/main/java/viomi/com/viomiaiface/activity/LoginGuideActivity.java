package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;

public class LoginGuideActivity extends BaseActivity {

    private ImageView back_icon;
    private TextView title_view;
    private TextView how_to_down;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_login_guide);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);
        how_to_down = findViewById(R.id.how_to_down);
        title_view.setText("登录指引");
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        how_to_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginGuideActivity.this, DownloadViomiActivity.class));
            }
        });
    }

    @Override
    protected void init() {

    }
}
