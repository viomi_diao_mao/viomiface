package viomi.com.viomiaiface.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.miot.api.MiotManager;
import com.miot.common.exception.MiotException;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.ToastUtil;

public class LoginOutActivity extends BaseActivity {

    private ImageView back_icon;
    private TextView title_view;
    private TextView userName;
    private TextView userId;
    private TextView login_out_btn;
    private ImageView user_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_login_out);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);

        user_icon = findViewById(R.id.user_icon);
        userName = findViewById(R.id.userName);
        userId = findViewById(R.id.userId);
        login_out_btn = findViewById(R.id.login_out_btn);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        login_out_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUtil.saveObject(LoginOutActivity.this, FaceConfig.USERFILENAME, null);
                ToastUtil.show("已注销登录！");
                try {
                    MiotManager.getPeopleManager().deletePeople();
                    sendBroadcast(new Intent(BroadcastAction.LOGINOUT));
                } catch (MiotException e) {
                    e.printStackTrace();
                }
                finish();
            }
        });
    }

    @Override
    protected void init() {
        title_view.setText(R.string.account_manager);
        Glide.with(this).load(R.drawable.default_user_icon).apply(new RequestOptions().circleCrop()).into(user_icon);

        UserEntity user = (UserEntity) FileUtil.getObject(this, FaceConfig.USERFILENAME);
        if (user != null) {
            Glide.with(this).load(user.getViomiHeadImg()).apply(new RequestOptions().circleCrop()).into(user_icon);
            userName.setText(user.getViomiNikeName());
            userId.setText("云米ID：" + user.getViomiAccount());
        }
    }
}
