package viomi.com.viomiaiface.activity;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.audiofx.Visualizer;
import android.os.IBinder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.mediaplayer.ICallBack;
import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.mediaplayer.Track;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.widget.GlideRoundTransform;
import viomi.com.viomiaiface.widget.MyClockView;

/**
 * Created by Mocc on 2018/3/4
 */

public class MediaPlayActivity extends BaseActivity {

    private ImageView back_icon;
    private ImageView img_cover;
    private TextView title;
    private TextView sub_title;
    private ImageView play_control_pre;
    private ImageView play_control_play;
    private ImageView play_control_next;

    private Intent intent;
    private ServiceConnection serviceConnection;
    private MusicService musicService;
    private String currentImg;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_media_play);

        MyClockView clock_view = findViewById(R.id.clock_view);
        clock_view.setVisibility(View.GONE);
        back_icon = findViewById(R.id.back_icon);
        img_cover = findViewById(R.id.img_cover);
        title = findViewById(R.id.title);
        sub_title = findViewById(R.id.sub_title);
        play_control_pre = findViewById(R.id.play_control_pre);
        play_control_play = findViewById(R.id.play_control_play);
        play_control_next = findViewById(R.id.play_control_next);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        play_control_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPre();
            }
        });

        play_control_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playOrPause();
            }
        });

        play_control_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNext();
            }
        });
    }

    private void playOrPause() {
        if (musicService.isPlaying()) {
            musicService.pause();
        } else {
            musicService.resume();
        }
    }

    private void playNext() {
        musicService.playNext();
    }

    private void playPre() {
        musicService.playPrev();
    }

    @Override
    protected void init() {
        intent = new Intent(this, MusicService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogUtils.e(TAG, "onServiceConnected");
                musicService = ((MusicService.MusicBinder) service).getService();
                afterConnect();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                LogUtils.e(TAG, "onServiceDisconnected");
            }
        };
        bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
        registerBroadcastReceive();
    }

    private void afterConnect() {
        addListner();
        updateView(musicService.getCurrentTrack());
    }

    private void addListner() {
        musicService.setCustomCallback(playCallBack);
    }

    private void removeListner() {
        musicService.removeCustomCallback();
    }

    private ICallBack playCallBack = new ICallBack() {
        @Override
        public void onPlay() {
            showPlayUI();
        }

        @Override
        public void onPause() {
            showStopUI();
        }

        @Override
        public void onResume() {
            showPlayUI();
        }

        @Override
        public void onStop() {
            showStopUI();
        }

        @Override
        public void onBuffering() {

        }

        @Override
        public void onBufferComplete() {

        }

        @Override
        public void onBufferingUpdate(int percent) {

        }

        @Override
        public void onUpdateSeekBar(int time) {

        }

        @Override
        public void onComplete(boolean isFinish) {
            showStopUI();
        }

        @Override
        public void onPlayError(int errorCode, String errorMsg) {

        }

        @Override
        public void onPrepareNext() {

        }

        @Override
        public void onPrepare() {

        }

        @Override
        public void onSetTotalTime(int time) {

        }

        @Override
        public void onSetVisualizer(Visualizer visualizer) {

        }

        @Override
        public void onUpdateUIState(UIState uiState) {

        }
    };

    private void showPlayUI() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                updateView(musicService.getCurrentTrack());
                play_control_play.setImageResource(R.drawable.ic_play_pause_btn);
            }
        });
    }

    private void showStopUI() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                play_control_play.setImageResource(R.drawable.ic_play_arrow_btn);
            }
        });
    }

    private void updateView(Track track) {
        if (track == null) {
            return;
        }
        title.setText(track.getTitle());
        sub_title.setText(track.getSubTitle());


        if (currentImg!=null&&currentImg.equals(track.getImageUrl())) {
            return;
        }
        currentImg = track.getImageUrl();
        Glide.with(this).load(track.getImageUrl()).apply(new RequestOptions()
                .transform(new GlideRoundTransform(this,getResources().getInteger(R.integer.img_cover_corner)))
                .placeholder(R.drawable.img_cover_placeholder)
                .error(R.drawable.img_cover_placeholder))
                .into(img_cover);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
        removeListner();
        unRegisterBroadcastReceive();
    }

    private void registerBroadcastReceive() {
        IntentFilter exitFilter = new IntentFilter(BroadcastAction.PLAYCONTROL_EXIT);
        registerReceiver(exitReceiver, exitFilter);

    }

    private void unRegisterBroadcastReceive() {
        unregisterReceiver(exitReceiver);
    }

    private BroadcastReceiver exitReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };
}
