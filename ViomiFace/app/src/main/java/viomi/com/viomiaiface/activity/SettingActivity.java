package viomi.com.viomiaiface.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.service.FloatService;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.wifilibrary.wifimodel.WifiScanActivity;

public class SettingActivity extends BaseActivity {

    private ImageView back_icon;
    private RelativeLayout login_layout;
    private ImageView user_icon;
    private TextView user_name;
    private RelativeLayout family_member;
    private SeekBar lightBar;
    private SeekBar sysVoiceBar;
    private SeekBar mediaVoiceBar;
    private RelativeLayout wifiSetting;
    private Switch face_switch_btn;
    private Switch gesture_switch_btn;

    private AudioManager mAudioManager;
    private TextView title_view;
    private boolean isLogin;
    private TextView wifi_name;
    private TextView user_id;
    private int clickcount;
    private RelativeLayout systemSetting;
    private RelativeLayout version_update;
    private RelativeLayout sys_version_update;
    private RelativeLayout about_device;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_setting);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);

        login_layout = findViewById(R.id.login_layout);
        user_icon = findViewById(R.id.user_icon);
        user_name = findViewById(R.id.user_name);
        user_id = findViewById(R.id.user_id);

        wifiSetting = findViewById(R.id.wifiSetting);
        wifi_name = findViewById(R.id.wifi_name);

        sysVoiceBar = findViewById(R.id.sysVoiceBar);
        mediaVoiceBar = findViewById(R.id.mediaVoiceBar);

        lightBar = findViewById(R.id.lightBar);

        family_member = findViewById(R.id.family_member);
        face_switch_btn = findViewById(R.id.face_switch_btn);
        gesture_switch_btn = findViewById(R.id.gesture_switch_btn);

        version_update = findViewById(R.id.version_update);
        TextView version_name = findViewById(R.id.version_name);
        version_name.setText("V" + FaceConfig.CURRENTVERSION);

        sys_version_update = findViewById(R.id.sys_version_update);
        TextView sys_version_name = findViewById(R.id.sys_version_name);
        sys_version_name.setText(Build.DISPLAY.length()>10?"":Build.DISPLAY);

        about_device = findViewById(R.id.about_device);

        systemSetting = findViewById(R.id.systemSetting);

        title_view.setText(R.string.system_title);

    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        title_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount++;
                if (clickcount == 5) {
                    systemSetting.setVisibility(View.VISIBLE);
                }
            }
        });

        login_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogin) {
                    Intent intent = new Intent(SettingActivity.this, LoginOutActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        wifiSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, WifiScanActivity.class);
                startActivity(intent);
            }
        });

        lightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                saveBrightness(SettingActivity.this, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sysVoiceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mediaVoiceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        family_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, FamilyMemberActivity.class));
            }
        });

        face_switch_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LogUtils.e(TAG, "face_switch_btn:isChecked=" + isChecked);
            }
        });

        gesture_switch_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LogUtils.e(TAG, "gesture_switch_btn:isChecked=" + isChecked);
            }
        });

        version_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, UpdateCheckActivity.class);
                startActivity(intent);
            }
        });

        sys_version_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, UpdateRomActivity.class);
                startActivity(intent);
            }
        });

        about_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });

        systemSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                startService(new Intent(SettingActivity.this, FloatService.class));
            }
        });
    }

    @Override
    protected void init() {
        //亮度相关
        int systemBrightness = 0;
        try {
            systemBrightness = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        lightBar.setMax(255);
        lightBar.setProgress(systemBrightness);


        //音量相关
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //当前系统音量
        int currentSysVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        //最大音量
        int maxSysVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        sysVoiceBar.setMax(maxSysVolume);
        sysVoiceBar.setProgress(currentSysVolume);

        //当前媒体音量
        int currentMediaVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        //最大音量
        int maxMediaVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mediaVoiceBar.setMax(maxMediaVolume);
        mediaVoiceBar.setProgress(currentMediaVolume);
    }

    private void saveBrightness(Context context, int brightness) {
        Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightness);
        context.getContentResolver().notifyChange(uri, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserEntity user = (UserEntity) FileUtil.getObject(this, FaceConfig.USERFILENAME);
        if (user != null) {
            LogUtils.e(TAG, user.toString());
            isLogin = true;
            Glide.with(this).load(user.getViomiHeadImg()).apply(new RequestOptions().circleCrop()).into(user_icon);
            user_name.setText(user.getViomiNikeName());
            user_id.setText(user.getViomiAccount());
        } else {
            isLogin = false;
            Glide.with(this).load(R.drawable.default_user_icon).apply(new RequestOptions().circleCrop()).into(user_icon);
            user_name.setText(getString(R.string.not_login_in));
            user_id.setText(null);
        }

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        String ssid = info.getSSID();
        ssid = ssid.replace("\"", "");

        if (!wifiManager.isWifiEnabled()) {
            ssid = "未连接";
        }
        wifi_name.setText(ssid);
    }
}