package viomi.com.viomiaiface.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.service.DownloadService;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.ToastUtil;

public class UpdateCheckActivity extends BaseActivity {

    private ImageView back_icon;
    private TextView title_view;
    private TextView version_name;
    private TextView update_btn;

    private String version;
    private String downlink;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_update_check);
        back_icon = findViewById(R.id.back_icon);
        title_view = findViewById(R.id.title_view);

        version_name = findViewById(R.id.version_name);
        update_btn = findViewById(R.id.update_btn);

        title_view.setText("应用版本");
        version_name.setText("当前版本：V" + FaceConfig.CURRENTVERSION);
    }

    @Override
    protected void initListener() {
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqUpdate();
            }
        });
    }

    @Override
    protected void init() {

    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                parseJson(msg.obj.toString());
            }
            break;

            case HandlerMsgWhat.MSG01: {
            }
            break;
        }
    }

    private void reqUpdate() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "version");
        map.put("package", "viomi.com.viomiaiface");
        map.put("channel", FaceConfig.UPDATEENVIRONMENT);
        map.put("p", "1");
        map.put("l", "1");
        HttpApi.getRequestHandler(MURL.UPDATE, map, mHandler, HandlerMsgWhat.MSG0, HandlerMsgWhat.MSG01);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONArray data = JsonUitls.getJSONArray(json, "data");
            if (data != null && data.length() > 0) {
                JSONObject item = data.getJSONObject(0);
                String detail = JsonUitls.getString(item, "detail");
                version = JsonUitls.getString(item, "code");
                downlink = JsonUitls.getString(item, "url");
                if (!FaceConfig.CURRENTVERSION.equals(version)) {
                    showUpdateDialog(detail);
                } else {
                    ToastUtil.show("当前为最新版本");
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void showUpdateDialog(String code) {
        final Dialog dialog = new Dialog(this, R.style.selectorDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.update_dialog_layout, null);
        dialog.setContentView(view);

        TextView update_title = (TextView) view.findViewById(R.id.update_title);
        ListView tv_list = (ListView) view.findViewById(R.id.tv_list);
        TextView no_thanks = (TextView) view.findViewById(R.id.no_thanks);
        TextView ok_update = (TextView) view.findViewById(R.id.ok_update);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < version.length(); i++) {
            sb.append(version.charAt(i));
            if (i != version.length() - 1) {
                sb.append(".");
            }
        }
        String version_a = sb.toString();
        update_title.setText("V" + version_a + "版本发布啦，马上下载体验吧！");

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ok_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUpdate();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void downloadUpdate() {
        ToastUtil.show("正在后台下载，请稍后...");
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("downlink", downlink);
        intent.putExtra("version", version);
        startService(intent);
    }
}
