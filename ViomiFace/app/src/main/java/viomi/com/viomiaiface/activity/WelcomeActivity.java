package viomi.com.viomiaiface.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.SharedPreferencesUtil;
import viomi.com.viomiaiface.utils.SystemUtil;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.wifilibrary.wifimodel.FirstConnectActivity;

public class WelcomeActivity extends BaseActivity {

    private WifiManager wifiManager;
    private int connectCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        boolean firstUseStatus = SharedPreferencesUtil.getFirstUseStatus();
        String systemModel = SystemUtil.getSystemModel();
        if ("rk312x".equals(systemModel) || "rk3128".equals(systemModel) || "rk3399-mid".equals(systemModel) || "MI 5s".equals(systemModel)) {
            if (firstUseStatus) {
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG01, 2000);
            } else {
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, 2000);
            }
        } else {
            mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 2000);
        }
        LogUtils.e(TAG, "系统版本号=" + SystemUtil.getSystemVersion() + ";手机型号=" + SystemUtil.getSystemModel() + ";手机厂商=" + SystemUtil.getDeviceBrand() + ";手机IMEI=" + SystemUtil.getIMEI(this));
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                if (isConnected()) {
                    Intent intent = new Intent(WelcomeActivity.this, DesktopActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
                connectCount++;
                if (connectCount == 5) {
                    Intent intent = new Intent(WelcomeActivity.this, FirstConnectActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG0, 2000);
                break;
            }

            case HandlerMsgWhat.MSG01: {
                Intent intent = new Intent(WelcomeActivity.this, FirstConnectActivity.class);
                intent.putExtra("isFisrt", true);
                startActivity(intent);
                finish();
                break;
            }

            case HandlerMsgWhat.MSG02: {
                ToastUtil.show("该设备不支持！即将退出");
                finish();
                break;
            }
            default:
                break;
        }
    }

    private boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info != null
                && info.getType() == ConnectivityManager.TYPE_WIFI) {
            return true;
        }
        return false;
    }


    @Override
    protected void initView() {
        setContentView(R.layout.activity_welcome);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    @Override
    public void onBackPressed() {

    }
}
