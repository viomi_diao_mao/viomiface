package viomi.com.viomiaiface.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;
import viomi.com.viomiaiface.model.WeatherBean2;
import viomi.com.viomiaiface.utils.WeatherIconUtil;

/**
 * Created by Mocc on 2018/5/10
 */
public class ConversationFloatAdapter extends BaseAdapter {
    private List<ConversationBean> list;
    private Context context;
    private LayoutInflater inflater;

    public ConversationFloatAdapter(List<ConversationBean> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ConversationType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType().ordinal();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ConversationBean conversationBean = list.get(position);

        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;
        ViewHolder3 holder3 = null;
        ViewHolder4 holder4 = null;
        ViewHolder5 holder5 = null;

        if (convertView == null) {
            switch (conversationBean.getType()) {
                case user_input:
                    convertView = inflater.inflate(R.layout.conversation_float_layout_item5, null);
                    holder1 = new ViewHolder1();
                    holder1.txt = convertView.findViewById(R.id.txt);
                    convertView.setTag(holder1);
                    break;
                case device_x5_temp:
                    convertView = inflater.inflate(R.layout.conversation_float_layout_item2, null);
                    holder2 = new ViewHolder2();
                    holder2.temp_bar = convertView.findViewById(R.id.temp_bar);
                    holder2.temp_tv = convertView.findViewById(R.id.temp_tv);
                    convertView.setTag(holder2);
                    break;
                case robot_alarm:
                    convertView = inflater.inflate(R.layout.conversation_float_layout_item3, null);
                    holder3 = new ViewHolder3();
                    holder3.time_txt = convertView.findViewById(R.id.time_txt);
                    convertView.setTag(holder3);
                    break;
                case weather:
                    convertView = inflater.inflate(R.layout.conversation_float_layout_item4, null);
                    holder4 = new ViewHolder4();
                    holder4.city = convertView.findViewById(R.id.city);
                    holder4.temperature = convertView.findViewById(R.id.temperature);
                    holder4.weather = convertView.findViewById(R.id.weather);
                    holder4.wind = convertView.findViewById(R.id.wind);
                    holder4.weather_icon = convertView.findViewById(R.id.weather_icon);
                    convertView.setTag(holder4);
                    break;
                case robot_txt:
                    convertView = inflater.inflate(R.layout.conversation_float_layout_item1, null);
                    holder5 = new ViewHolder5();
                    holder5.txt = convertView.findViewById(R.id.txt);
                    convertView.setTag(holder5);
                    break;
                default:
                    break;
            }
        } else {
            switch (conversationBean.getType()) {
                case user_input:
                    holder1 = (ViewHolder1) convertView.getTag();
                    break;
                case device_x5_temp:
                    holder2 = (ViewHolder2) convertView.getTag();
                    break;
                case robot_alarm:
                    holder3 = (ViewHolder3) convertView.getTag();
                    break;
                case weather:
                    holder4 = (ViewHolder4) convertView.getTag();
                    break;
                case robot_txt:
                    holder5 = (ViewHolder5) convertView.getTag();
                    break;
                default:
                    break;
            }
        }

        switch (conversationBean.getType()) {
            case user_input:
                holder1.txt.setText("\""+conversationBean.getParam().toString()+"\"");
                break;
            case device_x5_temp:
                int temp = ((int) conversationBean.getParam());
                holder2.temp_tv.setText(temp + "℃");
                int max = 50;
                int process = temp - 40;
                holder2.temp_bar.setMax(max);
                holder2.temp_bar.setProgress(process);
                break;
            case robot_alarm:
                holder3.time_txt.setText("设置提醒 " + conversationBean.getParam().toString());
                break;
            case weather:
                WeatherBean2 param = (WeatherBean2) conversationBean.getParam();
                holder4.city.setText(param.getCity());
                holder4.temperature.setText(param.getTemperature());
                holder4.weather.setText(param.getWeather());
                holder4.wind .setText(param.getWind());
                WeatherIconUtil.setWeatherIcon2(holder4.weather_icon, param.getWeather());
                break;
            case robot_txt:
                holder5.txt.setText("\""+conversationBean.getParam().toString()+"\"");
                break;
            default:
                break;
        }
        return convertView;
    }

    static class ViewHolder1 {
        TextView txt;
    }

    static class ViewHolder2 {
        TextView temp_tv;
        SeekBar temp_bar;
    }

    static class ViewHolder3 {
        TextView time_txt;
    }

    static class ViewHolder4 {
        TextView temperature, city,weather, wind;
        ImageView weather_icon;
    }

    static class ViewHolder5 {
        TextView txt;
    }
}
