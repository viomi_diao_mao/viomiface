package viomi.com.viomiaiface.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;
import com.miot.common.device.Device;

import java.util.List;

import viomi.com.viomiaiface.R;

/**
 * Created by Mocc on 2018/1/18
 */

public class DeviceAdapter extends BaseAdapter {

    private List<AbstractDevice> list;
    private Context context;
    private LayoutInflater inflater;

    public DeviceAdapter(List<AbstractDevice> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.device_list_item, null);
            holder = new ViewHolder();
            holder.device_name = convertView.findViewById(R.id.device_name);
            holder.device_did = convertView.findViewById(R.id.device_did);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AbstractDevice abstractDevice = list.get(position);
        Device device = abstractDevice.getDevice();
        holder.device_name.setText(device.getName());
        holder.device_did.setText(device.getDeviceId());
        return convertView;
    }

    static class ViewHolder {
        TextView device_name, device_did;
    }

}
