package viomi.com.viomiaiface.base;

import android.app.ActivityManager;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.miot.api.MiotManager;
import com.miot.api.bluetooth.BindStyle;
import com.miot.api.bluetooth.BluetoothDeviceConfig;
import com.miot.api.bluetooth.XmBluetoothManager;
import com.miot.common.ReturnCode;
import com.miot.common.config.AppConfiguration;
import com.miot.common.model.DeviceModel;
import com.miot.common.model.DeviceModelException;
import com.miot.common.model.DeviceModelFactory;
import com.miot.common.utils.Logger;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;

import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.mijia.WaterPurifierBase;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.utils.CrashHandler;
import viomi.com.viomiaiface.utils.SharedPreferencesUtil;

/**
 * Created by Mocc on 2018/1/3
 */

public class BaseApplication extends Application {

    private static final String TAG = BaseApplication.class.getSimpleName();
    private LocalBroadcastManager mBindBroadcastManager;
    private static BaseApplication app;
    public static final String PUSH_MESSAGE = "com.xiaomi.push.message";
    public static final String PUSH_COMMAND = "com.xiaomi.push.command";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initMijia();
        ApiClient.getInstance().init();// Retrofit 初始化
    }

    public static BaseApplication getAppContext() {
        return app;
    }

    private void initMijia() {
        Logger.enableLog(true);

        mBindBroadcastManager = LocalBroadcastManager.getInstance(this);

        if (isMainProcess()) {
            MiotManager.getInstance().initialize(this);
            registerPush();
            new MiotOpenTask().execute();
        }
        Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(this));
    }

    public void registerPush() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(PUSH_COMMAND);
        filter.addAction(PUSH_MESSAGE);
        registerReceiver(mReceiver, filter);
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case PUSH_COMMAND:
                    MiPushCommandMessage command = (MiPushCommandMessage) intent.getSerializableExtra("command");
                    Logger.d(TAG, "command: " + command.toString());
                    switch (command.getCommand()) {
                        case MiPushClient.COMMAND_REGISTER:
                            //TODO
                            break;
                    }
                    break;
                case PUSH_MESSAGE:
                    MiPushMessage message = (MiPushMessage) intent.getSerializableExtra("message");
                    Logger.d(TAG, "message: " + message.toString());
                    break;
            }
        }
    };

    private boolean isMainProcess() {
        String mainProcessName = getPackageName();
        String processName = getProcessName();
        return TextUtils.equals(processName, mainProcessName);
    }

    private String getProcessName() {
        int pid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : activityManager.getRunningAppProcesses()) {
            if (processInfo.pid == pid) {
                return processInfo.processName;
            }
        }
        return null;
    }

    @Override
    public void onTerminate() {
        new MiotCloseTask().execute();
        super.onTerminate();
    }

    private class MiotOpenTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {
            AppConfiguration appConfig = new AppConfiguration();
            String scanType = SharedPreferencesUtil.getScanType();
            switch (scanType) {
                case "android":
                    appConfig.setAppId(Miconfig.OAUTH_ANDROID_APP_ID);
                    appConfig.setAppKey(Miconfig.OAUTH_ANDROID_APP_KEY);
                    break;
                case "ios":
                    appConfig.setAppId(Miconfig.OAUTH_IOS_APP_ID);
                    appConfig.setAppKey(Miconfig.OAUTH_IOS_APP_KEY);
                    break;
                default:
                    break;
            }
            MiotManager.getInstance().setAppConfig(appConfig);

            try {
                /*
                 *添加扫描型号
                 * */
//                DeviceModel water1 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURIFIER_V1,
//                        Miconfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
//                MiotManager.getInstance().addModel(water1);
//                DeviceModel water2 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURIFIER_V2,
//                        Miconfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
//                MiotManager.getInstance().addModel(water2);
//                DeviceModel water3 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURIFIER_V3,
//                        Miconfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
//                MiotManager.getInstance().addModel(water3);
//                DeviceModel water4 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_LX2,
//                        Miconfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
//                MiotManager.getInstance().addModel(water4);
//                DeviceModel water5 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_LX3,
//                        Miconfig.YUNMI_WATERPURIFIER_URL, WaterPurifierBase.class);
//                MiotManager.getInstance().addModel(water5);
                DeviceModel water6 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_V1,
                        Miconfig.YUNMI_WATERPURI_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water6);
                DeviceModel water7 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_V2,
                        Miconfig.YUNMI_WATERPURI_V2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water7);
                DeviceModel water8 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_S1,
                        Miconfig.YUNMI_WATERPURI_S1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water8);
                DeviceModel water9 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_C1,
                        Miconfig.YUNMI_WATERPURI_C1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water9);
                DeviceModel water10 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_C2,
                        Miconfig.YUNMI_WATERPURI_C2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water10);
                DeviceModel water11 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_S2,
                        Miconfig.YUNMI_WATERPURI_S2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water11);
                DeviceModel water12 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_X3,
                        Miconfig.YUNMI_WATERPURI_X3_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water12);
                DeviceModel water13 = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_WATERPURI_X5,
                        Miconfig.YUNMI_WATERPURI_X5_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(water13);


                DeviceModel fridgeU1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_U1,
                        Miconfig.VIOMI_FRIDGE_U1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeU1Model);
                DeviceModel fridgeU2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_U2,
                        Miconfig.VIOMI_FRIDGE_U2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeU2Model);
                DeviceModel fridgeV1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_V1,
                        Miconfig.VIOMI_FRIDGE_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeV1Model);
                DeviceModel fridgeV2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_V2,
                        Miconfig.VIOMI_FRIDGE_V2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeV2Model);
                DeviceModel fridgeV3Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_V3,
                        Miconfig.VIOMI_FRIDGE_V3_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeV3Model);
                DeviceModel fridgeV4Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_V4,
                        Miconfig.VIOMI_FRIDGE_V4_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeV4Model);
                DeviceModel fridgeX1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_X1,
                        Miconfig.VIOMI_FRIDGE_X1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeX1Model);
                DeviceModel fridgeX2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_X2,
                        Miconfig.VIOMI_FRIDGE_X2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeX2Model);
                DeviceModel fridgeX3Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_X3,
                        Miconfig.VIOMI_FRIDGE_X3_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeX3Model);
                DeviceModel fridgeX4Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_X4,
                        Miconfig.VIOMI_FRIDGE_X4_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeX4Model);
                DeviceModel fridgeX5Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FRIDGE_X5,
                        Miconfig.VIOMI_FRIDGE_X5_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fridgeX5Model);

                DeviceModel hoodT8Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_T8,
                        Miconfig.VIOMI_HOOD_T8_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodT8Model);
                DeviceModel hoodA6Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_A6,
                        Miconfig.VIOMI_HOOD_A6_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA6Model);
                DeviceModel hoodA7Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_A7,
                        Miconfig.VIOMI_HOOD_A7_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA7Model);
                DeviceModel hoodC6Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_C6,
                        Miconfig.VIOMI_HOOD_C6_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodC6Model);
                DeviceModel hoodA4Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_A4,
                        Miconfig.VIOMI_HOOD_A4_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA4Model);
                DeviceModel hoodA5Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_A5,
                        Miconfig.VIOMI_HOOD_A5_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodA5Model);
                DeviceModel hoodC1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_C1,
                        Miconfig.VIOMI_HOOD_C1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodC1Model);
                DeviceModel hoodH1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_H1,
                        Miconfig.VIOMI_HOOD_H1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodH1Model);
                DeviceModel hoodH2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_H2,
                        Miconfig.VIOMI_HOOD_H2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodH2Model);
                DeviceModel hoodx1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_X1,
                        Miconfig.VIOMI_HOOD_X1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodx1Model);
                DeviceModel hoodx2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_HOOD_X2,
                        Miconfig.VIOMI_HOOD_X2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(hoodx2Model);

                DeviceModel R1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_KETTLE_R1,
                        Miconfig.YUNMI_KETTLE_R1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(R1Model);

                DeviceModel R2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_KETTLE_R2,
                        Miconfig.YUNMI_KETTLE_R1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(R2Model);

                DeviceModel R3Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_KETTLE_R3,
                        Miconfig.YUNMI_KETTLE_R1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(R3Model);

                DeviceModel mg2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.YUNMI_PL_MACHINE_MG2,
                        Miconfig.YUNMI_PL_MACHINE_MG2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(mg2Model);

                DeviceModel oven1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_OVEN_V1,
                        Miconfig.VIOMI_OVEN_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(oven1Model);
                DeviceModel oven2Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_OVEN_V2,
                        Miconfig.VIOMI_OVEN_V2_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(oven2Model);
                DeviceModel dish1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_DISHWASHER_V01,
                        Miconfig.VIOMI_DISHWASHER_V01_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(dish1Model);
                DeviceModel wahseru1Model = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_WASHER_U1,
                        Miconfig.VIOMI_WASHER_U1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(wahseru1Model);
                DeviceModel vacuumModel = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_VACUUM_V1,
                        Miconfig.YUNMI_VACUUM_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(vacuumModel);
                DeviceModel fanModel = DeviceModelFactory.createDeviceModel(BaseApplication.this, Miconfig.VIOMI_FAN_V1,
                        Miconfig.YUNMI_FAN_V1_URL, WaterPurifierBase.class);
                MiotManager.getInstance().addModel(fanModel);

            } catch (DeviceModelException e) {
                e.printStackTrace();
            }
            return MiotManager.getInstance().open();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            do {
                int result = integer;
                Log.d(TAG, "MiotOpen result: " + result);
                Intent intent = new Intent(Miconfig.ACTION_BIND_SERVICE_FAILED);
                if (result == ReturnCode.OK) {
                    intent = new Intent(Miconfig.ACTION_BIND_SERVICE_SUCCEED);
                }
                mBindBroadcastManager.sendBroadcast(intent);

                BluetoothDeviceConfig config = new BluetoothDeviceConfig();
                config.bindStyle = BindStyle.WEAK;
                config.model = "mijia.demo.v1";
                config.productId = 222;
                XmBluetoothManager.getInstance().setDeviceConfig(config);
            }
            while (false);
        }
    }

    private class MiotCloseTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... params) {
            return MiotManager.getInstance().close();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            do {
                int result = integer;
                Log.d(TAG, "MiotClose result: " + result);
            }
            while (false);
        }
    }

}