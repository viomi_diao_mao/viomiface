package viomi.com.viomiaiface.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.miot.common.abstractdevice.AbstractDevice;

import java.lang.ref.WeakReference;

import viomi.com.viomiaiface.utils.LogUtils;

/**
 * Created by Mocc on 2018/1/3
 */

public abstract class BaseFragment extends Fragment {

    protected String TAG = getClass().getSimpleName();
    protected MyHandler mHandler=new MyHandler(this);;
    public AbstractDevice mDevice;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.e(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = initView(inflater);
        initListener();
        init();
        return view;
    }

    protected abstract View initView(LayoutInflater inflater);

    protected abstract void initListener();

    protected abstract void init();

    protected static class MyHandler extends Handler {

        WeakReference<BaseFragment> weakReference;

        public MyHandler(BaseFragment fragment) {
            this.weakReference = new WeakReference<BaseFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            BaseFragment fragment = weakReference.get();
            if (fragment != null) {
                fragment.handleMessage(msg);
            }
        }
    }

    public void setDevice(AbstractDevice device) {
            mDevice = device;
    }

    protected void handleMessage(Message msg) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.e(TAG, "onDestroy");
        if (mHandler != null) {
            if ( mHandler.weakReference!=null) {
                mHandler.weakReference.clear();
            }
            mHandler.removeCallbacksAndMessages(null);
        }
    }
}