package viomi.com.viomiaiface.callback;

import java.util.List;

import viomi.com.viomiaiface.model.ConversationBean;

/**
 * Created by Mocc on 2018/1/31
 */

public interface ConversationUpdateCallback {
    void update(List<ConversationBean> conversationList);

    void onListeningStart();

    void onListeningStop();

    void onListenFail();

    void onSpeakingStart();

    void onSpeakingStop();

    void onWakeup();

    void onDialogStart();

    void onDialogStop();
}
