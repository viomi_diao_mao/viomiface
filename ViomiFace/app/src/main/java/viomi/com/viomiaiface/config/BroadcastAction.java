package viomi.com.viomiaiface.config;

/**
 * Created by Mocc on 2018/2/2
 */

public class BroadcastAction {

    public static final String GESTURE_FIST = "viomi.com.viomiaiface.GESTURE_FIST";
    public static final String GESTURE_PALM = "viomi.com.viomiaiface.GESTURE_PALM";
    public static final String GESTURE_OK_GESTURE = "viomi.com.viomiaiface.GESTURE_OK_GESTURE";
    public static final String GESTURE_V_GESTURE = "viomi.com.viomiaiface.GESTURE_V_GESTURE";
    public static final String GESTURE_ONE_FINGER = "viomi.com.viomiaiface.GESTURE_ONE_FINGER";

    public static final String ACTION_DEVICE_UNBIND = "com.viomi.fridge.action.ACTION_DEVICE_UNBIND";//设备被解绑
    public static final String ACTION_DEVICE_BIND = "com.viomi.fridge.action.ACTION_DEVICE_BIND";//设备被小米账号绑定

    public static final String ACTION_DEVICE_AWE = "com.viomi.fridge.action.ACTION_AWE";//awe 临时控制

    public static final String SPEECHSTART = "viomi.com.viomiaiface.SPEECHSTART";
    public static final String SPEECHSTOP = "viomi.com.viomiaiface.SPEECHSTOP";

    public static final String QIUCKORDER_PRE = "viomi.com.viomiaiface.QIUCKORDER_PRE";
    public static final String QIUCKORDER_NEXT = "viomi.com.viomiaiface.QIUCKORDER_NEXT";

    public static final String TTSSPKEAK = "viomi.com.viomiaiface.TTSSPKEAK";

    public static final String PLAYCONTROL_PAUSE = "viomi.com.viomiaiface.PLAYCONTROL_PAUSE";
    public static final String PLAYCONTROL_PLAY = "viomi.com.viomiaiface.PLAYCONTROL_PLAY";
    public static final String PLAYCONTROL_NEXT = "viomi.com.viomiaiface.PLAYCONTROL_NEXT";
    public static final String PLAYCONTROL_PREV = "viomi.com.viomiaiface.PLAYCONTROL_PREV";
    public static final String PLAYCONTROL_EXIT = "viomi.com.viomiaiface.PLAYCONTROL_EXIT";

    public static final String PLAYCONTROL_COOK_STATUS = "viomi.com.viomiaiface.PLAYCONTROL_COOK_STATUS";

    public static final String SHOWMUSICINMAIN = "viomi.com.viomiaiface.SHOWMUSICINMAIN";
    public static final String LOGINOUT = "viomi.com.viomiaiface.LOGINOUT";

    public static final String ACTION_SET_ALERM = "com.viomi.viomiaiface.action.ACTION_SET_ALERM";//状态栏显示

    public static final String VIOCESERVICEINIT ="com.viomi.viomiaiface.action.VIOCESERVICEINIT";
    public static final String VIOCESERVICEINITCOMPLETE ="com.viomi.viomiaiface.action.VIOCESERVICEINITCOMPLETE";
}
