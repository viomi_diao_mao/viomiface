package viomi.com.viomiaiface.config;

/**
 * Created by Mocc on 2018/1/3
 */

//发版要改-发版要改-发版要改

public class FaceConfig {

    public final static boolean LOGOUT = true;
    public final static boolean RELEASE = true;

    public final static String USERFILENAME = "userinfo";

    //测试服更新参数
//    public final static String UPDATEENVIRONMENT = "viomi_test";
    //正式服更新参数 mtk芯片
//    public final static String UPDATEENVIRONMENT = "viomi";

    // 正式服更新参数 rk芯片
    public final static String UPDATEENVIRONMENT = "viomi_rk";

    // 正式服更新参数 rk芯片 ROM更新
    public final static String UPDATEENVIRONMENT_ROM = "viomi_rk_rom";

    //当前版本号
    public final static String CURRENTVERSION = "123";


    public final static String MODEL = "viomi.wifispeaker.v1";//产品型号
    public final static String DefaultDeviceId = "85396805";  //默认did
    public final static String DefaultMac = "7C:49:EB:0F:42:85";  //默认mac
    public final static String DefaultMiotToken = "I6TWf5hIR2U0KGFd";  //默认token

    public final static String DefaultReply = "抱歉，小薇暂时帮不了您";

}
