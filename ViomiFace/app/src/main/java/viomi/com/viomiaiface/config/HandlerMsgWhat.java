package viomi.com.viomiaiface.config;

/**
 * Created by Mocc on 2018/1/9
 */

public class HandlerMsgWhat {

    public static final int MSG0 = 0;
    public static final int MSG01 = 1;
    public static final int MSG02 = 2;
    public static final int MSG03 = 3;
    public static final int MSG04 = 4;
    public static final int MSG05 = 5;
    public static final int MSG06 = 6;
    public static final int MSG07 = 7;
    public static final int MSG08 = 8;
    public static final int MSG09 = 9;
    public static final int MSG10 = 10;
    public static final int MSG11 = 11;
    public static final int MSG12 = 12;
    public static final int MSG13 = 13;
    public static final int MSG14 = 14;
    public static final int MSG15 = 15;
    public static final int MSG16 = 16;
    public static final int MSG17 = 17;
    public static final int MSG18 = 18;
    public static final int MSG19 = 19;
    public static final int MSG20 = 20;
    public static final int MSG21 = 21;
    public static final int MSG22 = 22;
    public static final int MSG23 = 23;
    public static final int MSG24 = 24;
    public static final int MSG25 = 25;
    public static final int MSG26 = 26;
    public static final int MSG27 = 27;
    public static final int MSG28 = 28;
    public static final int MSG29 = 29;
    public static final int MSG30 = 30;
}
