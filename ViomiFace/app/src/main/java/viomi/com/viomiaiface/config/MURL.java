package viomi.com.viomiaiface.config;

/**
 * Created by Mocc on 2018/1/9
 */

public class MURL {

    //版本更新检测
    public final static String UPDATE = "http://app.mi-ae.com.cn/getdata";

    private static final String BASEURL_DEBUG = "https://auth.mi-ae.net/services";// 扫码登陆商城测试环境
    private static final String BASEURL_RELEASE = "https://vmall-auth.mi-ae.net/services";// 扫码登陆商城正式环境

    private static final String BASEURL = FaceConfig.RELEASE ? BASEURL_RELEASE : BASEURL_DEBUG;

    public static final String QRCODE_URL = BASEURL + "/vmall/login/QRCode.json";// 生成登录二维码
    public static final String CHECK_LOGIN = BASEURL + "/vmall/login/QRCode.json";// 轮询获取二维码登录状态

    public static final String MOB_WEATHER = "http://apicloud.mob.com/v1/weather/query";

    public static final String URL_HULIAN = "http://vj.viomi.com.cn/wlink/devicesStatus/did/";//互联
    public static final String URL_HULIAN_SET = "http://vj.viomi.com.cn/wlink";//互联控制

}
