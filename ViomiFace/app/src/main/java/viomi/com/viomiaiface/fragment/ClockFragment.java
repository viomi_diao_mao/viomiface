package viomi.com.viomiaiface.fragment;


import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClockFragment extends BaseFragment {


    private TextView hour;
    private TextView twoPoint;
    private TextView minute;
    private TextView dateTv;

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_clock, null);

        hour = view.findViewById(R.id.hour);
        twoPoint = view.findViewById(R.id.twoPoint);
        minute = view.findViewById(R.id.minute);
        dateTv = view.findViewById(R.id.date);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.sendEmptyMessage(0);
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void handleMessage(Message msg) {
        updateClock();
        mHandler.sendEmptyMessageDelayed(0, 1000);
    }

    private void updateClock() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String time = df.format(date);

        SimpleDateFormat dfmonth = new SimpleDateFormat("M");
        SimpleDateFormat dfday = new SimpleDateFormat("d");
        SimpleDateFormat dfweek = new SimpleDateFormat("EEEE");
        SimpleDateFormat dfhour = new SimpleDateFormat("HH");
        SimpleDateFormat dfmimute = new SimpleDateFormat("mm");

        String dfmonth_tx = dfmonth.format(date);
        String dfday_tx = dfday.format(date);
        String dfweek_tx = dfweek.format(date);
        String dfhour_tx = dfhour.format(date);
        String dfmimute_tx = dfmimute.format(date);

        hour.setText(dfhour_tx);
        minute.setText(dfmimute_tx);
        dateTv.setText(dfmonth_tx + "/" + dfday_tx + "  " + dfweek_tx);

    }
}
