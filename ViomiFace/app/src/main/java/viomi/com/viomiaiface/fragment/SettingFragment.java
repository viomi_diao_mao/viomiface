package viomi.com.viomiaiface.fragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.SettingActivity;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.service.FloatService;
import viomi.com.viomiaiface.utils.ApkUtil;

/**
 * Created by Mocc on 2018/8/24
 */
public class SettingFragment extends BaseFragment {


    private ImageView btn_1;
    private ImageView btn_2;
    private ImageView btn_3;

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_setting, null);
        btn_1 = view.findViewById(R.id.btn_1);
        btn_2 = view.findViewById(R.id.btn_2);
        btn_3 = view.findViewById(R.id.btn_3);
        return view;
    }

    @Override
    protected void initListener() {

        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingActivity.class);
                startActivity(intent);
            }
        });

        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String packageName = "com.qiyi.video.pad";
                if (ApkUtil.checkApkExist(BaseApplication.getAppContext(), packageName)) {
                    ApkUtil.startOtherApp(getActivity(), packageName, false);
                }

                getActivity().startService(new Intent(getActivity(), FloatService.class));
            }
        });


        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String packageName = "com.tencent.qqmusicpad";
                if (ApkUtil.checkApkExist(BaseApplication.getAppContext(), packageName)) {
                    ApkUtil.startOtherApp(getActivity(), packageName, false);
                }

                getActivity().startService(new Intent(getActivity(), FloatService.class));
            }
        });

    }

    @Override
    protected void init() {

    }
}
