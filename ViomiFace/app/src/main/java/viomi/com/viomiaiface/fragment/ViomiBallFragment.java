package viomi.com.viomiaiface.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.DesktopActivity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.subdevices.DeviceGalleryActivity;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.viomiaiface.widget.tagcloudlib.DataBean;
import viomi.com.viomiaiface.widget.tagcloudlib.MyAdapter;
import viomi.com.viomiaiface.widget.tagcloudlib.TagCloudView;

public class ViomiBallFragment extends BaseFragment {

    private TagCloudView cloudview;
    private List<DataBean> dataBeanList;

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_viomi_ball, null);
        cloudview = view.findViewById(R.id.cloudview);
        setViewData();
        return view;
    }

    private void setViewData() {
        MyAdapter adapter = new MyAdapter(dataBeanList);
        cloudview.setAdapter(adapter);
    }

    @Override
    protected void initListener() {

        cloudview.setOnTagClickListener(new TagCloudView.OnTagClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View view, int position) {
                Intent intent = new Intent(getActivity(), DeviceGalleryActivity.class);
                intent.putExtra("DEVICE_GALLERY_LIST", ((DesktopActivity) getActivity()).allBinddevices);
                intent.putExtra("DEVICE_GALLERY_TYPE", position);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void init() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBeanList = new ArrayList<>();
        dataBeanList.add(new DataBean(R.drawable.iv_tag_2, R.drawable.icon_device_scene_fridge, "冰箱"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_1, R.drawable.icon_device_scene_center_water_purifier, "净水器"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_6, R.drawable.icon_device_scene_heat_kettle, "即热饮水吧"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_3, R.drawable.icon_device_scene_range_hood, "油烟机"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_5, R.drawable.icon_device_scene_washing_machine, "洗衣机"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_4, R.drawable.icon_device_scene_dish_washing, "洗碗机"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_2, R.drawable.icon_device_scene_fan, "风扇"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_1, R.drawable.icon_device_scene_sweep_robot, "扫地机器人"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_6, R.drawable.icon_device_scene_steamed_oven, "蒸烤箱"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_3, R.drawable.icon_device_scene_water_heater, "热水器"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_2, R.drawable.icon_device_scene_health_kettle, "养生壶"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_4, R.drawable.icon_device_scene_center_water_softener, "中央软水机"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_1, R.drawable.icon_device_scene_aromatherapy_machine, "香薰机"));
        dataBeanList.add(new DataBean(R.drawable.iv_tag_5, R.drawable.icon_device_scene_air_purifier, "空气净化器"));
    }

    @Override
    public void onResume() {
        super.onResume();
        cloudview.setAutoScrollMode(TagCloudView.MODE_DECELERATE);
    }

    @Override
    public void onPause() {
        super.onPause();
        cloudview.setAutoScrollMode(TagCloudView.MODE_DISABLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cloudview.removeAllViews();
        mHandler.removeCallbacksAndMessages(null);
    }
}
