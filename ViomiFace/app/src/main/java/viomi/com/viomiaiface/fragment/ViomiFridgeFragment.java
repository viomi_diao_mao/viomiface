package viomi.com.viomiaiface.fragment;

import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.DesktopActivity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.FrigeUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class ViomiFridgeFragment extends BaseFragment {

    private TextView device_name;
    private TextView isOnline;
    private TextView temp1_lc;
    private TextView temp2_bw;
    private TextView temp3_ld;
    private LinearLayout bwLayout;
    private View bwDivide;

    private String[] mParams = new String[]{"RCSetTemp", "CCSetTemp", "FCSetTemp"};
    private String[] dataResult = new String[]{"--", "--", "--"};

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_viomi_fridge, null);

        device_name = view.findViewById(R.id.device_name);
        isOnline = view.findViewById(R.id.isOnline);
        temp1_lc = view.findViewById(R.id.temp1_LC);
        temp2_bw = view.findViewById(R.id.temp2_BW);
        temp3_ld = view.findViewById(R.id.temp3_LD);
        bwLayout = view.findViewById(R.id.bwLayout);
        bwDivide = view.findViewById(R.id.bwDivide);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                getProps();
            }
            break;

            case HandlerMsgWhat.MSG01: {
                String result = msg.obj.toString();
                parseJson1(result);
            }
            break;

            case HandlerMsgWhat.MSG02: {
                refleshViewData();
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 3 * 1000);
            }
            break;
        }
    }

    private void getProps() {
        if (MiotManager.getPeopleManager().getPeople() == null || mDevice == null) return;
        LogUtils.e(TAG, "model:" + mDevice.getDeviceModel());
        Operation.getInstance().sendFridgeCommand0(mDevice.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, "onSuccess:" + result);
                Message message = mHandler.obtainMessage();
                message.what = HandlerMsgWhat.MSG01;
                message.obj = result;
                mHandler.sendMessage(message);
            }

            @Override
            public void onFail(String result) {
                LogUtils.e(TAG, "onFail:" + result);
            }
        }, mParams);
    }

    private void parseJson1(String result) {
        JSONObject rawjson = JsonUitls.getJSONObject(result);
        JSONArray resultArray = JsonUitls.getJSONArray(rawjson, "result");

        for (int i = 0; i < resultArray.length(); i++) {
            try {
                dataResult[i] = resultArray.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (i == 2) break;
        }

        int anInt = 0;
        try {
            anInt = Integer.parseInt(dataResult[2]);
            if (anInt == -32 && mDevice != null && (Miconfig.VIOMI_FRIDGE_X3.equals(mDevice.getDeviceModel()) || Miconfig.VIOMI_FRIDGE_X5.equals(mDevice.getDeviceModel()))) {
                anInt = -24;
                dataResult[2] = anInt + "";
            }
        } catch (NumberFormatException e) {
        }

        setView();
    }

    private void setView() {
        if (mDevice != null) device_name.setText(mDevice.getName());

        if (mDevice!=null&&FrigeUtil.containBianwen(mDevice.getDeviceModel())) {
            bwLayout.setVisibility(View.VISIBLE);
            bwDivide.setVisibility(View.VISIBLE);
        } else {
            bwLayout.setVisibility(View.GONE);
            bwDivide.setVisibility(View.GONE);
        }

        if (mDevice!=null&&mDevice.getDeviceModel().equals(Miconfig.VIOMI_FRIDGE_U2)) {
            temp1_lc.setText(dataResult[1]);
            temp3_ld.setText(dataResult[2]);
            return;
        }
        temp1_lc.setText(dataResult[0]);
        temp2_bw.setText(dataResult[1]);
        temp3_ld.setText(dataResult[2]);
    }

    @Override
    public void onResume() {
        super.onResume();
        refleshViewData();
        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 3 * 1000);
    }

    private void refleshViewData() {
        List<AbstractDevice> mOnlinedevices = ((DesktopActivity) getActivity()).mOnlinedevices;
        boolean isonline = false;

        for (int i = 0; i < mOnlinedevices.size(); i++) {
            String deviceId = mDevice.getDeviceId();
            String deviceId1 = mOnlinedevices.get(i).getDeviceId();
            if (mDevice != null && deviceId.equals(deviceId1)) {
                isonline = mOnlinedevices.get(i).isOnline();
                break;
            }
        }

        if (isonline) {
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG0);
            isOnline.setVisibility(View.GONE);
        } else {
            isOnline.setVisibility(View.VISIBLE);
            dataResult = new String[]{"--", "--", "--"};
        }
        setView();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }
}
