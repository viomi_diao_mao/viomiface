package viomi.com.viomiaiface.fragment;

import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.DesktopActivity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

public class ViomiHoodFragment extends BaseFragment {

    String[] mParams = {"power_state", "wind_state", "link_state", "light_state"};

    String[] usedResult = new String[]{"--", "--"};
    int[] dataResult = new int[4];

    private TextView device_name;
    private TextView isOnline;
    private TextView tv_runtime;
    private TextView tv_runcount;
    private TextView tv_switch;
    private ImageView iv_mode;
    private TextView tv_mode;
    private ImageView iv_connect_left;
    private TextView tv_connect_left;
    private ImageView iv_connect_right;
    private TextView tv_connect_right;
    private ImageView iv_connect_light;
    private TextView tv_connect_light;

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_viomi_hood, null);

        device_name = view.findViewById(R.id.device_name);
        isOnline = view.findViewById(R.id.isOnline);

        tv_runtime = view.findViewById(R.id.tv_runtime);
        tv_runcount = view.findViewById(R.id.tv_runcount);
        tv_switch = view.findViewById(R.id.tv_switch);

        iv_mode = view.findViewById(R.id.iv_mode);
        tv_mode = view.findViewById(R.id.tv_mode);
        iv_connect_left = view.findViewById(R.id.iv_connect_left);
        tv_connect_left = view.findViewById(R.id.tv_connect_left);
        iv_connect_right = view.findViewById(R.id.iv_connect_right);
        tv_connect_right = view.findViewById(R.id.tv_connect_right);
        iv_connect_light = view.findViewById(R.id.iv_connect_light);
        tv_connect_light = view.findViewById(R.id.tv_connect_light);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    @Override
    public void onResume() {
        super.onResume();
        refleshViewData();
        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG03, 3 * 1000);
    }

    private void refleshViewData() {
        List<AbstractDevice> mOnlinedevices = ((DesktopActivity) getActivity()).mOnlinedevices;
        boolean isonline = false;

        for (int i = 0; i < mOnlinedevices.size(); i++) {
            String deviceId = mDevice.getDeviceId();
            String deviceId1 = mOnlinedevices.get(i).getDeviceId();
            if (mDevice != null && deviceId.equals(deviceId1)) {
                isonline = mOnlinedevices.get(i).isOnline();
                break;
            }
        }

        if (isonline) {
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG0);
            isOnline.setVisibility(View.GONE);
        } else {
            isOnline.setVisibility(View.VISIBLE);
            usedResult = new String[]{"--", "--"};
            dataResult = new int[4];
        }
        setView1();
        setView2();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void handleMessage(Message msg) {

        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                getUsedData();
                getProps();
            }
            break;

            case HandlerMsgWhat.MSG01: {
                String result = msg.obj.toString();
                parseJson1(result);
            }
            break;

            case HandlerMsgWhat.MSG02: {
                String result = msg.obj.toString();
                parseJson2(result);
            }
            break;

            case HandlerMsgWhat.MSG03: {
                refleshViewData();
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG03, 3 * 1000);
            }
            break;
        }
    }

    private void getUsedData() {
        if (MiotManager.getPeopleManager().getPeople() == null || mDevice == null) return;
        Operation.getInstance().sendCookerHoodGetUserData(mDevice.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, "onSuccess:" + result);
                Message message = mHandler.obtainMessage();
                message.what = HandlerMsgWhat.MSG01;
                message.obj = result;
                mHandler.sendMessage(message);
            }

            @Override
            public void onFail(String result) {
                LogUtils.e(TAG, "onFail:" + result);
            }
        });

    }

    private void parseJson1(String result) {
        JSONObject rawjson = JsonUitls.getJSONObject(result);
        JSONArray resultArray = JsonUitls.getJSONArray(rawjson, "result");
        if (resultArray.length() > 0) {
            JSONObject item = JsonUitls.getJSONObject(resultArray, 0);
            String value = JsonUitls.getString(item, "value");
            String[] values = value.replace("[", "").replace("]", "").split(",");

            if (values != null && values.length > 2) {
                usedResult[0] = values[0];
                usedResult[1] = values[1];
            }
            setView1();
        }
    }

    private void setView1() {
        tv_runtime.setText(usedResult[1]);
        tv_runcount.setText(usedResult[0]);
    }


    private void getProps() {
        if (MiotManager.getPeopleManager().getPeople() == null || mDevice == null) return;
        Operation.getInstance().sendCookerHoodCommand0(mDevice.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, "onSuccess:" + result);
                Message message = mHandler.obtainMessage();
                message.what = HandlerMsgWhat.MSG02;
                message.obj = result;
                mHandler.sendMessage(message);
            }

            @Override
            public void onFail(String result) {
                LogUtils.e(TAG, "onFail:" + result);
            }
        }, mParams);

    }

    private void parseJson2(String result) {
        JSONObject rawjson = JsonUitls.getJSONObject(result);
        JSONArray resultArray = JsonUitls.getJSONArray(rawjson, "result");

        for (int i = 0; i < resultArray.length(); i++) {
            try {
                dataResult[i] = resultArray.getInt(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (i == 4) break;
        }
        setView2();
    }

    private void setView2() {
        if (mDevice != null)
            device_name.setText(mDevice.getName());

        switch (dataResult[0]) {
            case 0:
                tv_switch.setText(getString(R.string.hood_off));
                tv_switch.setBackground(getResources().getDrawable(R.drawable.hood_swith_off));
                break;
            default:
                tv_switch.setText(getString(R.string.hood_on));
                tv_switch.setBackground(getResources().getDrawable(R.drawable.hood_switch_on));
                break;
        }

        switch (dataResult[1]) {
            case 0:
                tv_mode.setText(getString(R.string.hood_mode));
                iv_mode.setImageResource(R.drawable.hood_wink_low_off);
                break;
            case 1:
                tv_mode.setText(getString(R.string.hood_mode_low));
                iv_mode.setImageResource(R.drawable.hood_wink_low_on);
                break;
            case 4:
                tv_mode.setText(getString(R.string.hood_mode_bao));
                iv_mode.setImageResource(R.drawable.hood_wink_bao_on);
                break;
            case 16:
                tv_mode.setText(getString(R.string.hood_mode_high));
                iv_mode.setImageResource(R.drawable.hood_wink_high_on);
                break;
            default:
                break;
        }


        switch (dataResult[2]) {
            case 2:
                iv_connect_left.setImageResource(R.drawable.hood_link_left_on);
                iv_connect_right.setImageResource(R.drawable.hood_link_right_on);
                break;
            default:
                iv_connect_left.setImageResource(R.drawable.hood_link_left_off);
                iv_connect_right.setImageResource(R.drawable.hood_link_right_off);
                break;
        }

        switch (dataResult[3]) {
            case 0:
                iv_connect_light.setImageResource(R.drawable.hood_light_off);
                break;
            case 1:
                iv_connect_light.setImageResource(R.drawable.hood_light_on);
                break;
        }
    }
}
