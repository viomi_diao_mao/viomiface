package viomi.com.viomiaiface.fragment;


import android.view.LayoutInflater;
import android.view.View;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseFragment;

public class ViomiLogoFragment extends BaseFragment {

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_viomi_logo, null);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

}
