package viomi.com.viomiaiface.fragment;

import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.DesktopActivity;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;


public class ViomiPurifierFragment extends BaseFragment {

    private String[] mParams = new String[]{"tds_out"};
    private int[] dataResult = new int[]{-1};
    private TextView device_name;
    private TextView isOnline;
    private TextView purity;
    private TextView tv_tds;
    private TextView suggestion;

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_viomi_purifier, null);

        device_name = view.findViewById(R.id.device_name);
        isOnline = view.findViewById(R.id.isOnline);
        purity = view.findViewById(R.id.purity);
        tv_tds = view.findViewById(R.id.tv_tds);
        suggestion = view.findViewById(R.id.suggestion);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                getProps();
            }
            break;

            case HandlerMsgWhat.MSG01: {
                String result = msg.obj.toString();
                parseJson1(result);
            }
            break;

            case HandlerMsgWhat.MSG02: {
                refleshViewData();
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 3 * 1000);
            }
            break;
        }
    }

    private void getProps() {
        if (MiotManager.getPeopleManager().getPeople() == null || mDevice == null) return;
        Operation.getInstance().sendPurifierCommand0(mDevice.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, "onSuccess:" + result);
                Message message = mHandler.obtainMessage();
                message.what = HandlerMsgWhat.MSG01;
                message.obj = result;
                mHandler.sendMessage(message);
            }

            @Override
            public void onFail(String result) {
                LogUtils.e(TAG, "onFail:" + result);

            }
        }, mParams);
    }


    private void parseJson1(String result) {

        JSONObject rawjson = JsonUitls.getJSONObject(result);
        JSONArray resultArray = JsonUitls.getJSONArray(rawjson, "result");

        for (int i = 0; i < resultArray.length(); i++) {
            try {
                dataResult[i] = resultArray.getInt(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (i == 1) break;
        }

        setView();
    }

    private void setView() {
        if (mDevice != null) device_name.setText(mDevice.getName());

        int value = dataResult[0];

        if (value < 0) {
            purity.setText("--");
            tv_tds.setText("--");
            suggestion.setText("--");
            return;
        }

        //TDS=0~50               纯度高             可放心饮用
        //TDS=50~100           纯度较高           可放心饮用
        //TDS=100~300         水质一般           可饮用
        //TDS=300~600         会结水垢          不建议直饮
        //TDS=600~1000        口感较差          不建议直饮
        //TDS=1000以上         不适合饮用       不适合饮用
        if (value < 50) {
            suggestion.setText(getString(R.string.water_drink0));
            purity.setText(getString(R.string.water_pure0));
        } else if (value < 100) {
            suggestion.setText(getString(R.string.water_drink1));
            purity.setText(getString(R.string.water_pure1));
        } else if (value < 300) {
            suggestion.setText(getString(R.string.water_drink2));
            purity.setText(getString(R.string.water_pure2));
        } else if (value < 600) {
            suggestion.setText(getString(R.string.water_drink3));
            purity.setText(getString(R.string.water_pure3));
        } else if (value < 1000) {
            suggestion.setText(getString(R.string.water_drink4));
            purity.setText(getString(R.string.water_pure4));
        } else {
            suggestion.setText(getString(R.string.water_drink5));
            purity.setText(getString(R.string.water_pure5));
        }
        tv_tds.setText(value+"");
    }


    @Override
    public void onResume() {
        super.onResume();
        refleshViewData();
        mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 3 * 1000);
    }

    private void refleshViewData() {
        List<AbstractDevice> mOnlinedevices = ((DesktopActivity) getActivity()).mOnlinedevices;
        boolean isonline = false;

        for (int i = 0; i < mOnlinedevices.size(); i++) {
            String deviceId = mDevice.getDeviceId();
            String deviceId1 = mOnlinedevices.get(i).getDeviceId();
            if (mDevice != null && deviceId.equals(deviceId1)) {
                isonline = mOnlinedevices.get(i).isOnline();
                break;
            }
        }

        if (isonline) {
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG0);
            isOnline.setVisibility(View.GONE);
        } else {
            isOnline.setVisibility(View.VISIBLE);
            dataResult = new int[]{-1};
        }
        setView();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }
}
