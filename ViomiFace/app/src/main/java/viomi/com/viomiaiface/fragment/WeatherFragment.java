package viomi.com.viomiaiface.fragment;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import viomi.com.gdloc.MyLocationUitl;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseFragment;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.config.MURL;
import viomi.com.viomiaiface.model.WeatherBean;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.ToastUtil;
import viomi.com.viomiaiface.utils.WeatherIconUtil;


public class WeatherFragment extends BaseFragment {

    private ImageView weather_icon;
    private TextView weather_temp;
    private TextView weather_txt1;
    private TextView weather_txt2;
    private WeatherBean bean;
    private String city;

    public String airCondition = "";
    public String humidity = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startLoc2Weather();
    }

    @Override
    protected View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.fragment_weather, null);
        weather_icon = view.findViewById(R.id.weather_icon);
        weather_temp = view.findViewById(R.id.weather_temp);
        weather_txt1 = view.findViewById(R.id.weather_txt1);
        weather_txt2 = view.findViewById(R.id.weather_txt2);
        return view;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void init() {

    }

    private void startLoc2Weather() {
        MyLocationUitl.getInstance().startloc(getActivity().getApplicationContext(), mLocationListener);
    }

    private void stopLoc() {
        MyLocationUitl.getInstance().stopLoc();
    }

    private AMapLocationListener mLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation aMapLocation) {
            LogUtils.e(TAG, aMapLocation.toString());

            city = aMapLocation.getCity();
            if (city != null&&city.length()>2) {
                if (city.endsWith("市") | city.endsWith("区") | city.endsWith("县")) {
                    city = city.substring(0, city.length() - 1);
                }
            }
            LogUtils.e(TAG, "city=" + city);
            getWeather(city);
        }
    };

    private void getWeather(String cityName) {
        Map<String, String> map = new HashMap<>();
        map.put("key", "185c9117f8138");
        map.put("city", cityName);
        HttpApi.getRequestHandler(MURL.MOB_WEATHER, map, mHandler, HandlerMsgWhat.MSG0, HandlerMsgWhat.MSG01);
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {
                showResult(msg, true);
                //获取成功后四小时更新
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 4 * 60 * 60 * 1000);
                break;
            }
            case HandlerMsgWhat.MSG01: {
                showResult(msg, false);
                //获取失败后5秒后重复请求
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 5 * 1000);
                ToastUtil.showLow("获取天气失败，请检查网络");
                break;
            }

            case HandlerMsgWhat.MSG02: {
                startLoc2Weather();
                break;
            }

            default:
                break;
        }
    }

    private void showResult(Message msg, boolean isSuccess) {
        if (isSuccess) {
            String result = (String) msg.obj;
           LogUtils.e(TAG, "showResult  " + result);
            JSONObject jsonObject = JsonUitls.getJSONObject(result);

            String retCode = JsonUitls.getString(jsonObject, "retCode");
            if ("200".equals(retCode)) {
                JSONArray resultArray = JsonUitls.getJSONArray(jsonObject, "result");
                if (resultArray.length() > 0) {
                    JSONObject item = JsonUitls.getJSONObject(resultArray, 0);
                    String date = JsonUitls.getString(item, "date");
                    airCondition = JsonUitls.getString(item, "airCondition");
                    humidity = JsonUitls.getString(item, "humidity");
                    String distrct = JsonUitls.getString(item, "distrct");
                    String pm25 = JsonUitls.getString(item, "pollutionIndex");
                    String temperature = JsonUitls.getString(item, "temperature");
                    temperature = temperature.replace("℃", "");
                    JSONArray future = JsonUitls.getJSONArray(item, "future");
                    if (future.length() > 0) {
                        JSONObject future_item = JsonUitls.getJSONObject(future, 0);
                        String weather_dayTime = JsonUitls.getString(future_item, "dayTime");
                        String weather_night = JsonUitls.getString(future_item, "night");
                        String weather = "".equals(weather_dayTime) ? weather_night : weather_dayTime;
                        bean = new WeatherBean(date, distrct, pm25, temperature, weather, "");
                        setView();
                    }
                }
            } else {
                mHandler.removeMessages(HandlerMsgWhat.MSG0);
                //获取失败后10秒后重复请求
                mHandler.sendEmptyMessageDelayed(HandlerMsgWhat.MSG02, 20 * 1000);
            }
        }
    }

    private void setView() {
        if (bean != null) {
            weather_temp.setText(bean.getTemperature());
            weather_txt1.setText(bean.getDistrct() + "   " + bean.getWeather_dayTime() + "   PM2.5: " + bean.getPm25());
            weather_txt2.setText(bean.getDate());
            WeatherIconUtil.setWeatherIcon(weather_icon, bean.getWeather_dayTime());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLoc();
    }
}
