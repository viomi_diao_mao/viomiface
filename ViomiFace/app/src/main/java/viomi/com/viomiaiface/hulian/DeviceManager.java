package viomi.com.viomiaiface.hulian;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.aispeech.aiutils.io.SystemPropertiesProxy;
import com.viomi.defined.device.ViomiFace;
import com.viomi.defined.service.FaceDeviceService;
import com.xiaomi.miot.host.manager.MiotDeviceConfig;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.device.DiscoveryType;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CompletedListener;
import com.xiaomi.miot.typedef.listener.OnBindListener;

import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiaiface.activity.AlermActivity;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.reciver.ScreenOffAdminReceiver;
import viomi.com.viomiaiface.utils.ApkUtil;
import viomi.com.viomiaiface.utils.IpUtils;

/**
 * Created by young2 on 2017/1/11.
 */

public class DeviceManager {
    private static final String TAG = "DeviceManager";
    private static DeviceManager INSTANCE;
    private ViomiFace mDevice;
    private AppCallback<String> mBindCallback;

    private FaceDeviceService.ActionHandler mActionHandler;
    private FaceDeviceService.PropertyGetter mPropertyGetter;
    private FaceDeviceService.PropertySetter mPropertySetter;
    private Context mContext = BaseApplication.getAppContext();

    public static DeviceManager getInstance() {
        if (INSTANCE == null) {
            synchronized (DeviceManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DeviceManager();
                }
            }
        }
        return INSTANCE;
    }

    /***
     * 设备是否已绑定
     * @return
     */
    public boolean isDeviceBind() {
        return GlobalParams.getInstance().isDeviceBindFlag();
    }

    /***
     * 设备绑定状态设置
     * @param flag
     */
    public void setDeviceBindFlag(boolean flag) {
        GlobalParams.getInstance().setDeviceBindFlag(flag);
    }

    /***
     *初始化设备服务
     * @param context
     */
    public void initDevice(Context context, final String miId, AppCallback<String> callback) {
        mBindCallback = callback;
        bindDevice(context, miId);
        //注册监听设备状态
        //bindRegister();
    }

    private void bindDevice(Context context, final String miId) {
        try {
            MiotHostManager.getInstance().bind(context, new CompletedListener() {
                @Override
                public void onSucceed() {
                    Log.e(TAG, "bind onSucceed");
                    try {
                        MiotHostManager.getInstance().start();
                        creatDevice(miId);
                    } catch (MiotException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailed(MiotError miotError) {
                    Log.e(TAG, "bind onFailed");
                    mBindCallback.onFail(-2, "bind fail!");
                }
            });
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    /***
     * 解除设备连接
     */
    private void unBindDevice(AppCallback<String> callback) {
        try {
            MiotHostManager.getInstance().reset(new CompletedListener() {
                @Override
                public void onSucceed() {
                    Log.e(TAG, "reset succss !");
                    callback.onSuccess("succss");
                    setDeviceBindFlag(false);
                    Intent intent = new Intent(BroadcastAction.ACTION_DEVICE_UNBIND);
                    LocalBroadcastManager.getInstance(BaseApplication.getAppContext()).sendBroadcast(intent);
                }

                @Override
                public void onFailed(MiotError miotError) {
                    Log.e(TAG, "reset error,msg=" + miotError.getMessage());
                    callback.onFail(-1, miotError.getMessage());
                }
            });
        } catch (MiotException e) {
            e.printStackTrace();
        }
    }

    /***
     * 设备切换绑定账号
     */
    public boolean reBindDevice(String userId, AppCallback<String> callback) {
        mBindCallback = callback;
        try {
            MiotHostManager.getInstance().reset(new CompletedListener() {
                @Override
                public void onSucceed() {
                    Log.e(TAG, "reset succss 111!");
                    try {
                        creatDevice(userId);
                    } catch (MiotException e) {
                        e.printStackTrace();
                        mBindCallback.onFail(-7, "creatDevice error!");
                    }
                }

                @Override
                public void onFailed(MiotError miotError) {
                    Log.e(TAG, "reset error,msg=" + miotError.getMessage());
                    mBindCallback.onFail(-5, "reset device fail!");
                }
            });
        } catch (MiotException e) {
            Log.e(TAG, "stopBindDevice error,msg=" + e.getMessage());
            e.printStackTrace();
            mBindCallback.onFail(-6, "reset device error!");
            return false;
        }
        return true;
    }

    /***
     * 停止服务
     * @param context
     */
    public boolean unBindService(Context context) {

        try {
            MiotHostManager.getInstance().stop();
            MiotHostManager.getInstance().unbind(context);
            return true;
        } catch (MiotException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.e(TAG, "unbind error,msg=" + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    private void bindRegister() {
        try {
            Log.i(TAG, "registerBindListener");
            MiotHostManager.getInstance().registerBindListener(new OnBindListener() {
                @Override
                public void onBind() {
                    Log.i(TAG, "device bind!");
                    setDeviceBindFlag(true);
                    Intent intent = new Intent(BroadcastAction.ACTION_DEVICE_BIND);
                    LocalBroadcastManager.getInstance(BaseApplication.getAppContext()).sendBroadcast(intent);
                }

                @Override
                public void onUnBind() {
                    Log.e(TAG, "device unbind!");
                    setDeviceBindFlag(false);
                    Intent intent = new Intent(BroadcastAction.ACTION_DEVICE_UNBIND);
                    LocalBroadcastManager.getInstance(BaseApplication.getAppContext()).sendBroadcast(intent);
                }
            }, new CompletedListener() {
                @Override
                public void onSucceed() {
                    Log.i(TAG, "device bind success!");
                }

                @Override
                public void onFailed(MiotError miotError) {
                    Log.e(TAG, "device bind fail!msg=" + miotError.getMessage());
                }
            });
        } catch (MiotException e) {
            Log.e(TAG, "registerBindListener,msg=" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void creatDevice(String userid) throws MiotException {
        MiIndentify miIndentify = getMiIdentify();
        Log.d(TAG, "did=" + miIndentify.did + ",mac=" + miIndentify.mac + ",token=" + miIndentify.token);
        MiotDeviceInfo info = new MiotDeviceInfo();
        info.deviceId = miIndentify.did;
        info.macAddress = miIndentify.mac;
        info.miotToken = miIndentify.token;
        info.miotInfo = getMiotInfo(userid);
        Log.d(TAG, "info.miotInfo=" + info.miotInfo);
        initDevice(info, userid);

    }

    /***
     * 获取小米标识
     * @return
     */
    public static MiIndentify getMiIdentify() {
        MiIndentify miIndentify = new MiIndentify();
        miIndentify.mac = DeviceConfig.DefaultMac;
        miIndentify.did = DeviceConfig.DefaultDeviceId;
        miIndentify.token = DeviceConfig.DefaultMiotToken;
        String sn = SystemPropertiesProxy.get(BaseApplication.getAppContext(), "gsm.serial");

        if (sn == null) {
            Log.e("getMiIdentify", "null");
            return miIndentify;
        } else {
            Log.d("getMiIdentify", sn + ",length=" + sn.length());
        }
        String[] list;
        if (sn.length() >= 24 && (!sn.contains("|"))) {
            list = new String[2];
            list[0] = sn.substring(0, 8);
            list[1] = sn.substring(8, 24);
        } else {
            list = sn.split("\\|");
            if (list == null || list.length < 2 || list[1].length() < 16) {
                Log.e("getMiIdentify", "error,sn=" + sn);
                return miIndentify;
            }
        }

        miIndentify.mac = getPhoneMac();
        miIndentify.did = list[0];
        miIndentify.token = list[1].substring(0, 16);
        return miIndentify;
    }

    /***
     * 获取手机mac
     * @return
     */
    public static String getPhoneMac() {
        String mac = "";
        try {
            WifiManager wifi = (WifiManager) BaseApplication.getAppContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifi.getConnectionInfo();
            mac = info.getMacAddress();
        } catch (Exception e) {
            Log.e("getLocalMacAddress", "fail,msg=" + e.getMessage());
        }
        return mac;
    }

    private void initDevice(MiotDeviceInfo info, String userid) throws MiotException {
        Log.i(TAG, "initDevice");
        /**
         * 1. Initialize Configuration
         */
        MiotDeviceConfig config = new MiotDeviceConfig();
        config.addDiscoveryType(DiscoveryType.MIOT);
        config.friendlyName("Viomi");
        config.deviceId(info.deviceId);
        config.macAddress(info.macAddress);
        config.manufacturer("viomi");
        config.modelName(DeviceConfig.MODEL);
        config.miotToken(info.miotToken);
        config.miotInfo(info.miotInfo);

        /**
         * 2. Create Device
         */
        mDevice = new ViomiFace(config);

        /**
         * 3. set Action Handler, setter & getter for property
         */
        if (mActionHandler == null) {
            mActionHandler = new FaceDeviceService.ActionHandler() {

                @Override
                public void onsetScreenStatus(boolean ScreenStatus) {

                }

                @Override
                public void onsetSceneMode(int SceneMode) {
                    Log.v(TAG, "ActionHandler  setSceneMode   " + SceneMode);
                    //0：回家模式；1：离家模式；2：睡眠模式；3：起床模式
                    if (SceneMode == 0) {
                        PowerManager pm = (PowerManager) BaseApplication.getAppContext().getSystemService(Context.POWER_SERVICE);
                        PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                                | PowerManager.ACQUIRE_CAUSES_WAKEUP, "bright");
                        mWakeLock.acquire(60 * 1000);
                    } else if (SceneMode == 1) {
                        screenOff();//息屏
                    }
                }

                @Override
                public void onsetWaterAlarm(boolean WaterAlarm) {
                    Log.v(TAG, "onsetWaterAlarm  " + WaterAlarm);
                    if (WaterAlarm) {
                        Intent intent = new Intent(mContext, AlermActivity.class);
                        intent.putExtra("mode", 0);
                        mContext.startActivity(intent);
                    } else {
                        sendAlermBrocast(0);
                    }

                }

                @Override
                public void onsetSmokeAlarm(boolean SmokeAlarm) {
                    Log.v(TAG, "onsetSmokeAlarm  " + SmokeAlarm);
                    if (SmokeAlarm) {
                        Intent intent = new Intent(mContext, AlermActivity.class);
                        intent.putExtra("mode", 1);
                        mContext.startActivity(intent);
                    } else {
                        sendAlermBrocast(1);
                    }

                }

                @Override
                public void onsetGasLeakAlarm(boolean GasLeakAlarm) {

                    Log.v(TAG, "onsetFireAlarm  " + GasLeakAlarm);
                    if (GasLeakAlarm) {
                        Intent intent = new Intent(mContext, AlermActivity.class);
                        intent.putExtra("mode", 2);
                        mContext.startActivity(intent);
                    } else {
                        sendAlermBrocast(2);
                    }
                }

                @Override
                public void onsetInvadeAlarm(boolean InvadeAlarm) {
                    Log.v(TAG, "onsetInvadeAlarm  " + InvadeAlarm);
                    if (InvadeAlarm) {
                        Intent intent = new Intent(mContext, AlermActivity.class);
                        intent.putExtra("mode", 3);
                        mContext.startActivity(intent);
                    } else {
                        sendAlermBrocast(3);
                    }
                }

                @Override
                public void onsetSeeLaoban(boolean SeeLaoban) {
                    Log.v(TAG, "onsetSeeLaoban  " + SeeLaoban);
                    Intent it = new Intent(BroadcastAction.ACTION_DEVICE_AWE);
                    it.putExtra("intent", "laoban");
                    it.putExtra("value", SeeLaoban);
//                    LocalBroadcastManager.getInstance(BaseApplication.getAppContext()).sendBroadcast(it);
                    BaseApplication.getAppContext().sendBroadcast(it);
                }

                @Override
                public void onsetSeeYanmi(boolean SeeYanmi) {
                    Log.v(TAG, "onsetSeeYanmi  " + SeeYanmi);
                    Intent it = new Intent(BroadcastAction.ACTION_DEVICE_AWE);
                    it.putExtra("intent", "yangmi");
                    it.putExtra("value", SeeYanmi);
//                    LocalBroadcastManager.getInstance(BaseApplication.getAppContext()).sendBroadcast(it);
                    BaseApplication.getAppContext().sendBroadcast(it);
                }

                @Override
                public void onsetLaunchState(boolean LaunchState) {
                    Log.v(TAG, "onsetLaunchState  " + LaunchState);
                    Intent it = new Intent(BroadcastAction.ACTION_DEVICE_AWE);
                    it.putExtra("intent", "launchstate");
                    it.putExtra("value", LaunchState);
//                    LocalBroadcastManager.getInstance(BaseApplication.getAppContext()).sendBroadcast(it);
                    BaseApplication.getAppContext().sendBroadcast(it);
                }
            };
        }
        mDevice.FaceDeviceService().setHandler(mActionHandler, mPropertyGetter, mPropertySetter);
        /**
         * 4. Start
         */
        Log.i(TAG, " mDevice.start");
        mDevice.start(new CompletedListener() {
            @Override
            public void onSucceed() {
                Log.i(TAG, "start onSucceed,userid=" + userid);
                if (userid != null) {
                    setDeviceBindFlag(true);
                }
                if (mBindCallback != null) {
                    mBindCallback.onSuccess(null);
                }
            }

            @Override
            public void onFailed(MiotError miotError) {
                Log.e(TAG, "start onFailed: " + miotError);
                if (mBindCallback != null) {
                    mBindCallback.onFail(-5, "mDevice.start fail!,msg=" + miotError.getMessage());
                }
            }
        });

        //注册监听设备状态
//        bindRegister();

    }

    public void screenOff() {
        DevicePolicyManager policyManager = (DevicePolicyManager) BaseApplication.getAppContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName adminReceiver = new ComponentName(mContext, ScreenOffAdminReceiver.class);
        boolean admin = policyManager.isAdminActive(adminReceiver);
        if (admin) {
            policyManager.lockNow();//息屏
        } else {
            //没有权限
            Log.v(TAG, "没有息屏权限");
        }
    }

    void sendAlermBrocast(int mode) {
        Intent it = new Intent(BroadcastAction.ACTION_SET_ALERM);
        it.putExtra("mode", mode);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
        localBroadcastManager.sendBroadcast(it);
    }

    /***
     * 设置失败事件
     */
    public void sendSetFail(String sendParam, String receiveParam) {

        String method = "event.set_fail";
        String params = "[" + sendParam + ";" + receiveParam + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /***
     * 异常发生事件
     */
    public void sendFaultHappen(int error) {

        String method = "event.fault_happen";
        String params = "[" + error + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /***
     * 异常恢复事件
     */
    public void sendFaultFix(int error) {
        String method = "event.fault_fixed";
        String params = "[" + error + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 0：回家模式；1：离家模式；2：睡眠模式；3：起床模式
     *
     * @param param
     */
    public void sendSceneMode(int param) {
        String method = "event.scene_mode";
        String params = "[" + param + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 0：熄屏；1：点屏
     *
     * @param param
     */
    public void sendScreenStatus(int param) {
        String method = "event.ScreenStatus";
        String params = "[" + param + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 0:水浸警报解除；1：水浸警报状态
     *
     * @param param
     */
    public void sendWaterAlarm(int param) {
        String method = "event.WaterAlarm";
        String params = "[" + param + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 0:入侵警报解除；1：入侵汽警报状态
     *
     * @param param
     */
    public void sendInvadeAlarm(int param) {
        String method = "event.InvadeAlarm";
        String params = "[" + param + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 0:烟雾警报解除；1：烟雾汽警报状态
     *
     * @param param
     */
    public void sendSmokeAlarm(int param) {
        String method = "event.SmokeAlarm";
        String params = "[" + param + "]";
        Log.i(TAG, "method=" + method + ",params=" + params);
        if (mDevice != null) {
            try {
                mDevice.send(method, params);
            } catch (MiotException e) {
                e.printStackTrace();
            }
        }
    }


    /***
     *设定设备信息
     * @return
     */
    private String getMiotInfo(String userId) {
        WifiManager wifiMng = (WifiManager) BaseApplication.getAppContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wi = wifiMng.getConnectionInfo();
        DhcpInfo di = wifiMng.getDhcpInfo();
        JSONObject jo = new JSONObject();

        try {
            jo.put("method", "_internal.info");
            jo.put("partner_id", "");
            JSONObject jop = new JSONObject();
            jop.put("hw_ver", "Android");
            jop.put("fw_ver", ApkUtil.getVersion());
            JSONObject jopa = new JSONObject();
            jopa.put("ssid", wi.getSSID());
            jopa.put("bssid", wi.getBSSID());
            jop.put("ap", jopa);
            JSONObject jopn = new JSONObject();
            jopn.put("localIp", IpUtils.intToIp(di.ipAddress));
            jopn.put("mask", IpUtils.intToIp(di.netmask));
            jopn.put("gw", IpUtils.intToIp(di.gateway));
            jop.put("netif", jopn);
            if (userId != null) {
                jop.put("uid", userId);
            }
            jo.put("params", jop);
            return jo.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


}
