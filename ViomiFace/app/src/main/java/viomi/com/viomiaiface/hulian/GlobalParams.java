package viomi.com.viomiaiface.hulian;

import android.content.Context;
import android.content.SharedPreferences;

import viomi.com.viomiaiface.base.BaseApplication;

/**
 * Created by young2 on 2016/12/15.
 */

public class GlobalParams {


    private static GlobalParams INSTANCE;
    private Context mContext = BaseApplication.getAppContext();

    private SharedPreferences sharedPreferences = mContext.getSharedPreferences(SharedPreferencesStr, Context.MODE_PRIVATE);
    private SharedPreferences.Editor editor = sharedPreferences.edit();
    private static String SharedPreferencesStr = "Params";


    public static String Key_Scene_Str = "Key_Scene_Str"; //保存场景列表
    public static String Value_Scene_Str = "";

    public static String Key_Scene_ChooseName = "Key_Scene_ChooseName"; //保存场景
    public static String Value_Scene_ChooseName = "";


    public static String Key_Device_Bind_Flag = "Key_Device_Bind_Flag"; //保存设备是否绑定
    public static boolean Value_Device_Bind_Flag = false;



    public static GlobalParams getInstance() {
        if (INSTANCE == null) {
            synchronized (GlobalParams.class) {
                if (INSTANCE == null) {
                    INSTANCE = new GlobalParams();
                }
            }
        }
        return INSTANCE;
    }

    /***
     * 设置选中的自定义场景

     * @return
     */
    public boolean setSceneChoose(String name) {
        editor.putString(Key_Scene_ChooseName, name);
        return editor.commit();
    }

    /***
     * 获取选中的自定义场景
     *
     * @return
     */
    public String getSceneChoose() {
        return sharedPreferences.getString(Key_Scene_ChooseName, Value_Scene_ChooseName);
    }

    /***
     * 用户场景获取
     *
     * @return
     */
    public String getSceneStr() {
        return sharedPreferences.getString(Key_Scene_Str, Value_Scene_Str);
    }

    /***
     * 是否设备绑定
     * @return
     */
    public boolean isDeviceBindFlag() {
        return sharedPreferences.getBoolean(Key_Device_Bind_Flag, Value_Device_Bind_Flag);
    }

    /***
     * 设置设备绑定标志
     * @param enable
     * @return
     */
    public boolean setDeviceBindFlag(boolean enable) {
        editor.putBoolean(Key_Device_Bind_Flag, enable);
        return editor.commit();
    }
}