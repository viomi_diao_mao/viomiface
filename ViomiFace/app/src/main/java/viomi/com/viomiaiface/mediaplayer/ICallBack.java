package viomi.com.viomiaiface.mediaplayer;

import android.media.audiofx.Visualizer;

public interface ICallBack {

    enum UIState {DISCONNECT, CONNECTED, LOAD_DATA_FAIL}

    void onPlay();

    void onPause();

    void onResume();

    void onStop();

    void onBuffering();

    void onBufferComplete();

    void onBufferingUpdate(int percent);

    void onUpdateSeekBar(int time);

    void onComplete(boolean isFinish);

    void onPlayError(int errorCode, String errorMsg);

    void onPrepareNext();

    void onPrepare();

    void onSetTotalTime(int time);

    void onSetVisualizer(Visualizer visualizer);

    void onUpdateUIState(UIState uiState);
}
