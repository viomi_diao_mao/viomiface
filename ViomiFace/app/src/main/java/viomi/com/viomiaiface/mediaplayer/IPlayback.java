package viomi.com.viomiaiface.mediaplayer;

/**
 * Created by airhome on 2015/12/8.
 *
 */
public interface IPlayback {

    void play(String url);

    void replay();

    void pause();

    void resume();

    void stop();

    void seekTo(int position);

    MusicService.State getState();

    boolean isPlaying();

    void destroy();

    void setCallBack(ICallBack callBack);

    int getPosition();

    boolean isPrepared();

    void setIsPrepared(boolean flag);

    void prepareNext(String url, String cp);
}
