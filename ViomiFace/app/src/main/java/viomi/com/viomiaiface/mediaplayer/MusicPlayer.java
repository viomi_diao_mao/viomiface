package viomi.com.viomiaiface.mediaplayer;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.text.TextUtils;

import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.NetUtils;


public class MusicPlayer implements IPlayback, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnBufferingUpdateListener {
    private static final String TAG = "MusicPlayer";
    private static final String FIRST_PLAYER = "FirstPlayer";
    private static final String SECOND_PLAYER = "SecondPlayer";
//    private static final int UPDATE_SEEK_BAR = 10;
    private static MusicPlayer mFirstPlayer = null;
    private static MusicPlayer mSecondPlayer = null;
    private Visualizer mFirstVisualizer, mSecondVisualizer;
    private Equalizer mFirstEqualizer, mSecondEqualizer;

    private WifiManager.WifiLock mWifiLock;
    private MediaPlayer mMediaPlayer;
    private MusicService.State mState = MusicService.State.IDLE;
    private Context mContext;
    private ICallBack mCallback;
    private String mCurrentUrl;
    private String mPrepareUrl;
    private boolean isPrePrepared;
    public String mPlayerName;

    private enum bufferState {BUFFERING, BUFFERCOMPLETE}

    private bufferState mBufferState;

    private MusicPlayer(Context context, boolean isPrePrepared, String name) {
        mContext = context;
        this.isPrePrepared = isPrePrepared;
        mPlayerName = name;
        if (mWifiLock != null && !mWifiLock.isHeld()) {
            mWifiLock = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE))
                    .createWifiLock(WifiManager.WIFI_MODE_FULL, "music_lock");
            mWifiLock.acquire();
        }
    }

    public static MusicPlayer getFirstPlayer(Context context) {
        if (mFirstPlayer == null) {
            synchronized (MusicPlayer.class) {
                if (mFirstPlayer == null) {
                    mFirstPlayer = new MusicPlayer(context, true, FIRST_PLAYER);
                }
            }
        }
        return mFirstPlayer;
    }

    public static MusicPlayer getSecondPlayer(Context context) {
        if (mSecondPlayer == null) {
            synchronized (MusicPlayer.class) {
                if (mSecondPlayer == null) {
                    mSecondPlayer = new MusicPlayer(context, false, SECOND_PLAYER);
                }
            }
        }
        return mSecondPlayer;
    }

    @Override
    public void setCallBack(ICallBack callBack) {
        mCallback = callBack;
    }

    @Override
    public boolean isPrepared() {
        return isPrePrepared;
    }

    @Override
    public void setIsPrepared(boolean flag) {
        isPrePrepared = flag;
    }

    @Override
    public void play(String url) {
        LogUtils.e(TAG, "Play :[" + url + "] " );
        if (TextUtils.isEmpty(url)) mCallback.onComplete(false);
        if (mMediaPlayer != null && mState == MusicService.State.PREPARED && url.equals(mPrepareUrl)) {
            stopIfPlaying();
            mCallback.onSetVisualizer(getVisualizer(mPlayerName));
            mCurrentUrl = mPrepareUrl;
            onPrepared(mMediaPlayer);
        } else {
            mCurrentUrl = url;
            stopIfPlaying();
            startPrepare();
        }
    }

    @Override
    public void prepareNext(String url, String cp) {
        mPrepareUrl = url;
        startPrepare();
    }

    public void buffering() {
        mCallback.onBuffering(); // musicPlayer
        mBufferState = bufferState.BUFFERING;
    }

    public void bufferComplete() {
        mCallback.onBufferComplete(); // 对应musicPlayer
        mBufferState = bufferState.BUFFERCOMPLETE;
    }

    private void startPrepare() {
        try {
            createMediaPlayerIfNeed(mContext);
            mMediaPlayer.setDataSource(isPrepared() ? mCurrentUrl : mPrepareUrl);
            mState = MusicService.State.PREPARING;
            if (isPrepared() && mCallback != null) {
                mCallback.onBuffering();
            }
            mMediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            destroy();
            if (!isPrePrepared || mCallback == null) return;
            if (!NetUtils.pingNet()) {
                mCallback.onUpdateUIState(ICallBack.UIState.DISCONNECT);
                return;
            }
            mCallback.onComplete(false);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mState = MusicService.State.PREPARED;
        mCallback.onPrepare();
        if (isPrePrepared) {
            stopIfPlaying();
            mMediaPlayer.start();
            mState = MusicService.State.STARTED;

            LogUtils.e(TAG, "返回时间"+mMediaPlayer.getDuration());

            mCallback.onSetTotalTime(mMediaPlayer.getDuration() / 1000);
            mCallback.onUpdateSeekBar(0);
            mCallback.onPlay();
//            mHandler.sendEmptyMessage(UPDATE_SEEK_BAR);
            mCallback.onPrepareNext();
        } else {
            //切换状态前先检查准备好的播放器是否在播放或暂停状态，若不在则不切换两个播放器状态
            MusicPlayer musicPlayer = mFirstPlayer.isPrepared() ? mFirstPlayer : mSecondPlayer;
            if (musicPlayer.getState() == MusicService.State.STARTED || musicPlayer.getState() == MusicService.State.PAUSED) {
                mFirstPlayer.setIsPrepared(!mFirstPlayer.isPrePrepared);
                mSecondPlayer.setIsPrepared(!mSecondPlayer.isPrePrepared);
            }
        }
    }

    @Override
    public void resume() {
        LogUtils.e(TAG, "resume: " + mState);
        if (mMediaPlayer != null && mState == MusicService.State.PAUSED) {
            mMediaPlayer.start();
            mState = MusicService.State.STARTED;
            mCallback.onResume();
//            mHandler.removeMessages(UPDATE_SEEK_BAR);
//            mHandler.sendEmptyMessageDelayed(UPDATE_SEEK_BAR, 200);
        } else {
            LogUtils.e(TAG, "resume: cannot resume " + mState);
        }
    }

    @Override
    public void replay() {
        if (!TextUtils.isEmpty(mCurrentUrl)) {
            play(mCurrentUrl);
        }
    }

    @Override
    public void pause() {
        LogUtils.e(TAG, "pause: ");
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            mState = MusicService.State.PAUSED;
            mCallback.onPause();
        } else {
            LogUtils.e(TAG, "pause: is not Playing, cannot pause " + mState);
        }
    }

    @Override
    public void stop() {
        LogUtils.e(TAG, "stop: ");
        if (mMediaPlayer != null && (mState == MusicService.State.STARTED || mState == MusicService.State.PAUSED)) {
            mMediaPlayer.stop();
            mState = MusicService.State.STOPPED;
            mCallback.onStop();
        }
    }

    @Override
    public void seekTo(int position) {
        if (mMediaPlayer != null) {
            buffering();
            mMediaPlayer.seekTo(position);
        }
    }

    @Override
    public MusicService.State getState() {
        return mState;
    }

    @Override
    public boolean isPlaying() {
        return (mMediaPlayer != null && mMediaPlayer.isPlaying());
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        LogUtils.e(TAG, "onCompletion");
        mState = MusicService.State.PLAYBACK_COMPLETED;
        mMediaPlayer.reset();
        mCallback.onComplete(true);
    }

    @Override
    public synchronized boolean onError(MediaPlayer mp, int what, int extra) {
        mState = MusicService.State.ERROR;
        LogUtils.e("doublePlayer", mPlayerName + ":onError:(" + what + "," + extra + ")\n" + mCurrentUrl);
        //如果是网络错误，检查网络连接是否正常，若不正常则消失加载图并返回
        if (!NetUtils.pingNet()) {
            destroy();
            mCallback.onUpdateUIState(ICallBack.UIState.DISCONNECT);
            return true;
        }
        //如果是系统错误，释放系统资源播放下一曲
        if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) {
            LogUtils.e(TAG, "MEDIA_ERROR_SERVER_DIED");
            destroy();
            mCallback.onPlayError(what, "MEDIA_ERROR_SERVER_DIED");
            mCallback.onComplete(false);
            return true;
        } else if (what == MediaPlayer.MEDIA_ERROR_UNKNOWN) {
            LogUtils.e(TAG, "MEDIA_ERROR_UNKNOWN");
        }
        String errorMsg = "";
        switch (extra) {
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                errorMsg = "MEDIA_ERROR_TIMED_OUT";
                break;
            case MediaPlayer.MEDIA_ERROR_IO:
                errorMsg = "MEDIA_ERROR_IO";
                break;
            case MediaPlayer.MEDIA_ERROR_MALFORMED:
                errorMsg = "MEDIA_ERROR_MALFORMED";
                break;
            case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
                errorMsg = "MEDIA_ERROR_UNSUPPORTED";
                break;
            case -2147483648:
                errorMsg = "MEDIA_ERROR_SYSTEM";
                break;
        }
        if (!TextUtils.isEmpty(errorMsg)) {
            LogUtils.e(TAG, errorMsg);
            mCallback.onPlayError(what, errorMsg);
            destroy();
            mCallback.onComplete(false);
        }
        return true;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        if (mCallback != null) {
            mCallback.onBufferingUpdate(percent);
        }
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        bufferComplete();
        mCallback.onUpdateSeekBar(mMediaPlayer.getCurrentPosition() / 1000);
//        mHandler.sendEmptyMessage(UPDATE_SEEK_BAR);
    }

    @Override
    public int getPosition() {
        return mMediaPlayer.getCurrentPosition();
    }

    private void stopIfPlaying() {
        if (mFirstPlayer != null && mFirstPlayer.isPlaying()) {
            mFirstPlayer.stop();
            mFirstPlayer.mState = MusicService.State.STOPPED;
        }
        if (mSecondPlayer != null && mSecondPlayer.isPlaying()) {
            mSecondPlayer.stop();
            mSecondPlayer.mState = MusicService.State.STOPPED;
        }
    }

    private void createMediaPlayerIfNeed(Context context) {
        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
            initVisualizer();
            mState = MusicService.State.IDLE;
            mMediaPlayer.setWakeMode(context,
                    PowerManager.PARTIAL_WAKE_LOCK);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.setOnErrorListener(this);
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnBufferingUpdateListener(this);
        } else {
            mMediaPlayer.reset();
        }
        if (isPrePrepared) mCallback.onSetVisualizer(getVisualizer(mPlayerName));
    }

    private void initVisualizer() {
        if (mPlayerName.equals(FIRST_PLAYER)) {
            mFirstVisualizer = new Visualizer(mMediaPlayer.getAudioSessionId());
            mFirstVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[0]);
            mFirstEqualizer = new Equalizer(0, mMediaPlayer.getAudioSessionId());
            if (mSecondEqualizer != null)
                mSecondEqualizer.setEnabled(!mSecondEqualizer.getEnabled());
            mFirstEqualizer.setEnabled(true);
        } else {
            mSecondVisualizer = new Visualizer(mMediaPlayer.getAudioSessionId());
            mSecondVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[0]);
            mSecondEqualizer = new Equalizer(0, mMediaPlayer.getAudioSessionId());
            if (mFirstEqualizer != null)
                mFirstEqualizer.setEnabled(!mFirstEqualizer.getEnabled());
            mSecondEqualizer.setEnabled(true);
        }
    }

    private Visualizer getVisualizer(String name) {
        if (name.equals(FIRST_PLAYER)) {
            return mFirstVisualizer;
        } else {
            return mSecondVisualizer;
        }
    }

    @Override
    public void destroy() {
        if (mMediaPlayer != null) {
            LogUtils.e(TAG, "destroy: ");
            mState = MusicService.State.IDLE;
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
            if (mCallback != null) {
                mCallback.onStop();
            }
        }
    }
}
