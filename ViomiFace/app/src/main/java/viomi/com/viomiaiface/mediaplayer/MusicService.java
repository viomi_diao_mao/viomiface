package viomi.com.viomiaiface.mediaplayer;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.audiofx.Visualizer;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.text.TextUtils;

import java.util.List;

import viomi.com.viomiaiface.activity.MediaPlayActivity;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiaiface.utils.ParsePlayList;


public class MusicService extends Service implements IPlayback, AudioManager.OnAudioFocusChangeListener {
    private static final String TAG = "MusicService";
    private IBinder mBinder = new MusicBinder();
    private IPlayback mCurrentPlayer;
    private HandlerThread mThread;
    private IPlayback mPreparePlayer;
    private Handler mHandler;
    private HandlerThread mPrepareThread;
    private Handler mPrepareHandler;
    private ICallBack mCallBack;
    private AudioManager mAudioManager;
    private boolean isAudioFocusGain;
    private boolean autoPause;
    private List<Track> playList;
    private int currentPostion;
    private ICallBack customCallback;
    private boolean isCookOpen;

    public void setCustomCallback(ICallBack customCallback) {
        this.customCallback = customCallback;
    }

    public void removeCustomCallback() {
        customCallback = null;
    }

    public Track getCurrentTrack() {

        if (playList==null) {
            return null;
        }

        if (currentPostion>playList.size()-1) {
            return null;
        }

        Track track = playList.get(currentPostion);
        return track;
    }

    public enum State {
        IDLE, INITIALIZED, PREPARING, PREPARED, STARTED, PAUSED, STOPPED, PLAYBACK_COMPLETED, END, ERROR
    }

    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS:
                LogUtils.e(TAG, "music loss audio focus!");
                isAudioFocusGain = false;
                if (mCurrentPlayer != null && mCurrentPlayer.isPlaying()) {
                    mCurrentPlayer.stop();
                    mCurrentPlayer.destroy();
                }
                if (mPreparePlayer != null) {
                    mPreparePlayer.stop();
                    mPreparePlayer.destroy();
                }
            case AudioManager.AUDIOFOCUS_GAIN:
                isAudioFocusGain = true;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCurrentPlayer = getPreparedPlayer();
        initBackgroundThread();
        initPrepareThread();
        requestAudioFocus();
        registerReciver();
        setCallBack(playCallBack);
    }

    private void initBackgroundThread() {
        mThread = new HandlerThread("background");
        mThread.start();
        mHandler = new Handler(mThread.getLooper());
    }

    private void initPrepareThread() {
        mPrepareThread = new HandlerThread("prepareThread");
        mPrepareThread.start();
        mPrepareHandler = new Handler(mPrepareThread.getLooper());
    }

    private boolean requestAudioFocus() {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }
        int result = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            isAudioFocusGain = true;
            return true;
        }
        return false;
    }

    public void setCallBack(ICallBack callBack) {
        mCallBack = callBack;
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        LogUtils.e(TAG, "onStartCommand");
        if (intent != null) {
            String music_directive = intent.getStringExtra("music_directive");
            String playtype = intent.getStringExtra("playtype");
            playtype = playtype == null ? "" : playtype;
            switch (playtype) {
                case "MUSIC_ID":
                    playList = ParsePlayList.parseMusic(music_directive);
                    break;
                case "TOY_NEWS_ID":
                    playList = ParsePlayList.parseToyNews(music_directive);
                    break;
                case "LT_NEWS_ID":
                    playList = ParsePlayList.parseLTNews(music_directive);
                    break;
                case "TOY_STORY_ID":
                    playList = ParsePlayList.parseToyStory(music_directive);
                    break;
                case "TOY_FUN_ID":
                    playList = ParsePlayList.parseContent(music_directive);
                    break;
                case "CHINESE_STUDY_ID":
                    playList = ParsePlayList.parseContent(music_directive);
                    break;

                case "INSIDE_MUSIC_ID":
                    playList = ParsePlayList.parseInsideMusicContent(music_directive);
                    break;
                case "FOLK_ART_ID":
                    playList = ParsePlayList.parseFolkArtContent(music_directive);
                    break;
                case "FUN_ID":
                    playList = ParsePlayList.parseFunContent(music_directive);
                    break;
                case "CHILD_SONG_ID":
                    playList = ParsePlayList.parseChildSongContent(music_directive);
                    break;
                case "STORY_ID":
                    playList = ParsePlayList.parseStoryContent(music_directive);
                    break;


                default:
                    playList = ParsePlayList.parseContent(music_directive);
                    break;
            }
            LogUtils.e(TAG, "playList.size=" + playList.size());
            if (playList.size() > 0) {
                currentPostion = 0;
                String url = playList.get(currentPostion).getLinkUrl();
                if (url != null && !"".equals(url)) {
                    play(url);
                    Intent intent1 = new Intent(this, MediaPlayActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent1);
                }
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.e(TAG, "MusicService onDestroy: ");
        destroy();
        if (mThread != null && mThread.isAlive()) {
            mThread.quit();
            mHandler.removeCallbacksAndMessages(null);
        }
        unRegisterReciver();
    }

    private void checkBackgroundThread() {
        if (!mThread.isAlive()) {
            try {
                mThread.start();
                mHandler = new Handler(mThread.getLooper());
            } catch (IllegalThreadStateException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkPrepareThread() {
        if (!mPrepareThread.isAlive()) {
            try {
                mPrepareThread.start();
                mPrepareHandler = new Handler(mPrepareThread.getLooper());
            } catch (IllegalThreadStateException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void play(final String url) {
        LogUtils.e(TAG, "play: " + url);
        mHandler.removeCallbacksAndMessages(null);
        mCurrentPlayer.stop();
        checkBackgroundThread();
        mHandler.post(() -> {
            if (TextUtils.isEmpty(url)) {
                mCallBack.onComplete(false);
                return;
            }
            if (!isAudioFocusGain) {
                requestAudioFocus();
            }
            initPlayer();
            mCurrentPlayer.play(url);
        });
    }

    @Override
    public void replay() {

    }

    @Override
    public void resume() {
        checkBackgroundThread();
        mHandler.post(() -> mCurrentPlayer.resume());
    }

    @Override
    public void pause() {
        checkBackgroundThread();
        mHandler.post(() -> mCurrentPlayer.pause());
    }

    @Override
    public void stop() {
        LogUtils.e(TAG, "stop: ");
        mHandler.post(() -> mCurrentPlayer.stop());
    }

    public void playNext() {
        if (playList != null && playList.size() > 0) {
            currentPostion = (++currentPostion) % playList.size();
            play(playList.get(currentPostion).getLinkUrl());
        }
    }

    public void playPrev() {
        if (playList != null && playList.size() > 0) {
            currentPostion = (--currentPostion) % playList.size();
            if (currentPostion < 0) {
                currentPostion = playList.size() + currentPostion;
            }
            play(playList.get(currentPostion).getLinkUrl());
        }
    }


    @Override
    public void seekTo(final int position) {
        checkBackgroundThread();
        mHandler.post(() -> {
            if (!mCurrentPlayer.isPlaying()) {
                mCurrentPlayer.replay();
            }
            mCurrentPlayer.seekTo(position);
        });
    }

    @Override
    public State getState() {
        return mCurrentPlayer.getState();
    }

    @Override
    public boolean isPlaying() {
        return mCurrentPlayer != null && mCurrentPlayer.isPlaying();
    }

    @Override
    public int getPosition() {
        if (getState() == State.STARTED || getState() == State.PAUSED) {
            return mCurrentPlayer.getPosition();
        }
        return 0;
    }

    @Override
    public void destroy() {
        LogUtils.e(TAG, "MusicService destroy: ");
        if (mCurrentPlayer != null)
            mCurrentPlayer.destroy();
        if (mPreparePlayer != null)
            mPreparePlayer.destroy();
    }

    @Override
    public boolean isPrepared() {
        return false;
    }

    @Override
    public void setIsPrepared(boolean flag) {

    }

    @Override
    public void prepareNext(final String url, final String cp) {
        LogUtils.e(TAG, "prepareNext: " + url);
        mPrepareHandler.removeCallbacksAndMessages(null);
        checkPrepareThread();
        mPrepareHandler.post(() -> {
            mPreparePlayer = getUnPreparedPlayer();
            if (mPreparePlayer == null) {
                return;
            }
            if (!TextUtils.isEmpty(url)) {
                mPreparePlayer.prepareNext(url, cp);
            }
        });
    }

    private void initPlayer() {
        LogUtils.e(TAG, "initPlayer: ");
        mCurrentPlayer = getPreparedPlayer();
        mCurrentPlayer.setCallBack(mCallBack);
    }

    private MusicPlayer getPreparedPlayer() {
        if (MusicPlayer.getFirstPlayer(this).isPrepared()) {
            return MusicPlayer.getFirstPlayer(this);
        } else if (MusicPlayer.getSecondPlayer(this).isPrepared()) {
            return MusicPlayer.getSecondPlayer(this);
        }
        return MusicPlayer.getFirstPlayer(this);
    }

    private MusicPlayer getUnPreparedPlayer() {
        //避免快速切换时获取正在播放的播放器
        MusicPlayer firstPlayer = MusicPlayer.getFirstPlayer(this);
        MusicPlayer secondPlayer = MusicPlayer.getSecondPlayer(this);
        if (firstPlayer.getState() != State.STARTED && !firstPlayer.isPrepared()) {
            return firstPlayer;
        } else if (secondPlayer.getState() != State.STARTED && !secondPlayer.isPrepared()) {
            return secondPlayer;
        }
        return null;
    }

    private ICallBack playCallBack = new ICallBack() {
        @Override
        public void onPlay() {
            if (customCallback != null) customCallback.onPlay();
            LogUtils.e(TAG, "onPlay");
            Intent intent = new Intent(BroadcastAction.SHOWMUSICINMAIN);
            intent.putExtra("isShowMusic", true);
            intent.putExtra("musicName", playList.get(currentPostion).getTitle());
            sendBroadcast(intent);
        }

        @Override
        public void onPause() {
            if (customCallback != null) customCallback.onPause();
            LogUtils.e(TAG, "onPause");
            Intent intent = new Intent(BroadcastAction.SHOWMUSICINMAIN);
            sendBroadcast(intent);
        }

        @Override
        public void onResume() {
            if (customCallback != null) customCallback.onResume();
            LogUtils.e(TAG, "onResume");
            Intent intent = new Intent(BroadcastAction.SHOWMUSICINMAIN);
            intent.putExtra("isShowMusic", true);
            intent.putExtra("musicName", playList.get(currentPostion).getTitle());
            sendBroadcast(intent);
        }

        @Override
        public void onStop() {
            if (customCallback != null) customCallback.onStop();
            LogUtils.e(TAG, "onStop");
            Intent intent = new Intent(BroadcastAction.SHOWMUSICINMAIN);
            sendBroadcast(intent);
        }

        @Override
        public void onBuffering() {
            if (customCallback != null) customCallback.onBuffering();
            LogUtils.e(TAG, "onBuffering");
        }

        @Override
        public void onBufferComplete() {
            if (customCallback != null) customCallback.onBufferComplete();
            LogUtils.e(TAG, "onBufferComplete");
        }

        @Override
        public void onBufferingUpdate(int percent) {
            if (customCallback != null) customCallback.onBufferingUpdate(percent);
            LogUtils.e(TAG, "onBufferingUpdate:percent=" + percent);
        }

        @Override
        public void onUpdateSeekBar(int time) {
            if (customCallback != null) customCallback.onUpdateSeekBar(time);
            LogUtils.e(TAG, "onUpdateSeekBar:time=" + time);
        }

        @Override
        public void onComplete(boolean isFinish) {
            if (customCallback != null) customCallback.onComplete(isFinish);
            LogUtils.e(TAG, "onComplete:isFinish=" + isFinish);
            playNext();
        }

        @Override
        public void onPlayError(int errorCode, String errorMsg) {

        }

        @Override
        public void onPrepareNext() {

        }

        @Override
        public void onPrepare() {

        }

        @Override
        public void onSetTotalTime(int time) {

        }

        @Override
        public void onSetVisualizer(Visualizer visualizer) {

        }

        @Override
        public void onUpdateUIState(UIState uiState) {
            LogUtils.e(TAG, "onUpdateUIState:UIState=" + uiState.name());
        }
    };


    private BroadcastReceiver speechStartReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "speechStartReciver-isPlaying()= : " + isPlaying());
            if (isPlaying()) {
                pause();
                autoPause = true;
            }
        }
    };

    private BroadcastReceiver speechStopReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "speechStopReciver- ");
            if (autoPause) {
                autoPause = false;
                if (!isPlaying()) {
                    resume();
                }
            }
        }
    };

    private BroadcastReceiver pauceReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "pauceReciver- ");
            autoPause = false;
        }
    };

    private BroadcastReceiver playReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "playReciver- ");
            if (playList != null && playList.size() > 0) {
                autoPause = true;
                Intent intent1 = new Intent(MusicService.this, MediaPlayActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
            }
        }
    };

    private BroadcastReceiver nextReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "nextReciver- ");
            playNext();
            if (playList != null && playList.size() > 0) {
                Intent intent1 = new Intent(MusicService.this, MediaPlayActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);

            }
        }
    };

    private BroadcastReceiver prevReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "prevReciver- ");
            playPrev();
            if (playList != null && playList.size() > 0) {
                Intent intent1 = new Intent(MusicService.this, MediaPlayActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);

            }
        }
    };


    private BroadcastReceiver nextReciverQiuck = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "nextReciverQiuck- ");
//            if (!isCookOpen) {
//                playNext();
//                if (playList != null && playList.size() > 0) {
//                    Intent intent1 = new Intent(MusicService.this, MediaPlayHJActivity.class);
//                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent1);
//
//                }
//            }
        }
    };

    private BroadcastReceiver prevReciverQuick = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "prevReciverQuick- ");
//            if (!isCookOpen) {
//                playPrev();
//                if (playList != null && playList.size() > 0) {
//                    Intent intent1 = new Intent(MusicService.this, MediaPlayHJActivity.class);
//                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent1);
//                }
//            }
        }
    };

    private BroadcastReceiver cookStatusReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "cookStatusReciver- ");
            isCookOpen = intent.getBooleanExtra("isCookOpen", false);
            if (isCookOpen) {
                autoPause = false;
                pause();
            }
        }
    };


    private void registerReciver() {
        IntentFilter filter1 = new IntentFilter(viomi.com.viomiaiface.config.BroadcastAction.SPEECHSTART);
        registerReceiver(speechStartReciver, filter1);

        IntentFilter filter2 = new IntentFilter(BroadcastAction.SPEECHSTOP);
        registerReceiver(speechStopReciver, filter2);

        IntentFilter filter3 = new IntentFilter(BroadcastAction.PLAYCONTROL_PAUSE);
        registerReceiver(pauceReciver, filter3);

        IntentFilter filter4 = new IntentFilter(BroadcastAction.PLAYCONTROL_PLAY);
        registerReceiver(playReciver, filter4);

        IntentFilter filter5 = new IntentFilter(BroadcastAction.PLAYCONTROL_NEXT);
        registerReceiver(nextReciver, filter5);

        IntentFilter filter6 = new IntentFilter(BroadcastAction.PLAYCONTROL_PREV);
        registerReceiver(prevReciver, filter6);

        IntentFilter filter7 = new IntentFilter(BroadcastAction.QIUCKORDER_NEXT);
        registerReceiver(nextReciverQiuck, filter7);

        IntentFilter filter8 = new IntentFilter(BroadcastAction.QIUCKORDER_PRE);
        registerReceiver(prevReciverQuick, filter8);

        IntentFilter filter9 = new IntentFilter(BroadcastAction.PLAYCONTROL_COOK_STATUS);
        registerReceiver(cookStatusReciver, filter9);
    }

    private void unRegisterReciver() {
        unregisterReceiver(speechStartReciver);
        unregisterReceiver(speechStopReciver);
        unregisterReceiver(pauceReciver);
        unregisterReceiver(playReciver);
        unregisterReceiver(nextReciver);
        unregisterReceiver(prevReciver);
        unregisterReceiver(nextReciverQiuck);
        unregisterReceiver(prevReciverQuick);
        unregisterReceiver(cookStatusReciver);
    }
}
