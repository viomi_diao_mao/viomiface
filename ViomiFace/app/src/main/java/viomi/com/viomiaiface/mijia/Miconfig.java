package viomi.com.viomiaiface.mijia;

import viomi.com.viomiaiface.utils.SharedPreferencesUtil;

/**
 * Created by Mocc on 2018/1/18
 */

public class Miconfig {

    public static  long getMI_OAUTH_APP_ID(){
        return "ios".equals(SharedPreferencesUtil.getScanType()) ? OAUTH_IOS_APP_ID : OAUTH_ANDROID_APP_ID;
    }

    public static  String getMI_OAUTH_APP_KEY(){
        return "ios".equals(SharedPreferencesUtil.getScanType()) ? OAUTH_IOS_APP_KEY : OAUTH_ANDROID_APP_KEY;
    }

    public static final long OAUTH_ANDROID_APP_ID  = 2882303761517454408L;
    public static final String OAUTH_ANDROID_APP_KEY = "5891745422408";

    public static final long OAUTH_IOS_APP_ID = 2882303761517484785L;// 云米 ios
    public static final String OAUTH_IOS_APP_KEY = "5701748476785";

    /*
     * 设备型号
     * */
    public static final String YUNMI_WATERPURI_V1 = "yunmi.waterpuri.v1";// V1 乐享版 人工智能系列
    public static final String YUNMI_WATERPURI_V2 = "yunmi.waterpuri.v2";// V1 尊享版 人工智能系列
    public static final String YUNMI_WATERPURI_S1 = "yunmi.waterpuri.s1";// S1 优享版 智能 3 合 1 系列
    public static final String YUNMI_WATERPURI_S2 = "yunmi.waterpuri.s2";// S1 标准版 智能 3 合 1 系列
    public static final String YUNMI_WATERPURI_C1 = "yunmi.waterpuri.c1";// C1 厨上版 新鲜水系列
    public static final String YUNMI_WATERPURI_C2 = "yunmi.waterpuri.c2";// C1 厨下版 新鲜水系列
    public static final String YUNMI_WATERPURI_X3 = "yunmi.waterpuri.x3";// 即热直饮净水器 X3 (100G)
    public static final String YUNMI_WATERPURI_X5 = "yunmi.waterpuri.x5";// 即热直饮净水器 X5 (400G)
    public static final String YUNMI_WATERPURI_n1 = "yunmi.waterpuri.n1";// 云米互联网净水器 Mee Pro
    public static final String YUNMI_WATERPURI_n2 = "yunmi.waterpuri.n2";// 云米互联网净水器 Mee
    public static final String YUNMI_WATERPURI_n3 = "yunmi.waterpuri.n3";// 云米互联网净水器 Mee（签名版）

    public static final String YUNMI_KETTLE_R1 = "yunmi.kettle.r1";// 云米智能即热饮水吧（MINI）
    public static final String YUNMI_KETTLE_R2 = "yunmi.kettle.r2";// 云米智能即热饮水吧（MINI）
    public static final String YUNMI_KETTLE_R3 = "yunmi.kettle.r3";// 云米智能即热饮水吧（MINI）

    public static final String YUNMI_PL_MACHINE_MG2 = "yunmi.plmachine.mg2";// 云米 MG2 型即热管线机

    public static final String VIOMI_FRIDGE_U1 = "viomi.fridge.u1";// 冰箱ilive
    public static final String VIOMI_FRIDGE_U2 = "viomi.fridge.u2";// 冰箱iLive 对开门版
    public static final String VIOMI_FRIDGE_V1 = "viomi.fridge.v1";// 冰箱 iLive语音版
    public static final String VIOMI_FRIDGE_V2 = "viomi.fridge.v2";// 冰箱iLive四门语音版
    public static final String VIOMI_FRIDGE_V3 = "viomi.fridge.v3";// 462大屏金属门冰箱
    public static final String VIOMI_FRIDGE_V4 = "viomi.fridge.v4";// 455大屏玻璃门冰箱
    public static final String VIOMI_FRIDGE_X1 = "viomi.fridge.x1";// 云米三门大屏冰箱X1
    public static final String VIOMI_FRIDGE_X2 = "viomi.fridge.x2";// 云米四门大屏冰箱428
    public static final String VIOMI_FRIDGE_X3 = "viomi.fridge.x3";// 云米大屏冰箱462
    public static final String VIOMI_FRIDGE_X4 = "viomi.fridge.x4";// 云米对开门大屏冰箱456
    public static final String VIOMI_FRIDGE_X5 = "viomi.fridge.x5";// 云米大屏冰箱521

    public static final String VIOMI_HOOD_A4 = "viomi.hood.a4";// 智能油烟机 Hurri
    public static final String VIOMI_HOOD_A5 = "viomi.hood.a5";// 智能油烟机 Free
    public static final String VIOMI_HOOD_A6 = "viomi.hood.a6";// 智能油烟机 Hurri 语音版
    public static final String VIOMI_HOOD_A7 = "viomi.hood.a7";// 智能油烟机 Free 语音版
    public static final String VIOMI_HOOD_C1 = "viomi.hood.c1";// 云米智能油烟机 Cross
    public static final String VIOMI_HOOD_C6 = "viomi.hood.c6";// 经济型侧吸机
    public static final String VIOMI_HOOD_H1 = "viomi.hood.h1";// 云米互联网智能油烟机 Hurri尊享
    public static final String VIOMI_HOOD_H2 = "viomi.hood.h2";// 云米互联网智能油烟机Hurri标准
    public static final String VIOMI_HOOD_T8 = "viomi.hood.t8";// 标准型 T 型机
    public static final String VIOMI_HOOD_X1 = "viomi.hood.x1";// 云米油烟机X1
    public static final String VIOMI_HOOD_X2 = "viomi.hood.x2";// 云米油烟机X2
    public static final String VIOMI_HOOD_X3 = "viomi.hood.x3";// 云米油烟机X3

    public static final String VIOMI_OVEN_V1 = "viomi.oven.v1";// 云米互联网蒸烤一体机
    public static final String VIOMI_OVEN_V2 = "viomi.oven.v2";// 云米互联网蒸烤一体机（嵌入式）

    public static final String VIOMI_DISHWASHER_V01 = "viomi.dishwasher.v01";// 云米互联网洗碗机（8套嵌入式）

    public static final String VIOMI_WASHER_U1 = "viomi.washer.u1";// 云米互联网洗衣机（9KG 语音版）
    public static final String VIOMI_WASHER_U2 = "viomi.washer.u2";// 云米互联网洗烘一体机

    public static final String VIOMI_VACUUM_V1 = "viomi.vacuum.v1";// 云米互联网扫地机器人

    public static final String VIOMI_FAN_V1 = "viomi.fan.v1";// 云米互联网直流变频风扇


    /*
     * 设备协议url
     * */
    public static final String YUNMI_WATERPURI_V1_URL = "device/ddd_WaterPuri_V1.xml";
    public static final String YUNMI_WATERPURI_V2_URL = "device/ddd_WaterPuri_V2.xml";
    public static final String YUNMI_WATERPURI_S1_URL = "device/ddd_WaterPuri_S1.xml";
    public static final String YUNMI_WATERPURI_S2_URL = "device/ddd_WaterPuri_S2.xml";
    public static final String YUNMI_WATERPURI_C1_URL = "device/ddd_WaterPuri_C1.xml";
    public static final String YUNMI_WATERPURI_C2_URL = "device/ddd_WaterPuri_C2.xml";
    public static final String YUNMI_WATERPURI_X3_URL = "device/ddd_WaterPuri_X3.xml";
    public static final String YUNMI_WATERPURI_X5_URL = "device/ddd_WaterPuri_X5.xml";
    public static final String YUNMI_WATERPURIFIER_URL = "device/ddd_WaterPurifier.xml";

    public static final String VIOMI_FRIDGE_U1_URL = "device/ddd_Fridge_U1.xml";
    public static final String VIOMI_FRIDGE_U2_URL = "device/ddd_Fridge_U2.xml";
    public static final String VIOMI_FRIDGE_W1_URL = "device/ddd_Fridge_W1.xml";
    public static final String VIOMI_FRIDGE_W2_URL = "device/ddd_Fridge_W2.xml";
    public static final String VIOMI_FRIDGE_V1_URL = "device/ddd_Fridge_V1.xml";
    public static final String VIOMI_FRIDGE_V2_URL = "device/ddd_Fridge_V2.xml";
    public static final String VIOMI_FRIDGE_V3_URL = "device/ddd_Fridge_V3.xml";
    public static final String VIOMI_FRIDGE_V4_URL = "device/ddd_Fridge_V4.xml";
    public static final String VIOMI_FRIDGE_X1_URL = "device/ddd_Fridge_X1.xml";
    public static final String VIOMI_FRIDGE_X2_URL = "device/ddd_Fridge_X2.xml";
    public static final String VIOMI_FRIDGE_X3_URL = "device/ddd_Fridge_X3.xml";
    public static final String VIOMI_FRIDGE_X4_URL = "device/ddd_Fridge_X4.xml";
    public static final String VIOMI_FRIDGE_X5_URL = "device/ddd_Fridge_X5.xml";

    public static final String VIOMI_HOOD_T8_URL = "device/Viomi_Hood_T8.xml";
    public static final String VIOMI_HOOD_A6_URL = "device/Viomi_Hood_A6.xml";
    public static final String VIOMI_HOOD_A5_URL = "device/Viomi_Hood_A5.xml";
    public static final String VIOMI_HOOD_A4_URL = "device/Viomi_Hood_A4.xml";
    public static final String VIOMI_HOOD_C6_URL = "device/Viomi_Hood_C6.xml";
    public static final String VIOMI_HOOD_A7_URL = "device/Viomi_Hood_A7.xml";
    public static final String VIOMI_HOOD_C1_URL = "device/Viomi_Hood_C1.xml";
    public static final String VIOMI_HOOD_H1_URL = "device/Viomi_Hood_H1.xml";
    public static final String VIOMI_HOOD_H2_URL = "device/Viomi_Hood_H2.xml";
    public static final String VIOMI_HOOD_X1_URL = "device/Viomi_Hood_X1.xml";
    public static final String VIOMI_HOOD_X2_URL = "device/Viomi_Hood_X2.xml";

    public static final String VIOMI_OVEN_V1_URL = "device/Viomi_Oven_V1.xml";
    public static final String VIOMI_OVEN_V2_URL = "device/Viomi_Oven_V2.xml";
    public static final String VIOMI_DISHWASHER_V01_URL = "device/Viomi_Dishwasher_V01.xml";
    public static final String VIOMI_WASHER_U1_URL = "device/Viomi_Washer_U1.xml";
    public static final String YUNMI_KETTLE_R1_URL = "device/Viomi_Kettle_R1.xml";
    public static final String YUNMI_PL_MACHINE_MG2_URL = "device/Viomi_Plmachine_Mg2.xml";
    public static final String YUNMI_VACUUM_V1_URL= "device/Viomi.vacuum.v1.xml";
    public static final String YUNMI_FAN_V1_URL= "device/Viomi.fan.v1.xml";


    public static final String ACTION_BIND_SERVICE_SUCCEED = "viomi.com.viomiaiface.action.BIND_SERVICE_SUCCEED";
    public static final String ACTION_BIND_SERVICE_FAILED = "viomi.com.viomiaiface.action.BIND_SERVICE_FAILED";

    public static final String ACTION_DISCOVERY_DEVICE_SUCCEED = "viomi.com.viomiaiface.action.DISCOVERY_DEVICE_SUCCEED";
    public static final String ACTION_DISCOVERY_DEVICE_FAILED = "viomi.com.viomiaiface.action.DISCOVERY_DEVICE_FAILED";
}
