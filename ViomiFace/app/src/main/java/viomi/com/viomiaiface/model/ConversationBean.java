package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/1/26
 */

public class ConversationBean {

    private ConversationType type;
    private Object param;

    public ConversationBean() {
    }

    public ConversationBean(ConversationType type, Object param) {
        this.type = type;
        this.param = param;
    }

    public ConversationType getType() {
        return type;
    }

    public void setType(ConversationType type) {
        this.type = type;
    }

    public Object getParam() {
        return param;
    }

    public void setParam(Object param) {
        this.param = param;
    }
}

