package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/3/3
 */

public class FoodStepBean {

    private String title;
    private String content;
    private String pic;

    public FoodStepBean() {
    }

    public FoodStepBean(String title, String content, String pic) {
        this.title = title;
        this.content = content;
        this.pic = pic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
