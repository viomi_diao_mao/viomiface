package viomi.com.viomiaiface.model;

import org.json.JSONObject;

import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/1/29
 */

public class ReMindBean {

    private String intentName;
    private String intent;
    private String time;
    private String date;
    private String event;
    private String relativeTime;
    private String timeLocation;

    public ReMindBean() {

    }

    public ReMindBean(String intentName, String intent, String time, String date, String event, String relativeTime, String timeLocation) {
        this.intentName = intentName;
        this.intent = intent;
        this.time = time;
        this.date = date;
        this.event = event;
        this.relativeTime = relativeTime;
        this.timeLocation = timeLocation;
    }

    public String getIntentName() {
        return intentName;
    }

    public void setIntentName(String intentName) {
        this.intentName = intentName;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getRelativeTime() {
        return relativeTime;
    }

    public void setRelativeTime(String relativeTime) {
        this.relativeTime = relativeTime;
    }

    public String getTimeLocation() {
        return timeLocation;
    }

    public void setTimeLocation(String timeLocation) {
        this.timeLocation = timeLocation;
    }

    @Override
    public String toString() {
        return "ReMindBean{" +
                "intentName='" + intentName + '\'' +
                ", intent='" + intent + '\'' +
                ", time='" + time + '\'' +
                ", date='" + date + '\'' +
                ", event='" + event + '\'' +
                ", relativeTime='" + relativeTime + '\'' +
                ", timeLocation='" + timeLocation + '\'' +
                '}';
    }

    public static ReMindBean reMindBeansResolver(JSONObject data) {

        String intentName = JsonUitls.getString(data, "intentName");
        String intent= JsonUitls.getString(data, "intent");
        String time= JsonUitls.getString(data, "time");
        String date= JsonUitls.getString(data, "date");
        String event= JsonUitls.getString(data, "event");
        String relativeTime= JsonUitls.getString(data, "relativeTime");
        String timeLocation= JsonUitls.getString(data, "timeLocation");

        return new ReMindBean(intentName, intent, time, date, event, relativeTime, timeLocation);
    }
}