package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/6/26
 */
public class WLinkDeviceBean {

    private String gwID;
    private String devID;
    private String devName;
    private String type;
    private String mode;

    public WLinkDeviceBean() {
    }

    public WLinkDeviceBean(String gwID, String devID, String devName, String type, String mode) {
        this.gwID = gwID;
        this.devID = devID;
        this.devName = devName;
        this.type = type;
        this.mode = mode;
    }

    public String getGwID() {
        return gwID;
    }

    public void setGwID(String gwID) {
        this.gwID = gwID;
    }

    public String getDevID() {
        return devID;
    }

    public void setDevID(String devID) {
        this.devID = devID;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
