package viomi.com.viomiaiface.model;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/1/29
 */

public class WeatherBean {

    private String date;
    private String distrct;
    private String pm25;
    private String temperature;
    //"weather" : "多云转阵雨" ,
    private String weather_dayTime;
    //"wind" : "无持续风向,3级"
    private String wind;

    public WeatherBean() {

    }

    public WeatherBean(String date, String distrct, String pm25, String temperature, String weather_dayTime) {
        this.date = date;
        this.distrct = distrct;
        this.pm25 = pm25;
        this.temperature = temperature;
        this.weather_dayTime = weather_dayTime;
    }

    public WeatherBean(String date, String distrct, String pm25, String temperature, String weather_dayTime, String wind) {
        this.date = date;
        this.distrct = distrct;
        this.pm25 = pm25;
        this.temperature = temperature;
        this.weather_dayTime = weather_dayTime;
        this.wind = wind;

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistrct() {
        return distrct;
    }

    public void setDistrct(String distrct) {
        this.distrct = distrct;
    }

    public String getPm25() {
        return pm25;
    }

    public void setPm25(String pm25) {
        this.pm25 = pm25;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getWeather_dayTime() {
        return weather_dayTime;
    }

    public void setWeather_dayTime(String weather_dayTime) {
        this.weather_dayTime = weather_dayTime;
    }

    public static WeatherBean weatherBeanResolver(String sth) {
        String unit = "℃";
        String date = "";
        String distrct = "";
        String pm25 = "";
        String temperature = "";
        String weather_dayTime = "";
        String wind = "";
        JSONObject jsonObject = JsonUitls.getJSONObject(sth);
//        JSONObject dm = jsonObject.optJSONObject("dm");
//        if (dm != null) {
//            JSONObject widget = dm.optJSONObject("widget");
//            if (widget != null) {
                JSONObject extra = jsonObject.optJSONObject("extra");
                if (extra != null) {
                    distrct = extra.optString("city");
                    weather_dayTime = extra.optString("weather");
                    temperature = extra.optString("temperature");
                    if (temperature.contains(unit)) {
                        temperature = temperature.replaceAll(unit, "");
                    }
                    wind = extra.optString("wind");
                    date = extra.optString("date");

                }
//            }
//        }

//        JSONObject nlu = jsonObject.optJSONObject("nlu");
//        if (nlu != null) {
//            JSONObject semantics = nlu.optJSONObject("semantics");
//            if (semantics != null) {
//                JSONObject request = semantics.optJSONObject("request");
//                if (request != null) {
//                    JSONArray slots = request.optJSONArray("slots");
//                    if (slots != null) {
//                        for (int i = 0; i < slots.length(); i++) {
//                            JSONObject objectTag = slots.optJSONObject(i);
//                            if (objectTag != null) {
//                                String name = objectTag.optString("name");
//                                if ("日期".equals(name)) {
//                                    String dataValue = objectTag.optString("rawvalue");
//                                    String value = objectTag.optString("value");
//                                    date = value;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
        return new WeatherBean(date, distrct, pm25, temperature, weather_dayTime, wind);
    }
}