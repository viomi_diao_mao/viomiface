package viomi.com.viomiaiface.model;

/**
 * Created by Mocc on 2018/5/21
 */
public class WeatherBean2 {

    private String wind;
    private String weather;
    private String temperature;
    private String city;


    public WeatherBean2() {
    }

    public WeatherBean2(String wind, String weather, String temperature, String city) {
        this.wind = wind;
        this.weather = weather;
        this.temperature = temperature;
        this.city = city;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
