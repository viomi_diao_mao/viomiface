package viomi.com.viomiaiface.reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import viomi.com.viomiaiface.activity.AlarmRemindActivity;



/**
 * Created by Mocc on 2018/3/2
 */

public class AlarmReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent newIntent = new Intent(context, AlarmRemindActivity.class);
        newIntent.putExtra("alarmtime", intent.getLongExtra("alarmtime", 0));
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(newIntent);
    }
}
