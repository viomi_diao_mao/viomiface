package viomi.com.viomiaiface.reciver;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receiver class which shows notifications when the Device Administrator status
 * of the application changes.
 */
public class ScreenOffAdminReceiver extends DeviceAdminReceiver {

    private String TAG = ScreenOffAdminReceiver.class.getSimpleName();

    @Override
    public void onEnabled(Context context, Intent intent) {
        Log.v(TAG, "设备管理器使能");
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        Log.v(TAG, "设备管理器使能");
    }

}