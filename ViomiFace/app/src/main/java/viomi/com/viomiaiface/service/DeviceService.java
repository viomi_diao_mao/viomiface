package viomi.com.viomiaiface.service;

import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.miot.api.MiotManager;
import com.xiaomi.miot.host.manager.MiotHostManager;
import com.xiaomi.miot.typedef.error.MiotError;
import com.xiaomi.miot.typedef.exception.MiotException;
import com.xiaomi.miot.typedef.listener.CompletedListener;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.base.BaseService;
import viomi.com.viomiaiface.hulian.AppCallback;
import viomi.com.viomiaiface.hulian.DeviceManager;
import viomi.com.viomiaiface.hulian.ViomiUser;

/**
 * Created by hailang on 2018/3/3 0003.
 */

public class DeviceService extends BaseService {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new DeviceService.ThisBinder();
    }

    public class ThisBinder extends Binder {
        public DeviceService getService() {
            return DeviceService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        while (true){
            if (MiotManager.getPeopleManager()!=null) {
                break;
            }
        }
        initDevice();
        return START_STICKY;
    }

    /***
     * 设备初始化
     */
    private void initDevice() {
        if (MiotManager.getPeopleManager().getPeople() == null)
            return;
        ViomiUser viomiUser = getViomiUser(BaseApplication.getAppContext());
        String miId = null;
        if (viomiUser != null) {
            miId = viomiUser.getMiId();
        }
        Log.d(TAG, "miId=" + miId);
        DeviceManager.getInstance().initDevice(BaseApplication.getAppContext(), miId, new AppCallback<String>() {
            @Override
            public void onSuccess(String data) {
                Log.e(TAG, "initDevice success");
            }

            @Override
            public void onFail(int errorCode, String msg) {
                Log.e(TAG, "initDevice fail,err=" + errorCode + ",msg=" + msg);
            }
        });
    }

    public static ViomiUser getViomiUser(Context context) {
        ViomiUser user = new ViomiUser();
        user.setToken(MiotManager.getPeopleManager().getPeople().getAccessToken());
        user.setMiId(MiotManager.getPeopleManager().getPeople().getUserId());
        user.setHeadImg(MiotManager.getPeopleManager().getPeople().getIcon());
        user.setAccount("");
        user.setUserCode("");
        user.setMobile("");
        user.setNickname(MiotManager.getPeopleManager().getPeople().getUserName());
        return user;
    }


    /***
     * 解除设备连接
     */

    public boolean unBindDevice(AppCallback<String> callback) {
        try {
            Log.i(TAG, "reset device!");
            MiotHostManager.getInstance().reset(new CompletedListener() {
                @Override
                public void onSucceed() {
                    Log.e(TAG, "reset succss !");
                }

                @Override
                public void onFailed(MiotError miotError) {
                    Log.e(TAG, "reset error,msg=" + miotError.getMessage());
                    callback.onFail(-1, miotError.getMessage());
                }
            });
        } catch (MiotException e) {
            Log.e(TAG, "stopBindDevice error,msg=" + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onDestroy() {
        try {
            unBindDevice(null);
            DeviceManager.getInstance().unBindService(BaseApplication.getAppContext());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
