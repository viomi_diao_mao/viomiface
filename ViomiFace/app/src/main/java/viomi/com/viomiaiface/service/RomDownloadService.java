package viomi.com.viomiaiface.service;

import android.app.DownloadManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;

import java.io.File;

import viomi.com.viomiaiface.utils.LogUtils;

public class RomDownloadService extends Service {

    private long mTaskId;
    private DownloadManager downloadManager;
    private boolean isRunning;
    private static final String TAG = "RomDownloadService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Toast.makeText(this,"onCreate",Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //  Toast.makeText(this,"onStartCommand",Toast.LENGTH_SHORT).show();
        if (!isRunning) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String downlink = bundle.getString("downlink");

                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "update.zip");
                if (file.exists()) {
                    file.delete();
                }
                downloadROM(downlink);
            }
            isRunning = true;
        }

        return START_NOT_STICKY;
    }


    private void downloadROM(String versionUrl) {
        //创建下载任务
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(versionUrl));
        request.setAllowedOverRoaming(false);//漫游网络是否可以下载

        //设置文件类型，可以在下载结束后自动打开该文件
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(versionUrl));
        request.setMimeType(mimeString);

        //在通知栏中显示，默认就是显示的
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.setVisibleInDownloadsUi(true);

        //sdcard的目录下的download文件夹，必须设置
        request.setDestinationInExternalPublicDir("", "update.zip");
        //request.setDestinationInExternalFilesDir(),也可以自己制定下载路径

        //将下载请求加入下载队列
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        //加入下载队列后会给该任务返回一个long型的id，
        //通过该id可以取消任务，重启任务等等，看上面源码中框起来的方法
        mTaskId = downloadManager.enqueue(request);

        //注册广播接收者，监听下载状态
        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkDownloadStatus();//检查下载状态
        }
    };


    //检查下载状态
    private void checkDownloadStatus() {
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(mTaskId);//筛选下载任务，传入任务ID，可变参数
        Cursor c = downloadManager.query(query);
        if (c.moveToFirst()) {
            int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
            switch (status) {
                case DownloadManager.STATUS_PAUSED:
                    LogUtils.e(TAG,"下载暂停");
                    break;
                case DownloadManager.STATUS_PENDING:
                    LogUtils.e(TAG,"下载延迟");
                    break;
                case DownloadManager.STATUS_RUNNING:
                    LogUtils.e(TAG,"正在下载");
                    break;
                case DownloadManager.STATUS_SUCCESSFUL:
                    LogUtils.e(TAG,"下载完成");
                    //下载完成
                    installRom();
                    break;
                case DownloadManager.STATUS_FAILED:
                    LogUtils.e(TAG,"下载失败");
                    break;
            }
        }
    }

    //下载到本地后执行安装
    protected void installRom() {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "update.zip");
        if (!file.exists()) return;

        LogUtils.e("installRom","sendBroadcast");
        Intent recoveryIntent = new Intent("android.intent.action.DO_RECOVERY_SYSTEM");
        sendBroadcast(recoveryIntent);

        unregisterReceiver(receiver);
        stopSelf();
    }
}
