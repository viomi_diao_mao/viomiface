package viomi.com.viomiaiface.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.com.viomi.ailib.api.VioAI;
import cn.com.viomi.ailib.api.VioAIListener;
import cn.com.viomi.ailib.api.VioAIUpdateListener;
import cn.com.viomi.ailib.api.VioAIUpdater;
import cn.com.viomi.ailib.api.aispeechConfig;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.activity.ConversationActivity;
import viomi.com.viomiaiface.base.BaseService;
import viomi.com.viomiaiface.callback.ConversationUpdateCallback;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;
import viomi.com.viomiaiface.skill.handle.HandleSkill;
import viomi.com.viomiaiface.utils.LogUtils;

public class ViomiVoiceService extends BaseService {

    private ConversationUpdateCallback conversationUpdateCallback;
    private List<ConversationBean> conversationList;
    private boolean isEndSentence = true;

    private static final String QUICK_WORD_1 = "next.step";
    private static final String QUICK_WORD_2 = "last.step";
    private static final String QUICK_WORD_N1 = "number.one";
    private static final String QUICK_WORD_N2 = "number.two";
    private static final String QUICK_WORD_N3 = "number.three";
    private static final String QUICK_WORD_N4 = "number.four";
    private static final String QUICK_WORD_N5 = "number.five";
    private static final String QUICK_WORD_N6 = "number.six";
    private static final String QUICK_WORD_N7 = "number.seven";
    private static final String QUICK_WORD_N8 = "number.eight";
    private static final String QUICK_WORD_N9 = "number.night";
    private static final String QUICK_WORD_N10 = "number.ten";
    private static final String QUICK_WORD_NL = "number.last";

    private WindowManager windowManager;
    private View updateView;
    private WindowManager.LayoutParams updateViewParams;
    private TextView progressTitle;
    private SeekBar updateProgressBar;
    private boolean isAddUpdateView;

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.e(TAG, "onBind");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        LogUtils.e(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {
        public ViomiVoiceService getService() {
            return ViomiVoiceService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sendBroadcast(new Intent(BroadcastAction.VIOCESERVICEINIT));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        HandleSkill.getInstance().setmHandler(mHandler);
        init();
        registerBroadcastReceive();
        return START_STICKY;
    }

    private void init() {
        VioAI.setPlatform("aispeech");
        String keys = "db0b04a824b8db0b04a824b85b2a0bd8";
        String productId = "278572254";

        VioAI.addConfig(aispeechConfig.K_API_KEY, keys);
        VioAI.addConfig(aispeechConfig.K_PRODUCT_ID, productId);
        VioAI.addConfig(aispeechConfig.K_USER_ID, "15948012178");
        VioAI.addConfig(aispeechConfig.K_ALIAS_KEY, "prod");//产品分支
        VioAI.addConfig(aispeechConfig.K_MIC_TYPE,"2");

//        VioAI.setDebugMode(2);
//        VioAI.addConfig(aispeechConfig.K_WAKEUP_DEBUG,"true");
//        VioAI.addConfig(aispeechConfig.K_VAD_DEBUG,"true");
//        VioAI.addConfig(aispeechConfig.K_ASR_DEBUG,"true");
//        VioAI.addConfig(aispeechConfig.K_TTS_DEBUG,"true");
//        VioAI.setLogLevel(2);

        VioAI.init(this, vioAIListener);

        //对话列表
        conversationList = new ArrayList<>();

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        updateViewParams = new WindowManager.LayoutParams();
        updateViewParams.width = -1;
        updateViewParams.height = -1;
        updateViewParams.format = PixelFormat.RGBA_8888;
        updateViewParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        updateViewParams.type = WindowManager.LayoutParams.TYPE_PHONE;

        updateView = LayoutInflater.from(this).inflate(R.layout.voice_service_update_layout, null);
        progressTitle = updateView.findViewById(R.id.progressTitle);
        updateProgressBar = updateView.findViewById(R.id.updateProgressBar);
        RelativeLayout cover = updateView.findViewById(R.id.cover);
        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void unInit() {
        VioAI.getInstance().release();
    }

    private VioAIListener vioAIListener = new VioAIListener() {
        @Override
        public void isAuthSuccess() {
            LogUtils.e(TAG, "VioAIListener-isAuthSuccess");
//            checkLibUpdate();
        }

        @Override
        public void onAIInitComplete(boolean b) {
            LogUtils.e(TAG, "VioAIListener-onAIInitComplete:" + b);
            sendBroadcast(new Intent(BroadcastAction.VIOCESERVICEINITCOMPLETE));
            //if add 20180831
            if(b) {
               //
            }
            else{
                //    init();
                checkLibUpdate();
            }
        }

        @Override
        public void onAuthSuccess() {
            LogUtils.e(TAG, "VioAIListener-onAuthSuccess");
        }

        @Override
        public void onAIInitError(int i, String s) {
            LogUtils.e(TAG, "VioAIListener-onAIInitError:" + s);
            sendBroadcast(new Intent(BroadcastAction.VIOCESERVICEINITCOMPLETE));
        }


        @Override
        public void onAuthFailed(String errId, String errCont) {
            sendBroadcast(new Intent(BroadcastAction.VIOCESERVICEINITCOMPLETE));
            LogUtils.e(TAG, "VioAIListener-onAuthFailed: " + errId + ", error:" + errCont);
        }

        @Override
        public void onInitWakeupEngine(int code) {
            LogUtils.e(TAG, "VioAIListener-onInitWakeupEngine: " + code);
            if (code == 0) ;
        }

        @Override
        public void onInitAsrEngine(int code) {
            LogUtils.e(TAG, "VioAIListener-onInitAsrEngine: " + code);
            if (code == 0) ;
        }

        @Override
        public void onInitTTSEngine(int code) {
            LogUtils.e(TAG, "VioAIListener-onInitTTSEngine: " + code);
            if (code == 0) ;
        }

        @Override
        public void onWakeUp(String data) {
            LogUtils.e(TAG, "VioAIListener-onWakeUp :" + data);
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG01);
        }

        @Override
        public void onTTSBegining(String s) {
            LogUtils.e(TAG, "VioAIListener-onTTSBegining: " + s);
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG07);
        }

        @Override
        public void onTTSReceived(byte[] bytes) {

        }

        @Override
        public void onTTSEnd(String s, int i) {
            LogUtils.e(TAG, "VioAIListener-onTTSEnd: " + s);
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG08);
        }

        @Override
        public void onTTSError(String s) {
            LogUtils.e(TAG, "VioAIListener-onTTSError: " + s);
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG08);
        }

        @Override
        public void onAsrResult(int type, String result) {
            LogUtils.e(TAG, "VioAIListener-onResult :type=" + type + "; result=" + result);
            if ("云米小薇".equals(result)) {
                return;
            }

            if (result != null && result.length() > 0) {
                Message msg = mHandler.obtainMessage();
                msg.what = HandlerMsgWhat.MSG06;
                msg.obj = result;
                msg.arg1 = type;
                mHandler.sendMessage(msg);
            }

            if (type == 1) {
                mHandler.sendEmptyMessage(HandlerMsgWhat.MSG05);
            }
        }

        //语音播报
        @Override
        public void onAsrSpeechOut(String s) {
            LogUtils.e(TAG, "VioAIListener-onAsrSpeechOut : " + s);
            if (s != null && s.length() > 0) {
                Message msg = mHandler.obtainMessage();
                msg.what = HandlerMsgWhat.MSG09;
                msg.obj = s;
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onEndOfSpeech() {
            LogUtils.e(TAG, "VioAIListener-onEndOfSpeech");
        }

        @Override
        public void onNluSuccess(JSONObject jsonObject) {
            //LogUtils.e(TAG, "VioAIListener-onNluSuccess JSONObject=" + jsonObject);
        }

        @Override
        public void onNluSuccess(String data) {
            LogUtils.e(TAG, "VioAIListener-onNluSuccess String=*****" + data);
            HandleSkill.getInstance().handleNlp(data);
            HandleSkill.getInstance().handleNlu(data);
        }

        @Override
        public void onNluSuccess(String message, String data) {
            LogUtils.e(TAG, "VioAIListener-onNluSuccess-qiuckorder -----" + message);
            switch (message) {
                case QUICK_WORD_1: {
                    sendBroadcast(new Intent(BroadcastAction.QIUCKORDER_NEXT));
                }
                break;

                case QUICK_WORD_2: {
                    sendBroadcast(new Intent(BroadcastAction.QIUCKORDER_PRE));
                }
                break;

                default:
                    break;
            }
        }

        @Override
        public void onNluError() {
            LogUtils.e(TAG, "VioAIListener-onNluError");
        }

        @Override
        public void onEventError(int i, String s) {

        }

        @Override
        public void onEventState(int i) {

        }

        @Override
        public void onStartDialog(String s) {
            LogUtils.e(TAG, "VioAIListener-onStartDialog:" + s);

            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG02);
            sendBroadcast(new Intent(BroadcastAction.SPEECHSTART));
        }

        @Override
        public void onEndDialog(String s) {
            LogUtils.e(TAG, "VioAIListener-onEndDialog:" + s);

            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG03);
            sendBroadcast(new Intent(BroadcastAction.SPEECHSTOP));
        }

        @Override
        public void onStartListening() {

        }

        @Override
        public void onStopListening() {

        }
    };

    private void checkLibUpdate() {
        VioAIUpdater.getInstance().checkResourceUpdate(vioAIUpdateListener);
    }

    private VioAIUpdateListener vioAIUpdateListener = new VioAIUpdateListener() {
        @Override
        public void onUpdateFound(String s) {
            LogUtils.e(TAG, "VioAIUpdateListener-onUpdateFound:" + s);
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG10);
        }

        @Override
        public void onUpdateFinish() {
            LogUtils.e(TAG, "VioAIUpdateListener-onUpdateFinish");
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG11);
        }

        @Override
        public void onDownloadProgress(float v) {
            LogUtils.e(TAG, "VioAIUpdateListener-onDownloadProgress:" + v);
            Message msg = mHandler.obtainMessage();
            msg.what = HandlerMsgWhat.MSG12;
            msg.obj = v;
            mHandler.sendMessage(msg);
        }

        @Override
        public void onError(int i, String s) {
            LogUtils.e(TAG, "VioAIUpdateListener-onError:" + s);
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG11);
        }

        @Override
        public void onUpgrade(String s) {
            LogUtils.e(TAG, "VioAIUpdateListener-onUpgrade:" + s);
        }
    };

    @Override
    protected void handleMessage(Message msg) {
        /*
         * 0.预留
         * 1.唤醒
         * 2.对话开始
         * 3.对话结束
         * 4.识别开始
         * 5.识别结束
         * 6.识别内容
         * 7.TTS开始
         * 8.TTS结束
         * 9.TTS内容
         * 10.更新开始
         * 11.更新完成
         * 12.更新进度
         * 13.对话自定义语音反馈
         * 14.对话自定义UI显示
         *
         * */
        switch (msg.what) {
            case HandlerMsgWhat.MSG0: {

                break;
            }

            case HandlerMsgWhat.MSG01: {

                break;
            }

            //对话开始
            case HandlerMsgWhat.MSG02: {
                startDialog();
                break;
            }

            //对话结束
            case HandlerMsgWhat.MSG03: {
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onDialogStop();
                }
                break;
            }

            //识别开始
            case HandlerMsgWhat.MSG04: {

                break;
            }

            //识别结束
            case HandlerMsgWhat.MSG05: {
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onListeningStop();
                }
                break;
            }

            //识别内容
            case HandlerMsgWhat.MSG06: {
                String result = msg.obj + "";
                if (isEndSentence) {
                    conversationList.add(new ConversationBean(ConversationType.user_input, result));
                } else {
                    if (conversationList.size() > 0) {
                        ConversationBean conversationBean = conversationList.get(conversationList.size() - 1);
                        if (ConversationType.user_input.equals(conversationBean.getType())) {
                            conversationBean.setParam(result);
                        } else {
                            conversationList.add(new ConversationBean(ConversationType.user_input, result));
                        }
                    } else {
                        conversationList.add(new ConversationBean(ConversationType.user_input, result));
                    }
                }

                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                isEndSentence = msg.arg1 == 1;
                break;
            }

            //TTS开始
            case HandlerMsgWhat.MSG07: {
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStart();
                }
                break;
            }

            //TTS结束
            case HandlerMsgWhat.MSG08: {
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.onSpeakingStop();
                }
                break;
            }

            //TTS内容
            case HandlerMsgWhat.MSG09: {
                String result = msg.obj + "";
                conversationList.add(new ConversationBean(ConversationType.robot_txt, result));
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;
            }

            case HandlerMsgWhat.MSG10: {
                showUpdateUI();
                break;
            }

            case HandlerMsgWhat.MSG11: {
                dismissUpdateUI();
                break;
            }

            case HandlerMsgWhat.MSG12: {
                float v = (float) msg.obj;
                setUpdateProgress((int) v);
                break;
            }

            //技能处理，自定义语音反馈
            case HandlerMsgWhat.MSG13: {
                String result = msg.obj + "";
                speakTTs(result);
                conversationList.add(new ConversationBean(ConversationType.robot_txt, result));
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;
            }

            //对话流反馈UI
            case HandlerMsgWhat.MSG14: {
                ConversationBean bean = (ConversationBean) msg.obj;
                conversationList.add(bean);
                if (conversationUpdateCallback != null) {
                    conversationUpdateCallback.update(conversationList);
                }
                break;
            }
            default:
                break;
        }
    }

    public List<ConversationBean> getConversationList() {
        return conversationList;
    }

    public void clearConversationList() {
        if (conversationList != null) {
            conversationList.clear();
        }
    }

    public void setConversationUpdateCallback(ConversationUpdateCallback conversationUpdateCallback) {
        this.conversationUpdateCallback = conversationUpdateCallback;
    }

    public void removeConversationUpdateCallback() {
        this.conversationUpdateCallback = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unInit();
        unRegisterBroadcastReceive();
    }

    private void speakTTs(String s) {
        VioAI.getInstance().speak(s);
    }

    private void sendText(String s) {
        VioAI.getInstance().sendText(s);
    }

    private void startDialogByHand(String s) {
        VioAI.getInstance().wakeupByHand(s);
        startDialog();
    }

    private void stopDialogByHand() {
        //打断不起麦，中断会话
        VioAI.getInstance().stopDialog();
    }

    private void startDialog() {

        Intent intent = new Intent(ViomiVoiceService.this, ConversationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        conversationList.clear();
        if (conversationUpdateCallback != null) {
            conversationUpdateCallback.onDialogStart();
            conversationUpdateCallback.update(conversationList);
        }
    }

    private BroadcastReceiver wakeUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String wakeup_hello = intent.getStringExtra("wakeup_hello");
            startDialogByHand(wakeup_hello);
        }
    };

    private BroadcastReceiver wakeUpBreak = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.e(TAG, "wakeUpBreak");
            stopDialogByHand();
            mHandler.sendEmptyMessage(HandlerMsgWhat.MSG03);
        }
    };

    private BroadcastReceiver speakRecive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String skeak_word = intent.getStringExtra("skeak_word");
            speakTTs(skeak_word);
        }
    };

    private void registerBroadcastReceive() {
        IntentFilter filter2 = new IntentFilter(BroadcastAction.GESTURE_V_GESTURE);
        registerReceiver(wakeUpReceiver, filter2);

        IntentFilter filter3 = new IntentFilter(BroadcastAction.GESTURE_OK_GESTURE);
        registerReceiver(wakeUpBreak, filter3);

        IntentFilter filter4 = new IntentFilter(BroadcastAction.TTSSPKEAK);
        registerReceiver(speakRecive, filter4);
    }

    private void unRegisterBroadcastReceive() {
        unregisterReceiver(wakeUpReceiver);
        unregisterReceiver(wakeUpBreak);
        unregisterReceiver(speakRecive);
    }

    public void showUpdateUI() {
        progressTitle.setText("语音资源更新中(0%)...");
        updateProgressBar.setProgress(0);

        if (!isAddUpdateView) {
            windowManager.addView(updateView, updateViewParams);
            isAddUpdateView = true;
        }
    }

    private void dismissUpdateUI() {
        if (isAddUpdateView) {
            windowManager.removeViewImmediate(updateView);
            isAddUpdateView = false;
        }
    }

    private void setUpdateProgress(int progress) {
        progressTitle.setText("语音资源更新中(" + progress + "%)...");
        updateProgressBar.setProgress(progress);
    }
}
