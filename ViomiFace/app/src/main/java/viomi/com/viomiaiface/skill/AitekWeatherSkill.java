package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import org.json.JSONObject;

import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;
import viomi.com.viomiaiface.model.WeatherBean2;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/7/18
 */
public class AitekWeatherSkill {

    private static final String TAG = "WeatherSkill";

    public static void handleSkill(String data, Handler mHandler) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);

        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "天气": {
                JSONObject weatherExtra = JsonUitls.getJSONObject(rawJosn, "extra");
                String wind = JsonUitls.getString(weatherExtra, "wind");
                String weather = JsonUitls.getString(weatherExtra, "weather");
                String temperature = JsonUitls.getString(weatherExtra, "temperature");
                String city = JsonUitls.getString(weatherExtra, "city");

                WeatherBean2 bean2 = new WeatherBean2(wind, weather, temperature, city);
                Message msg = mHandler.obtainMessage();
                ConversationBean bean = new ConversationBean(ConversationType.weather, bean2);
                msg.obj = bean;
                msg.what = HandlerMsgWhat.MSG14;
                mHandler.sendMessage(msg);

            }
            break;


            case "风向查询":
            case "风力查询":
            case "温度查询":
            case "气象": {
                JSONObject weatherExtra = JsonUitls.getJSONObject(rawJosn, "extra");
                String wind = JsonUitls.getString(weatherExtra, "wind");
                String temperature = JsonUitls.getString(weatherExtra, "temperature");
                String city = JsonUitls.getString(weatherExtra, "city");
                String weather = JsonUitls.getString(rawJosn, "text");

                WeatherBean2 bean2 = new WeatherBean2(wind, weather, temperature, city);
                Message msg = mHandler.obtainMessage();
                ConversationBean bean = new ConversationBean(ConversationType.weather, bean2);
                msg.obj = bean;
                msg.what = HandlerMsgWhat.MSG14;
                mHandler.sendMessage(msg);
            }
            break;



//            case "气象": {
//                JSONObject weatherExtra = JsonUitls.getJSONObject(rawJosn, "extra");
//                String wind = JsonUitls.getString(weatherExtra, "wind");
//                String temperature = JsonUitls.getString(weatherExtra, "temperature");
//                String city = JsonUitls.getString(weatherExtra, "city");
//                String weather = JsonUitls.getString(rawJosn, "text");
//
//                WeatherBean2 bean2 = new WeatherBean2(wind, weather, temperature, city);
//                Message msg = mHandler.obtainMessage();
//                ConversationBean bean = new ConversationBean(ConversationType.weather, bean2);
//                msg.obj = bean;
//                msg.what = HandlerMsgWhat.MSG14;
//                mHandler.sendMessage(msg);
//            }
//            break;

        }
    }
}
