package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.activity.DesktopActivity;
import viomi.com.viomiaiface.activity.SettingActivity;
import viomi.com.viomiaiface.config.BroadcastAction;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.utils.JsonUitls;

public class ControlManager {

    private static final String TAG = "ControlManager";

    public static void handleSkill(String data, Handler mHandler, Context context) {

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");

        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if ("intent".equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;

        switch (value) {

            //音量控制
            case "soundUp": {
                soundUp(5, context);
                msg.obj = "正在为您调大音量";
                mHandler.sendMessage(msg);
            }
            break;

            case "soundDown": {
                soundDown(5, context);
                msg.obj = "正在为您调小音量";
                mHandler.sendMessage(msg);
            }
            break;

            case "soundSet": {
                int value_set_int = -1;
                for (int i = 0; i < slots.length(); i++) {
                    JSONObject item = JsonUitls.getJSONObject(slots, i);
                    String name = JsonUitls.getString(item, "name");
                    if ("整数".equals(name)) {
                        value_set_int = JsonUitls.getInt(item, "value");
                        break;
                    }
                }
                if (value_set_int != -1) {
                    setSound(value_set_int, context);
                    msg.obj = "正在为您设置音量";
                    mHandler.sendMessage(msg);
                }

            }
            break;

            case "soundMax": {
                setSound(100, context);
                msg.obj = "正在为您设置最大音量";
                mHandler.sendMessage(msg);
            }
            break;

            case "soundMin": {
                setSound(0, context);
                msg.obj = "正在为您设置最小音量";
                mHandler.sendMessage(msg);
            }
            break;

            //跳转控制
            case "openset": {
                Intent settingIntent = new Intent(context, SettingActivity.class);
                settingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(settingIntent);
                msg.obj = "正在为您打开设置";
                mHandler.sendMessage(msg);
            }
            break;

            case "BackToTheDesktop": {
                Intent settingIntent = new Intent(context, DesktopActivity.class);
                settingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(settingIntent);
                msg.obj = "正在为您返回桌面";
                mHandler.sendMessage(msg);
            }
            break;

            //亮度控制
            case "亮度调节": {
            }
            break;

            case "最低亮度": {
            }
            break;

            case "最高亮度": {
            }
            break;

            //播放控制
            case "next": {
                context.sendBroadcast(new Intent(BroadcastAction.PLAYCONTROL_NEXT));
            }
            break;

            case "prev": {
                context.sendBroadcast(new Intent(BroadcastAction.PLAYCONTROL_PREV));
            }
            break;

            case "play": {
                context.sendBroadcast(new Intent(BroadcastAction.PLAYCONTROL_PLAY));
            }
            break;

            case "pause": {
                context.sendBroadcast(new Intent(BroadcastAction.PLAYCONTROL_PAUSE));
            }
            break;

            case "exit": {
                context.sendBroadcast(new Intent(BroadcastAction.PLAYCONTROL_PAUSE));
                context.sendBroadcast(new Intent(BroadcastAction.PLAYCONTROL_EXIT));
            }
            break;

            default:
                break;
        }
    }

    private static void soundDown(int soundValue, Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int streamMaxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volume = (int) Math.ceil(soundValue / 100.0 * streamMaxVolume);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        setMusicSound(currentVolume - volume, context);
    }

    private static void soundUp(int soundValue, Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int streamMaxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volume = (int) Math.ceil(soundValue / 100.0 * streamMaxVolume);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        setMusicSound(currentVolume + volume, context);
    }

    private static void setSound(int soundValue, Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int streamMaxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volume = (int) Math.ceil(soundValue / 100.0 * streamMaxVolume);
        setMusicSound(volume, context);
    }

    private static void setMusicSound(int soundValue, Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, soundValue, 0);
    }
}
