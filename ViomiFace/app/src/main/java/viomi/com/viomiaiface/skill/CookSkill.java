package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.activity.CookDetailActivity;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/8/22
 */
public class CookSkill {

    private static final String TAG = "CookSkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "查询做法":
            case "查询食材":

                JSONObject extra = JsonUitls.getJSONObject(rawJosn, "extra");
                JSONArray list = JsonUitls.getJSONArray(extra, "list");
                if (list.length() > 0) {
                    Intent cookIntent = new Intent(context, CookDetailActivity.class);
                    cookIntent.putExtra("jsonData", data);
                    cookIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(cookIntent);
                }
                break;
            case "推荐菜名":
                break;
        }


    }
}
