package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/7/26
 */
public class FolkArtSkill {

    private static final String TAG = "FolkArtSkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "播放曲艺": {
                JSONArray content = JsonUitls.getJSONArray(rawJosn, "content");
                if (content.length() > 0) {
                    Intent serviceintent = new Intent(context, MusicService.class);
                    serviceintent.putExtra("music_directive", data);
                    serviceintent.putExtra("playtype", "FOLK_ART_ID");
                    context.startService(serviceintent);
                }
            }
            break;

            case "播放控制": {

            }
            break;

            case "新手引导": {

            }
            break;

            case "曲艺查询": {

            }
            break;
        }
    }
}
