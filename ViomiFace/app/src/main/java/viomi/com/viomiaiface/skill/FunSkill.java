package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/7/26
 */
public class FunSkill {

    private static final String TAG = "FunSkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "播放笑话": {

                JSONArray content = JsonUitls.getJSONArray(rawJosn, "content");
                if (content.length() > 0) {
                    Intent serviceintent = new Intent(context, MusicService.class);
                    serviceintent.putExtra("music_directive", data);
                    serviceintent.putExtra("playtype", "FUN_ID");
                    context.startService(serviceintent);
                }
            }
            break;

            case "换一个": {

            }
            break;

            default:
                break;
        }
    }
}
