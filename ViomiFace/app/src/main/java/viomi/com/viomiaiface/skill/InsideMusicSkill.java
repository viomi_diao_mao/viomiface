package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/8/22
 */
public class InsideMusicSkill {

    private static final String TAG = "InsideMusicSkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);

        JSONArray content = JsonUitls.getJSONArray(rawJosn, "content");
        if (content.length() > 0) {
            Intent serviceintent = new Intent(context, MusicService.class);
            serviceintent.putExtra("music_directive", data);
            serviceintent.putExtra("playtype", "INSIDE_MUSIC_ID");
            context.startService(serviceintent);
        }
    }
}
