package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONObject;

import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/7/26
 */
public class LTNewsSkill {

    private static final String TAG = "LTNewsSkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "新闻播报": {
                Intent serviceintent = new Intent(context, MusicService.class);
                serviceintent.putExtra("music_directive", data);
                serviceintent.putExtra("playtype", "LT_NEWS_ID");
                context.startService(serviceintent);
            }
            break;

            case "订阅": {

            }
            break;

            default:
                break;
        }
    }

}
