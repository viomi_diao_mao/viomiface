package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;
import viomi.com.viomiaiface.model.WeatherBean2;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * Created by Mocc on 2018/7/18
 */
public class NewWeatherSkill {

    private static final String TAG = "NewWeatherSkill";

    public static void handleSkill(String data, Handler mHandler) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");
        LogUtils.e(TAG, intentName);

        switch (intentName) {

            case "查询风力":
            case "查询气象":
            case "查询温度":
            case "查询风向":
            case "查询日出日落":
            case "查询天气": {
                String city = JsonUitls.getString(rawJosn, "cityName");
                JSONArray forecastChoose = JsonUitls.getJSONArray(rawJosn, "forecastChoose");

                if (forecastChoose.length() > 0) {
                    JSONObject item = JsonUitls.getJSONObject(forecastChoose, 0);
                    String wind = JsonUitls.getString(item, "windDirDay") + JsonUitls.getString(item, "windLevelDay") + "级";
                    String temperature = JsonUitls.getString(item, "tempNight") + "-" + JsonUitls.getString(item, "tempDay" )+ "℃";
                    String weather = JsonUitls.getString(item, "conditionDayNight");
                    WeatherBean2 bean2 = new WeatherBean2(wind, weather, temperature, city);
                    Message msg = mHandler.obtainMessage();
                    ConversationBean bean = new ConversationBean(ConversationType.weather, bean2);
                    msg.obj = bean;
                    msg.what = HandlerMsgWhat.MSG14;
                    mHandler.sendMessage(msg);
                }
            }
            break;

            case "查询湿度":
            case "查询空气质量":
            case "查询紫外线":
            case "查询活动":
            case "查询装备":
            case "穿衣推荐":
            case "查询日期":
            case "查询城市":
            case "查询场景":
            case "查询护肤品":
            case "查询能见度":
            case "查询指数":
            case "查询降水量":
            case "查询降雪量":
            case "新手引导":
            case "非城市天气处理":
                break;
        }
    }
}
