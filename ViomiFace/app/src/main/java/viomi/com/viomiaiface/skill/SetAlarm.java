package viomi.com.viomiaiface.skill;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.reciver.AlarmReciver;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Mocc on 2018/3/2
 */

public class SetAlarm {

    private static SetAlarm setAlarm;
    private AlarmManager alarmManager;
    private int ationInt;

    public SetAlarm() {
        alarmManager = (AlarmManager) BaseApplication.getAppContext().getSystemService(ALARM_SERVICE);
    }

    public static SetAlarm getInstance() {
        if (setAlarm == null) {
            synchronized (SetAlarm.class) {
                if (setAlarm == null) {
                    setAlarm = new SetAlarm();
                }
            }
        }
        return setAlarm;
    }

    public void setAlarm(long alarmtime) {
        Intent broadcastintent = new Intent(BaseApplication.getAppContext(), AlarmReciver.class);
        broadcastintent.setAction("SET_ALARM_1" + (ationInt++));
        broadcastintent.putExtra("alarmtime", alarmtime);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(BaseApplication.getAppContext(), 0, broadcastintent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarmtime, pendingIntent);
    }
}
