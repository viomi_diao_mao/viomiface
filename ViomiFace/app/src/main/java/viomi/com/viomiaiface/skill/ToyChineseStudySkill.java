package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONObject;

import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/7/26
 */
public class ToyChineseStudySkill {
    private static final String TAG = "ToyChineseStudySkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "播放国学": {
                Intent serviceintent = new Intent(context, MusicService.class);
                serviceintent.putExtra("music_directive", data);
                serviceintent.putExtra("playtype", "CHINESE_STUDY_ID");
                context.startService(serviceintent);
            }
            break;

            case "查询上文": {

            }
            break;

            case "查询下文": {

            }
            break;

            case "查询出处": {
            }
            break;

            case "查询作者": {
            }
            break;

            case "查询简介": {
            }
            break;

            case "查询释义": {
            }
            break;

            default:
                break;
        }
    }
}
