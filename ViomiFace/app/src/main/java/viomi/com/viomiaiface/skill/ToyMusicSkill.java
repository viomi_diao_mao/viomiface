package viomi.com.viomiaiface.skill;

import android.content.Context;
import android.content.Intent;

import org.json.JSONObject;

import viomi.com.viomiaiface.mediaplayer.MusicService;
import viomi.com.viomiaiface.utils.JsonUitls;

/**
 * Created by Mocc on 2018/7/26
 */
public class ToyMusicSkill {

    private static final String TAG = "ToyMusicSkill";

    public static void handleSkill(String data, Context context) {

        JSONObject rawJosn = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawJosn, "intentName");

        switch (intentName) {
            case "音乐播放": {
                Intent serviceintent = new Intent(context, MusicService.class);
                serviceintent.putExtra("music_directive", data);
                serviceintent.putExtra("playtype", "MUSIC_ID");
                context.startService(serviceintent);
            }
            break;

            case "本地音乐": {

            }
            break;

            case "查询歌手名": {

            }
            break;

            case "查询歌曲名": {

            }
            break;
        }
    }
}
