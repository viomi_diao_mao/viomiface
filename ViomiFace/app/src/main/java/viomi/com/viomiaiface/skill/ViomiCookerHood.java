package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodPowerMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.HoodWindMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/7/2
 */
public class ViomiCookerHood {

    private static String TAG = "ViomiCookerHood";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到抽油烟机";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {

            case "打开油烟机": {
                setPower(device, HoodPowerMode.mode2);
                msg.obj = "正在为你开启" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "关闭油烟机": {
                setPower(device, HoodPowerMode.mode0);
                msg.obj = "正在为你关闭" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "设置档位": {
                String value_shift = getIntentString(slots, "档位变量");
                switch (value_shift) {
                    case "高档": {
                        setShift(device, HoodWindMode.mode5);
                        msg.obj = "正在为你把" + device.getName() + "设为高挡模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "爆炒": {
                        setShift(device, HoodWindMode.mode3);
                        msg.obj = "正在为你把" + device.getName() + "设为爆炒模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "低档": {
                        setShift(device, HoodWindMode.mode1);
                        msg.obj = "正在为你把" + device.getName() + "设为低挡模式";
                        mHandler.sendMessage(msg);
                    }
                    break;
                }
            }
            break;


            case "打开灯光": {
                setLight(device, Switch_ON_OFF.on);
                msg.obj = "正在为你开启" + device.getName() + "灯光";
                mHandler.sendMessage(msg);
            }
            break;

            case "关闭灯光": {
                setLight(device, Switch_ON_OFF.off);
                msg.obj = "正在为你关闭" + device.getName() + "灯光";
                mHandler.sendMessage(msg);
            }
            break;

            case "打开增压巡航": {
                setCruise(device, Switch_ON_OFF.on);
                msg.obj = "正在为你开启" + device.getName() + "增压巡航";
                mHandler.sendMessage(msg);
            }
            break;

            case "关闭增压巡航": {
                setCruise(device, Switch_ON_OFF.off);
                msg.obj = "正在为你关闭" + device.getName() + "增压巡航";
                mHandler.sendMessage(msg);
            }
            break;

            default:
                break;
        }
    }

    private static void setCruise(AbstractDevice device, Switch_ON_OFF mode) {
        Operation.getInstance().sendCookerHoodCommand6(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static void setLight(AbstractDevice device, Switch_ON_OFF mode) {
        Operation.getInstance().sendCookerHoodCommand3(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static void setShift(AbstractDevice device, HoodWindMode mode) {
        Operation.getInstance().sendCookerHoodCommand2(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static void setPower(AbstractDevice device, HoodPowerMode mode) {
        Operation.getInstance().sendCookerHoodCommand1(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }
}
