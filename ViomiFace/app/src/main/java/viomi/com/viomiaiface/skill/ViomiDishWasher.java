package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashCusMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashProMode;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/7/3
 */
public class ViomiDishWasher {

    private static String TAG = "ViomiDishWasher";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到洗碗机";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {
            case "设置洗涤模式": {
                String value_model = getIntentString(slots, "洗涤模式");
                switch (value_model) {
                    case "除菌": {
                        setMode(device,WashCusMode.mode6);
                        msg.obj = "正在为你把"+device.getName()+"设置为除菌模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "强力洗": {
                        setMode(device,WashCusMode.mode3);
                        msg.obj = "正在为你把"+device.getName()+"设置为强力洗模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "玻璃洗": {
                        setMode(device,WashCusMode.mode5);
                        msg.obj = "正在为你把"+device.getName()+"设置为玻璃洗模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "预洗": {
                        setMode(device,WashCusMode.mode4);
                        msg.obj = "正在为你把"+device.getName()+"设置为预洗模式";
                        mHandler.sendMessage(msg);
                    }
                    break;
                }
            }
            break;

            case "设置洗涤程序": {
                String value_model = getIntentString(slots, "洗涤程序");
                switch (value_model) {
                    case "经济洗": {
                        setProcess(device, WashProMode.mode1);
                        msg.obj = "正在为你把"+device.getName()+"设置为经济洗模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "快速洗": {
                        setProcess(device, WashProMode.mode2);
                        msg.obj = "正在为你把"+device.getName()+"设置为快速洗模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "标准洗": {
                        setProcess(device, WashProMode.mode0);
                        msg.obj = "正在为你把"+device.getName()+"设置为标准洗模式";
                        mHandler.sendMessage(msg);
                    }
                    break;

                    case "自定义": {
                        setProcess(device, WashProMode.mode3);
                        msg.obj = "正在为你把"+device.getName()+"设置为自定义模式";
                        mHandler.sendMessage(msg);
                    }
                    break;
                }
            }
            break;

            case "洗碗机启动": {
                setPower(device, Switch_ON_OFF.on);
                msg.obj = "正在为你启动" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "洗碗机关闭": {
                setPower(device, Switch_ON_OFF.off);
                msg.obj = "正在为你关闭" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "洗碗机暂停": {
                setPower(device, Switch_ON_OFF.off);
                msg.obj = "正在为你暂停" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;
        }
    }

    private static void setProcess(AbstractDevice device, WashProMode mode) {
        Operation.getInstance().sendDishwasherCommand3(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static void setMode(AbstractDevice device, WashCusMode mode) {
        setProcess(device, WashProMode.mode3);
        Operation.getInstance().sendDishwasherCommand5(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static void setPower(AbstractDevice device, Switch_ON_OFF on_off) {
        Operation.getInstance().sendDishwasherCommand4(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(),
                new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, on_off);
    }


    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }
}
