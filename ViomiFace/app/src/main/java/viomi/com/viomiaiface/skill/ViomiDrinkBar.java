package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/6/28
 */
public class ViomiDrinkBar {

    private static String TAG = "ViomiDrinkBar";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到饮水吧";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if ("intent".equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }

        switch (value) {
            case "设置温水键温度": {
                int value_set_int = -1;
                for (int i = 0; i < slots.length(); i++) {
                    JSONObject item = JsonUitls.getJSONObject(slots, i);
                    String name = JsonUitls.getString(item, "name");
                    if ("整数".equals(name)) {
                        value_set_int = JsonUitls.getInt(item, "value");
                        break;
                    }
                }

                if (value_set_int == -1) return;

                if (value_set_int < 40 || value_set_int > 90) {
                    msg.obj = "饮水吧温度只能设为40至90摄氏度";
                    mHandler.sendMessage(msg);
                    return;
                }

                setTemp(device, value_set_int);
                msg.obj = "正在为你把" + device.getName() + "温度调整为" + value_set_int + "度";
                mHandler.sendMessage(msg);
            }
            break;

            case "快捷设置温度": {
                int value_set_int = 50;
                setTemp(device, value_set_int);
                msg.obj = "正在为你把" + device.getName() + "温度调整为" + value_set_int + "度";
                mHandler.sendMessage(msg);
            }
            break;

            case "查询水温": {
                Operation.getInstance().sendDrinkBarCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                        JSONObject resultjson = JsonUitls.getJSONObject(result);
                        String code = JsonUitls.getString(resultjson, "code");
                        if ("0".equals(code)) {
                            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
                            if (resultarray.length() > 0) {
                                try {
                                    int anInt = resultarray.getInt(0);
                                    msg.obj = device.getName() + "当前热水键水温为" + anInt + "度";
                                    mHandler.sendMessage(msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, new String[]{"custom_tempe1"});
            }
            break;

            case "查询水质": {
                Operation.getInstance().sendDrinkBarCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                        JSONObject resultjson = JsonUitls.getJSONObject(result);
                        String code = JsonUitls.getString(resultjson, "code");
                        if ("0".equals(code)) {
                            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
                            if (resultarray.length() > 0) {
                                try {
                                    int anInt = resultarray.getInt(0);
                                    msg.obj = device.getName() + "当前TDS值为" + anInt;
                                    mHandler.sendMessage(msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, new String[]{"tds"});
            }
            break;

            case "查询存水时间": {
                Operation.getInstance().sendDrinkBarCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                        JSONObject resultjson = JsonUitls.getJSONObject(result);
                        String code = JsonUitls.getString(resultjson, "code");
                        if ("0".equals(code)) {
                            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
                            if (resultarray.length() > 0) {
                                try {
                                    int anInt = resultarray.getInt(0);
                                    msg.obj = device.getName() + "存水时间为" + anInt + "小时";
                                    mHandler.sendMessage(msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, new String[]{"water_remain_time"});
            }
            break;

            default:
                break;
        }
    }

    private static void setTemp(AbstractDevice device, int value_set_int) {

        Operation.getInstance().sendDrinkBarCommand1(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFail(String result) {

            }
        }, value_set_int);
    }
}
