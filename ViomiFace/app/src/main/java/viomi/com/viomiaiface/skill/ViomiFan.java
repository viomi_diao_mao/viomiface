package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FanSpeedMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FanWorkMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/7/4
 */
public class ViomiFan {

    private static String TAG = "ViomiFan";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到电风扇";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {


            case "打开风扇": {
                setPower(device, Switch_ON_OFF.on);
                msg.obj = "正在为你开启"+device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "关闭风扇": {
                setPower(device, Switch_ON_OFF.off);
                msg.obj = "正在为你关闭"+device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "加大风速": {
                setUpDown(device, Switch_ON_OFF.on);
                msg.obj = "正在为你加大"+device.getName()+"风速";
                mHandler.sendMessage(msg);
            }
            break;


            case "减小风速": {
                setUpDown(device, Switch_ON_OFF.off);
                msg.obj =  "正在为你降低"+device.getName()+"风速";
                mHandler.sendMessage(msg);
            }
            break;

            case "开启摆风": {
                stSwing(device, Switch_ON_OFF.on);
                msg.obj = "正在为你开启"+device.getName()+"摇头";
                mHandler.sendMessage(msg);
            }
            break;

            case "关闭摆风": {
                stSwing(device, Switch_ON_OFF.off);
                msg.obj = "正在为你关闭"+device.getName()+"摇头";
                mHandler.sendMessage(msg);
            }
            break;

            case "设置风速": {
                String value_switch = getIntentString(slots, "档位");
                for (int i = 0; i < FanSpeedMode.values().length; i++) {
                    if (FanSpeedMode.values()[i].name().equals(value_switch)) {
                        setSpeedMode(device, FanSpeedMode.values()[i]);
                        msg.obj = "正在为你把"+device.getName()+"设为"+value_switch;
                        mHandler.sendMessage(msg);
                        break;
                    }
                }
            }
            break;

            case "设置风扇模式": {
                String value_switch = getIntentString(slots, "风扇模式");
                for (int i = 0; i < FanWorkMode.values().length; i++) {
                    if (FanWorkMode.values()[i].name().equals(value_switch)) {
                        setWorkMode(device, FanWorkMode.values()[i]);
                        msg.obj = "正在为你把"+device.getName()+"设为"+value_switch;
                        mHandler.sendMessage(msg);
                        break;
                    }
                }
            }
            break;
        }
    }

    private static void stSwing(AbstractDevice device, Switch_ON_OFF on_off) {
        Operation.getInstance().sendFanCommand1(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, on_off);
    }

    private static void setWorkMode(AbstractDevice device, FanWorkMode workMode) {
        Operation.getInstance().sendFanCommand3(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, workMode);
    }

    private static void setUpDown(AbstractDevice device,  Switch_ON_OFF on_off) {
        Operation.getInstance().sendFanCommand8(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, on_off);
    }

    private static void setSpeedMode(AbstractDevice device, FanSpeedMode mode) {
        Operation.getInstance().sendFanCommand2(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static void setPower(AbstractDevice device, Switch_ON_OFF on_off) {
        Operation.getInstance().sendFanCommand7(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, on_off);
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }

}
