package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.FrigeUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.FridgeTempMode;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/6/29
 */
public class ViomiFridge {

    private static String TAG = "ViomiFridge";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到冰箱";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {
            case "设置冷藏室温度": {
                int intValue = getIntentInt(slots);

                if (intValue < 2 || intValue > 8) {
                    msg.obj = "冷藏室只能设置为2度至8度";
                    mHandler.sendMessage(msg);
                    return;
                }

                Operation.getInstance().sendFridgeCommand5(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, intValue);

                msg.obj = "正在为你把" + device.getName() + "冷藏室温度调整为" + intValue + "度";
                mHandler.sendMessage(msg);
            }
            break;

            case "设置变温室温度": {

                if (!FrigeUtil.containBianwen(device.getDeviceModel())) {
                    msg.obj = device.getName() + "没有变温室哦。";
                    mHandler.sendMessage(msg);
                    return;
                }

                int intValue = getIntentInt(slots);
                if (isNegative(slots)) {
                    intValue = -intValue;
                }

                if (Miconfig.VIOMI_FRIDGE_X5.equals(device.getDeviceModel())) {
                    if (intValue < -18 || intValue > 5) {
                        msg.obj = device.getName() + "变温室只能设置为零下十八度至五度";
                        mHandler.sendMessage(msg);
                        return;
                    }
                }

                if (Miconfig.VIOMI_FRIDGE_V2.equals(device.getDeviceModel())) {
                    if (intValue < -18 || intValue > -3) {
                        msg.obj = device.getName() + "变温室只能设置为零下十八度至零下三度";
                        mHandler.sendMessage(msg);
                        return;
                    }
                }

                if (intValue < -18 || intValue > 8) {
                    msg.obj = "变温室只能设置为零下十八度至八度";
                    mHandler.sendMessage(msg);
                    return;
                }

                Operation.getInstance().sendFridgeCommand6(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, intValue);

                msg.obj = "正在为你把" + device.getName() + "变温室温度调整为" + intValue + "度";
                mHandler.sendMessage(msg);
            }
            break;

            case "设置冷冻室温度": {
                int intValue = getIntentInt(slots);

                if (isNegative(slots)) {
                    intValue = -intValue;
                }

                if (Miconfig.VIOMI_FRIDGE_U2.equals(device.getDeviceModel()) || Miconfig.VIOMI_FRIDGE_X4.equals(device.getDeviceModel())) {
                    if (intValue < -23 || intValue > -15) {
                        msg.obj = device.getName() + "冷冻室只能设置为零下二十三度至零下十五度";
                        mHandler.sendMessage(msg);
                        return;
                    }
                }

                if (Miconfig.VIOMI_FRIDGE_X3.equals(device.getDeviceModel()) || Miconfig.VIOMI_FRIDGE_X5.equals(device.getDeviceModel()) || Miconfig.VIOMI_FRIDGE_V2.equals(device.getDeviceModel())) {
                    if (intValue < -24 || intValue > -16) {
                        msg.obj = device.getName() + "冷冻室只能设置为零下二十四度至零下十六度";
                        mHandler.sendMessage(msg);
                        return;
                    }
                }


                if (intValue < -25 || intValue > -15) {
                    msg.obj = "冷冻室只能设置为零下二十五度至零下十五度";
                    mHandler.sendMessage(msg);
                    return;
                }

                Operation.getInstance().sendFridgeCommand7(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, intValue);

                msg.obj = "正在为你把" + device.getName() + "冷冻室温度调整为" + intValue + "度";
                mHandler.sendMessage(msg);
            }
            break;

            case "设置变温室模式": {

                if (!FrigeUtil.containBianwen(device.getDeviceModel())) {
                    msg.obj = device.getName() + "没有变温室哦。";
                    mHandler.sendMessage(msg);
                    return;
                }

                String value_mode = getIntentString(slots, "变温室模式");
                FridgeTempMode mode1 = null;
                for (FridgeTempMode mode : FridgeTempMode.values()) {
                    if (mode.name().equals(value_mode)) {
                        mode1 = mode;
                        break;
                    }
                }

                if (mode1 == null) return;

                Operation.getInstance().sendFridgeCommand8(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                            @Override
                            public void onSuccess(String result) {

                            }

                            @Override
                            public void onFail(String result) {

                            }
                        }, mode1);
                msg.obj = "正在为你把" + device.getName() + "变温室模式设置为" + value_mode;
                mHandler.sendMessage(msg);
            }
            break;

            case "设置冰箱模式": {

                String value_mode = getIntentString(slots, "冰箱模式");
                FridgeMode mode1 = null;
                for (FridgeMode mode : FridgeMode.values()) {
                    if (mode.getName().equals(value_mode)) {
                        mode1 = mode;
                        break;
                    }
                }

                if (mode1 == null) return;

                Operation.getInstance().sendFridgeCommand4(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                            @Override
                            public void onSuccess(String result) {

                            }

                            @Override
                            public void onFail(String result) {

                            }
                        }, mode1);
                msg.obj = "正在为你把" + device.getName() + "模式设置为" + value_mode;
                mHandler.sendMessage(msg);

            }
            break;

            case "查询冷藏室温度": {

                Operation.getInstance().sendFridgeCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                        JSONObject resultjson = JsonUitls.getJSONObject(result);
                        String code = JsonUitls.getString(resultjson, "code");
                        if ("0".equals(code)) {
                            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
                            if (resultarray.length() > 0) {
                                try {
                                    int anInt = resultarray.getInt(0);
                                    msg.obj = device.getName() + "当前冷藏室温度为" + anInt + "度";
                                    mHandler.sendMessage(msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, new String[]{"RCSetTemp"});

            }
            break;

            case "查询变温室温度": {

                if (!FrigeUtil.containBianwen(device.getDeviceModel())) {
                    msg.obj = device.getName() + "没有变温室哦。";
                    mHandler.sendMessage(msg);
                    return;
                }

                Operation.getInstance().sendFridgeCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                        JSONObject resultjson = JsonUitls.getJSONObject(result);
                        String code = JsonUitls.getString(resultjson, "code");
                        if ("0".equals(code)) {
                            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
                            if (resultarray.length() > 0) {
                                try {
                                    int anInt = resultarray.getInt(0);
                                    msg.obj = device.getName() + "当前变温室温度为" + anInt + "度";
                                    mHandler.sendMessage(msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, new String[]{"CCSetTemp"});
            }
            break;

            case "查询冷冻室温度": {
                Operation.getInstance().sendFridgeCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                        JSONObject resultjson = JsonUitls.getJSONObject(result);
                        String code = JsonUitls.getString(resultjson, "code");
                        if ("0".equals(code)) {
                            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
                            if (resultarray.length() > 0) {
                                try {
                                    int anInt = resultarray.getInt(0);

                                    if (anInt==-32&&(Miconfig.VIOMI_FRIDGE_X3.equals(device.getDeviceModel())||Miconfig.VIOMI_FRIDGE_X5.equals(device.getDeviceModel()))) {
                                        anInt = -24;
                                    }

                                    msg.obj = device.getName() + "当前冷冻室温度为" + anInt + "度";
                                    mHandler.sendMessage(msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, new String[]{"FCSetTemp"});

            }
            break;

            case "查询变温室模式": {
                if (!FrigeUtil.containBianwen(device.getDeviceModel())) {
                    msg.obj = device.getName() + "没有变温室哦。";
                    mHandler.sendMessage(msg);
                    return;
                }

                Operation.getInstance().get_prop(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                            @Override
                            public void onSuccess(String result) {
                                LogUtils.e(TAG, "onresult:" + result);
                                try {
                                    JSONObject object = new JSONObject(result);
                                    String mode = object.getJSONArray("result").getString(0);
                                    msg.obj = "当前" + device.getName() + "冰箱变温室处于" + mode + "模式";
                                    mHandler.sendMessage(msg);
                                } catch (Throwable e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(String result) {

                            }
                        }, "SceneName");
            }
            break;

            case "查询冰箱模式": {
                Operation.getInstance().get_prop(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                        , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                            @Override
                            public void onSuccess(String result) {
                                LogUtils.e(TAG, "onresult:" + result);
                                try {
                                    String text = "";
                                    JSONObject object = new JSONObject(result);
                                    String mode = object.getJSONArray("result").getString(0);
                                    for (FridgeMode modeT : FridgeMode.values()) {
                                        if (modeT.toString().equals(mode)) {
                                            text = "当前" + device.getName() + "冰箱处于" + modeT.getName();
                                            break;
                                        }
                                    }
                                    msg.obj = text;
                                    mHandler.sendMessage(msg);
                                } catch (Throwable e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFail(String result) {
                            }
                        }, "Mode");
            }
            break;
        }
    }

    private static int getIntentInt(JSONArray slots) {
        int value_set_int = -100;
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if ("整数".equals(name)) {
                value_set_int = JsonUitls.getInt(item, "value");
                break;
            }
        }
        return value_set_int;
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }

    private static boolean isNegative(JSONArray slots) {
        boolean value =false;
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if ("负数".equals(name)) {
                value = true;
                break;
            }
        }
        return value;
    }
}