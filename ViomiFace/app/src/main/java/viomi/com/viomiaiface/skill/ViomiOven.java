package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_P_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/6/29
 */
public class ViomiOven {

    private static String TAG = "ViomiOven";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {


        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        msg.obj = "改功能暂未开通！";
        mHandler.sendMessage(msg);

//        Message msg = mHandler.obtainMessage();
//        msg.what = HandlerMsgWhat.MSG13;
//        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
//        if (user == null) {
//            msg.obj = "请先登录云米账号";
//            mHandler.sendMessage(msg);
//            return;
//        }
//
//        if (device == null) {
//            msg.obj = "没有连接到蒸烤箱";
//            mHandler.sendMessage(msg);
//            return;
//        }
//
//        JSONObject rawjson = JsonUitls.getJSONObject(data);
//        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
//        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
//        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
//        JSONArray slots = JsonUitls.getJSONArray(request, "slots");
//
//        String value = getIntentString(slots, "intent");
//
//        switch (value) {
//
//            case "蒸烤启动": {
//                setPower(device,Switch_ON_P_OFF.on);
//                msg.obj = "正在为你开启"+device.getName();
//                mHandler.sendMessage(msg);
//            }
//            break;
//
//            case "蒸烤暂停": {
//                setPower(device,Switch_ON_P_OFF.pause);
//                msg.obj = "正在为你暂停"+device.getName();
//                mHandler.sendMessage(msg);
//            }
//            break;
//
//            case "蒸烤关闭": {
//                setPower(device,Switch_ON_P_OFF.off);
//                msg.obj = "正在为你关闭"+device.getName();
//                mHandler.sendMessage(msg);
//            }
//            break;
//
//        }
    }

    private static void setPower(AbstractDevice device, Switch_ON_P_OFF switchOnPOff) {
        Operation.getInstance().sendOvenCommand1(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, switchOnPOff);
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }
}
