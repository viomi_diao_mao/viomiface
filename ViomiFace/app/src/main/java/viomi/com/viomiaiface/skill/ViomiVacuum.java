package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.VacuumWorkMode;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/7/4
 */
public class ViomiVacuum {
    private static String TAG = "ViomiVacuum";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到扫地机";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {

            case "设置工作模式": {
                String value_switch = getIntentString(slots, "工作模式");
                String value_raw = getIntentRawString(slots, "工作模式");

                for (int i = 0; i < VacuumWorkMode.values().length; i++) {
                    if (VacuumWorkMode.values()[i].name().equals(value_switch)) {
//                        setWorkMode(device, VacuumWorkMode.values()[i]);
                        stPower(device, Switch_ON_OFF.on, VacuumWorkMode.values()[i]);
                        msg.obj = "正在为你把" + device.getName() + "设为" + value_raw + "模式";
                        mHandler.sendMessage(msg);
                        break;
                    }
                }
            }
            break;


            case "扫地机启动": {
                stPower(device, Switch_ON_OFF.on, VacuumWorkMode.自动清扫);
                msg.obj = "正在为你开启" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "扫地机关闭": {
                stPower(device, Switch_ON_OFF.off, null);
                msg.obj = "正在为你关闭" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

        }
    }

    private static void stPower(AbstractDevice device, Switch_ON_OFF on_off, VacuumWorkMode mode) {
        Operation.getInstance().sendVacuumCommand2(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                        if (on_off.equals(Switch_ON_OFF.on)) {
                            setWorkMode(device, mode);
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, on_off);
    }

    private static void setWorkMode(AbstractDevice device, VacuumWorkMode mode) {
        Operation.getInstance().sendVacuumCommand3(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onSuccess:" + result);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, mode);
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }

    private static String getIntentRawString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "rawvalue");
                break;
            }
        }
        return value;
    }
}
