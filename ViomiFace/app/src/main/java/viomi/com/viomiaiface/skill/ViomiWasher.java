package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.Switch_P_ON_OFF;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashMacProMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingDryRmpMode;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTemp;
import viomi.com.viomiconnectsdk.viomiconnect.sw_mode.WashingTimes;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/7/3
 */
public class ViomiWasher {

    private static String TAG = "ViomiWasher";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到洗衣机";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {

            case "洗衣机启动": {
                setModeByStart(device, WashMacProMode.黄金洗, new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "洗衣机启动:-mode" + result);
                       mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                LogUtils.e(TAG, "洗衣机启动:-start" + result);
                                setPower(device, Switch_P_ON_OFF.on);
                            }
                        },4000);
                    }

                    @Override
                    public void onFail(String result) {

                    }
                });

                msg.obj = "正在为你启动" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "洗衣机暂停": {
                setPower(device, Switch_P_ON_OFF.pause);
                msg.obj = "正在为你暂停" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "洗衣机关闭": {
                setPower(device, Switch_P_ON_OFF.off);
                msg.obj = "正在为你关闭" + device.getName();
                mHandler.sendMessage(msg);
            }
            break;

            case "设置水温": {

                String tempValue=getIntentString(slots,"常温");
                if ("常温".equals(tempValue)) {
                    setTemp(device, WashingTemp.normal);
                    msg.obj = "正在为你把" + device.getName() + "水温设置为常温";
                    mHandler.sendMessage(msg);
                    return;
                }

                int intValue = getIntentInt(slots);
                if (!(intValue == 30 || intValue == 40 || intValue == 60 || intValue == 90)) {
                    msg.obj = "洗衣机水温只能设为30度，40度，60度，90度";
                    mHandler.sendMessage(msg);
                    return;
                }

                switch (intValue) {
                    case 30:
                        setTemp(device, WashingTemp.t30);
                        break;
                    case 40:
                        setTemp(device, WashingTemp.t40);
                        break;
                    case 60:
                        setTemp(device, WashingTemp.t60);
                        break;
                    case 90:
                        setTemp(device, WashingTemp.t90);
                        break;
                    default:
                        return;
                }

                msg.obj = "正在为你把" + device.getName() + "水温设置为" + intValue + "度";
                mHandler.sendMessage(msg);
            }
            break;

            case "设置漂洗次数": {
                int intValue = getIntentInt(slots);

                if (intValue > 5 || intValue < 1) {
                    msg.obj = "洗衣机漂洗次数只能设为1至5次";
                    mHandler.sendMessage(msg);
                    return;
                }

                switch (intValue) {
                    case 1:
                        setRinseTimes(device, WashingTimes.one);
                        break;
                    case 2:
                        setRinseTimes(device, WashingTimes.two);
                        break;
                    case 3:
                        setRinseTimes(device, WashingTimes.three);
                        break;
                    case 4:
                        setRinseTimes(device, WashingTimes.four);
                        break;
                    case 5:
                        setRinseTimes(device, WashingTimes.five);
                        break;
                    default:
                        return;
                }

                msg.obj = "正在为你把" + device.getName() + "漂洗次数设置为" + intValue + "次";
                mHandler.sendMessage(msg);
            }
            break;

            case "设置脱水转速": {
                String valueSpeed = getIntentString(slots, "脱水转速");
                for (int i = 0; i < WashingDryRmpMode.values().length; i++) {
                    String name = WashingDryRmpMode.values()[i].name;
                    if (name.equals(valueSpeed)) {
                        setDryMode(device, WashingDryRmpMode.values()[i]);
                        msg.obj = "正在为你把" + device.getName() + "脱水强度设置为" + valueSpeed;
                        mHandler.sendMessage(msg);
                        break;
                    }
                }
            }
            break;

            case "设置洗涤模式": {
                String valueMode = getIntentString(slots, "洗涤模式");
                for (int i = 0; i < WashMacProMode.values().length; i++) {
                    if (WashMacProMode.values()[i].name().equals(valueMode)) {
                        setMode(device, WashMacProMode.values()[i]);
                        msg.obj = "正在为你把" + device.getName() + "洗衣模式设置为" + valueMode;
                        mHandler.sendMessage(msg);
                        break;
                    }
                }
            }
            break;

            case "查询剩余时间": {
                requestLeftTime(device, mHandler, msg);
            }
            break;
        }
    }

    private static void requestLeftTime(AbstractDevice device, Handler mHandler, Message msg) {
        Operation.getInstance().get_prop(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, "onresult:" + result);
                        try {
                            JSONObject object = new JSONObject(result);
                            int wash_process = object.getJSONArray("result").getInt(0);
                            int remain_time = object.getJSONArray("result").getInt(1);
                            String text = "";
                            if (wash_process == 0) {
                                text = device.getName() + "还没开始洗衣服呢。";
                            } else if (wash_process == 5) {
                                text = device.getName() + "已经洗完衣服了。";
                            } else if (wash_process == 6) {
                                text = device.getName() + "停机了哦。";
                            } else if (wash_process == 7) {
                                text = device.getName() + "还没开机哦。";
                            } else {
                                text = device.getName() + "还有" + remain_time + "分钟洗完衣服。";
                            }
                            msg.obj = text;
                            mHandler.sendMessage(msg);
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, "wash_process", "remain_time");
    }

    private static void setModeByStart(AbstractDevice device, WashMacProMode mode, ViomiDeviceCallback callback) {
        Operation.getInstance().sendWashingMachineCommand3(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), callback, mode);
    }

    private static void setMode(AbstractDevice device, WashMacProMode mode) {
        Operation.getInstance().sendWashingMachineCommand3(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, result + "");
            }

            @Override
            public void onFail(String result) {

            }
        }, mode);
    }

    private static void setDryMode(AbstractDevice device, WashingDryRmpMode mode) {
        Operation.getInstance().sendWashingMachineCommand6(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, result + "");
            }

            @Override
            public void onFail(String result) {

            }
        }, mode);
    }

    private static void setRinseTimes(AbstractDevice device, WashingTimes times) {
        Operation.getInstance().sendWashingMachineCommand5(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, result + "");
            }

            @Override
            public void onFail(String result) {

            }
        }, times);
    }

    private static void setTemp(AbstractDevice device, WashingTemp temp) {
        Operation.getInstance().sendWashingMachineCommand4(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
            @Override
            public void onSuccess(String result) {
                LogUtils.e(TAG, result + "");
            }

            @Override
            public void onFail(String result) {

            }
        }, temp);
    }

    private static void setPower(AbstractDevice device, Switch_P_ON_OFF switchOnOff) {
        Operation.getInstance().sendWashingMachineCommand2(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                    @Override
                    public void onSuccess(String result) {
                        LogUtils.e(TAG, result + "");
                    }

                    @Override
                    public void onFail(String result) {

                    }
                }, switchOnOff);
    }

    private static int getIntentInt(JSONArray slots) {
        int value_set_int = -100;
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if ("整数".equals(name)) {
                value_set_int = JsonUitls.getInt(item, "value");
                break;
            }
        }
        return value_set_int;
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }
}
