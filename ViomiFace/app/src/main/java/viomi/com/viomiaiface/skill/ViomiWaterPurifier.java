package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import com.miot.api.MiotManager;
import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.model.ConversationBean;
import viomi.com.viomiaiface.model.ConversationType;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;
import viomi.com.viomiconnectsdk.viomiconnect.operation.Operation;
import viomi.com.viomiconnectsdk.viomiconnect.util.ViomiDeviceCallback;

/**
 * Created by Mocc on 2018/7/2
 */
public class ViomiWaterPurifier {

    private static String TAG = "ViomiWaterPurifier";

    public static void handleSkill(String data, Handler mHandler, AbstractDevice device) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        if (device == null) {
            msg.obj = "没有连接到净水器";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(rawjson, "nlu");
        JSONObject semantics = JsonUitls.getJSONObject(nlu, "semantics");
        JSONObject request = JsonUitls.getJSONObject(semantics, "request");
        JSONArray slots = JsonUitls.getJSONArray(request, "slots");

        String value = getIntentString(slots, "intent");

        switch (value) {
            case "设置温水键温度": {
                int intValue = getIntentInt(slots);
                setOutTemp(mHandler, device, msg, intValue);
            }
            break;

            case "快捷设置温度": {
                int intValue = 50;
                setOutTemp(mHandler, device, msg, intValue);
            }
            break;

            case "查询水质": {
                Operation.getInstance().sendPurifierCommand0(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(),
                        new ViomiDeviceCallback() {
                            @Override
                            public void onSuccess(String result) {
                                LogUtils.e(TAG, result + "");
                                int resultValue = parseResult(result);
                                if (resultValue != -100) {

                                    if (0 < resultValue && resultValue < 100) {
                                        msg.obj = device.getName() + "当前的TDS值为" + resultValue + "可以直接饮用";
                                        mHandler.sendMessage(msg);
                                    } else if (resultValue >= 100) {
                                        msg.obj = device.getName() + "当前的TDS值为" + resultValue + "不能直接饮用";
                                        mHandler.sendMessage(msg);
                                    } else {
                                        msg.obj = device.getName() + "当前的TDS值为" + resultValue;
                                        mHandler.sendMessage(msg);
                                    }
                                }
                            }

                            @Override
                            public void onFail(String result) {

                            }
                        }, new String[]{"tds_out"});
            }
            break;

            case "查询水温": {
                switch (device.getDeviceModel()) {
                    case Miconfig.YUNMI_WATERPURI_X3:
                    case Miconfig.YUNMI_WATERPURI_X5: {
                        Operation.getInstance().get_prop(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + ""
                                , MiotManager.getPeopleManager().getPeople().getAccessToken(), new ViomiDeviceCallback() {
                                    @Override
                                    public void onSuccess(String result) {
                                        LogUtils.e(TAG, result + "");
                                        int resultValue = parseResult(result);
                                        if (resultValue != -100) {
                                            msg.obj = device.getName() + "当前的出水水温为" + resultValue + "度";
                                            mHandler.sendMessage(msg);
                                        }
                                    }

                                    @Override
                                    public void onFail(String result) {

                                    }
                                }, "setup_tempe");
                    }
                    break;
                    default:
                        msg.obj = device.getName() + "不支持出水温度查询";
                        mHandler.sendMessage(msg);
                        break;
                }
            }
            break;
            default:
                break;
        }
    }

    private static void setOutTemp(Handler mHandler, AbstractDevice device, Message msg, int intValue) {
        switch (device.getDeviceModel()) {
            case Miconfig.YUNMI_WATERPURI_X3:
            case Miconfig.YUNMI_WATERPURI_X5:

                if (intValue < 40 || intValue > 90) {
                    msg.obj = "净水器温度只能设置40到90度";
                    mHandler.sendMessage(msg);
                    return;
                }

                Operation.getInstance().sendPurifierCommand1(device.getDeviceId(), Miconfig.getMI_OAUTH_APP_ID() + "", MiotManager.getPeopleManager().getPeople().getAccessToken(),
                        new ViomiDeviceCallback() {
                            @Override
                            public void onSuccess(String result) {

                            }

                            @Override
                            public void onFail(String result) {

                            }
                        }, intValue);

                msg.obj = "正在为你把" + device.getName() + "出水温度设置为" + intValue + "度";
                mHandler.sendMessage(msg);

                Message message = mHandler.obtainMessage();
                message.what = HandlerMsgWhat.MSG14;
                message.obj = new ConversationBean(ConversationType.device_x5_temp, intValue);
                mHandler.sendMessage(message);
                break;
            default:
                msg.obj = device.getName() + "不能设置温度";
                mHandler.sendMessage(msg);
                break;
        }
    }

    private static int parseResult(String result) {
        JSONObject resultjson = JsonUitls.getJSONObject(result);
        String code = JsonUitls.getString(resultjson, "code");
        if ("0".equals(code)) {
            JSONArray resultarray = JsonUitls.getJSONArray(resultjson, "result");
            if (resultarray.length() > 0) {
                try {
                    int anInt = resultarray.getInt(0);
                    return anInt;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return -100;
    }

    private static int getIntentInt(JSONArray slots) {
        int value_set_int = -100;
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if ("整数".equals(name)) {
                value_set_int = JsonUitls.getInt(item, "value");
                break;
            }
        }
        return value_set_int;
    }

    private static String getIntentString(JSONArray slots, String namestr) {
        String value = "";
        for (int i = 0; i < slots.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(slots, i);
            String name = JsonUitls.getString(item, "name");
            if (namestr.equals(name)) {
                value = JsonUitls.getString(item, "value");
                break;
            }
        }
        return value;
    }
}