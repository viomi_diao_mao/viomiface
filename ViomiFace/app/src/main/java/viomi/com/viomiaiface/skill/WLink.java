package viomi.com.viomiaiface.skill;

import android.os.Handler;
import android.os.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.config.FaceConfig;
import viomi.com.viomiaiface.config.HandlerMsgWhat;
import viomi.com.viomiaiface.model.UserEntity;
import viomi.com.viomiaiface.model.WLinkDeviceBean;
import viomi.com.viomiaiface.utils.FileUtil;
import viomi.com.viomiaiface.utils.HttpApi;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * Created by Mocc on 2018/6/26
 */
public class WLink {

    private static String BaseUrl = "http://ms.viomi.com.cn";
    private static String TAG = "WLink";

    public static void handleSkill(String data, Handler mHandler) {

        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        if (user == null) {
            msg.obj = "请先登录云米账号";
            mHandler.sendMessage(msg);
            return;
        }

        JSONObject rawjson = JsonUitls.getJSONObject(data);
        String intentName = JsonUitls.getString(rawjson, "intentName");
        if ("".equals(intentName)) return;

        getWLinkDevices(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.e(TAG, e.toString() + "");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                LogUtils.e(TAG, result + "");
                List<WLinkDeviceBean> linkDeviceBeans = parseDevices(result);

                switch (intentName) {
                    case "打开窗帘": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "open");
                        map.put("parameter", "0");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"80"});
                        controlResult(mHandler, hasdevice, "打开窗帘", "窗帘");
                    }
                    break;

                    case "关闭窗帘": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "close");
                        map.put("parameter", "100");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"80"});
                        controlResult(mHandler, hasdevice, "关闭窗帘", "窗帘");
                    }
                    break;

                    case "打开门锁": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "unLock");
                        map.put("parameter", "000000");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"Bc"});
                        controlResult(mHandler, hasdevice, "打开门锁", "门锁");
                    }
                    break;

                    case "开关开启": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "turnOn");
                        map.put("endpointNumber", "1");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "2");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "3");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        controlResult(mHandler, hasdevice, "打开开关", "开关");
                    }
                    break;

                    case "开关关闭": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "turnOff");
                        map.put("endpointNumber", "1");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "2");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "3");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        controlResult(mHandler, hasdevice, "关闭开关", "开关");
                    }
                    break;

                    case "打开插座": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "turnOn");
                        map.put("endpointNumber", "1");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "2");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "3");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        controlResult(mHandler, hasdevice, "打开插座", "插座");
                    }
                    break;

                    case "关闭插座": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "turnOff");
                        map.put("endpointNumber", "1");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "2");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "3");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        controlResult(mHandler, hasdevice, "关闭插座", "插座");
                    }
                    break;


                    //上线时屏蔽
                    case "开灯": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "turnOn");
                        map.put("endpointNumber", "1");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "2");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "3");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        controlResult(mHandler, hasdevice, "开灯", "灯");
                    }
                    break;

                    //上线时屏蔽
                    case "关灯": {
                        Map<String, String> map = new HashMap<>();
                        map.put("method", "turnOff");
                        map.put("endpointNumber", "1");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "2");
                        control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        map.put("endpointNumber", "3");
                        boolean hasdevice = control(linkDeviceBeans, map, new String[]{"An", "62", "Ao"});
                        controlResult(mHandler, hasdevice, "关灯", "灯");
                    }
                    break;

                    default:
                        break;
                }
            }
        });
    }

    private static void getWLinkDevices(Callback callback) {
        String url = BaseUrl + "/wlink/device/children";
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        Map<String, String> map = new HashMap<>();
        map.put("token", user.getViomiToken());
        HttpApi.getRequestParam(url, map, callback);
    }

    private static List<WLinkDeviceBean> parseDevices(String result) {

        JSONObject rawjson = JsonUitls.getJSONObject(result);
        JSONObject mobBaseRes = JsonUitls.getJSONObject(rawjson, "mobBaseRes");
        JSONArray resultArray = JsonUitls.getJSONArray(mobBaseRes, "result");

        List<WLinkDeviceBean> linkDeviceBeans = new ArrayList<>();
        for (int i = 0; i < resultArray.length(); i++) {
            JSONObject item = JsonUitls.getJSONObject(resultArray, i);
            String gwID = JsonUitls.getString(item, "gwID");
            String devID = JsonUitls.getString(item, "devID");
            String devName = JsonUitls.getString(item, "devName");
            String type = JsonUitls.getString(item, "type");
            String mode = JsonUitls.getString(item, "mode");
            linkDeviceBeans.add(new WLinkDeviceBean(gwID, devID, devName, type, mode));
        }
        return linkDeviceBeans;
    }

    private static boolean control(List<WLinkDeviceBean> linkDeviceBeans, Map<String, String> map, String[] dtypes) {
        boolean hasdevice = false;
        for (int i = 0; i < linkDeviceBeans.size(); i++) {
            WLinkDeviceBean wLinkDeviceBean = linkDeviceBeans.get(i);
            for (int j = 0; j < dtypes.length; j++) {
                if (dtypes[j].equals(wLinkDeviceBean.getType())) {
                    hasdevice = true;
                    map.put("devID", wLinkDeviceBean.getDevID());
                    map.put("gwID", wLinkDeviceBean.getGwID());
                    map.put("type", dtypes[j]);
                    controlWLink(map);
                }
            }
        }
        return hasdevice;
    }

    private static void controlResult(Handler mHandler, boolean hasdevice, String action, String devcie) {
        Message msg = mHandler.obtainMessage();
        msg.what = HandlerMsgWhat.MSG13;
        if (hasdevice) {
            msg.obj = "正在为你" + action;
        } else {
            msg.obj = "没有连接到" + devcie;
        }
        mHandler.sendMessage(msg);
    }

    private static void controlWLink(Map<String, String> paramsMap) {
        UserEntity user = (UserEntity) FileUtil.getObject(BaseApplication.getAppContext(), FaceConfig.USERFILENAME);
        String url = BaseUrl + "/wlink/action/execute?token=" + user.getViomiToken();
        HttpApi.postRequest(url, paramsMap, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

}
