package viomi.com.viomiaiface.skill.handle;

import android.content.Context;
import android.os.Handler;

import com.miot.common.abstractdevice.AbstractDevice;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.skill.AitekWeatherSkill;
import viomi.com.viomiaiface.skill.ChildSongSkill;
import viomi.com.viomiaiface.skill.ChineseStudySkill;
import viomi.com.viomiaiface.skill.ControlManager;
import viomi.com.viomiaiface.skill.CookSkill;
import viomi.com.viomiaiface.skill.FolkArtSkill;
import viomi.com.viomiaiface.skill.FunSkill;
import viomi.com.viomiaiface.skill.InsideMusicSkill;
import viomi.com.viomiaiface.skill.LTNewsSkill;
import viomi.com.viomiaiface.skill.NewWeatherSkill;
import viomi.com.viomiaiface.skill.StorySkill;
import viomi.com.viomiaiface.skill.ToyChineseStudySkill;
import viomi.com.viomiaiface.skill.ToyFunSkill;
import viomi.com.viomiaiface.skill.ToyMusicSkill;
import viomi.com.viomiaiface.skill.ToyNewsSkill;
import viomi.com.viomiaiface.skill.ToyStorySkill;
import viomi.com.viomiaiface.skill.ViomiCookerHood;
import viomi.com.viomiaiface.skill.ViomiDishWasher;
import viomi.com.viomiaiface.skill.ViomiDrinkBar;
import viomi.com.viomiaiface.skill.ViomiFan;
import viomi.com.viomiaiface.skill.ViomiFridge;
import viomi.com.viomiaiface.skill.ViomiOven;
import viomi.com.viomiaiface.skill.ViomiSceneModel;
import viomi.com.viomiaiface.skill.ViomiVacuum;
import viomi.com.viomiaiface.skill.ViomiWasher;
import viomi.com.viomiaiface.skill.ViomiWaterPurifier;
import viomi.com.viomiaiface.skill.WLink;
import viomi.com.viomiaiface.utils.DeviceUtil;
import viomi.com.viomiaiface.utils.JsonUitls;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * Created by Mocc on 2018/2/27
 */

public class HandleSkill {
    private String TAG = "HandleSkill";
    private static final String VIOMISCENE_ID = "2018070200000039";//云米场景模式
    private static final String AITEK_WEATHER_ID = "2018032300000017";
    private static final String COOK_MENU_ID = "2018080800000021";
    private static final String TOY_NEWS_ID = "2017122600000017";
    private static final String TOY_STORY_ID = "2017120200000007";
    private static final String TOY_FUN_ID = "2017120200000010";
    private static final String TOY_TRANSLATE_ID = "2017120200000009";
    private static final String TOY_CHINESE_STUDY_ID = "2017120200000008";
    private static final String LT_NEWS_ID = "100001825";
    private static final String TOY_MUSIC_ID = "2018050900000076";

    private static final String WLINK_ID = "2018061300000038";//云米物联设备控制
    private static final String REMID_ID = "2018032100000037";
    private static final String NEW_WEAHER_ID = "2018081400000044";
    private static final String INSIDE_MUSIC_ID = "2018080800000015";
    private static final String CHINESE_STUDY_ID = "2018060500000011";
    private static final String FOLK_ART_ID = "2018041600000006";
    private static final String FUN_ID = "100001760";
    private static final String CHILD_SONG_ID = "2018082200000085";
    private static final String STORY_ID = "2018040200000012";

    private static final String DRINKBAR = "云米饮水吧";
    private static final String PURIFIER = "云米净水器";
    private static final String FRIGE = "云米冰箱";
    private static final String HOOD = "云米油烟机";
    private static final String DISHWASHER = "云米洗碗机";
    private static final String WASHER = "云米洗衣机";

    private static final String OVEN = "云米蒸烤机";
    private static final String VACUUM = "云米扫地机";
    private static final String FAN = "云米风扇";
    private static final String CONTROL = "云米小V控制";

    private Context context;
    private Handler mHandler;
    private List<AbstractDevice> mOnlinedevices = new ArrayList<>();
    private static volatile HandleSkill instance;

    private HandleSkill(Context context) {
        this.context = context;
    }

    public static HandleSkill getInstance() {
        if (instance == null) {
            synchronized (HandleSkill.class) {
                if (instance == null) {
                    instance = new HandleSkill(BaseApplication.getAppContext());
                }
            }
        }
        return instance;
    }

    public void setmHandler(Handler mHandler) {
        this.mHandler = mHandler;
    }

    public void setmOnlinedevices(List<AbstractDevice> mOnlinedevices) {
        this.mOnlinedevices = mOnlinedevices;
    }

    public void handleNlu(String data) {
        JSONObject jsonData = JsonUitls.getJSONObject(data);
        JSONObject nlu = JsonUitls.getJSONObject(jsonData, "nlu");
        String skill = JsonUitls.getString(nlu, "skill");
        LogUtils.e(TAG, "handleNlu-skill:" + skill);
        LogUtils.e(TAG, "handleNlu-data:" + data);
        switch (skill) {
            case DRINKBAR: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.DRINKBAR);
                AbstractDevice adevice = null;
                if (adevices.size() > 0) adevice = adevices.get(0);
                ViomiDrinkBar.handleSkill(data, mHandler, adevice);
            }
            break;

            case VACUUM: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.VACUUM);
                AbstractDevice adevice = null;
                if (adevices.size() > 0) adevice = adevices.get(0);
                ViomiVacuum.handleSkill(data, mHandler, adevice);

            }
            break;

            case FAN: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.FAN);
                AbstractDevice adevice = null;
                if (adevices.size() > 0) adevice = adevices.get(0);
                ViomiFan.handleSkill(data, mHandler, adevice);
            }
            break;

            case OVEN: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.OVEN);
                AbstractDevice adevice = null;
                if (adevices.size() > 0) adevice = adevices.get(0);
                ViomiOven.handleSkill(data, mHandler, adevice);
            }
            break;

            case WASHER: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.WASHER);
                AbstractDevice adevice = null;
                if (adevices.size() > 0) adevice = adevices.get(0);
                ViomiWasher.handleSkill(data, mHandler, adevice);
            }
            break;

            case DISHWASHER: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.DISHWAHSER);
                AbstractDevice adevice = null;
                if (adevices.size() > 0) adevice = adevices.get(0);
                ViomiDishWasher.handleSkill(data, mHandler, adevice);
            }
            break;

            case HOOD: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.HOOD);
                AbstractDevice adevice = DeviceUtil.getHoodFirstControl(adevices);
                ViomiCookerHood.handleSkill(data, mHandler, adevice);
            }
            break;

            case PURIFIER: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.WATERPURIFIER);
                AbstractDevice adevice = DeviceUtil.getPuriFirstControl(adevices);
                ViomiWaterPurifier.handleSkill(data, mHandler, adevice);
            }
            break;

            case FRIGE: {
                List<AbstractDevice> adevices = DeviceUtil.filterDevices(mOnlinedevices, DeviceUtil.FRIGE);
                AbstractDevice adevice = DeviceUtil.getFridgeFirstControl(adevices);
                ViomiFridge.handleSkill(data, mHandler, adevice);
            }
            break;

            case CONTROL: {
                ControlManager.handleSkill(data, mHandler, context);
            }
            break;

            default:
                break;
        }
    }

    public void handleNlp(String data) {
        JSONObject jsonData = JsonUitls.getJSONObject(data);
        String skillId = JsonUitls.getString(jsonData, "skillId");
        LogUtils.e(TAG, "handleNlp-skillId:" + skillId);
        LogUtils.e(TAG, "handleNlp-data:" + data);

        String recordId= JsonUitls.getString(jsonData, "recordId");
        if (!"".equals(recordId)) {
            return;
        }

        switch (skillId) {
            case VIOMISCENE_ID: {
                ViomiSceneModel.handleSkill(data, mHandler);
            }
            break;

            case AITEK_WEATHER_ID: {
                AitekWeatherSkill.handleSkill(data, mHandler);
            }
            break;

            case LT_NEWS_ID: {
                LTNewsSkill.handleSkill(data, context);
            }
            break;

            case TOY_NEWS_ID: {
                ToyNewsSkill.handleSkill(data, context);
            }
            break;

            case TOY_STORY_ID: {
                ToyStorySkill.handleSkill(data, context);
            }
            break;

            case TOY_FUN_ID: {
                ToyFunSkill.handleSkill(data, context);
            }
            break;

            case TOY_TRANSLATE_ID: {
            }
            break;

            case TOY_CHINESE_STUDY_ID: {
                ToyChineseStudySkill.handleSkill(data, context);
            }
            break;

            case TOY_MUSIC_ID: {
                ToyMusicSkill.handleSkill(data, context);
            }
            break;

            case COOK_MENU_ID: {
                CookSkill.handleSkill(data,context );
            }
            break;

            case WLINK_ID: {
                WLink.handleSkill(data, mHandler);
            }
            break;

            case NEW_WEAHER_ID: {
                NewWeatherSkill.handleSkill(data, mHandler);
            }
            break;

            case INSIDE_MUSIC_ID: {
                InsideMusicSkill.handleSkill(data,context );
            }
            break;

            case CHINESE_STUDY_ID: {
                ChineseStudySkill.handleSkill(data,context );
            }
            break;

            case FOLK_ART_ID: {
                FolkArtSkill.handleSkill(data,context );
            }
            break;

            case FUN_ID: {
                FunSkill.handleSkill(data,context );
            }
            break;

            case CHILD_SONG_ID: {
                ChildSongSkill.handleSkill(data,context );
            }
            break;

            case STORY_ID: {
                StorySkill.handleSkill(data,context );
            }
            break;



            case REMID_ID:

//                ReMindBean reMindBean = ReMindBean.reMindBeansResolver(jsonData);
//
//                if (!"".equals(reMindBean.getDate()) && !"".equals(reMindBean.getTime())) {
//                    String dateStr = reMindBean.getDate() + " " + reMindBean.getTime();
//                    long alarmtime = DateUtils.getTimestamp(dateStr);
//                    if (alarmtime > System.currentTimeMillis()) {
//                        SetAlarm.getInstance().setAlarm(alarmtime);
//                        Message message = mHandler.obtainMessage();
//                        message.what = HandlerMsgWhat.MSG9;
//                        message.obj = alarmtime;
//                        mHandler.sendMessage(message);
//                    }
//                }
//
//                if ("".equals(reMindBean.getDate()) && !"".equals(reMindBean.getTime())) {
//                    long current = System.currentTimeMillis();
//                    long zero = current / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();
//                    long alarmtime = zero + DateUtils.getStringTomm(reMindBean.getTime());
//                    if (alarmtime > System.currentTimeMillis()) {
//                        SetAlarm.getInstance().setAlarm(alarmtime);
//                        Message message = mHandler.obtainMessage();
//                        message.what = HandlerMsgWhat.MSG9;
//                        message.obj = alarmtime;
//                        mHandler.sendMessage(message);
//                    }
//                }
//
//                if (!"".equals(reMindBean.getRelativeTime())) {
//                    long alarmtime = System.currentTimeMillis();
//                    alarmtime += DateUtils.getStringTomm(reMindBean.getRelativeTime());
//                    if (alarmtime > System.currentTimeMillis()) {
//                        SetAlarm.getInstance().setAlarm(alarmtime);
//                        Message message = mHandler.obtainMessage();
//                        message.what = HandlerMsgWhat.MSG9;
//                        message.obj = alarmtime;
//                        mHandler.sendMessage(message);
//                    }
//                }
//                LogUtils.e(TAG, reMindBean.toString());
                break;

            default:
                break;
        }
    }
}
