package viomi.com.viomiaiface.subdevices;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseActivity;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.subdevices.adapter.BaseRecyclerViewAdapter;
import viomi.com.viomiaiface.subdevices.adapter.DishWashingAdapter;
import viomi.com.viomiaiface.subdevices.adapter.FanAdapter;
import viomi.com.viomiaiface.subdevices.adapter.HeatKettleAdapter;
import viomi.com.viomiaiface.subdevices.adapter.RangeHoodAdapter;
import viomi.com.viomiaiface.subdevices.adapter.SweepingRobotAdapter;
import viomi.com.viomiaiface.subdevices.adapter.WashingMachineAdapter;
import viomi.com.viomiaiface.subdevices.adapter.WaterPurifierAdapter;
import viomi.com.viomiaiface.subdevices.gallery.AnimManager;
import viomi.com.viomiaiface.subdevices.gallery.GalleryRecyclerView;

/**
 * 设备卡片 Activity
 * Created by William on 2018/7/4.
 */
public class DeviceGalleryActivity extends BaseActivity {

    private GalleryRecyclerView mGalleryRecyclerView;
    private ImageView icon_close;

    protected void initView() {
        setContentView(R.layout.activity_device_gallery);
        mGalleryRecyclerView = findViewById(R.id.device_gallery_list);
        icon_close = findViewById(R.id.device_gallery_close);

        int type = getIntent().getIntExtra("DEVICE_GALLERY_TYPE", -1);// 设备类型
        if (getIntent().getExtras() == null) return;
        List<AbstractDevice> list = getIntent().getExtras().getParcelableArrayList("DEVICE_GALLERY_LIST");// 设备集合
        LinearLayoutManager layoutManager = new LinearLayoutManager(BaseApplication.getAppContext(), LinearLayoutManager.HORIZONTAL, false);
        mGalleryRecyclerView.setLayoutManager(layoutManager);
        mGalleryRecyclerView.setAdapter(switchAdapter(type, list));
        mGalleryRecyclerView.initFlingSpeed(9000)                           // 设置滑动速度（像素 / s）
                .initPageParams(40, 230) // 设置页边距和左右 item 的可见宽度，单位 dp
                .setAnimFactor(0.15f)                                      // 设置切换动画的参数因子
                .setAnimType(AnimManager.ANIM_BOTTOM_TO_TOP);             // 设置切换动画类型，目前有 AnimManager.ANIM_BOTTOM_TO_TOP 和 AnimManager.ANIM_TOP_TO_BOTTOM
        mGalleryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (list != null) {
                        String did = "Did：" + list.get(mGalleryRecyclerView.getScrolledPosition()).getDeviceId();
                    }
                }
            }
        });
    }

    @Override
    protected void initListener() {
        icon_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGalleryRecyclerView.setAdapter(new RecyclerView.Adapter() {
                    @Override
                    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                        return null;
                    }

                    @Override
                    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

                    }

                    @Override
                    public int getItemCount() {
                        return 0;
                    }
                });
                finish();
            }
        });
    }

    @Override
    protected void init() {

    }

    /**
     * 根据设备类型选择适配器
     */
    private BaseRecyclerViewAdapter switchAdapter(int type, List<AbstractDevice> list) {
        BaseRecyclerViewAdapter adapter = null;
        switch (type) {

            case 0: // 冰箱
                finish();
                break;
            case 1: // 净水器
                adapter = new WaterPurifierAdapter(getTypeDevices(list, type));
                break;
            case 2: // 即热饮水吧
                adapter = new HeatKettleAdapter(getTypeDevices(list, type));
                break;
            case 3: // 烟灶
                adapter = new RangeHoodAdapter(getTypeDevices(list, type));
                break;
            case 4: // 洗衣机
                adapter = new WashingMachineAdapter(getTypeDevices(list, type));
                break;
            case 5: // 洗碗机
                adapter = new DishWashingAdapter(getTypeDevices(list, type));
                break;
            case 6: // 风扇
                adapter = new FanAdapter(getTypeDevices(list, type));
                break;
            case 7: // 扫地机器人
                adapter = new SweepingRobotAdapter(getTypeDevices(list, type));
                break;
            case 8: // 蒸烤箱
                finish();
                break;
            case 9: // 热水器
                finish();
                break;
            case 10: // 养生壶
                finish();
                break;
            case 11: // 中央软水机
                finish();
                break;
            case 12: // 香熏机
                finish();
                break;
            case 13: // 空气净化器
                finish();
                break;
            default:
                finish();
                break;
        }
        return adapter;
    }

    private List<AbstractDevice> getTypeDevices(List<AbstractDevice> list, int type) {

        List<AbstractDevice> rawlist = getRawTypeDevices(list, type);

        if (rawlist != null && rawlist.size() > 0) {
            return rawlist;
        } else {
            finish();
            return null;
        }
    }

    private List<AbstractDevice> getRawTypeDevices(List<AbstractDevice> list, int type) {
        List<AbstractDevice> list0 = new ArrayList<>();//冰箱
        List<AbstractDevice> list1 = new ArrayList<>();//净水器
        List<AbstractDevice> list2 = new ArrayList<>();//即热饮水吧
        List<AbstractDevice> list3 = new ArrayList<>();//烟灶
        List<AbstractDevice> list4 = new ArrayList<>();//洗衣机
        List<AbstractDevice> list5 = new ArrayList<>();//洗碗机
        List<AbstractDevice> list6 = new ArrayList<>();//风扇
        List<AbstractDevice> list7 = new ArrayList<>();//扫地机器人

        for (int i = 0; i < list.size(); i++) {

            switch (list.get(i).getDeviceModel()) {
                case Miconfig.VIOMI_FRIDGE_U1:// 冰箱ilive
                case Miconfig.VIOMI_FRIDGE_U2:// 冰箱iLive 对开门版
                case Miconfig.VIOMI_FRIDGE_V1:// 冰箱 iLive语音版
                case Miconfig.VIOMI_FRIDGE_V2:// 冰箱iLive四门语音版
                case Miconfig.VIOMI_FRIDGE_V3:// 462大屏金属门冰箱
                case Miconfig.VIOMI_FRIDGE_V4:// 455大屏玻璃门冰箱
                case Miconfig.VIOMI_FRIDGE_X1:// 云米三门大屏冰箱X1
                case Miconfig.VIOMI_FRIDGE_X2:// 云米四门大屏冰箱428
                case Miconfig.VIOMI_FRIDGE_X3:// 云米大屏冰箱462
                case Miconfig.VIOMI_FRIDGE_X4:// 云米对开门大屏冰箱456
                case Miconfig.VIOMI_FRIDGE_X5:// 云米大屏冰箱521
                    list0.add(list.get(i));
                    break;

                case Miconfig.YUNMI_WATERPURI_V1:// V1 乐享版 人工智能系列
                case Miconfig.YUNMI_WATERPURI_V2:// V1 尊享版 人工智能系列
                case Miconfig.YUNMI_WATERPURI_S1:// S1 优享版 智能 3 合 1 系列
                case Miconfig.YUNMI_WATERPURI_S2:// S1 标准版 智能 3 合 1 系列
                case Miconfig.YUNMI_WATERPURI_C1:// C1 厨上版 新鲜水系列
                case Miconfig.YUNMI_WATERPURI_C2:// C1 厨下版 新鲜水系列
                case Miconfig.YUNMI_WATERPURI_X3:// 即热直饮净水器 X3 (100G)
                case Miconfig.YUNMI_WATERPURI_X5:// 即热直饮净水器 X5 (400G)
                case Miconfig.YUNMI_WATERPURI_n1:// 云米互联网净水器 Mee Pro
                case Miconfig.YUNMI_WATERPURI_n2:// 云米互联网净水器 Mee
                case Miconfig.YUNMI_WATERPURI_n3:// 云米互联网净水器 Mee（签名版）
                    list1.add(list.get(i));
                    break;

                case Miconfig.YUNMI_KETTLE_R1:// 云米智能即热饮水吧（MINI）
                case Miconfig.YUNMI_KETTLE_R2:// 云米智能即热饮水吧（MINI）
                case Miconfig.YUNMI_KETTLE_R3:// 云米智能即热饮水吧（MINI）
                    list2.add(list.get(i));
                    break;

                case Miconfig.VIOMI_HOOD_A4:// 智能油烟机 Hurri
                case Miconfig.VIOMI_HOOD_A5:// 智能油烟机 Free
                case Miconfig.VIOMI_HOOD_A6:// 智能油烟机 Hurri 语音版
                case Miconfig.VIOMI_HOOD_A7:// 智能油烟机 Free 语音版
                case Miconfig.VIOMI_HOOD_C1:// 云米智能油烟机 Cross
                case Miconfig.VIOMI_HOOD_C6:// 经济型侧吸机
                case Miconfig.VIOMI_HOOD_H1:// 云米互联网智能油烟机 Hurri尊享
                case Miconfig.VIOMI_HOOD_H2:// 云米互联网智能油烟机Hurri标准
                case Miconfig.VIOMI_HOOD_T8:// 标准型 T 型机
                case Miconfig.VIOMI_HOOD_X1:// 云米油烟机X1
                case Miconfig.VIOMI_HOOD_X2:// 云米油烟机X2
                case Miconfig.VIOMI_HOOD_X3:// 云米油烟机X3
                    list3.add(list.get(i));
                    break;

                case Miconfig.VIOMI_WASHER_U1:// 云米互联网洗衣机（9KG 语音版）
                    list4.add(list.get(i));
                    break;

                case Miconfig.VIOMI_DISHWASHER_V01:// 云米互联网洗碗机（8套嵌入式）
                    list5.add(list.get(i));
                    break;

                case Miconfig.VIOMI_FAN_V1:// 云米互联网直流变频风扇
                    list6.add(list.get(i));
                    break;

                case Miconfig.VIOMI_VACUUM_V1:// 云米互联网扫地机器人
                    list7.add(list.get(i));
                    break;
                default:
                    break;
            }
        }
        switch (type) {
            case 0:
                return list0;
            case 1:
                return list1;
            case 2:
                return list2;
            case 3:
                return list3;
            case 4:
                return list4;
            case 5:
                return list5;
            case 6:
                return list6;
            case 7:
                return list7;
            default:
                return null;
        }
    }

}