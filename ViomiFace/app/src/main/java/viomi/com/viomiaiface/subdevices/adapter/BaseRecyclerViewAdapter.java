package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.widget.AdapterView;

/**
 * 全局 RecyclerView 适配器
 * Created by William on 2018/1/22.
 */
public abstract class BaseRecyclerViewAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {
    private AdapterView.OnItemClickListener onItemClickListener;
    // 两次点击间隔不能少于 1000 毫秒
    private long lastClickTime;

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    protected void onItemHolderClick(RecyclerView.ViewHolder itemHolder, int minTime) {
        if (onItemClickListener != null) {
            long curClickTime = System.currentTimeMillis();
            if ((curClickTime - lastClickTime) >= minTime) {
                onItemClickListener.onItemClick(null, itemHolder.itemView,
                        itemHolder.getAdapterPosition(), itemHolder.getItemId());
            }
            lastClickTime = curClickTime;
        } else {
            throw new IllegalStateException("Please call setOnItemClickListener method set the click event listeners");
        }
    }
}