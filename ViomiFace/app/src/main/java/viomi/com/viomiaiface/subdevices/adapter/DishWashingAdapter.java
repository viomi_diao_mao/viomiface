package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.entity.DishWashingProp;
import viomi.com.viomiaiface.subdevices.repository.DishWashingRepository;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * 洗碗机卡片适配器
 * Created by William on 2018/7/7.
 */
public class DishWashingAdapter extends BaseRecyclerViewAdapter<DishWashingAdapter.HolderView> {
    private static final String TAG = DishWashingAdapter.class.getSimpleName();
    private List<AbstractDevice> mList;
    private SparseArray<Subscription> mSparseArray;

    public DishWashingAdapter(List<AbstractDevice> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
        mSparseArray = new SparseArray<>();
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_dish_washing, parent, false);
        return new HolderView(itemView);
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        AbstractDevice device = mList.get(position);
        holder.count = 0;
        holder.timeTextView.setVisibility(View.GONE);
        holder.nameTextView.setText(device.getName());// 设备名称
        holder.modeTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.saltTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.cleanerTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        if (device.isOnline()) { // 在线
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
            holder.statusTextView.setTextColor(0xFF37D58E);
        } else {
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
            holder.statusTextView.setTextColor(0x4D252525);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onViewAttachedToWindow(HolderView holder) {
        super.onViewAttachedToWindow(holder);
        String did = mList.get(holder.getAdapterPosition()).getDeviceId();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> DishWashingRepository.getProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holder.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holder.count = 0;
                        DishWashingProp prop = new DishWashingProp(rpcResult.getList());
                        holder.statusTextView.setTextColor(0xFF37D58E);
                        holder.statusTextView.setText(DishWashingRepository.switchWashProcess(prop.getWash_process(), prop.getWash_status(), prop.getBespeak_status()));
                        holder.modeTextView.setText(DishWashingRepository.switchProgram(prop.getProgram(), prop.getCustom_program()));
                        holder.saltTextView.setText(prop.getSalt_state() == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_soft_salt_yes) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_soft_salt_no));
                        holder.cleanerTextView.setText(prop.getLdj_state() == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_cleaner_yes) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_cleaner_no));
                        if (prop.getWash_process() == 0 && prop.getBespeak_status() == 1) {
                            holder.timeTextView.setVisibility(View.VISIBLE);
                            String hour = prop.getBespeak_h() < 10 ? "0" + prop.getBespeak_h() : prop.getBespeak_h() + "";
                            String min = prop.getBespeak_min() < 10 ? "0" + prop.getBespeak_min() : prop.getBespeak_min() + "";
                            String str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_book_time) + hour + ":" + min;
                            holder.timeTextView.setText(str);
                        } else if (prop.getWash_process() == 1 || prop.getWash_process() == 2 || prop.getWash_process() == 3 || prop.getWash_process() == 4) {
                            holder.timeTextView.setVisibility(View.VISIBLE);
                            int hour = prop.getLeft_time() / 3600;
                            int min = (prop.getLeft_time() - hour * 3600) / 60;
                            String hour_s = hour < 10 ? "0" + hour : hour + "";
                            String min_s = min < 10 ? "0" + min : min + "";
                            String str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_finish_time) + hour_s + ":" + min_s;
                            holder.timeTextView.setText(str);
                        } else holder.timeTextView.setVisibility(View.GONE);
                    } else { // 数据异常
                        if (holder.count >= 5) {
                            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holder.statusTextView.setTextColor(0x4D252525);
                        } else holder.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(holder.getAdapterPosition(), subscription);
    }

    @Override
    public void onViewDetachedFromWindow(HolderView holder) {
        super.onViewDetachedFromWindow(holder);
        if (mSparseArray.get(holder.getAdapterPosition()) != null) {
            mSparseArray.get(holder.getAdapterPosition()).unsubscribe();
            mSparseArray.remove(holder.getAdapterPosition());
        }
    }

    class HolderView extends RecyclerView.ViewHolder {
        int count = 0;

        @BindView(R.id.dish_washing_title)
        TextView nameTextView;// 设备名称
        @BindView(R.id.dish_washing_status)
        TextView statusTextView;// 设备状态
        @BindView(R.id.dish_washing_mode)
        TextView modeTextView;// 洗涤模式
        @BindView(R.id.dish_washing_salt)
        TextView saltTextView;// 软化盐
        @BindView(R.id.dish_washing_cleaner)
        TextView cleanerTextView;// 漂洗剂
        @BindView(R.id.dish_washing_time)
        TextView timeTextView;// 预约开始时间

        HolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}