package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.subdevices.http.entity.HeatKettleProp;
import viomi.com.viomiaiface.subdevices.repository.HeatKettleRepository;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * 即热饮水吧卡片适配器
 * Created by William on 2018/7/7.
 */
public class HeatKettleAdapter extends BaseRecyclerViewAdapter<HeatKettleAdapter.HolderView> {
    private static final String TAG = HeatKettleAdapter.class.getSimpleName();
    private List<AbstractDevice> mList;
    private SparseArray<Subscription> mSparseArray;
    private Subscription mSubscription;

    public HeatKettleAdapter(List<AbstractDevice> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
        mSparseArray = new SparseArray<>();
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_heat_kettle, parent, false);
        return new HolderView(itemView);
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        holder.seekBar.setOnSeekBarChangeListener(null);
        AbstractDevice device = mList.get(position);
        holder.nameTextView.setText(device.getName());// 设备名称
        holder.count = 0;
        holder.isSetting = false;
        holder.minTemp = 40;
        holder.seekBar.setMax(90 - holder.minTemp);
        holder.tempTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.tempDescTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_heat_kettle_temp));
        holder.tdsTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.setTempTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.seekBar.setProgress(0);
        if (device.isOnline()) { // 在线
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
            holder.statusTextView.setTextColor(0xFF37D58E);
            holder.seekBar.setEnabled(true);
        } else {
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
            holder.statusTextView.setTextColor(0x4D252525);
            holder.seekBar.setEnabled(false);
        }
        // 解决滑动冲突
        holder.seekBar.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
        holder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                holder.setTempTextView.setText(HeatKettleRepository.switchTempTip(holder.minTemp + progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                holder.isSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                setTemp(device.getDeviceId(), holder.minTemp + seekBar.getProgress(), holder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    @Override
    public void onViewAttachedToWindow(HolderView holder) {
        super.onViewAttachedToWindow(holder);
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> HeatKettleRepository.getProp(mList.get(holder.getAdapterPosition()).getDeviceId()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holder.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holder.count = 0;
                        holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holder.statusTextView.setTextColor(0xFF37D58E);
                        holder.seekBar.setEnabled(true);
                        HeatKettleProp prop = new HeatKettleProp(rpcResult.getList());
                        if (mList.get(holder.getAdapterPosition()).getDeviceModel().equals(Miconfig.YUNMI_KETTLE_R1))
                            holder.minTemp = prop.getMin_set_tempe();
                        holder.seekBar.setMax(90 - holder.minTemp);
                        holder.tempTextView.setText(String.valueOf(prop.getSetup_tempe()));
                        holder.tempDescTextView.setText(HeatKettleRepository.switchMode(prop.getWork_mode()));
                        holder.tdsTextView.setText(String.format(BaseApplication.getAppContext().getResources().getString(R.string.iot_heat_kettle_tds_value), prop.getTds()));
                        if (!holder.isSetting) {
                            holder.seekBar.setProgress(prop.getCustom_tempe1() - holder.minTemp);
                            holder.setTempTextView.setText(HeatKettleRepository.switchTempTip(prop.getCustom_tempe1()));
                        }
                    } else { // 数据异常
                        if (holder.count >= 5) {
                            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holder.statusTextView.setTextColor(0x4D252525);
                        } else holder.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(holder.getAdapterPosition(), subscription);
    }

    @Override
    public void onViewDetachedFromWindow(HolderView holder) {
        super.onViewDetachedFromWindow(holder);
        if (mSparseArray.get(holder.getAdapterPosition()) != null) {
            mSparseArray.get(holder.getAdapterPosition()).unsubscribe();
            mSparseArray.remove(holder.getAdapterPosition());
        }
    }

    /**
     * 温水键温度设置
     */
    private void setTemp(String did, int temp, HolderView holderView) {
        mSubscription = HeatKettleRepository.setTemp(did, temp)
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> holderView.isSetting = false, throwable -> {
                    holderView.isSetting = false;
                });
    }

    class HolderView extends RecyclerView.ViewHolder {
        boolean isSetting = false;
        int minTemp = 40;
        int count = 0;

        @BindView(R.id.heat_kettle_title)
        TextView nameTextView;// 设备名称
        @BindView(R.id.heat_kettle_status)
        TextView statusTextView;// 设备状态
        @BindView(R.id.heat_kettle_temp)
        TextView tempTextView;// 出水温度
        @BindView(R.id.heat_kettle_temp_desc)
        TextView tempDescTextView;// 出水温度描述
        @BindView(R.id.heat_kettle_tds_value)
        TextView tdsTextView;// TDS 值
        @BindView(R.id.heat_kettle_set_temp)
        TextView setTempTextView;// 温水键温度

        @BindView(R.id.heat_kettle_seek_bar)
        SeekBar seekBar;// 温水键温度设置

        HolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}