package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.RxSchedulerUtil;
import viomi.com.viomiaiface.subdevices.http.entity.RangeHoodProp;
import viomi.com.viomiaiface.subdevices.repository.RangeHoodRepository;
import viomi.com.viomiaiface.subdevices.weight.SwitchButton;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * 烟机卡片适配器
 * Created by William on 2018/7/7.
 */
public class RangeHoodAdapter extends BaseRecyclerViewAdapter<RangeHoodAdapter.HolderView> {
    private static final String TAG = RangeHoodAdapter.class.getSimpleName();
    private List<AbstractDevice> mList;
    private SparseArray<Subscription> mSparseArray;
    private CompositeSubscription mCompositeSubscription;

    public RangeHoodAdapter(List<AbstractDevice> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
        mSparseArray = new SparseArray<>();
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_range_hood, parent, false);
        return new HolderView(view);
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        holder.switchButton.setOnSwitchStateChangeListener(null);
        AbstractDevice device = mList.get(position);
        holder.count = 0;
        holder.nameTextView.setText(device.getName());// 设备名称
        holder.isIgnore = false;
        holder.isSetting = false;
        holder.delayTime = 0;
        holder.switchButton.setOn(false);
        holder.modeTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.switchTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.open));
        if (device.isOnline()) { // 在线
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
            holder.statusTextView.setTextColor(0xFF37D58E);
            holder.switchButton.setEnabled(true);
            holder.switchTextView.setEnabled(true);
            holder.switchTextView.setBackgroundResource(R.drawable.btn_bg_green_bottom_corner_20);
        } else {
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
            holder.statusTextView.setTextColor(0x4D252525);
            holder.switchButton.setEnabled(false);
            holder.switchTextView.setEnabled(false);
            holder.switchTextView.setBackgroundResource(R.drawable.btn_bg_gray_bottom_corner_20);
        }
        holder.switchTextView.setOnClickListener(v -> setPower(holder.delayTime, device.getDeviceId(), holder));
        holder.switchButton.setOnSwitchStateChangeListener(isOn -> {
            if (holder.isIgnore) return;
            holder.isSetting = true;
            setLight(device.getDeviceId(), holder);
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void onViewAttachedToWindow(HolderView holder) {
        super.onViewAttachedToWindow(holder);
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> RangeHoodRepository.getProp(mList.get(holder.getAdapterPosition()).getDeviceId()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holder.statusTextView.setTextColor(0x4D252525);
                        holder.switchButton.setEnabled(false);
                        holder.switchTextView.setEnabled(false);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holder.count = 0;
                        holder.statusTextView.setTextColor(0xFF37D58E);
                        holder.switchButton.setEnabled(true);
                        holder.switchTextView.setEnabled(true);
                        holder.switchTextView.setBackgroundResource(R.drawable.btn_bg_green_bottom_corner_20);
                        RangeHoodProp prop = new RangeHoodProp(rpcResult.getList());
                        if (prop.getPoweroff_delaytime().equals("error")) holder.delayTime = 0;
                        else holder.delayTime = Integer.valueOf(prop.getPoweroff_delaytime());
                        holder.statusTextView.setText(RangeHoodRepository.switchHoodPower(prop.getPower_state()));
                        holder.modeTextView.setText(RangeHoodRepository.switchHoodStatus(Integer.parseInt(prop.getPower_state()),
                                Integer.parseInt(prop.getWind_state())));
                        if (!holder.isSetting) {
                            holder.isIgnore = true;
                            holder.switchButton.setOn(prop.getLight_state().equals("1"));
                            holder.isIgnore = false;
                        }
                        if (prop.getPower_state().equals("0"))
                            holder.switchTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.open));
                        else
                            holder.switchTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.close));
                    } else { // 数据异常
                        if (holder.count >= 5) {
                            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holder.statusTextView.setTextColor(0x4D252525);
                        } else holder.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(holder.getAdapterPosition(), subscription);
    }

    @Override
    public void onViewDetachedFromWindow(HolderView holder) {
        super.onViewDetachedFromWindow(holder);
        if (mSparseArray.get(holder.getAdapterPosition()) != null) {
            mSparseArray.get(holder.getAdapterPosition()).unsubscribe();
            mSparseArray.remove(holder.getAdapterPosition());
        }
    }

    /**
     * 设置电源开关
     */
    private void setPower(int delay, String did, HolderView holderView) {
        String param;
        if (delay != 0 && !holderView.switchTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.open))
                && !holderView.statusTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_closing_delay)))
            param = "1";// 延时关机
        else if (holderView.switchTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.open)))
            param = "2";// 开机
        else param = "0";// 关机
        Subscription subscription = RangeHoodRepository.setPower(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 设置灯光开关
     */
    private void setLight(String did, HolderView holderView) {
        String param = holderView.switchButton.isOn() ? "1" : "0";
        Subscription subscription = RangeHoodRepository.setLight(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> holderView.isSetting = false, throwable -> {
                    holderView.isSetting = false;
                    LogUtils.e(TAG, throwable.getMessage());
                });
        mCompositeSubscription.add(subscription);
    }

    class HolderView extends RecyclerView.ViewHolder {
        boolean isSetting;
        boolean isIgnore;
        int delayTime;
        int count;

        @BindView(R.id.range_hood_title)
        TextView nameTextView;// 设备名称
        @BindView(R.id.range_hood_status)
        TextView statusTextView;// 设备状态
        @BindView(R.id.range_hood_mode)
        TextView modeTextView;// 工作模式
        @BindView(R.id.range_hood_switch)
        TextView switchTextView;// 烟机开关

        @BindView(R.id.range_hood_light)
        SwitchButton switchButton;// 灯光开关

        HolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}