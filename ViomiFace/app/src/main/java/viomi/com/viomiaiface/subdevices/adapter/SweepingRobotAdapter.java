package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.RxSchedulerUtil;
import viomi.com.viomiaiface.subdevices.http.entity.SweepingRobotProp;
import viomi.com.viomiaiface.subdevices.repository.SweepingRobotRepository;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * 扫地机器人卡片适配器
 * Created by William on 2018/7/7.
 */
public class SweepingRobotAdapter extends BaseRecyclerViewAdapter<SweepingRobotAdapter.HolderView> {
    private static final String TAG = SweepingRobotAdapter.class.getSimpleName();
    private List<AbstractDevice> mList;
    private SparseArray<Subscription> mSparseArray;
    private CompositeSubscription mCompositeSubscription;

    public SweepingRobotAdapter(List<AbstractDevice> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
        mSparseArray = new SparseArray<>();
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_sweeping_robot, parent, false);
        return new HolderView(view);
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        AbstractDevice device = mList.get(position);
        holder.nameTextView.setText(device.getName());// 设备名称
        holder.count = 0;
        holder.modeTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.areaTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.powerTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.timeTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.startTextView.setVisibility(View.VISIBLE);
        holder.backTextView.setVisibility(View.GONE);
        holder.stopTextView.setVisibility(View.GONE);
        holder.view.setVisibility(View.GONE);
        if (device.isOnline()) { // 在线
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
            holder.statusTextView.setTextColor(0xFF37D58E);
            holder.startTextView.setBackgroundResource(R.drawable.btn_bg_green_bottom_corner_20);
            holder.startTextView.setEnabled(true);
            holder.stopTextView.setEnabled(true);
            holder.backTextView.setEnabled(true);
        } else {
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
            holder.statusTextView.setTextColor(0x4D252525);
            holder.startTextView.setBackgroundResource(R.drawable.btn_bg_gray_bottom_corner_20);
            holder.startTextView.setEnabled(false);
            holder.stopTextView.setEnabled(false);
            holder.backTextView.setEnabled(false);
        }
        holder.startTextView.setOnClickListener(view -> setPower(1, device.getDeviceId(), holder));
        holder.stopTextView.setOnClickListener(view -> {
            if (holder.stopTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_stop)))
                setPower(0, device.getDeviceId(), holder);
            else setPower(1, device.getDeviceId(), holder);
        });
        holder.backTextView.setOnClickListener(view -> {
            if (holder.backTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_stop)))
                setPower(0, device.getDeviceId(), holder);
            else setMode(3, device.getDeviceId());
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = null;
        }
    }

    @Override
    public void onViewAttachedToWindow(HolderView holder) {
        super.onViewAttachedToWindow(holder);
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> SweepingRobotRepository.getProp(mList.get(holder.getAdapterPosition()).getDeviceId()))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holder.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holder.count = 0;
                        holder.startTextView.setBackgroundResource(R.drawable.btn_bg_green_bottom_corner_20);
                        holder.startTextView.setEnabled(true);
                        holder.stopTextView.setEnabled(true);
                        holder.backTextView.setEnabled(true);
                        holder.statusTextView.setTextColor(0xFF37D58E);
                        SweepingRobotProp prop = new SweepingRobotProp(rpcResult.getList());
                        holder.statusTextView.setText(SweepingRobotRepository.switchRunState(prop.getRun_state(), prop.getBattary_life()));
                        holder.modeTextView.setText(SweepingRobotRepository.switchMode(prop.getRun_state()));
                        holder.areaTextView.setText(String.format(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_sweep_area_value), prop.getS_area()));
                        holder.powerTextView.setText(String.valueOf(prop.getBattary_life()) + "%");
                        DecimalFormat df = new DecimalFormat("0.0");
                        holder.timeTextView.setText(df.format((prop.getS_time() / 60.0)) + "min");
                        if (prop.getRun_state() == 1 || prop.getRun_state() == 2 || prop.getRun_state() == 4 ||
                                prop.getRun_state() == 8 || prop.getRun_state() == 16 || prop.getRun_state() == 32) {
                            holder.startTextView.setVisibility(View.GONE);
                            holder.view.setVisibility(View.VISIBLE);
                            holder.backTextView.setVisibility(View.VISIBLE);
                            holder.stopTextView.setVisibility(View.VISIBLE);
                        } else {
                            holder.startTextView.setVisibility(View.VISIBLE);
                            holder.view.setVisibility(View.GONE);
                            holder.backTextView.setVisibility(View.GONE);
                            holder.stopTextView.setVisibility(View.GONE);
                        }
                        if (prop.getRun_state() == 32) {
                            holder.backTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_stop));
                        } else {
                            holder.backTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_back));
                        }
                        if (prop.getRun_state() == 32 || prop.getRun_state() == 1 || prop.getRun_state() == 2) {
                            holder.stopTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_clean));
                        } else
                            holder.stopTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_stop));
                    } else { // 数据异常
                        if (holder.count >= 5) {
                            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holder.statusTextView.setTextColor(0x4D252525);
                        } else holder.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(holder.getAdapterPosition(), subscription);
    }

    @Override
    public void onViewDetachedFromWindow(HolderView holder) {
        super.onViewDetachedFromWindow(holder);
        if (mSparseArray.get(holder.getAdapterPosition()) != null) {
            mSparseArray.get(holder.getAdapterPosition()).unsubscribe();
            mSparseArray.remove(holder.getAdapterPosition());
        }
    }

    /**
     * 设置电源开关
     */
    private void setPower(int param, String did, HolderView holderView) {
        Subscription subscription = SweepingRobotRepository.setPower(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (param == 1) {
                        int mode;
                        if (holderView.modeTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_mop))) {
                            mode = 1;
                        } else if (holderView.modeTextView.getText().toString().equals(BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_cleaning))) {
                            mode = 2;
                        } else mode = 0;
                        setMode(mode, did);
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    /**
     * 设置工作模式
     */
    private void setMode(int param, String did) {
        Subscription subscription = SweepingRobotRepository.setMode(param, did)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mCompositeSubscription.add(subscription);
    }

    class HolderView extends RecyclerView.ViewHolder {
        int count;

        @BindView(R.id.sweeping_robot_title)
        TextView nameTextView;// 设备名称
        @BindView(R.id.sweeping_robot_status)
        TextView statusTextView;// 设备状态
        @BindView(R.id.sweeping_robot_mode)
        TextView modeTextView;// 清扫模式
        @BindView(R.id.sweeping_robot_area)
        TextView areaTextView;// 清扫面积
        @BindView(R.id.sweeping_robot_power)
        TextView powerTextView;// 剩余电量
        @BindView(R.id.sweeping_robot_time)
        TextView timeTextView;// 清扫时间
        @BindView(R.id.sweeping_robot_start)
        TextView startTextView;// 清扫开关
        @BindView(R.id.sweeping_robot_back)
        TextView backTextView;// 回充
        @BindView(R.id.sweeping_robot_stop)
        TextView stopTextView;// 停止

        @BindView(R.id.sweeping_robot_divide)
        View view;// 分割线

        HolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}