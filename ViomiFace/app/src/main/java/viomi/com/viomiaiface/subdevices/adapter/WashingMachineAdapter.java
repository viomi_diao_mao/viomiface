package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.entity.WashingMachineProp;
import viomi.com.viomiaiface.subdevices.repository.WashingMachineRepository;
import viomi.com.viomiaiface.utils.LogUtils;

/**
 * 洗衣机卡片适配器
 * Created by William on 2018/7/7.
 */
public class WashingMachineAdapter extends BaseRecyclerViewAdapter<WashingMachineAdapter.HolderView> {
    private static final String TAG = WashingMachineAdapter.class.getSimpleName();
    private List<AbstractDevice> mList;
    private SparseArray<Subscription> mSparseArray;

    public WashingMachineAdapter(List<AbstractDevice> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
        mSparseArray = new SparseArray<>();
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_washing_machine, parent, false);
        return new HolderView(itemView);
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        AbstractDevice device = mList.get(position);
        holder.count = 0;
        holder.nameTextView.setText(device.getName());// 设备名称
        holder.timeTextView.setVisibility(View.GONE);
        holder.modeTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.tempTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.countTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.rateTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        if (device.isOnline()) { // 在线
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
            holder.statusTextView.setTextColor(0xFF37D58E);
        } else {
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
            holder.statusTextView.setTextColor(0x4D252525);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onViewAttachedToWindow(HolderView holder) {
        super.onViewAttachedToWindow(holder);
        String did = mList.get(holder.getAdapterPosition()).getDeviceId();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WashingMachineRepository.getProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holder.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holder.count = 0;
                        WashingMachineProp prop = new WashingMachineProp(rpcResult.getList());
                        holder.statusTextView.setTextColor(0xFF37D58E);
                        holder.statusTextView.setText(WashingMachineRepository.switchWashProcess(prop.getWash_process(), prop.getWash_status()));
                        holder.modeTextView.setText(WashingMachineRepository.switchProgram(prop.getProgram()));
                        holder.tempTextView.setText(prop.getWater_temp() == 0 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_water_temp_normal) : String.format(BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_water_temp_value), prop.getWater_temp()));
                        holder.countTextView.setText(String.format(BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_wash_count_value), prop.getRinse_time()));
                        holder.rateTextView.setText(WashingMachineRepository.switchLevel(prop.getSpin_level()));
                        WashingMachineRepository.remainTime(holder.timeTextView, prop.getWash_process(), prop.getWash_status(), prop.getRemain_time());
                    } else { // 数据异常
                        if (holder.count >= 5) {
                            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holder.statusTextView.setTextColor(0x4D252525);
                        } else holder.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(holder.getAdapterPosition(), subscription);
    }

    @Override
    public void onViewDetachedFromWindow(HolderView holder) {
        super.onViewDetachedFromWindow(holder);
        if (mSparseArray.get(holder.getAdapterPosition()) != null) {
            mSparseArray.get(holder.getAdapterPosition()).unsubscribe();
            mSparseArray.remove(holder.getAdapterPosition());
        }
    }

    class HolderView extends RecyclerView.ViewHolder {
        int count;

        @BindView(R.id.washing_machine_title)
        TextView nameTextView;// 设备名称
        @BindView(R.id.washing_machine_status)
        TextView statusTextView;// 设备状态
        @BindView(R.id.washing_machine_mode)
        TextView modeTextView;// 洗涤模式
        @BindView(R.id.washing_machine_temp)
        TextView tempTextView;// 设定水温
        @BindView(R.id.washing_machine_count)
        TextView countTextView;// 漂洗次数
        @BindView(R.id.washing_machine_rate)
        TextView rateTextView;// 脱水速度
        @BindView(R.id.washing_machine_time)
        TextView timeTextView;// 预计结束时间

        HolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}