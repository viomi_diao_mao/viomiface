package viomi.com.viomiaiface.subdevices.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.mijia.Miconfig;
import viomi.com.viomiaiface.subdevices.http.RxSchedulerUtil;
import viomi.com.viomiaiface.subdevices.http.entity.CSeriesPurifierProp;
import viomi.com.viomiaiface.subdevices.http.entity.Mi1APurifierProp;
import viomi.com.viomiaiface.subdevices.http.entity.MiPurifierProp;
import viomi.com.viomiaiface.subdevices.http.entity.SSeriesPurifierProp;
import viomi.com.viomiaiface.subdevices.http.entity.VSeriesPurifierProp;
import viomi.com.viomiaiface.subdevices.http.entity.X3PurifierProp;
import viomi.com.viomiaiface.subdevices.http.entity.X5PurifierProp;
import viomi.com.viomiaiface.subdevices.repository.WaterPurifierRepository;
import viomi.com.viomiaiface.utils.LogUtils;


/**
 * 净水器卡片适配器
 * Created by William on 2018/7/4.
 */
public class WaterPurifierAdapter extends BaseRecyclerViewAdapter<WaterPurifierAdapter.HolderView> {
    private static final String TAG = WaterPurifierAdapter.class.getSimpleName();
    private List<AbstractDevice> mList;
    private SparseArray<Subscription> mSparseArray;
    private Subscription mSubscription;

    public WaterPurifierAdapter(List<AbstractDevice> list) {
        this.mList = list;
        if (mList == null) mList = new ArrayList<>();
        mSparseArray = new SparseArray<>();
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_water_purifier, parent, false);
        return new HolderView(itemView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    @Override
    public void onViewAttachedToWindow(HolderView holder) {
        super.onViewAttachedToWindow(holder);
        String did = mList.get(holder.getAdapterPosition()).getDeviceId();
        switch (mList.get(holder.getAdapterPosition()).getDeviceModel()) {
            case Miconfig.YUNMI_WATERPURI_V1:
            case Miconfig.YUNMI_WATERPURI_V2:
                VSeriesGetProp(holder.getAdapterPosition(), holder, did);
                break;
            case Miconfig.YUNMI_WATERPURI_S1:
            case Miconfig.YUNMI_WATERPURI_S2:
                SSeriesGetProp(holder.getAdapterPosition(), holder, did);
                break;
            case Miconfig.YUNMI_WATERPURI_C1:
            case Miconfig.YUNMI_WATERPURI_C2:
                CSeriesGetProp(holder.getAdapterPosition(), holder, did);
                break;
            case Miconfig.YUNMI_WATERPURI_X3:
                X3SeriesGetProp(holder.getAdapterPosition(), holder, did);
                break;
            case Miconfig.YUNMI_WATERPURI_X5:
                X5SeriesGetProp(holder.getAdapterPosition(), holder, did);
                break;
        }
    }

    @Override
    public void onViewDetachedFromWindow(HolderView holder) {
        super.onViewDetachedFromWindow(holder);
        if (mSparseArray.get(holder.getAdapterPosition()) != null) {
            mSparseArray.get(holder.getAdapterPosition()).unsubscribe();
            mSparseArray.remove(holder.getAdapterPosition());
        }
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        holder.seekBar.setOnSeekBarChangeListener(null);
        AbstractDevice device = mList.get(position);
        holder.count = 0;
        holder.nameTextView.setText(device.getName());// 设备名称
        if (device.isOnline()) { // 在线
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
            holder.statusTextView.setTextColor(0xFF37D58E);
            holder.seekBar.setEnabled(true);
        } else {
            holder.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
            holder.statusTextView.setTextColor(0x4D252525);
            holder.seekBar.setEnabled(false);
        }
        holder.tdsTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.tempTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.filter1TextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.filter2TextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.filter3TextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        holder.filter4TextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default));
        // 根据 Model 显示不同 UI
        switchModel(device.getDeviceModel(), holder);
        // 解决滑动冲突
        holder.seekBar.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
        holder.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                holder.tempTextView.setText(WaterPurifierRepository.switchTempTip(holder.minTemp + progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                holder.isSetting = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                setWaterTemp(holder, device.getDeviceId(), holder.minTemp + seekBar.getProgress());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    /**
     * 根据净水器 model 显示不同 UI
     */
    private void switchModel(String model, HolderView holderView) {
        switch (model) {
            case Miconfig.YUNMI_WATERPURI_V1:
            case Miconfig.YUNMI_WATERPURI_V2:
                holderView.line1View.setVisibility(View.GONE);
                holderView.tempLinearLayout.setVisibility(View.GONE);
                holderView.seekBar.setVisibility(View.GONE);
                holderView.filter3RelativeLayout.setVisibility(View.VISIBLE);
                holderView.filter4RelativeLayout.setVisibility(View.VISIBLE);
                break;
            case Miconfig.YUNMI_WATERPURI_S1:
            case Miconfig.YUNMI_WATERPURI_S2:
                holderView.line1View.setVisibility(View.GONE);
                holderView.tempLinearLayout.setVisibility(View.GONE);
                holderView.seekBar.setVisibility(View.GONE);
                holderView.filter3RelativeLayout.setVisibility(View.VISIBLE);
                holderView.filter4RelativeLayout.setVisibility(View.GONE);
                break;
            case Miconfig.YUNMI_WATERPURI_C1:
            case Miconfig.YUNMI_WATERPURI_C2:
                holderView.line1View.setVisibility(View.GONE);
                holderView.tempLinearLayout.setVisibility(View.GONE);
                holderView.seekBar.setVisibility(View.GONE);
                holderView.filter3RelativeLayout.setVisibility(View.VISIBLE);
                holderView.filter4RelativeLayout.setVisibility(View.VISIBLE);
                break;
            case Miconfig.YUNMI_WATERPURI_X3:
                holderView.line1View.setVisibility(View.VISIBLE);
                holderView.tempLinearLayout.setVisibility(View.VISIBLE);
                holderView.seekBar.setVisibility(View.VISIBLE);
                holderView.filter3RelativeLayout.setVisibility(View.VISIBLE);
                holderView.filter4RelativeLayout.setVisibility(View.GONE);
                holderView.minTemp = 40;
                holderView.isSetting = false;
                holderView.seekBar.setProgress(0);
                break;
            case Miconfig.YUNMI_WATERPURI_X5:
                holderView.line1View.setVisibility(View.VISIBLE);
                holderView.tempLinearLayout.setVisibility(View.VISIBLE);
                holderView.seekBar.setVisibility(View.VISIBLE);
                holderView.filter3RelativeLayout.setVisibility(View.VISIBLE);
                holderView.filter4RelativeLayout.setVisibility(View.VISIBLE);
                holderView.minTemp = 40;
                holderView.isSetting = false;
                holderView.seekBar.setProgress(0);
                break;
        }
    }

    /**
     * 小米净水器系列
     */
    private void MiGetProp(int position, HolderView holderView, String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(result -> {
                    if (result == null) return;
                    if (result.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (result.getCode() == 0 && result.getList() != null && result.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        MiPurifierProp prop = new MiPurifierProp(result.getList());
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                        holderView.filter3TextView.setText(percentStr(prop.getF3_Life()));
                        holderView.filter4TextView.setText(percentStr(prop.getF4_Life()));
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 云米净水器 V 系列
     */
    private void VSeriesGetProp(int position, HolderView holderView, String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        VSeriesPurifierProp prop = new VSeriesPurifierProp();
                        prop.initGetProp(rpcResult.getList());
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                        holderView.filter3TextView.setText(percentStr(prop.getF3_Life()));
                        holderView.filter4TextView.setText(percentStr(prop.getF4_Life()));
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 云米净水器 S 系列
     */
    private void SSeriesGetProp(int position, HolderView holderView, String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        SSeriesPurifierProp prop = new SSeriesPurifierProp(rpcResult.getList());
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                        holderView.filter3TextView.setText(percentStr(prop.getF3_Life()));
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 云米净水器 C 系列
     */
    private void CSeriesGetProp(int position, HolderView holderView, String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        CSeriesPurifierProp prop = new CSeriesPurifierProp();
                        prop.initGetProp(rpcResult.getList());
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                        holderView.filter3TextView.setText(percentStr(prop.getF3_Life()));
                        holderView.filter4TextView.setText(percentStr(prop.getF4_Life()));
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 云米净水器 X3
     */
    private void X3SeriesGetProp(int position, HolderView holderView, String did) {
        X3PurifierProp prop = new X3PurifierProp();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(did))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .flatMap(rpcResult -> {
                    prop.initGetProp(rpcResult.getList());
                    return WaterPurifierRepository.x3GetProp(did);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        holderView.seekBar.setEnabled(true);
                        prop.initGetExtraProp(rpcResult.getList());
                        holderView.minTemp = prop.getMin_set_tempe();
                        holderView.seekBar.setMax(90 - holderView.minTemp);
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                        holderView.filter3TextView.setText(percentStr(prop.getF3_Life()));
                        if (!holderView.isSetting) {
                            holderView.seekBar.setProgress(prop.getCustom_tempe1() - holderView.minTemp);
                            holderView.tempTextView.setText(WaterPurifierRepository.switchTempTip(prop.getCustom_tempe1()));
                        }
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;

                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 云米净水器 X5
     */
    private void X5SeriesGetProp(int position, HolderView holderView, String did) {
        X5PurifierProp prop = new X5PurifierProp();
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.miGetProp(did))
                .filter(rpcResult -> rpcResult.getCode() == 0 && rpcResult.getList().size() > 0)
                .onTerminateDetach()
                .flatMap(rpcResult -> {
                    prop.initGetProp(rpcResult.getList());
                    return WaterPurifierRepository.x5GetProp(did);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(rpcResult -> {
                    if (rpcResult == null) return;
                    if (rpcResult.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (rpcResult.getCode() == 0 && rpcResult.getList() != null && rpcResult.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        holderView.seekBar.setEnabled(true);
                        prop.initGetExtraProp(rpcResult.getList());
                        holderView.seekBar.setMax(90 - holderView.minTemp);
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                        holderView.filter3TextView.setText(percentStr(prop.getF3_Life()));
                        holderView.filter4TextView.setText(percentStr(prop.getF4_Life()));
                        if (!holderView.isSetting) {
                            holderView.seekBar.setProgress(prop.getCustom_tempe1() - holderView.minTemp);
                            holderView.tempTextView.setText(WaterPurifierRepository.switchTempTip(prop.getCustom_tempe1()));
                        }
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;

                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 小米净水器 1A
     */
    private void Mi1AGetProp(int position, HolderView holderView, String did) {
        Subscription subscription = Observable.interval(0, 5, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .subscribeOn(Schedulers.io())
                .onTerminateDetach()
                .flatMap(aLong -> WaterPurifierRepository.mi1AGetProp(did))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .subscribe(result -> {
                    if (result == null) return;
                    if (result.getCode() == -2) { // 设备离线
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_offline));
                        holderView.statusTextView.setTextColor(0x4D252525);
                    } else if (result.getCode() == 0 && result.getList() != null && result.getList().size() > 0) { // 正常返回
                        holderView.count = 0;
                        holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online));
                        holderView.statusTextView.setTextColor(0xFF37D58E);
                        Mi1APurifierProp prop = new Mi1APurifierProp(result.getList());
                        holderView.tdsTextView.setText(String.valueOf(prop.getTds_out()));
                        holderView.filter1TextView.setText(percentStr(prop.getF1_Life()));
                        holderView.filter2TextView.setText(percentStr(prop.getF2_Life()));
                    } else { // 数据异常
                        if (holderView.count >= 5) {
                            holderView.statusTextView.setText(BaseApplication.getAppContext().getResources().getString(R.string.iot_load_data_fail));
                            holderView.statusTextView.setTextColor(0x4D252525);
                        } else holderView.count++;
                    }
                }, throwable -> LogUtils.e(TAG, throwable.getMessage()));
        mSparseArray.put(position, subscription);
    }

    /**
     * 设置温水键出水温度
     */
    private void setWaterTemp(HolderView holderView, String did, int temp) {
        mSubscription = WaterPurifierRepository.setTemp(did, temp)
                .compose(RxSchedulerUtil.SchedulersTransformer1())
                .onTerminateDetach()
                .subscribe(rpcResult -> holderView.isSetting = false,
                        throwable -> {
                            LogUtils.e(TAG, throwable.getMessage());
                            holderView.isSetting = false;
                        });
    }

    private String percentStr(int percent) {
        return (percent < 0 ? 0 : percent) + "%";
    }

    class HolderView extends RecyclerView.ViewHolder {
        boolean isSetting;
        int minTemp;
        int count;

        @BindView(R.id.water_purifier_title)
        TextView nameTextView;// 设备名称
        @BindView(R.id.water_purifier_status)
        TextView statusTextView;// 设备状态
        @BindView(R.id.water_purifier_tds)
        TextView tdsTextView;// TDS 值
        @BindView(R.id.water_purifier_temp)
        TextView tempTextView;// 温水键温度
        @BindView(R.id.water_purifier_filter_1_text)
        TextView filter1TextView;// 滤芯 1 寿命
        @BindView(R.id.water_purifier_filter_2_text)
        TextView filter2TextView;// 滤芯 2 寿命
        @BindView(R.id.water_purifier_filter_3_text)
        TextView filter3TextView;// 滤芯 3 寿命
        @BindView(R.id.water_purifier_filter_4_text)
        TextView filter4TextView;// 滤芯 4 寿命

        @BindView(R.id.water_purifier_line_1)
        View line1View;// 分割线

        @BindView(R.id.water_purifier_temp_setting_layout)
        LinearLayout tempLinearLayout;// 温水键温度设置布局

        @BindView(R.id.water_purifier_seek_bar)
        SeekBar seekBar;// 温水键温度调节

        @BindView(R.id.water_purifier_filter_3_layout)
        RelativeLayout filter3RelativeLayout;// 滤芯 3 布局
        @BindView(R.id.water_purifier_filter_4_layout)
        RelativeLayout filter4RelativeLayout;// 滤芯 4 布局

        HolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}