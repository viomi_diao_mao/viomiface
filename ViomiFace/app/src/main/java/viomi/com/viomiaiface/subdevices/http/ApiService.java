package viomi.com.viomiaiface.subdevices.http;


import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;

/**
 * Http 请求 Api
 * Created by William on 2018/1/4.
 */
public interface ApiService {


    // 意见反馈
    @Headers({"domain:store"})
    @POST("/services/user/feedbacks.json")
    Observable<String> feedback(@Body RequestBody requestBody);

    // 小米开放接口
    @Headers({"domain:xiaomi"})
    @GET
    Observable<RPCResult> miOpen(@Url String url, @Query("data") String data, @Query("clientId") String clientId, @Query("accessToken") String accessToken);

    /**
     * @param mid    小米id
     * @param lastId 本地最新消息id
     * @return 获取系统消息
     */
    @Headers({"domain:message"})
    @GET("/information/fridge21/system/{mid}/{infoId}")
    Observable<String> getSystemMessages(@Path("mid") String mid, @Path("infoId") String lastId);

    /**
     * @param mid    小米id
     * @param lastId 本地最新消息id
     * @return 获取未读消息
     */
    @Headers({"domain:message"})
    @GET("/information/fridge21/checkNewInfos/{mid}/{yid}")
    Observable<String> getUnreadMessages(@Path("mid") String mid, @Path("yid") String lastId);

    /**
     * @param alias 请求内容
     * @return 上报系统消息别名
     */
    @Headers({"domain:message"})
    @FormUrlEncoded
    @POST("/information/alias")
    Observable<String> reportAlias(@Field("alias") String alias);

    // 南京物联，操作设备
    @Headers({"domain:wulian"})
    @POST("/wlink/action/execute")
    Observable<String> wulianOperate(@Query("token") String token, @Body RequestBody requestBody);

    // 南京物联，开灯
    @Headers({"domain:debug"})
    @POST("/wlink/switch/turnOn")
    Observable<String> openLight(@Body RequestBody requestBody);

    // 南京物联，关灯
    @Headers({"domain:debug"})
    @POST("/wlink/switch/turnOff")
    Observable<String> closeLight(@Body RequestBody requestBody);

    // 南京物联，开门
    @Headers({"domain:debug"})
    @POST("/wlink/door/unlock")
    Observable<String> openDoor(@Body RequestBody requestBody);

    // 南京物联，获取状态
    @Headers({"domain:debug"})
    @GET("/wlink/devicesStatus/did/{deviceId}")
    Observable<String> getStatus(@Path("deviceId") String deviceId);

    // 南京物联，开启
    @Headers({"domain:debug"})
    @PUT("wlink/switch/turnOn")
    Observable<String> statusOn(@Body RequestBody requestBody);

    // 南京物联，关闭
    @Headers({"domain:debug"})
    @PUT("wlink/switch/turnOff")
    Observable<String> statusOff(@Body RequestBody requestBody);

    // 南京物联，窗帘开
    @Headers({"domain:debug"})
    @PUT("wlink/curtain/turnOn")
    Observable<String> windowOn(@Body RequestBody requestBody);

    // 南京物联，窗帘关
    @Headers({"domain:debug"})
    @PUT("wlink/curtain/turnOff")
    Observable<String> windowOff(@Body RequestBody requestBody);

    // 南京物联，插座开
    @Headers({"domain:debug"})
    @PUT("wlink/socket/turnOn")
    Observable<String> socketOn(@Body RequestBody requestBody);

    // 南京物联，插座关
    @Headers({"domain:debug"})
    @PUT("wlink/socket/turnOff")
    Observable<String> socketOff(@Body RequestBody requestBody);

    // 南京物联，锁 摄像头开
    @Headers({"domain:debug"})
    @POST
    Observable<String> wlinkActionExecute(@Url String url, @Body RequestBody requestBody);

    // 获取用户下网关列表接口,具体返回参考物联协议
    @Headers({"domain:debug"})
    @GET("/wlink/devices")
    Observable<String> wlinkDevices(@Query("token") String token);

    @Headers({"domain:debug"})
    @GET("/wlink/device/children ")
    Observable<String> wlinkDeviceChildren(@Query("token") String token);


    /**
     * 下载文件
     *
     * @param url: 下载链接
     */
    @Streaming
    @GET
    Observable<ResponseBody> download(@Url String url);
}