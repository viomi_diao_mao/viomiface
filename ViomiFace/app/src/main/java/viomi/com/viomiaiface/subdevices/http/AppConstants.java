package viomi.com.viomiaiface.subdevices.http;

/**
 * 存放 App 全局常量  有问题找志威，嘿嘿
 * Created by William on 2018/1/2.
 */
public class AppConstants {

    // 小米开放接口
    public static final String URL_MI_RPC = "openapp/device/rpc/";// 设备 rpc
    public static final String URL_MI_DATA = "openapp/user/get_user_device_data/";// 设备统计数据

}