package viomi.com.viomiaiface.subdevices.http.entity;

import java.util.List;

/**
 * 洗碗机属性
 * Created by William on 2018/7/5.
 */
public class DishWashingProp {
    private int child_lock;// 童锁功能（0:童锁功能关闭，1:童锁功能开启）
    private int program;// 洗涤程序（0:标准洗；1:经济洗；2:快速洗；3:自定义；249:工厂老化模式 （只可读取不能设置））
    private int custom_program;// 自定义模式（3:强力洗；4:预洗；5:玻璃洗；6:除菌；250:自动保鲜功能模式 / 独立干燥模式）
    private int wash_status;// 设备工作状态，工作中才判断（0:暂停；1:正常）
    private int wash_process;// 设备洗涤过程（0:准备洗；1:预洗；2:主洗；3:冲洗；4:漂洗；5:干燥；6:洗涤结束）
    private int ldj_state;// 0:无亮碟剂；1：有亮碟剂
    private int salt_state;// 0:无盐；1：有盐
    private int flash_dry_time;// 冲洗干燥时间（单位秒）
    private int flash_interval;// 已经多长时间没有洗涤或开门（单位小时）
    private int flashdry_interval;// 持续多长时间不开门或不使用机器，自动启动加热杀菌功能（单位小时，0，12、24、36、48、72 小时选择，默认是 0 小时，0 表示关闭）
    private int fastwash_dry_t;// 快速洗后干燥启动的间隔时间，这个时间内开门干燥取消（单位分钟，0，10、20、30、40、50、60 分钟选择，默认 30 分钟，0 表示关闭）
    private int fastwash_drying;// 快速洗干燥状态（0：未进入快速洗干燥状态；1：已进入快速洗干燥状态）
    private String city;// 城市名（暂时停用）
    private int bespeak_h;// 预约洗，当前设置的小时数值（0 - 23）
    private int bespeak_min;// 预约洗，当前设置的分钟数值（0 - 59）
    private int bespeak_status;// 预约状态，只有空闲才判断（0：未预约；1：已预约）
    private int left_time;// 洗涤剩余时间（单位秒）
    private int run_status;// 异常状态
    private String[] wash_info;// 保存洗涤完成后的记录（用水时长（int，单位秒）、用水量（int,单位mL）有无亮碟剂、有无软水盐、上次洗涤开始时间（时间戳，单位s））
    private int wash_temp;// 洗碗机洗涤温度
    private int dry_temp;// 洗碗机烘干阶段的温度
    private int temp_info;// 现本次洗涤的温度采集，40个点（用"-"隔开，例如：40-50-45-60-70-65…”）
    private int total_time;// 洗涤总时间时间（单位秒）

    public DishWashingProp(List<Object> list) {
        program = (int) list.get(0);
        ldj_state = (int) list.get(1);
        salt_state = (int) list.get(2);
        left_time = (int) list.get(3);
        bespeak_h = (int) list.get(4);
        bespeak_min = (int) list.get(5);
        bespeak_status = (int) list.get(6);
        wash_process = (int) list.get(7);
        wash_status = (int) list.get(8);
        custom_program = (int) list.get(9);
    }

    public int getProgram() {
        return program;
    }

    public void setProgram(int program) {
        this.program = program;
    }

    public int getLdj_state() {
        return ldj_state;
    }

    public void setLdj_state(int ldj_state) {
        this.ldj_state = ldj_state;
    }

    public int getSalt_state() {
        return salt_state;
    }

    public void setSalt_state(int salt_state) {
        this.salt_state = salt_state;
    }

    public int getLeft_time() {
        return left_time;
    }

    public void setLeft_time(int left_time) {
        this.left_time = left_time;
    }

    public int getBespeak_h() {
        return bespeak_h;
    }

    public void setBespeak_h(int bespeak_h) {
        this.bespeak_h = bespeak_h;
    }

    public int getBespeak_min() {
        return bespeak_min;
    }

    public void setBespeak_min(int bespeak_min) {
        this.bespeak_min = bespeak_min;
    }

    public int getBespeak_status() {
        return bespeak_status;
    }

    public void setBespeak_status(int bespeak_status) {
        this.bespeak_status = bespeak_status;
    }

    public int getWash_process() {
        return wash_process;
    }

    public void setWash_process(int wash_process) {
        this.wash_process = wash_process;
    }

    public int getWash_status() {
        return wash_status;
    }

    public void setWash_status(int wash_status) {
        this.wash_status = wash_status;
    }

    public int getChild_lock() {
        return child_lock;
    }

    public void setChild_lock(int child_lock) {
        this.child_lock = child_lock;
    }

    public int getCustom_program() {
        return custom_program;
    }

    public void setCustom_program(int custom_program) {
        this.custom_program = custom_program;
    }

    public int getFlash_dry_time() {
        return flash_dry_time;
    }

    public void setFlash_dry_time(int flash_dry_time) {
        this.flash_dry_time = flash_dry_time;
    }

    public int getFlash_interval() {
        return flash_interval;
    }

    public void setFlash_interval(int flash_interval) {
        this.flash_interval = flash_interval;
    }

    public int getFlashdry_interval() {
        return flashdry_interval;
    }

    public void setFlashdry_interval(int flashdry_interval) {
        this.flashdry_interval = flashdry_interval;
    }

    public int getFastwash_dry_t() {
        return fastwash_dry_t;
    }

    public void setFastwash_dry_t(int fastwash_dry_t) {
        this.fastwash_dry_t = fastwash_dry_t;
    }

    public int getFastwash_drying() {
        return fastwash_drying;
    }

    public void setFastwash_drying(int fastwash_drying) {
        this.fastwash_drying = fastwash_drying;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getRun_status() {
        return run_status;
    }

    public void setRun_status(int run_status) {
        this.run_status = run_status;
    }

    public String[] getWash_info() {
        return wash_info;
    }

    public void setWash_info(String[] wash_info) {
        this.wash_info = wash_info;
    }

    public int getWash_temp() {
        return wash_temp;
    }

    public void setWash_temp(int wash_temp) {
        this.wash_temp = wash_temp;
    }

    public int getDry_temp() {
        return dry_temp;
    }

    public void setDry_temp(int dry_temp) {
        this.dry_temp = dry_temp;
    }

    public int getTemp_info() {
        return temp_info;
    }

    public void setTemp_info(int temp_info) {
        this.temp_info = temp_info;
    }

    public int getTotal_time() {
        return total_time;
    }

    public void setTotal_time(int total_time) {
        this.total_time = total_time;
    }
}