package viomi.com.viomiaiface.subdevices.http.entity;

import java.util.List;

/**
 * 风扇属性
 * Created by William on 2018/7/6.
 */
public class FanProp {
    private int wind_speed;// 返回当前设定的风速（取值：1 - 206）
    private int wind_mode;// 返回当前工作模式（0：标准风；1：自然风；2：节能风）
    private int temperature;// 返回当前室内温度（取值：-20 - 50）
    private int power_state;// 当前设备工作状态（0：设备关闭；1：设备开启）
    private int shake_state;// 摇头功能开启标识（0：关闭摇头功能；1：开启摇头功能）
    private int voice_state;// 语音功能开启标识（0：关闭语音识别功能；1：开启语音识别功能）
    private int led_state;// LED 持续显示功能开启标识（0：关闭LED持续显示功能；1：开启LED持续显示功能）
    private int work_time;// 定时剩余时间（单位 / min）(65535：当前无定时，取值：0 - 360)
    private int volume;// 当前语音音量百分比（取值：0 - 100）
    private int playmode;// 播报模式开启标识（0：关闭正常播报，开启简洁模式；1：开启正常播报，关闭简洁模式）

    public FanProp(List<Object> list) {
        wind_mode = (int) list.get(0);
        shake_state = (int) list.get(1);
        power_state = (int) list.get(2);
        work_time = (int) list.get(3);
    }

    public int getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(int wind_speed) {
        this.wind_speed = wind_speed;
    }

    public int getWind_mode() {
        return wind_mode;
    }

    public void setWind_mode(int wind_mode) {
        this.wind_mode = wind_mode;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getPower_state() {
        return power_state;
    }

    public void setPower_state(int power_state) {
        this.power_state = power_state;
    }

    public int getShake_state() {
        return shake_state;
    }

    public void setShake_state(int shake_state) {
        this.shake_state = shake_state;
    }

    public int getVoice_state() {
        return voice_state;
    }

    public void setVoice_state(int voice_state) {
        this.voice_state = voice_state;
    }

    public int getLed_state() {
        return led_state;
    }

    public void setLed_state(int led_state) {
        this.led_state = led_state;
    }

    public int getWork_time() {
        return work_time;
    }

    public void setWork_time(int work_time) {
        this.work_time = work_time;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getPlaymode() {
        return playmode;
    }

    public void setPlaymode(int playmode) {
        this.playmode = playmode;
    }
}