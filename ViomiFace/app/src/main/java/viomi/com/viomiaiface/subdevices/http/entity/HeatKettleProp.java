package viomi.com.viomiaiface.subdevices.http.entity;

import java.util.List;

/**
 * 即热饮水吧属性
 * Created by William on 2018/2/8.
 */
public class HeatKettleProp {
    private int setup_tempe;// 当前设定温度
    private int tds;// 本次制水入水水质（平均值）
    private int water_remain_time;// 水的留存时间，换水后清 0（单位：小时）
    private int flush_time;// 待冲洗时间，冲洗完后开始计算（单位：小时）
    private int custom_tempe1;// 温水按键，自定义温度
    private int min_set_tempe;// 温水键最低设置温度
    private int work_mode;// 工作模式 0.常温，1.温水，2.鲜开水，3.未选中
    private int run_status;// 故障，每一位代表一个故障

    public HeatKettleProp(List<Object> list) {
        setup_tempe = (int) list.get(0);
        tds = (int) list.get(1);
        water_remain_time = (int) list.get(2);
        flush_time = (int) list.get(3);
        custom_tempe1 = (int) list.get(4);
        min_set_tempe = (int) list.get(5);
        work_mode = (int) list.get(6);
        run_status = (int) list.get(7);
    }

    public int getSetup_tempe() {
        return setup_tempe;
    }

    public int getTds() {
        return tds;
    }

    public int getWater_remain_time() {
        return water_remain_time;
    }

    public int getFlush_time() {
        return flush_time;
    }

    public int getCustom_tempe1() {
        return custom_tempe1;
    }

    public int getMin_set_tempe() {
        return min_set_tempe;
    }

    public int getWork_mode() {
        return work_mode;
    }

    public int getRun_status() {
        return run_status;
    }
}