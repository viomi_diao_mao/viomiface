package viomi.com.viomiaiface.subdevices.http.entity;

import java.util.List;

/**
 * 小米净水器 1A 属性
 * Created by William on 2018/7/6.
 */
public class Mi1APurifierProp {
    private int run_status;// 故障信息
    private int f1_totalflow;// 滤芯流量总寿命
    private int f1_totaltime;// 滤芯时间总寿命
    private int f1_usedflow;// 滤芯使用流量
    private int f1_usedtime;// 滤芯使用时间
    private int f2_totalflow;// 滤芯流量总寿命
    private int f2_totaltime;// 滤芯时间总寿命
    private int f2_usedflow;// 滤芯使用流量
    private int f2_usedtime;// 滤芯使用时间
    private int tds_in;// 本次制水进水水水质（平均值）
    private int tds_out;// 本次制水出水水质（平均值）
    private int[] err_list;// 最近10次异常记录
    private int rinse;// 是否正在冲洗
    private int temperature;// 进水水温
    private int tds_warn_thd;// TDS 警告阈值
    private int tds_out_avg;// 最近 15 天出水 TDS 平均值
    private int f1_Life;// 滤芯剩余寿命
    private int f2_Life;// 滤芯剩余寿命

    public Mi1APurifierProp(List<Object> list) {
        tds_out = (int) list.get(0);
        f1_totaltime = (int) list.get(1);
        f1_usedtime = (int) list.get(2);
        f2_totaltime = (int) list.get(3);
        f2_usedtime = (int) list.get(4);

        f1_Life = (f1_totaltime - f1_usedtime) * 100 / f1_totaltime;
        f2_Life = (f2_totaltime - f2_usedtime) * 100 / f2_totaltime;
    }

    public int getRun_status() {
        return run_status;
    }

    public void setRun_status(int run_status) {
        this.run_status = run_status;
    }

    public int getF1_totalflow() {
        return f1_totalflow;
    }

    public void setF1_totalflow(int f1_totalflow) {
        this.f1_totalflow = f1_totalflow;
    }

    public int getF1_totaltime() {
        return f1_totaltime;
    }

    public void setF1_totaltime(int f1_totaltime) {
        this.f1_totaltime = f1_totaltime;
    }

    public int getF1_usedflow() {
        return f1_usedflow;
    }

    public void setF1_usedflow(int f1_usedflow) {
        this.f1_usedflow = f1_usedflow;
    }

    public int getF1_usedtime() {
        return f1_usedtime;
    }

    public void setF1_usedtime(int f1_usedtime) {
        this.f1_usedtime = f1_usedtime;
    }

    public int getF2_totalflow() {
        return f2_totalflow;
    }

    public void setF2_totalflow(int f2_totalflow) {
        this.f2_totalflow = f2_totalflow;
    }

    public int getF2_totaltime() {
        return f2_totaltime;
    }

    public void setF2_totaltime(int f2_totaltime) {
        this.f2_totaltime = f2_totaltime;
    }

    public int getF2_usedflow() {
        return f2_usedflow;
    }

    public void setF2_usedflow(int f2_usedflow) {
        this.f2_usedflow = f2_usedflow;
    }

    public int getF2_usedtime() {
        return f2_usedtime;
    }

    public void setF2_usedtime(int f2_usedtime) {
        this.f2_usedtime = f2_usedtime;
    }

    public int getTds_in() {
        return tds_in;
    }

    public void setTds_in(int tds_in) {
        this.tds_in = tds_in;
    }

    public int getTds_out() {
        return tds_out;
    }

    public void setTds_out(int tds_out) {
        this.tds_out = tds_out;
    }

    public int[] getErr_list() {
        return err_list;
    }

    public void setErr_list(int[] err_list) {
        this.err_list = err_list;
    }

    public int getRinse() {
        return rinse;
    }

    public void setRinse(int rinse) {
        this.rinse = rinse;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getTds_warn_thd() {
        return tds_warn_thd;
    }

    public void setTds_warn_thd(int tds_warn_thd) {
        this.tds_warn_thd = tds_warn_thd;
    }

    public int getTds_out_avg() {
        return tds_out_avg;
    }

    public void setTds_out_avg(int tds_out_avg) {
        this.tds_out_avg = tds_out_avg;
    }

    public int getF1_Life() {
        return f1_Life;
    }

    public void setF1_Life(int f1_Life) {
        this.f1_Life = f1_Life;
    }

    public int getF2_Life() {
        return f2_Life;
    }

    public void setF2_Life(int f2_Life) {
        this.f2_Life = f2_Life;
    }
}