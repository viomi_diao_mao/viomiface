package viomi.com.viomiaiface.subdevices.http.entity;

import java.util.List;

/**
 * 扫地机器人属性
 * Created by William on 2018/7/6.
 */
public class SweepingRobotProp {
    private int run_state;// 参数每一位代表不同的工作状态：bit0：睡眠/暂停；bit1：待机；bit2：自动清扫；bit3：拖地；bit4：重点清扫；bit5：回充中；bit6：充电中
    private int err_state;// 参数每一位代表不同的异常状态：bit0：左轮异常；bit1：右轮异常；bit2：边刷异常；bit3：主刷异常；bit4：地面传感器异常；bit5：碰撞传感器异常；bit6：垃圾箱异常；bit7：吸尘风机异常；bit8：其它故障
    private int battary_life;// 电池剩余电量百分比，0 - 0%，100 - 100%
    private int start_time;// 扫地开始时间，单位 / s
    private int[] order_time;// 扫地机预约时间（{[0,127],[0,23],[0,59]}）
    private int s_time;// 本次清扫时间
    private int s_area;// 本次清扫面积

    public SweepingRobotProp(List<Object> list) {
        run_state = isError(list, 0);
        battary_life = isError(list, 1);
        start_time = isError(list, 2);
        s_time = isError(list, 3);
        s_area = isError(list, 4);
    }

    public int getRun_state() {
        return run_state;
    }

    public void setRun_state(int run_state) {
        this.run_state = run_state;
    }

    public int getErr_state() {
        return err_state;
    }

    public void setErr_state(int err_state) {
        this.err_state = err_state;
    }

    public int getBattary_life() {
        return battary_life;
    }

    public void setBattary_life(int battary_life) {
        this.battary_life = battary_life;
    }

    public int getStart_time() {
        return start_time;
    }

    public void setStart_time(int start_time) {
        this.start_time = start_time;
    }

    public int[] getOrder_time() {
        return order_time;
    }

    public void setOrder_time(int[] order_time) {
        this.order_time = order_time;
    }

    public int getS_time() {
        return s_time;
    }

    public void setS_time(int s_time) {
        this.s_time = s_time;
    }

    public int getS_area() {
        return s_area;
    }

    public void setS_area(int s_area) {
        this.s_area = s_area;
    }

    private int isError(List<Object> list, int index) {
        if (list.size() <= index) return 0;
        else return (int) list.get(index);
    }
}