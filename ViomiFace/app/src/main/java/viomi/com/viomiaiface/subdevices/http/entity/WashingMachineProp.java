package viomi.com.viomiaiface.subdevices.http.entity;

import java.util.List;

/**
 * 洗衣机属性
 * Created by William on 2018/7/6.
 */
public class WashingMachineProp {
    private int child_lock;// 童锁功能（0:童锁功能关闭；1:童锁功能开启 ）
    /**
     * 洗衣机洗涤程序，包括："goldenwash","drumclean","spin","antibacterial","super_quick","cottons","shirts","child_cloth",
     * "jeans", "wool", "down", "chiffon", "outdoor", "delicates", "underwears"，"rinse_spin","My Time","cotton Eco"。
     * 分别代表  “黄金洗”程序，“洁筒洗”程序，“单脱水”程序，“除菌螨”程序，“超快洗”程序，“棉织物”程序，“衬衣”程序，
     * “童衣洗”程序, “牛仔”程序，“羊毛”程序，“羽绒服”程序，“雪纺”程序，“运动服”程序，“精细织物”程序，“内衣”程序，
     * “漂+脱”程序，“自定义”程序，“棉Eco"程序
     */
    private String program;
    private int wash_process;// 设备洗涤过程（0:空闲；1:称重；2:洗涤；3:漂洗；4:脱水；5:洗涤结束；6:停机 （发生致命故障，需解除故障才能继续运行时）；7:关机）
    private int wash_status;// 设备工作状态，工作中才判断（0:暂停；1:正常）
    private int water_temp;// 洗涤水温设置（0, 30,40,60,90 (0 代表常温））
    private int rinse_time;// 漂洗次数设置（1,2,3,4,5）
    private String spin_level;// 脱水强度（速度）设置（"none","gentle","mild","middle","strong"。分别代表不脱水，柔脱水，轻脱水，中脱水，强脱水）
    private int remain_time;// 剩余洗涤时间（单位:分钟）
    private int de_auto;// 洗涤剂自动投放（0：不投放；1：少量；2：正常；3：多量）
    private int so_auto;// 柔顺剂自动投放（0：不投放；1：少量；2：正常；3：多量）
    private int cleaning_auto;// 开机默认程序是否为筒清洁（0: 不默认程序为筒清洁；1：默认程序为筒清洁）
    private int appoint_time;// 预约完成时间（洗涤完成的时间，单位 小时）
    private int be_status;// 预约状态，只有空闲才判断（0：未预约；1：已预约）
    private String[] err_list;// 最近 10 次异常记录
    private int run_status;// 异常状态
    private List<WashInfo> wash_info;// 保存洗涤完成后的记录
    private int mytime;// 自定义模式下，已设置的洗涤时长
    private int voice;// 0：语音功能关闭； 1：语音功能开启
    private int de_status;// 洗衣液使用情况（0：已耗尽；1：未耗尽）
    private int so_status;// 柔顺剂使用情况（0：已耗尽；1：未耗尽）

    public WashingMachineProp(List<Object> list) {
        program = (String) list.get(0);
        wash_process = (int) list.get(1);
        wash_status = (int) list.get(2);
        water_temp = (int) list.get(3);
        rinse_time = (int) list.get(4);
        remain_time = (int) list.get(5);
        spin_level = (String) list.get(6);
        appoint_time = (int) list.get(7);
        be_status = (int) list.get(8);
    }

    public int getChild_lock() {
        return child_lock;
    }

    public void setChild_lock(int child_lock) {
        this.child_lock = child_lock;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public int getWash_process() {
        return wash_process;
    }

    public void setWash_process(int wash_process) {
        this.wash_process = wash_process;
    }

    public int getWash_status() {
        return wash_status;
    }

    public void setWash_status(int wash_status) {
        this.wash_status = wash_status;
    }

    public int getWater_temp() {
        return water_temp;
    }

    public void setWater_temp(int water_temp) {
        this.water_temp = water_temp;
    }

    public int getRinse_time() {
        return rinse_time;
    }

    public void setRinse_time(int rinse_time) {
        this.rinse_time = rinse_time;
    }

    public String getSpin_level() {
        return spin_level;
    }

    public void setSpin_level(String spin_level) {
        this.spin_level = spin_level;
    }

    public int getRemain_time() {
        return remain_time;
    }

    public void setRemain_time(int remain_time) {
        this.remain_time = remain_time;
    }

    public int getDe_auto() {
        return de_auto;
    }

    public void setDe_auto(int de_auto) {
        this.de_auto = de_auto;
    }

    public int getSo_auto() {
        return so_auto;
    }

    public void setSo_auto(int so_auto) {
        this.so_auto = so_auto;
    }

    public int getCleaning_auto() {
        return cleaning_auto;
    }

    public void setCleaning_auto(int cleaning_auto) {
        this.cleaning_auto = cleaning_auto;
    }

    public int getAppoint_time() {
        return appoint_time;
    }

    public void setAppoint_time(int appoint_time) {
        this.appoint_time = appoint_time;
    }

    public int getBe_status() {
        return be_status;
    }

    public void setBe_status(int be_status) {
        this.be_status = be_status;
    }

    public String[] getErr_list() {
        return err_list;
    }

    public void setErr_list(String[] err_list) {
        this.err_list = err_list;
    }

    public int getRun_status() {
        return run_status;
    }

    public void setRun_status(int run_status) {
        this.run_status = run_status;
    }

    public List<WashInfo> getWash_info() {
        return wash_info;
    }

    public void setWash_info(List<WashInfo> wash_info) {
        this.wash_info = wash_info;
    }

    public int getMytime() {
        return mytime;
    }

    public void setMytime(int mytime) {
        this.mytime = mytime;
    }

    public int getVoice() {
        return voice;
    }

    public void setVoice(int voice) {
        this.voice = voice;
    }

    public int getDe_status() {
        return de_status;
    }

    public void setDe_status(int de_status) {
        this.de_status = de_status;
    }

    public int getSo_status() {
        return so_status;
    }

    public void setSo_status(int so_status) {
        this.so_status = so_status;
    }

    private class WashInfo {
        int water_flow;// 耗水量(单位：mL)
        int power;// 耗电量(单位：w)
        int use_time;// 本次洗涤时长(单位：秒)
        int temp;// 本次洗涤水温(水温，摄氏度 0～100)
        int spinning_drop;// 脱水降档(是否正常)
        int dress_bias;// 衣服偏心(是否正常)
        int program;// 洗涤程序(参照属性 program)
        int rinse_time;// 漂洗次数(参照属性 rinse_time)
        int spin_level;// 脱水强度（速度）设置(参照属性spin_level)
        int detergent_auto;// 洗涤剂自动投放(参照属性detergent_auto)
        int softener_auto;// 柔顺剂自动投放(参照属性softener_auto)
        int detergent_use;// 洗涤剂添加量(单位mL)
        int softener_use;// 柔顺剂添加量(单位mL)
        int load;// 负载量(小、中、大、超大,分别是1，2，3，4)
        int time;// 上传时间戳(单位：s)
    }
}