package viomi.com.viomiaiface.subdevices.repository;

import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.subdevices.http.AppConstants;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;

/**
 * 洗碗机相关 Api
 * Created by William on 2018/7/5.
 */
public class DishWashingRepository {
    private static final String TAG = DishWashingRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("program");
            jsonArray.put("ldj_state");
            jsonArray.put("salt_state");
            jsonArray.put("left_time");
            jsonArray.put("bespeak_h");
            jsonArray.put("bespeak_min");
            jsonArray.put("bespeak_status");
            jsonArray.put("wash_process");
            jsonArray.put("wash_status");
            jsonArray.put("custom_program");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 洗涤过程
     */
    public static String switchWashProcess(int wash_process, int wash_status, int bespeak_status) {
        String str = "";
        switch (wash_process) {
            case 0:
                str = bespeak_status == 0 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_free) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_booking);
                break;
            case 1:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_preparing) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_prepare_pause);
                break;
            case 2:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_main) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_main_pause);
                break;
            case 3:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_washing) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_washing_pause);
                break;
            case 4:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_poaching) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_poach_pause);
                break;
            case 5:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_drying) : BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_drying_pause);
                break;
            case 6:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_finish);
                break;
        }
        return str;
    }

    /**
     * 洗涤程序
     */
    public static String switchProgram(int program, int custom_program) {
        String str = "";
        switch (program) {
            case 0:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_mode_0);
                break;
            case 1:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_mode_1);
                break;
            case 2:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_mode_2);
                break;
            case 3:
                str = switchDefine(custom_program, BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_mode_3));
                break;
            case 249:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_mode_249);
                break;
        }
        return str;
    }

    /**
     * 洗涤程序自定义
     */
    private static String switchDefine(int custom_program, String str) {
        switch (custom_program) {
            case 3:
                str = str + "-" + BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_strong);
                break;
            case 4:
                str = str + "-" + BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_prepare);
                break;
            case 5:
                str = str + "-" + BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_glass);
                break;
            case 6:
                str = str + "-" + BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_disinfect);
                break;
            case 250:
                str = str + "-" + BaseApplication.getAppContext().getResources().getString(R.string.iot_dish_washing_dry);
                break;
        }
        return str;
    }
}