package viomi.com.viomiaiface.subdevices.repository;

import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.subdevices.http.AppConstants;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;

/**
 * 风扇相关 Api
 * Created by William on 2018/7/6.
 */
public class FanRepository {
    private static final String TAG = FanRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("wind_mode");
            jsonArray.put("shake_state");
            jsonArray.put("power_state");
            jsonArray.put("work_time");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置摇头状态
     */
    public static Observable<RPCResult> setShake(String param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_shake");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 电源开关设置
     */
    public static Observable<RPCResult> setPower(String param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 工作模式
     */
    public static String switchWindMode(int wind_mode) {
        String str = "";
        switch (wind_mode) {
            case 0:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_fan_mode_standard);
                break;
            case 1:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_fan_mode_natural);
                break;
            case 2:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_fan_mode_energy);
                break;
        }
        return str;
    }
}