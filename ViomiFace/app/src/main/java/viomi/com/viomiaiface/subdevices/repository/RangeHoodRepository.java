package viomi.com.viomiaiface.subdevices.repository;


import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.subdevices.http.AppConstants;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;

/**
 * 烟机相关 Api
 * Created by William on 2018/2/21.
 */
public class RangeHoodRepository {
    private static final String TAG = RangeHoodRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("run_time");
            jsonArray.put("power_state");
            jsonArray.put("wind_state");
            jsonArray.put("light_state");
            jsonArray.put("link_state");
            jsonArray.put("stove1_data");
            jsonArray.put("stove2_data");
            jsonArray.put("pm2_5");
            jsonArray.put("battary_life");
            jsonArray.put("poweroff_delaytime");
            jsonArray.put("aqi_state");
            jsonArray.put("aqi_thd");
            jsonArray.put("aqi_hour");
            jsonArray.put("aqi_min");
            jsonArray.put("aqi_time");
            jsonArray.put("curise_state");
            jsonArray.put("light_sync_state");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 获取统计数据
     */
    public static Observable<RPCResult> getUserData(String did) {
        long time_end = System.currentTimeMillis() / 1000;
        long time_start = time_end - 365 * 24 * 60 * 60;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("did", did);
            jsonObject.put("type", "store");
            jsonObject.put("key", "life_record");
            jsonObject.put("time_start", time_start);
            jsonObject.put("time_end", time_end);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_DATA, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 电源开关设置
     */
    public static Observable<RPCResult> setPower(String param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置风速
     */
    public static Observable<RPCResult> setWind(String param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_wind");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 设置灯光
     */
    public static Observable<RPCResult> setLight(String param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_light");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 工作状态
     */
    public static String switchHoodStatus(int power_state, int wind_state) {
        String str = "";
        if (power_state == 0) {
            str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_power_off);
        } else {
            switch (wind_state) {
                case 0:
                    str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_free);
                    break;
                case 1:
                    str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_low_desc);
                    break;
                case 4:
                    str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_fry_desc);
                    break;
                case 16:
                    str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_high_desc);
                    break;
            }
        }
        return str;
    }

    /**
     * 电源状态
     */
    public static String switchHoodPower(String power_state) {
        String str = "";
        switch (power_state) {
            case "0":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_power_off);
                break;
            case "1":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_closing_delay);
                break;
            case "2":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_range_hood_power_on);
                break;
        }
        return str;
    }
}