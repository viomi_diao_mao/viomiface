package viomi.com.viomiaiface.subdevices.repository;

import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.subdevices.http.AppConstants;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;

/**
 * 扫地机器人相关 Api
 * Created by William on 2018/7/6.
 */
public class SweepingRobotRepository {
    private static final String TAG = SweepingRobotRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("run_state");
            jsonArray.put("battary_life");
            jsonArray.put("start_time");
            jsonArray.put("s_time");
            jsonArray.put("s_area");
//            jsonArray.put("order_time");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 电源开关设置
     */
    public static Observable<RPCResult> setPower(int param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_power");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 电源开关设置
     * 0 - 自动清扫  1 - 拖地   2 - 重点清扫   3 - 回充
     */
    public static Observable<RPCResult> setMode(int param, String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_mode");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 清扫模式
     */
    public static String switchMode(int run_state) {
        String str = BaseApplication.getAppContext().getResources().getString(R.string.iot_value_default);
        switch (run_state) {
            case 1:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_auto);
                break;
            case 2:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_auto);
                break;
            case 4:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_auto);
                break;
            case 8:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_mop);
                break;
            case 16:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_cleaning);
                break;
            case 32:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_auto);
                break;
            case 64:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_auto);
                break;
        }
        return str;
    }

    /**
     * 工作状态
     */
    public static String switchRunState(int run_state, int Battary_life) {
        String str = BaseApplication.getAppContext().getResources().getString(R.string.iot_device_type_online);
        switch (run_state) {
            case 1:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_sleep);
                break;
            case 2:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_await);
                break;
            case 4:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_working);
                break;
            case 8:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_working);
                break;
            case 16:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_working);
                break;
            case 32:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_backing);
                break;
            case 64:
                str = Battary_life == 100 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_charge_finish) : BaseApplication.getAppContext().getResources().getString(R.string.iot_sweeping_robot_charging);
                break;
        }
        return str;
    }
}