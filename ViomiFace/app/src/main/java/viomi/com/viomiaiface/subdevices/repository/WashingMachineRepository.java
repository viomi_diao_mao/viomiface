package viomi.com.viomiaiface.subdevices.repository;

import android.view.View;
import android.widget.TextView;

import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import rx.Observable;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.subdevices.http.AppConstants;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;

/**
 * 洗衣机相关 Api
 * Created by William on 2018/7/5.
 */
public class WashingMachineRepository {
    private static final String TAG = WashingMachineRepository.class.getSimpleName();

    /**
     * GetProp
     */
    public static Observable<RPCResult> getProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("program");
            jsonArray.put("wash_process");
            jsonArray.put("wash_status");
            jsonArray.put("water_temp");
            jsonArray.put("rinse_time");
            jsonArray.put("remain_time");
            jsonArray.put("spin_level");
            jsonArray.put("appoint_time");
            jsonArray.put("be_status");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 洗涤过程
     */
    public static String switchWashProcess(int wash_process, int wash_status) {
        String str = "";
        switch (wash_process) {
            case 0:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_free);
                break;
            case 1:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_weighing) : BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 2:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_wash) : BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 3:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_poaching) : BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 4:
                str = wash_status == 1 ? BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_dry) : BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_pause);
                break;
            case 5:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_finish);
                break;
            case 6:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_stop);
                break;
            case 7:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_power_off);
                break;
        }
        return str;
    }

    /**
     * 洗涤程序
     */
    public static String switchProgram(String program) {
        String str = "";
        switch (program) {
            case "goldenwash":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_golden);
                break;
            case "drumclean":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_drum);
                break;
            case "spin":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_spin);
                break;
            case "antibacterial":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_antibacterial);
                break;
            case "super_quick":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_quick);
                break;
            case "cottons":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_cottons);
                break;
            case "shirts":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_shirts);
                break;
            case "child_cloth":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_child_cloth);
                break;
            case "jeans":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_jeans);
                break;
            case "wool":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_wool);
                break;
            case "down":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_down);
                break;
            case "chiffon":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_chiffon);
                break;
            case "outdoor":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_outdoor);
                break;
            case "delicates":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_delicates);
                break;
            case "underwears":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_underwear);
                break;
            case "rinse_spin":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_rinse_spin);
                break;
            case "My Time":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_define);
                break;
            case "cotton Eco":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_mode_cotton_eco);
                break;
        }
        return str;
    }

    /**
     * 脱水速度
     */
    public static String switchLevel(String spin_level) {
        String str = "";
        switch (spin_level) {
            case "none":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_dry_rate_not);
                break;
            case "gentle":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_dry_rate_gentle);
                break;
            case "mild":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_dry_rate_mild);
                break;
            case "middle":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_dry_rate_middle);
                break;
            case "strong":
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_dry_rate_strong);
                break;
        }
        return str;
    }

    /**
     * 完成时间
     */
    public static void remainTime(TextView textView, int wash_process, int wash_status, int remain_time) {
        if ((wash_process == 1 || wash_process == 2 || wash_process == 3 || wash_process == 4) && wash_status == 1) {
            textView.setVisibility(View.VISIBLE);
            long time = System.currentTimeMillis() + remain_time * 60 * 1000;
            SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date d1 = new Date(time);
            textView.setText(String.format(BaseApplication.getAppContext().getResources().getString(R.string.iot_washing_machine_time), format.format(d1)));
        } else {
            textView.setVisibility(View.GONE);
        }
    }
}