package viomi.com.viomiaiface.subdevices.repository;


import com.miot.api.MiotManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;
import viomi.com.viomiaiface.R;
import viomi.com.viomiaiface.base.BaseApplication;
import viomi.com.viomiaiface.subdevices.http.ApiClient;
import viomi.com.viomiaiface.subdevices.http.AppConstants;
import viomi.com.viomiaiface.subdevices.http.entity.RPCResult;


/**
 * 净水器相关 Api
 * Created by William on 2018/2/3.
 */
public class WaterPurifierRepository {
    private static final String TAG = WaterPurifierRepository.class.getSimpleName();

    /**
     * 小米净水器 miOpen
     */
    public static Observable<RPCResult> miGetProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 云米 V 系列 miOpen
     */
    public static Observable<RPCResult> vGetProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("temperature");
            jsonArray.put("uv_state");
            jsonArray.put("press");
            jsonArray.put("elecval_state");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 云米 C 系列 GetPrp
     */
    public static Observable<RPCResult> cGetProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("temperature");
            jsonArray.put("uv_state");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * X3 miOpen
     */
    public static Observable<RPCResult> x3GetProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("setup_tempe");
            jsonArray.put("setup_flow");
            jsonArray.put("custom_tempe1");
            jsonArray.put("custom_flow0");
            jsonArray.put("custom_flow1");
            jsonArray.put("min_set_tempe");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * X5 miOpen
     */
    public static Observable<RPCResult> x5GetProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("temperature");
            jsonArray.put("uv_state");
            jsonArray.put("setup_tempe");
            jsonArray.put("setup_flow");
            jsonArray.put("custom_tempe1");
            jsonArray.put("custom_flow0");
            jsonArray.put("custom_flow1");
            jsonArray.put("custom_flow2");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 小米净水器 1A miOpen
     */
    public static Observable<RPCResult> mi1AGetProp(String did) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "get_prop");
            jsonObject.put("did", did);
            jsonObject.put("id", 1);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("tds_out");
            jsonArray.put("f1_totaltime");
            jsonArray.put("f1_usedtime");
            jsonArray.put("f2_totaltime");
            jsonArray.put("f2_usedtime");
            jsonObject.put("params", jsonArray);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 出水温度设定
     */
    public static Observable<RPCResult> setTemp(String did, int temp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_tempe_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(1);
            jsonArray.put(temp);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 出水流量设定
     */
    public static Observable<RPCResult> setFlow(String did, int flow, int index) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", "set_flow_setup");
            jsonObject.put("did", did);
            jsonObject.put("id", 12);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(index);
            jsonArray.put(flow);
            jsonObject.put("params", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ApiClient.getInstance().getApiService().miOpen(AppConstants.URL_MI_RPC + did, jsonObject.toString(), ApiClient.getInstance().getMiClientId(),
                MiotManager.getPeopleManager().getPeople().getAccessToken());
    }

    /**
     * 根据温度显示对应提示
     */
    public static String switchTempTip(int temp) {
        String str = temp + BaseApplication.getAppContext().getResources().getString(R.string.fridge_temp_unit);
        switch (temp) {
            case 40:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_40);
                break;
            case 50:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_50);
                break;
            case 60:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_60);
                break;
            case 75:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_75);
                break;
            case 80:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_80);
                break;
            case 85:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_85);
                break;
            case 90:
                str = BaseApplication.getAppContext().getResources().getString(R.string.iot_water_purifier_temp_tip_90);
                break;
        }
        return str;
    }
}