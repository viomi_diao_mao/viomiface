package viomi.com.viomiaiface.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;


import viomi.com.viomiaiface.base.BaseApplication;

/**
 * Created by young2 on 2016/12/19.
 */

public class ApkUtil {
    private static Context mContext= BaseApplication.getAppContext();

    /***
     * 获取application里的meta_data的value信息
     * @param context
     * @param dataName
     * @return
     */
    public static String getApplicationMetaDataValue(Context context, String dataName){
        ApplicationInfo appInfo = null;
        String msg=null;
        try {
            appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(),
                            PackageManager.GET_META_DATA);
             msg=appInfo.metaData.getString(dataName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return msg;
    }

    /***
     * 获取activity里的meta_data的value信息
     * @param context
     * @param componentName
     * @param dataName
     * @return
     */
    public static String getActivityMetaDataValue(Context context, ComponentName componentName, String dataName){
        ActivityInfo info = null;
        String msg=null;
        try {
            info=context.getPackageManager().getActivityInfo(componentName,
                            PackageManager.GET_META_DATA);
             msg =info.metaData.getString(dataName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return msg;
    }

    /**
     * 获取版本号
     * @return 当前应用的版本号
     */
    public static String getVersion() {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 获取包名
     * @return 当前应用的包名
     */
    public static String getPackageName() {
        return  mContext.getPackageName();
    }

    /***
     * 获取version code
     * @return
     */
    public static  int getVersionCode(){
//        return 0;
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            int code = info.versionCode;
            return code;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /***
     * 根据包名和aitivity名启动第三方app
     * @param context
     * @param packageName
     * @param activityName
     * @param isActivity 是否avtivity启动
     */
    public static void startOtherAppClass(Context context, String packageName, String activityName, boolean isActivity ){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setClassName(packageName, activityName);
        if(!isActivity){
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    /***
     * 根据包名启动第三方app
     * @param context
     * @param packageName
     * @param isActivity 是否avtivity启动
     */
    public static void startOtherApp(Context context, String packageName, boolean isActivity){
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if(!isActivity){
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    /***
     * 判断是否存在第三方app
     * @param context
     * @param packageName
     * @return
     */
    public static boolean checkApkExist(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = context.getPackageManager()
                    .getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
