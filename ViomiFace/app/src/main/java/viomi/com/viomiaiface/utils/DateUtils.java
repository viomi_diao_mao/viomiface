package viomi.com.viomiaiface.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mocc on 2018/3/2
 */

public class DateUtils {

    /*
    * @param dateStr etc:200180101 00:00:00
    * */
    public static long getTimestamp(String dateStr) {
        long timestamp = 0L;
        try {
            Date date = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(dateStr);
            timestamp = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;
    }

    public static String getDate(long timestamp) {
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        dateStr = sdf.format(timestamp);
        return dateStr;
    }

    public static String getDate2(long timestamp) {
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        dateStr = sdf.format(timestamp);
        return dateStr;
    }

    /*
    * @param timeStr etc:00:00:00
    * */
    public static long getStringTomm(String timeStr) {
        long time = -1L;
        try {
            int index1 = timeStr.indexOf(":");
            int index2 = timeStr.indexOf(":", index1 + 1);
            int hh = Integer.parseInt(timeStr.substring(0, index1));
            int mi = Integer.parseInt(timeStr.substring(index1 + 1, index2));
            int ss = Integer.parseInt(timeStr.substring(index2 + 1));
            time = (hh * 60 * 60 + mi * 60 + ss) * 1000;
        } catch (Exception e) {
        }
        return time;
    }
}
