package viomi.com.viomiaiface.utils;

import com.miot.common.abstractdevice.AbstractDevice;

import java.util.ArrayList;
import java.util.List;

import viomi.com.viomiaiface.mijia.Miconfig;

/**
 * Created by hailang on 2018/5/17 0017.
 */

public class DeviceUtil {
    public static String FRIGE = "viomi.fridge";
    public static String DRINKBAR = "yunmi.kettle.r";
    public static String WATERPURIFIER = "yunmi.waterpuri";
    public static String HOOD = "viomi.hood";
    public static String DISHWAHSER = "viomi.dishwasher";
    public static String WASHER = "viomi.washer";
    public static String OVEN = "viomi.oven";
    public static String FAN = "viomi.fan";
    public static String VACUUM = "viomi.vacuum";

    public static List<AbstractDevice> filterDevices(List<AbstractDevice> Devices, String... deviceType) {
        List<AbstractDevice> list = new ArrayList<>();
        if (Devices != null && deviceType != null)
            for (String type : deviceType) {
                for (AbstractDevice device : Devices) {
                    if (device.getDeviceModel().startsWith(type)) {
                        list.add(device);
                    }
                }
            }
        return list;
    }

    public static AbstractDevice findRecommendedPuri(List<AbstractDevice> Devices) {
        AbstractDevice device = null;
        List<AbstractDevice> devices1 = DeviceUtil.filterDevices(Devices, DeviceUtil.WATERPURIFIER);
        boolean b = false;
        for (AbstractDevice abstractDevice : devices1) {
            if (abstractDevice.getDeviceModel().equals(Miconfig.YUNMI_WATERPURI_X5)) {
                device = abstractDevice;
                b = true;
                break;
            }
        }
        if (!b && devices1.size() > 0) {
            device = devices1.get(devices1.size() - 1);
        }
        return device;
    }

    public static AbstractDevice findRecommendedFrige(List<AbstractDevice> Devices) {
        AbstractDevice device = null;
        List<AbstractDevice> devices1 = DeviceUtil.filterDevices(Devices, DeviceUtil.FRIGE);
        boolean b = false;
        for (AbstractDevice abstractDevice : devices1) {
            if (abstractDevice.getDeviceModel().equals(Miconfig.VIOMI_HOOD_X2)) {
                device = abstractDevice;
                b = true;
                break;
            }
        }
        if (!b && devices1.size() > 0) {
            device = devices1.get(devices1.size() - 1);
        }
        return device;
    }

    public static AbstractDevice findRecommendedHood(List<AbstractDevice> Devices) {
        AbstractDevice device = null;
        List<AbstractDevice> devices1 = DeviceUtil.filterDevices(Devices, DeviceUtil.HOOD);
        boolean b = false;
        for (AbstractDevice abstractDevice : devices1) {
            if (abstractDevice.getDeviceModel().equals(Miconfig.VIOMI_FRIDGE_X3)) {
                device = abstractDevice;
                b = true;
                break;
            }
        }
        if (!b)
            for (AbstractDevice abstractDevice : devices1) {
                if (abstractDevice.getDeviceModel().equals(Miconfig.VIOMI_HOOD_C1)) {
                    device = abstractDevice;
                    b = true;
                    break;
                }
            }
        if (!b && devices1.size() > 0) {
            device = devices1.get(devices1.size() - 1);
        }
        return device;
    }

    public static AbstractDevice getPuriFirstControl(List<AbstractDevice> devices) {

        if (devices == null || devices.size() == 0) {
            return null;
        }

        for (int i = 0; i < devices.size(); i++) {
            if (Miconfig.YUNMI_WATERPURI_X5.equals(devices.get(i).getDeviceModel())) {
                return devices.get(i);
            }
        }

        for (int i = 0; i < devices.size(); i++) {
            if (Miconfig.YUNMI_WATERPURI_X3.equals(devices.get(i).getDeviceModel())) {
                return devices.get(i);
            }
        }
        return devices.get(0);
    }

    public static AbstractDevice getHoodFirstControl(List<AbstractDevice> devices) {

        if (devices == null || devices.size() == 0) {
            return null;
        }

        for (int i = 0; i < devices.size(); i++) {
            if (Miconfig.VIOMI_HOOD_X2.equals(devices.get(i).getDeviceModel())) {
                return devices.get(i);
            }
        }

        for (int i = 0; i < devices.size(); i++) {
            if (Miconfig.VIOMI_HOOD_C1.equals(devices.get(i).getDeviceModel())) {
                return devices.get(i);
            }
        }
        return devices.get(0);
    }

    public static AbstractDevice getFridgeFirstControl(List<AbstractDevice> devices) {
        if (devices == null || devices.size() == 0) {
            return null;
        }

        for (int i = 0; i < devices.size(); i++) {
            if (Miconfig.VIOMI_FRIDGE_X3.equals(devices.get(i).getDeviceModel())) {
                return devices.get(i);
            }
        }
        return devices.get(0);
    }
}
