package viomi.com.viomiaiface.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by viomi on 2016/10/25.
 * json解析封装
 */

public class JsonUitls {

    public static String getString(JSONObject json, String name) {
        String result;
        if (json != null) {
            try {
                result = json.getString(name);
            } catch (JSONException e) {
                e.printStackTrace();
                result = "";
            }
        } else {
            result = "";
        }
        result = "null".equals(result) ? "" : result;
        return result;
    }

    public static JSONObject getJSONObject(String jsonStr) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonObject == null || jsonStr == null) {
            jsonObject = new JSONObject();
        }
        return jsonObject;
    }

    public static JSONObject getJSONObject(JSONObject json, String name) {
        JSONObject jsonObject;
        if (json != null) {
            try {
                jsonObject = json.getJSONObject(name);
            } catch (JSONException e) {
                e.printStackTrace();
                jsonObject = new JSONObject();
            }
        } else {
            jsonObject = new JSONObject();
        }
        if (jsonObject == null) {
            jsonObject = new JSONObject();
        }
        return jsonObject;
    }

    public static JSONObject getJSONObject(JSONArray array, int index) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = array.getJSONObject(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    public static JSONArray getJSONArray(JSONObject json, String name) {
        JSONArray jsonArray = new JSONArray();
        if (json != null) {
            try {
                jsonArray = json.getJSONArray(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    public static int getInt(JSONObject json, String name) {
        int result = 0;
        if (json == null) {
            return result;
        }
        try {
            result = json.getInt(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static double getDouble(JSONObject json, String name) {
        double result = 0d;
        if (json == null) {
            return result;
        }
        try {
            result = json.getDouble(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static long getLong(JSONObject json, String name) {
        long result = 0L;
        if (json == null) {
            return result;
        }
        try {
            result = json.getLong(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

}
