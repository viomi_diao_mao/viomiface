package viomi.com.viomiaiface.utils;

/**
 * Created by admin on 2017/11/7 0007.
 */

public class MathUtil {
    public static byte[] intToByte32(int number) {

        byte[] statusByte = new byte[32];
        int temp = number;
        for (int i = 0; i < statusByte.length; i++) {
            statusByte[i] = (byte) (temp & 1);
            temp = temp >> 1;
        }
        return statusByte;
    }

    public static int byte32ToInt(byte[] arr) {
        int sum = 0;
        if (arr != null)
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == 1)
                    sum += Math.pow(2, i);
            }
        return sum;
    }
}
