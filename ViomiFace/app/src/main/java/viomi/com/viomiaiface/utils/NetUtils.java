package viomi.com.viomiaiface.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.telephony.TelephonyManager;

import java.io.IOException;

/**
 * @author wing
 *         网络连接工具,需要权限---android.permission.INTERNET
 */
public class NetUtils {
    private static final String TAG = NetUtils.class.getName();

    public static final String NETWORKTYPE_INVALID = "INVALID";
    public static final String NETWORKTYPE_WIFI = "wifi";
    public static final String NETWORKTYPE_2G = "2G";
    public static final String NETWORKTYPE_3G = "3G";
    public static final String NETWORKTYPE_4G = "4G";


    private static String currentState = NETWORKTYPE_INVALID;

    public static String queryNetWorkState(Context context) {
        if (!hasNetwork(context)) {
            //没有网络
            currentState = NETWORKTYPE_INVALID;
        }
        if (checkWifiState(context)) {
            currentState = NETWORKTYPE_WIFI;
        } else {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            switch (tm.getNetworkType()) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    currentState = NETWORKTYPE_2G; // ~ 50-100 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    currentState = NETWORKTYPE_2G; // ~ 14-64 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    currentState = NETWORKTYPE_2G; // ~ 50-100 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    currentState = NETWORKTYPE_3G; // ~ 400-1000 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    currentState = NETWORKTYPE_3G; // ~ 600-1400 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    currentState = NETWORKTYPE_2G; // ~ 100 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    currentState = NETWORKTYPE_3G; // ~ 2-14 Mbps
                    break;
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    currentState = NETWORKTYPE_3G; // ~ 700-1700 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    currentState = NETWORKTYPE_3G; // ~ 1-23 Mbps
                    break;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    currentState = NETWORKTYPE_3G; // ~ 400-7000 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    currentState = NETWORKTYPE_3G; // ~ 1-2 Mbps
                    break;
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    currentState = NETWORKTYPE_3G; // ~ 5 Mbps
                    break;
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    currentState = NETWORKTYPE_3G; // ~ 10-20 Mbps
                    break;
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    currentState = NETWORKTYPE_2G; // ~25 kbps
                    break;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    currentState = NETWORKTYPE_4G; // ~ 10+ Mbps
                    break;
                default:
                    currentState = NETWORKTYPE_INVALID;
            }
        }
        return currentState;
    }

    public static String getCurrentNetWorkState(Context context) {
        if (NETWORKTYPE_INVALID.equals(currentState)) {
            queryNetWorkState(context);
        }
        return currentState;
    }

    /**
     * 获得网络连接是否可用
     *
     * @return
     */
    public static boolean hasNetwork(Context context) {
        boolean ret;
        ConnectivityManager con = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (con == null) {
            ret = false;
        } else {
            NetworkInfo workinfo = con.getActiveNetworkInfo();
            ret = !(workinfo == null || !workinfo.isAvailable());
        }
        //StatusErrorManager.getInstance().setNetworkStatus(ret?StatusErrorManager.SPEECH_NORMAL:StatusErrorManager.NETWORK_ERROR);
        return ret;
    }

    /**
     * 判断是否是wifi连接
     */
    public static boolean checkWifiState(Context context) {
        boolean isWifiConnect = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
        for (int i = 0; i < networkInfos.length; i++) {
            if (networkInfos[i].getState() == NetworkInfo.State.CONNECTED) {
                if (networkInfos[i].getType() == ConnectivityManager.TYPE_MOBILE) {
                    isWifiConnect = false;
                }
                if (networkInfos[i].getType() == ConnectivityManager.TYPE_WIFI) {
                    isWifiConnect = true;
                }
            }
        }
        return isWifiConnect;
    }

    /**
     * 打开网络设置界面
     */
    public static void openNet(Activity activity) {
        Intent intent = new Intent("/");
        ComponentName cm = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
        intent.setComponent(cm);
        intent.setAction("android.intent.action.VIEW");
        activity.startActivityForResult(intent, 0);
    }

    public static String getNetOperator(Context context) {
        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telManager.getSimOperator();
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007")) {
                //中国移动
                return "ChinaMobile";
            } else if (operator.equals("46001")) {
                //中国联通
                return "ChinaUnion";
            } else if (operator.equals("46003")) {
                //中国电信
                return "ChinaTelecom";
            }
        }
        return "Unknown";
    }

    public interface OnPingNetListener{
        void onSuccess();
        void onFail();
    }


    public static void pingNetAsync(final OnPingNetListener listener){
        if (listener == null){
            LogUtils.e(TAG, "pingNetAsync: Listener is null");
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (pingNet()){
                    listener.onSuccess();
                }else{
                    listener.onFail();
                }
            }
        }).start();
    }


    public static boolean pingNet() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            try {
                throw new Exception("pingNet should not use in MainThread!!!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String result = null;
        String ipArray[] = new String[]{
                "www.meizu.com",
                "www.baidu.com",
                "www.qq.com"
        };
        for (int i = 0; i < 3; i++) {
            try {
                Process p = Runtime.getRuntime().exec("ping -c 3 -w 5 " + ipArray[i]);// ping网址3次
                // ping的状态
                int status = p.waitFor();
                if (status == 0) {
                    result = "success";
                    return true;
                } else {
                    result = "failed";
                    Thread.sleep(1000);
                }
            } catch (IOException e) {
                result = "IOException";
            } catch (InterruptedException e) {
                result = "InterruptedException";
            } finally {
                LogUtils.e(TAG, "pingNet: " + ipArray[i] + ":result = " + result);
            }
        }
        return false;
    }
}
