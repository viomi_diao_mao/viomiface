package viomi.com.viomiaiface.utils;

import android.app.ActivityManager;
import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Mocc on 2018/2/28
 */

public class ServiceUtils {
    /**
     * 判断服务是否开启
     *
     * @return
     */
    public static boolean isServiceRunning(Context context, String ServiceName) {
        if (("").equals(ServiceName) || ServiceName == null)
            return false;
        ActivityManager myManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        ArrayList<ActivityManager.RunningServiceInfo> runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager
                .getRunningServices(30);
        for (int i = 0; i < runningService.size(); i++) {
            String name = runningService.get(i).service.getClassName().toString();
            LogUtils.e("ServiceUtils",name+"");
            if (name.equals(ServiceName)) {
                return true;
            }
        }
        return false;
    }
}
