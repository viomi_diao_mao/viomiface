package viomi.com.viomiaiface.utils;

/**
 * Created by Administrator on 2017/6/28.
 * 时间相关的工具类
 */

public class TimeUtils {

    /**
     * 将秒数转换成 00:30 的格式
     */
    public static String formatTime(int time) {
        int sec = time % 60;
        int min = time / 60;
        return (min < 10 ? ("0" + min) : min) + ":" + (sec < 10 ? ("0" + sec) : sec);
    }

}
