package viomi.com.viomiaiface.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by hailang on 2018/3/2 0002.
 */

public class FaceTextView extends android.support.v7.widget.AppCompatTextView {

    public FaceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public FaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FaceTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/DINCond-Medium.otf"));
    }


}