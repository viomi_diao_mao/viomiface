package viomi.com.viomiaiface.widget.tagcloudlib;

/**
 * Created by Mocc on 2018/7/4
 */
public class DataBean {

    private int colorBg;
    private int imgDrawable;
    private String deviceName;

    public DataBean() {
    }

    public DataBean(int colorBg, int imgDrawable, String deviceName) {
        this.colorBg = colorBg;
        this.imgDrawable = imgDrawable;
        this.deviceName = deviceName;
    }

    public int getColorBg() {
        return colorBg;
    }

    public void setColorBg(int colorBg) {
        this.colorBg = colorBg;
    }

    public int getImgDrawable() {
        return imgDrawable;
    }

    public void setImgDrawable(int imgDrawable) {
        this.imgDrawable = imgDrawable;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
