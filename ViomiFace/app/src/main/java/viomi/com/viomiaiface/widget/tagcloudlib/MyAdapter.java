package viomi.com.viomiaiface.widget.tagcloudlib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import viomi.com.viomiaiface.R;

/**
 * Created by Mocc on 2018/7/4
 */
public class MyAdapter extends TagsAdapter {

    private List<DataBean> list;

    public MyAdapter(List<DataBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(Context context, int position, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.face_3d_item_layout, null);
        LinearLayout device_bg = view.findViewById(R.id.device_bg);
        ImageView device_icon = view.findViewById(R.id.device_icon);
        TextView device_name = view.findViewById(R.id.device_name);

        DataBean bean = list.get(position);
        device_bg.setBackground(context.getResources().getDrawable(bean.getColorBg()));
        device_icon.setImageResource(bean.getImgDrawable());
        device_name.setText(bean.getDeviceName());
        return view;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getPopularity(int position) {
        return position % 7;
    }

    @Override
    public void onThemeColorChanged(View view, int themeColor) {

    }
}
