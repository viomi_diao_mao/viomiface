package viomi.com.gdloc;

import android.content.Context;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;

/**
 * Created by Mocc on 2017/8/18
 */

public class MyLocationUitl {

    private static MyLocationUitl instance;

    //声明AMapLocationClient类对象
    public AMapLocationClient mLocationClient = null;
    //声明AMapLocationClientOption对象
    public AMapLocationClientOption mLocationOption = null;

    private MyLocationUitl() {
    }

    public static synchronized MyLocationUitl getInstance() {
        if (instance == null) {
            synchronized (MyLocationUitl.class) {
                instance = new MyLocationUitl();
            }
        }
        return instance;
    }


    public void startloc(Context applicationContext, AMapLocationListener mLocationListener) {
        stopLoc();
        mLocationClient = new AMapLocationClient(applicationContext);
        mLocationClient.setLocationListener(mLocationListener);
        mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        mLocationOption.setOnceLocation(true);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

    public void stopLoc() {
        if (mLocationClient!=null) {
            mLocationClient.stopLocation();//停止定位后，本地定位服务并不会被销毁
            mLocationClient.onDestroy();//销毁定位客户端，同时销毁本地定位服务。
        }
    }

}
