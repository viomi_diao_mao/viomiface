package viomi.com.wifilibrary.wifimodel;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import viomi.com.wifilibrary.R;

public class FirstConnectActivity extends AppCompatActivity {

    private ListView wifiListview;
    private WifiAdapter adapter;
    private WifiManager wifiManager;
    private List<ScanResult> scanlist;
    private boolean isFisrt;
    private Button skip_btn;
    private MyHandler mhandler;
    private TextView title;
    private int clickCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_first_connect);

        title = findViewById(R.id.title);

        wifiListview = findViewById(R.id.wifiListview);
        skip_btn = findViewById(R.id.skip_btn);

        skip_btn.setVisibility(View.GONE);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        scanlist = new ArrayList<>();
        adapter = new WifiAdapter(FirstConnectActivity.this, scanlist, wifiManager);
        wifiListview.setAdapter(adapter);

        initListener();

        wifiManager.setWifiEnabled(true);

        Intent intent = getIntent();
        isFisrt = intent.getBooleanExtra("isFisrt", false);

        mhandler = new MyHandler(this);
        mhandler.sendEmptyMessageDelayed(0, 120 * 1000);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mReceiver, filter);

        IntentFilter filter2 = new IntentFilter();
//        filter2.addAction(WifiManager.RSSI_CHANGED_ACTION); //信号强度变化
        filter2.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION); //网络状态变化
        filter2.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION); //wifi状态，是否连上，密码
        filter2.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION); //是不是正在获得IP地址
        filter2.addAction(WifiManager.NETWORK_IDS_CHANGED_ACTION);
        filter2.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mReceiver2, filter2);
        wifiManager.startScan();
        showwifilist();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
        unregisterReceiver(mReceiver2);
    }

    private BroadcastReceiver mReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            List<ScanResult> scanResults = wifiManager.getScanResults();
            if (scanResults != null) {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                showwifilist();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wifiManager.startScan();
                    }
                }, 2000);
            }
        }
    };

    private void showwifilist() {
        List<ScanResult> sResults = wifiManager.getScanResults();
        if (sResults == null) return;
        Map<String, ScanResult> sMap = new HashMap<>();
        for (int i = 0; i < sResults.size(); i++) {
            ScanResult result = sResults.get(i);
            if (result.SSID != null && "" != result.SSID) {
                if (sMap.containsKey(result.SSID)) {
                    String security = Wifi.ConfigSec.getScanResultSecurity(result);
                    WifiConfiguration config = Wifi.getWifiConfiguration(wifiManager, result, security);
                    WifiInfo info = wifiManager.getConnectionInfo();
                    boolean isCurrentNetwork_WifiInfo = info != null && android.text.TextUtils.equals(info.getSSID(), "\"" + result.SSID + "\"") && android.text.TextUtils.equals(info.getBSSID(), result.BSSID);
                    if (!(config != null && isCurrentNetwork_WifiInfo)) {
                        continue;
                    }
                }
                sMap.put(result.SSID, result);
            }
        }

        sMap.remove(null);
        sMap.remove("");

        Collection<ScanResult> values = sMap.values();
        List<ScanResult> scanResults = new ArrayList<>();
        scanResults.addAll(values);

        ScanResult scanResult = null;
        for (int i = 0; i < scanResults.size(); i++) {
            WifiInfo info = wifiManager.getConnectionInfo();
            scanResult = scanResults.get(i);
            boolean isCurrentNetwork_WifiInfo = info != null
                    && android.text.TextUtils.equals(info.getSSID(), "\"" + scanResult.SSID + "\"")
                    && android.text.TextUtils.equals(info.getBSSID(), scanResult.BSSID);
            if (isCurrentNetwork_WifiInfo) break;
        }

        if (scanResult != null) {
            scanResults.remove(scanResult);
            scanResults.add(0, scanResult);
        }


        for (int i = 0; i < scanResults.size(); i++) {
            ScanResult scanResult1 = scanResults.get(i);
            String security = Wifi.ConfigSec.getScanResultSecurity(scanResult1);
            WifiConfiguration config = Wifi.getWifiConfiguration(wifiManager, scanResult1, security);

            if (config == null) {
                continue;
            }

            boolean isCurrentNetwork_ConfigurationStatus = config.status == WifiConfiguration.Status.CURRENT;
            WifiInfo info = wifiManager.getConnectionInfo();
            boolean isCurrentNetwork_WifiInfo = info != null
                    && android.text.TextUtils.equals(info.getSSID(), "\"" + scanResult1.SSID + "\"")
                    && android.text.TextUtils.equals(info.getBSSID(), scanResult1.BSSID);

            if (isCurrentNetwork_WifiInfo && isCurrentNetwork_ConfigurationStatus) {
                if (isFisrt) {
                    Intent intent = new Intent("viomi.com.viomiaiface.activity.LoginActivity");
                    intent.putExtra("isFirst", true);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent("viomi.com.viomiaiface.activity.DesktopActivity");
                    startActivity(intent);
                    finish();
                }
                break;
            }
        }

        scanlist.clear();
        scanlist.addAll(scanResults);
        adapter.notifyDataSetChanged();
    }

    private void launchWifiConnecter(final Activity activity, final ScanResult hotspot) {
        Intent intent = new Intent(FirstConnectActivity.this, WifiConnectActivity.class);
        intent.putExtra("EXTRA_HOTSPOT", hotspot);
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
        }
    }

    private void initListener() {
        wifiListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ScanResult result = scanlist.get(position);
                launchWifiConnecter(FirstConnectActivity.this, result);
            }
        });

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount ++;
                if (clickCount>=5) {
                    try {
                        String packageName = "ai.hij.device";
                        if (checkApkExist(FirstConnectActivity.this, packageName)) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_MAIN);
                            String className = "ai.hij.device.activity.HijTestActivity";
                            ComponentName componentName = new ComponentName(packageName, className);
                            intent.setComponent(componentName);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        });
    }

    private  boolean checkApkExist(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = context.getPackageManager()
                    .getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void btnClick(View view) {
        Intent intent = new Intent("viomi.com.viomiaiface.activity.DesktopActivity");
        startActivity(intent);
        finish();
    }

    protected static class MyHandler extends Handler {

        private WeakReference<FirstConnectActivity> weakReference;

        public MyHandler(FirstConnectActivity activity) {
            this.weakReference = new WeakReference<FirstConnectActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            FirstConnectActivity mActivity = weakReference.get();
            if (mActivity != null) {
                FirstConnectActivity activity = weakReference.get();
                if (activity != null && !activity.isFinishing()) {
                    activity.handleMessage(msg);
                }
            }
        }
    }

    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                skip_btn.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mhandler.removeCallbacksAndMessages(null);
    }
}
